@Echo Off
SETLOCAL EnableDelayedExpansion
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do     rem"') do (
  set "DEL=%%a"
)

for /f %%a in ('WMIC OS GET LocalDateTime ^| find "."') do set DTS=%%a
set fecha=%DTS:~0,8%-%DTS:~8,6%

echo(
call :colorEcho 0e "                    Artisan RESTful"
echo(
call :colorEcho 0f " _________________________________________________________ "
echo(
call :colorEcho 03 "   Generando recursos..."
call :colorEcho 03 "   "

set /p DUMMY=Hit ENTER to continue...

:: ------[ Start] ------
php artisan make:controller DepartamentoController --resource
php artisan make:controller SuscriptorController --resource
php artisan make:controller TipodocumentoController --resource
php artisan make:controller UsrempController --resource
php artisan make:controller DocumentoController --resource
php artisan make:controller GuiaturnadoController --resource
php artisan make:controller ReporterevisionController --resource
php artisan make:controller RespuestaController --resource
php artisan make:controller CitaController --resource

php artisan make:controller DocumentosdbviewController --resource


pause
exit
:colorEcho
echo off
<nul set /p ".=%DEL%" > "%~2"
findstr /v /a:%1 /R "^$" "%~2" nul
del "%~2" > nul 2>&1i