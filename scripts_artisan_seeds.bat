@Echo Off
SETLOCAL EnableDelayedExpansion
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do     rem"') do (
  set "DEL=%%a"
)

for /f %%a in ('WMIC OS GET LocalDateTime ^| find "."') do set DTS=%%a
set fecha=%DTS:~0,8%-%DTS:~8,6%

echo(
call :colorEcho 0e "                    Artisan Seed"
echo(
call :colorEcho 0f " _________________________________________________________ "
echo(
call :colorEcho 03 "   Generando inicializadores..."
call :colorEcho 03 "   "

set /p DUMMY=Hit ENTER to continue...

:: ------[ Start] ------
php artisan make:seeder DepartamentosTableSeeder
::php artisan make:seeder PuestosTableSeeder
php artisan make:seeder SuscriptoresTableSeeder
php artisan make:seeder TiposdocumentosTableSeeder
php artisan make:seeder DocumentosTableSeeder
php artisan make:seeder EmpleadosTableSeeder
php artisan make:seeder UsersTableSeeder
php artisan make:seeder EmpleadosdocumentosTableSeeder
php artisan make:seeder SolicitudescitasTableSeeder
php artisan make:seeder CitasTableSeeder
php artisan make:seeder AgendasTableSeeder
php artisan make:seeder SolicitudesrespuestasTableSeeder
php artisan make:seeder GuiasturnadosTableSeeder
php artisan make:seeder DetallesguiasturnadosTableSeeder
php artisan make:seeder ReportesrevisionesTableSeeder

php artisan make:seeder CpsoaxacaTableSeeder

pause
exit
:colorEcho
echo off
<nul set /p ".=%DEL%" > "%~2"
findstr /v /a:%1 /R "^$" "%~2" nul
del "%~2" > nul 2>&1i