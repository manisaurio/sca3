var Restriccion = (function(){ 
    return {
        "Documento": undefined,
        "Empleado": undefined,
        "Departamento": undefined
    }
})();

Restriccion.Departamento = (function(){
    var _edit = function(){
        console.info("Restriccion.Departamento.edit");
        var _formulario = document.getElementById("departamentoForm");

        _formulario.onkeypress = function( _event ){
            var _keypressedCode = _event.keyCode || _event.which;
            if(_keypressedCode === 13){  // Tecla ENTER
                _event.preventDefault(); // Prevenir que el formulario sea enviado
                return false;
            }
        }

        _alfanumerico( "nombre", /^[a-zA-Z0-9 ,.'-]+$/i );
    }

    var _init = function(){
        console.info("Restriccion.Departamento.init");
        var _formulario = document.getElementById("departamentoForm");

        _formulario.onkeypress = function( _event ){
            var _keypressedCode = _event.keyCode || _event.which;
            if(_keypressedCode === 13){  // Tecla ENTER
                _event.preventDefault(); // Prevenir que el formulario sea enviado
                return false;
            }
        }

        _alfanumerico( "nombre", /^[a-zA-Z0-9 ,.'-]+$/i );
        _alfanumerico( "otra_area", /^[a-zA-Z0-9 ,.'-]+$/i );
    }

    var _alfanumerico = function( _idInput, _regexp ){
        var _inputX = document.getElementById( _idInput );

        _inputX.onkeypress = function( _event ){
            var _keypressedCode = (document.all) ? _event.keyCode : _event.which;

            if( _controlkey(_event.keyCode) ){  // Teclas de control
                return true;
            }else{
                var _keyString = String.fromCharCode( _keypressedCode );
                return _regexp.test( _keyString );
            }
        }

        _inputX.onkeyup = function( _event ){
            var _str = _event.target.value;
            if(_str.length !== 0){
                _str = ( /\s/.test(_str.charAt(0)) ) ? _str.substring(1) : _str;
            }
            _event.target.value = _str.replace(/ {2,}/g, ' ');
            //_event.target.value = _event.target.value.toUpperCase();
        }
    }

    var _controlkey = function( _keycode){
        switch ( _keycode ) {
            case 8:     //backspace
            case 9:     //tab
            case 16:    //shift
            case 27:    //escape
            case 35:    //end
            case 36:    //home
            case 37:    //left arrow
            case 39:    //right arrow
            case 46:    //delete
                return true;
        };
        return false
    };

    return{
        "edit": _edit,
        "init": _init
    };
})();

Restriccion.Documento = (function(){

    var _numoficio = function( _event ){
        if( _controlkey( _event.keyCode ) ){
            return true;
        };

        var MAXLENGTH = 10;
        return _event.target.value.length < MAXLENGTH;
    };

    var _numhojas = function( _event ){
        if( _controlkey( _event.keyCode ) ){
            return true;
        };

        var MAXLENGTH = 2;

        var _key = (document.all) ? _event.keyCode : _event.which;
        
        var _regexp  = /[0-9]/;  //regular expression
        var _keyString = String.fromCharCode( _key );

        return _regexp.test( _keyString ) && ( _event.target.value.length < MAXLENGTH );
    };

    var _otrotipoinput = function( _event ){
        if( _controlkey( _event.keyCode ) ){
            return true;
        };

        var MAXLENGTH = 25;

        var _key = (document.all) ? _event.keyCode : _event.which;
        
        var _regexp  = /^[a-zA-ZñÑ]+$/;  //regular expression
        var _keyString = String.fromCharCode( _key );

        return _regexp.test( _keyString ) && ( _event.target.value.length < MAXLENGTH );
    };

    var _numanexos = function( _event ){
        if( _controlkey( _event.keyCode ) ){
            return true;
        };

        var MAXLENGTH = 2;

        var _key = (document.all) ? _event.keyCode : _event.which;
        
        var _regexp  = /[0-9]/;  //regular expression
        var _keyString = String.fromCharCode( _key );

        return _regexp.test( _keyString ) && ( _event.target.value.length < MAXLENGTH );
    };

    var _suscriptorinput = function( _event ){
        if( _controlkey( _event.keyCode ) ){
            return true;
        };

        var MAXLENGTH = 100;

        var _key = (document.all) ? _event.keyCode : _event.which;
        
        var _regexp  = /^[a-zA-ZñÑ. ]+$/;  //regular expression
        var _keyString = String.fromCharCode( _key );

        return _regexp.test( _keyString ) && ( _event.target.value.length < MAXLENGTH );
    };

    var _cargo = function( _event ){
        if( _controlkey( _event.keyCode ) ){
            return true;
        };

        var MAXLENGTH = 25;

        var _key = (document.all) ? _event.keyCode : _event.which;
        
        var _regexp  = /^[a-zA-ZñÑ]+$/;  //regular expression
        var _keyString = String.fromCharCode( _key );

        return _regexp.test( _keyString ) && ( _event.target.value.length < MAXLENGTH );
    };

    var _controlkey = function( _keycode){
        switch ( _keycode ) {
            case 8:     //backspace
            case 9:     //tab
            case 13:    //enter
            case 16:    //shift
            case 35:    //end
            case 36:    //home
            case 37:    //left arrow
            case 39:    //right arrow
            case 46:    //delete
                return true;
        };
        return false
    };

    return{
        "numoficio": _numoficio,
        "numhojas": _numhojas,
        "otrotipoinput": _otrotipoinput,
        "numanexos": _numanexos,
        "suscriptorinput": _suscriptorinput,
        "cargo": _cargo
    };
})();

Restriccion.Empleado = (function(){

    var _init = function(){
        console.info("Restriccion.Empleado.init");
        var _formulario = document.getElementById("createEmpleadoForm");

        _formulario.onkeypress = function( _event ){
            var _keypressedCode = _event.keyCode || _event.which;
            if(_keypressedCode === 13){  // Tecla ENTER
                _event.preventDefault(); // Prevenir que el formulario sea enviado
                return false;
            }
        }

        //--------[ Limpiar input file - AVATAR ]----------------------------------
        var _inputAvatar = document.getElementById("avatarInputFile");
        _inputAvatar.value = "";
        //--------[ Fin Limpiar input file - AVATAR ]------------------------------

        //---------------[ Validaciones ]---------------------------------------------------------
        var _regexps = [
            { id : "nombre", regexp : /^[a-z ,.'-]{3,100}$/i },
            { id : "ap_paterno", regexp : /^[a-z ,.'-]{3,25}$/i },
            { id : "ap_materno", regexp : /^[a-z ,.'-]{3,25}$/i },
            { id : "curp", regexp : /^[a-z]{4}\d{6}(H|M)[a-z]{5}\d{2}$/i },
            { id : "email", regexp : /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i }
        ];

        _formulario.addEventListener("submit", function (_event) {
            var _errors = 0;
            _regexps.forEach( function(_element) {
                _errors = ( _evaluaRegexp(_element.regexp, _element.id) ) ? (_errors): (_errors + 1);
            });

            if( document.getElementById("passwordDiv").hasAttribute("hidden") ){
                if(_errors > 0 ) {
                    _event.preventDefault();
                }
            }else{
                if(_errors > 0 | !_evaluaPassword("psw", "pswconfirm")) {
                    _event.preventDefault();
                }
            }

        }, false);
        //---------------[ Fin Validaciones ]-----------------------------------------------------

        _palabra( "nombre", 3, 100, /^[a-z ,.'-]+$/i );
        _palabra( "ap_paterno", 3, 50, /^[a-z ,.'-]+$/i );
        _palabra( "ap_materno", 3, 50, /^[a-z ,.'-]+$/i );
        _curp( "curp" );
        _email( "email" );
        _avatar( "avatarInputFile", "avatarImg" );
        _checkPassword("psw", "pswconfirm");
    }

    var _evaluaPassword = function(_idInputPsw, _idInputConfirm){
        var _inputPsw = document.getElementById(_idInputPsw);
        var _inputPswConfirm = document.getElementById(_idInputConfirm);
        var _parentP = _inputPsw.parentNode;
        var _parentC = _inputPswConfirm.parentNode;
        var _smallMensajeP = document.getElementById( _idInputPsw + "Small" );
        var _smallMensajeC = document.getElementById( _idInputConfirm + "Small" );

        if( _inputPsw.value.length >= 6){
            if(_inputPsw.value === _inputPswConfirm.value) {
                _smallMensajeC.setAttribute("hidden", "");
                _parentC.setAttribute("class", "form-group col-md-6 has-success");
                _parentP.setAttribute("class", "form-group col-md-6 has-success");
                return true;
            }else{
                _smallMensajeC.setAttribute("class", "form-text text-danger");
                _smallMensajeC.childNodes[0].setAttribute("class", "glyphicon glyphicon-remove");
                _smallMensajeC.childNodes[1].textContent = " Contraseña no coincide";
                _parentC.setAttribute("class", "form-group col-md-6 has-warning");
                _smallMensajeC.removeAttribute("hidden");
            }
        }else{
            _smallMensajeP.setAttribute("class", "form-text text-danger");
            _smallMensajeP.childNodes[0].setAttribute("class", "glyphicon glyphicon-remove");
            _smallMensajeP.childNodes[1].textContent = (_inputPsw.value.length === 0) ? "Campo requerido" : " Minimo 6 caracteres";
            _parentP.setAttribute("class", "form-group col-md-6 has-warning");
            _smallMensajeP.removeAttribute("hidden");

            _smallMensajeC.setAttribute("class", "form-text text-danger");
            _smallMensajeC.childNodes[0].setAttribute("class", "glyphicon glyphicon-remove");
            _smallMensajeC.childNodes[1].textContent = (_inputPswConfirm.value.length === 0) ? "Campo requerido" : " Formato invalido";
            _parentC.setAttribute("class", "form-group col-md-6 has-warning");
            _smallMensajeC.removeAttribute("hidden");
        }

        return false;
    }

    var _evaluaRegexp = function( _regexp, _idInput){
        var _inputNombre = document.getElementById( _idInput );
        if( !_regexp.test(_inputNombre.value) ){
            var _class = _inputNombre.parentNode.getAttribute("class");
            _class = _class.replace(" has-success", "");
            _class = _class.replace(" has-warning", "");
            _class = _class.replace(" has-error", "");
            _class = _class + " has-error";
            _inputNombre.parentNode.setAttribute("class", _class);

            _smallMensaje = document.getElementById( _inputNombre.id + "Small" );
            _smallMensaje.setAttribute("class", "form-text text-danger");
            _smallMensaje.childNodes[0].setAttribute("class", "glyphicon glyphicon-remove");
            _smallMensaje.childNodes[1].textContent = ((_inputNombre.value.length === 0) ? " Campo requerido" : " Formato invalido");
            _smallMensaje.removeAttribute("hidden");
            return false;
        }
        return true;
    }

    var _checkPassword = function( _idInputPsw, _idInputConfirm ){
        var _inputPsw = document.getElementById(_idInputPsw);
        var _inputPswConfirm = document.getElementById(_idInputConfirm);
        var _parentP = _inputPsw.parentNode;
        var _parentC = _inputPswConfirm.parentNode;
        var _smallMensajeP = document.getElementById( _idInputPsw + "Small" );
        var _smallMensajeC = document.getElementById( _idInputConfirm + "Small" );

        _inputPsw.onkeyup = function(){
            if(_inputPsw.value.length < 6){
                _smallMensajeP.setAttribute("class", "form-text text-warning");
                _smallMensajeP.childNodes[0].setAttribute("class", "glyphicon glyphicon-warning-sign");
                _smallMensajeP.childNodes[1].textContent = " Minimo 6 caracteres";
                _parentP.setAttribute("class", "form-group col-md-6 has-warning");
                _smallMensajeP.removeAttribute("hidden");

                _parentC.setAttribute("class", "form-group col-md-6 has-default");
                _smallMensajeC.setAttribute("hidden", "");
            }else{
                _parentP.setAttribute("class", "form-group col-md-6 has-default");
                _smallMensajeP.setAttribute("hidden", "");

                if(_inputPsw.value === _inputPswConfirm.value) {
                    _smallMensajeC.setAttribute("hidden", "");
                    _parentP.setAttribute("class", "form-group col-md-6 has-success");
                    _parentC.setAttribute("class", "form-group col-md-6 has-success");
                }else{
                    _smallMensajeC.setAttribute("class", "form-text text-warning");
                    _smallMensajeC.childNodes[0].setAttribute("class", "glyphicon glyphicon-warning-sign");
                    _smallMensajeC.childNodes[1].textContent = " Contraseña no coincide";
                    _parentP.setAttribute("class", "form-group col-md-6 has-warning");
                    _parentC.setAttribute("class", "form-group col-md-6 has-warning");
                    _smallMensajeC.removeAttribute("hidden");
                }
            }
        }
        
        _inputPswConfirm.onkeyup = function(){
            if( _inputPsw.value.length >= 6){
                if(_inputPsw.value === _inputPswConfirm.value) {
                    _smallMensajeC.setAttribute("hidden", "");
                    _parentP.setAttribute("class", "form-group col-md-6 has-success");
                    _parentC.setAttribute("class", "form-group col-md-6 has-success");
                }else{
                    _smallMensajeC.setAttribute("class", "form-text text-warning");
                    _smallMensajeC.childNodes[0].setAttribute("class", "glyphicon glyphicon-warning-sign");
                    _smallMensajeC.childNodes[1].textContent = " Contraseña no coincide";
                    _parentP.setAttribute("class", "form-group col-md-6 has-warning");
                    _parentC.setAttribute("class", "form-group col-md-6 has-warning");
                    _smallMensajeC.removeAttribute("hidden");
                }
            }
        }
    }

    var _avatar = function(_idInput, idImg){
        var _inputAvatar = document.getElementById(_idInput);
        var _imgImagenPerfil = document.getElementById(idImg);
        var _urlSrcImg = _imgImagenPerfil.getAttribute( "defaultSrc" );

        _inputAvatar.onchange = function(_event){
            console.log(_inputAvatar.value);

            //---------------[   UPDATE   ]------------------------------
            var _checkboxAvatar =  document.getElementById("cambioAvatar");
            if( typeof(_checkboxAvatar) != 'undefined' && _checkboxAvatar != null ){
                _checkboxAvatar.setAttribute("checked", "");   
            }
            //---------------[ FIN UPDATE ]------------------------------

            if (_inputAvatar.files && _inputAvatar.files[0]) {
                var _file = _inputAvatar.files[0];
                var _fileTypes = [ 'image/jpeg', 'image/pjpeg', 'image/png' ];
                var _valido = false;
                var _smallMensaje = document.getElementById("avatarSmall");

                for( var i = 0; i < _fileTypes.length; i++ ) {
                    if(_file.type === _fileTypes[i]) {
                        _valido = true;
                    }
                }

                if( _valido ){
                    if( _file.size < 1048576){
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            var _imagen = new Image();
                            _imagen.src = e.target.result;
                            //Validar Height & Width.
                            _imagen.onload = function () {
                                var _height = this.height;
                                var _width = this.width;
                                if ( _height > 1080 || _width > 900 || _width > _height ) {
                                    _inputAvatar.value = "";
                                    _imgImagenPerfil.src = _urlSrcImg;
                                    _smallMensaje.childNodes[1].textContent = " Resolucion inadecuada";
                                    _smallMensaje.removeAttribute("hidden");
                                    window.setTimeout(function() {
                                        _smallMensaje.setAttribute("hidden","");
                                    }, 1000);
                                }else{
                                    _imgImagenPerfil.setAttribute("src", e.target.result);
                                    //$('#avatarImg').attr('src', e.target.result).fadeIn('slow');
                                }
                            };
                        };

                        reader.readAsDataURL(_inputAvatar.files[0]);
                    }else{
                        _inputAvatar.value = "";
                        _imgImagenPerfil.src = _urlSrcImg;
                        _smallMensaje.childNodes[1].textContent = " Imagen > 1MB";
                        _smallMensaje.removeAttribute("hidden");
                        window.setTimeout(function() {
                            _smallMensaje.setAttribute("hidden","");
                        }, 1000);
                    }
                }else{
                    _inputAvatar.value = "";
                    _imgImagenPerfil.src = _urlSrcImg;
                    _smallMensaje.childNodes[1].textContent = " Formato invalido.";
                    _smallMensaje.removeAttribute("hidden");
                    window.setTimeout(function() {
                        _smallMensaje.setAttribute("hidden","");
                    }, 1000);
                }
            }
        }
    }

    var _email = function( _idInput ){
        var _regexp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i;
        var _inputX = document.getElementById( _idInput );
        var _parent = _inputX.parentNode;
        var _smallMensaje = document.getElementById( _inputX.id + "Small" );

        _inputX.onkeypress = function( _event ){
            var _keypressedCode = (document.all) ? _event.keyCode : _event.which;

            if( _inputX.value.length < 256){
                var _keyString = String.fromCharCode( _keypressedCode );
                if( !/[a-z0-9.-_&@]/i.test(_keyString) ){
                    if( _controlkey(_event.keyCode) ){  // Teclas de control
                        _smallMensaje.setAttribute("hidden", "");
                        return true;
                    }else{
                        _smallMensaje.setAttribute("class", "form-text text-info");
                        _smallMensaje.childNodes[0].setAttribute("class", "glyphicon glyphicon-info-sign");
                        _smallMensaje.childNodes[1].textContent = " Caracter deshabilitado";
                        _smallMensaje.removeAttribute("hidden");
                    }
                }else{
                    _smallMensaje.setAttribute("hidden", "");
                }
                return /[a-z0-9.-_&@]/i.test(_keyString);
            }else{
                if( _controlkey(_event.keyCode) ){  // Teclas de control
                    _smallMensaje.setAttribute("hidden", "");
                    return true;
                }else{
                    _smallMensaje.setAttribute("class", "form-text text-info");
                    _smallMensaje.childNodes[0].setAttribute("class", "glyphicon glyphicon-info-sign");
                    _smallMensaje.childNodes[1].textContent = " Limite alcanzado";
                    _smallMensaje.removeAttribute("hidden");
                    return false;
                }
            }
        }

        _inputX.onkeyup = function( _event ){
            if( _regexp.test(_inputX.value) ){
                if( _smallMensaje.hasAttribute("hidden") ){
                    _smallMensaje.setAttribute("hidden", "");
                }
                _parent.setAttribute("class", "form-group col-md-7 has-success");
            }else{
                if( _smallMensaje.hasAttribute("hidden") ){
                    _smallMensaje.setAttribute("class", "form-text text-warning");
                    _smallMensaje.childNodes[0].setAttribute("class", "glyphicon glyphicon-warning-sign");
                    _smallMensaje.childNodes[1].textContent = " Formato invalido";
                    _parent.setAttribute("class", "form-group col-md-7 has-warning");
                    _smallMensaje.removeAttribute("hidden");
                }
            }
        }
    }

    var _curp = function( _idInput ){
        var _regexp = /^[a-z]{4}\d{6}(H|M)[a-z]{5}\d{2}$/i;
        var _inputX = document.getElementById( _idInput );
        var _parent = _inputX.parentNode;
        var _smallMensaje = document.getElementById( _inputX.id + "Small" );

        _inputX.onkeypress = function( _event ){
            var _keypressedCode = (document.all) ? _event.keyCode : _event.which;

            if( _inputX.value.length < 18){
                var _keyString = String.fromCharCode( _keypressedCode );
                if( !/[a-z0-9]/i.test(_keyString) ){
                    if( _controlkey(_event.keyCode) ){  // Teclas de control
                        _smallMensaje.setAttribute("hidden", "");
                        return true;
                    }else{
                        _smallMensaje.setAttribute("class", "form-text text-info");
                        _smallMensaje.childNodes[0].setAttribute("class", "glyphicon glyphicon-info-sign");
                        _smallMensaje.childNodes[1].textContent = " Caracter deshabilitado";
                        _smallMensaje.removeAttribute("hidden");
                    }
                }else{
                    _smallMensaje.setAttribute("hidden", "");
                }
                return /[a-z0-9]/i.test(_keyString);
            }else{
                if( _controlkey(_event.keyCode) ){  // Teclas de control
                    _smallMensaje.setAttribute("hidden", "");
                    return true;
                }else{
                    _smallMensaje.setAttribute("class", "form-text text-info");
                    _smallMensaje.childNodes[0].setAttribute("class", "glyphicon glyphicon-info-sign");
                    _smallMensaje.childNodes[1].textContent = " Limite alcanzado";
                    _smallMensaje.removeAttribute("hidden");
                    return false;
                }
            }
        }

        _inputX.onkeyup = function( _event ){
            if( _regexp.test(_inputX.value) ){
                if( _smallMensaje.hasAttribute("hidden") ){
                    _smallMensaje.setAttribute("hidden", "");
                }
                _parent.setAttribute("class", "form-group col-md-5 has-success");
            }else{
                if( _smallMensaje.hasAttribute("hidden") ){
                    _smallMensaje.setAttribute("class", "form-text text-warning");
                    _smallMensaje.childNodes[0].setAttribute("class", "glyphicon glyphicon-warning-sign");
                    _smallMensaje.childNodes[1].textContent = " Formato invalido";
                    _parent.setAttribute("class", "form-group col-md-5 has-warning");
                    _smallMensaje.removeAttribute("hidden");
                }
            }
        }
    }

    var _palabra = function( _idInput, _minlength, _maxlength, _regexp ){
        var _inputX = document.getElementById( _idInput );
        var _parent = _inputX.parentNode;
        var _smallMensaje = document.getElementById( _inputX.id + "Small" );

        _inputX.onkeypress = function( _event ){
            var _keypressedCode = (document.all) ? _event.keyCode : _event.which;
            
            if( _inputX.value.length < _maxlength ){
                var _keyString = String.fromCharCode( _keypressedCode );
                if( _regexp.test(_keyString) ){
                    _smallMensaje.setAttribute("hidden", "");
                    _parent.setAttribute("class", "form-group col-md-8 has-success");
                }else{
                    if( _controlkey(_event.keyCode) ){  // Teclas de control
                        _smallMensaje.setAttribute("hidden", "");
                        return true;
                    }else{
                        _smallMensaje.setAttribute("class", "form-text text-info");
                        _smallMensaje.childNodes[0].setAttribute("class", "glyphicon glyphicon-info-sign");
                        _smallMensaje.childNodes[1].textContent = " Caracteres especiales deshabilitados";
                        _smallMensaje.removeAttribute("hidden");
                    }
                }
                return _regexp.test( _keyString );
            }else{
                if( _controlkey(_event.keyCode) ){  // Teclas de control
                    _smallMensaje.setAttribute("hidden", "");
                    return true;
                }else{
                    _smallMensaje.setAttribute("class", "form-text text-info");
                    _smallMensaje.childNodes[0].setAttribute("class", "glyphicon glyphicon-info-sign");
                    _smallMensaje.childNodes[1].textContent = " Limite de caracteres alcanzado";
                    _smallMensaje.removeAttribute("hidden");
                    return false;
                }
            }
        }

        _inputX.onkeyup = function( _event ){
            var _str = _event.target.value;
            if(_str.length !== 0){
                _str = ( /\s/.test(_str.charAt(0)) ) ? _str.substring(1) : _str;
            }
            _event.target.value = _str.replace(/ {2,}/g, ' ');
            if(_inputX.value.length < _minlength){
                _parent.setAttribute("class", "form-group col-md-8 has-warning");
                if( _smallMensaje.hasAttribute("hidden") ){
                    _smallMensaje.setAttribute("class", "form-text text-warning");
                    _smallMensaje.childNodes[0].setAttribute("class", "glyphicon glyphicon-warning-sign");
                    _smallMensaje.childNodes[1].textContent = " Minimo 3 caracteres";
                    _smallMensaje.removeAttribute("hidden");
                }
            }
        }
    }

    var _controlkey = function( _keycode){
        switch ( _keycode ) {
            case 8:     //backspace
            case 9:     //tab
            case 16:    //shift
            case 27:    //escape
            case 35:    //end
            case 36:    //home
            case 37:    //left arrow
            case 39:    //right arrow
            case 46:    //delete
                return true;
        };
        return false
    };

    return{
        "init": _init
    };
})();