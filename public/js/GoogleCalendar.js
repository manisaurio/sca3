/**
 *  GOOGLE CALENDAR
 */

// Variable GoogleDeveloperConsole

var CLIENT_ID = '268975910670-eqcvevlgnq7g46r8bhap12k8hsi8114v.apps.googleusercontent.com';
var API_KEY = 'AIzaSyADnzxwMdxbgRQvQRu0f_a57o-G5y3SxlI';
var SCOPES = 'https://www.googleapis.com/auth/gmail.readonly https://www.googleapis.com/auth/calendar';
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest", "https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest"];

var authorizeButton = document.getElementById('authorize-button');      
var signoutButton = document.getElementById('signout-button');
      
/** 
*  Función invocada cuando se carga la biblioteca javascript de cliente
*/
function handleClientLoad() {
  console.info("Desde handleClientLoad ...");
  gapi.load('client:auth2', initClient);
}

/**
*  Inicializa la biblioteca de cliente de API y configura los usuarios 
*  de estado de inicio de sesión.
*/
function initClient() {
  gapi.client.init({
    discoveryDocs: DISCOVERY_DOCS,
    clientId: CLIENT_ID,
    scope: SCOPES
  }).then(function () {
    // Escuche los cambios de estado de inicio de sesión.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

    // Maneja el estado de inicio de sesión inicial.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    authorizeButton.onclick = handleAuthClick;
    signoutButton.onclick = handleSignoutClick;
  });
}

/**
*  Se llama cuando el estado de inicio de sesión cambia, para
*  acualizar la interfaz de usuario apropiadamente. Después de 
*  un inicio de sesión, se llama a la API.
*/
function updateSigninStatus(isSignedIn) {
  console.log("Desde updateSigninStatus... ");
  if (isSignedIn) {
    authorizeButton.style.display = 'none';
    //---[ SINGOUT ]------------------------
    signoutButton.style.display = 'none';
    //signoutButton.style.display = 'block';
    //addButton.style.display = 'block';
    
    datosUsuario();
  } else {
    authorizeButton.style.display = 'block';
    signoutButton.style.display = 'none';
    //addButton.style.display = 'none';
  }
}

/**
*  Sign in usuario al hacer clic en el botón.
*/
function handleAuthClick(event) {    
  gapi.auth2.getAuthInstance().signIn();
}

/**
*  Sign out usuario al hacer clic en el botón.
*/
function handleSignoutClick(event) {
  gapi.auth2.getAuthInstance().signOut();

  // Logout Gmail
  document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://localhost:8080";
}

/**
*  Fin - Proceso de autenticación. 
*/

/**
* PARTE 2 - Tratando los eventos desde la interfaz de usuario y
* realizar llamadas API. 
**/

function datosUsuario() {
  gapi.client.request({
    'path': 'https://www.googleapis.com/gmail/v1/users/me/profile'
  }).then(function(response) { // Realizar solicitud a la API
    // Procesar respuesta
    //var _pusuario = document.getElementById("idpusuario");
    //_pusuario.textContent = "Email: " + response.result.emailAddress;
    var _textNodeEmail = document.createTextNode("Email: " + response.result.emailAddress);
    var _panelFooterCalendar = document.getElementById("panelFooterCalendar");
    _panelFooterCalendar.appendChild(_textNodeEmail);
    
    }, function(reason) { // En caso de error
      console.error('Error: ' + reason.result.error.message);
    }
  );
}