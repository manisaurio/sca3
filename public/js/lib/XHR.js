var XHR = (function() {
    var _get = function(_url, _en_caso_de_exito, _en_caso_de_error) {
        var _detectar_cambios_de_estado = function(_xhr, _en_caso_de_exito, _en_caso_de_error) {
            return function() {
                if (_xhr.readyState === 4) {
                    if (_xhr.status >= 200 && _xhr.status <= 299) {
                        _en_caso_de_exito(_xhr);
                    } else {
                        _en_caso_de_error(_xhr);
                    }
                }
            };
        };
        var xhr = new XMLHttpRequest();
        xhr.open("GET", _url, /*true*/false);
        xhr.onreadystatechange = _detectar_cambios_de_estado(xhr, _en_caso_de_exito, _en_caso_de_error);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.send();
    };

    var _post = function(_url, _body, _en_caso_de_exito, _en_caso_de_error) {
        var _detectar_cambios_de_estado = function(_xhr, _en_caso_de_exito, _en_caso_de_error) {
            return function() {
                if (_xhr.readyState === 4) {
                    if (_xhr.status >= 200 && _xhr.status <= 299) {
                        _en_caso_de_exito(_xhr);
                    } else {
                        _en_caso_de_error(_xhr);
                    }
                }
            };
        };
        var xhr = new XMLHttpRequest();
        //xhr.onreadystatechange = _detectar_cambios_de_estado(xhr, _en_caso_de_exito, _en_caso_de_error);
        xhr.open("POST", _url /*+ "?r=" + Math.random()*/, /*true*/false);
        xhr.onreadystatechange = _detectar_cambios_de_estado(xhr, _en_caso_de_exito, _en_caso_de_error);
        xhr.setRequestHeader("Content-Type", "application/json");
        //xhr.setRequestHeader("Authorization", "e12bddd0-5325-401e-b31f-774768adc019");
        xhr.send( _body );
    };

  var _del = function(_url, _en_caso_de_exito, _en_caso_de_error) {
        var _detectar_cambios_de_estado = function(_xhr, _en_caso_de_exito, _en_caso_de_error) {
            return function() {
                if (_xhr.readyState === 4) {
                    if (_xhr.status >= 200 && _xhr.status <= 299) {
                        _en_caso_de_exito(_xhr);
                    } else {
                        _en_caso_de_error(_xhr);
                    }
                }
            };
        };
        var xhr = new XMLHttpRequest();
        xhr.open("DELETE", _url , true);
        xhr.onreadystatechange = _detectar_cambios_de_estado(xhr, _en_caso_de_exito, _en_caso_de_error);
        xhr.send();
    };


    
    var _get_urls = function(_urls, _continuacion) {
        var _estado = {
            "exitos": [],
            "errores": []
        };
        
        var _exito = function(_estado, _urls, _continuacion) {
            return function(_xhr) {
                _estado.exitos.push(_xhr);
                if ((_estado.exitos.length + _estado.errores.length) === _urls.length) {
                    _continuacion(_estado);
                }
            };
        };
        
        var _error = function(_estado, _urls, _continuacion) {
            return function(_xhr) {
                _estado.errores.push(_xhr);
                if ((_estado.exitos.length + _estado.errores.length) === _urls.length) {
                    _continuacion(_estado);
                }
            };
        };
        
        var _acumular_exitos = _exito(_estado, _urls, _continuacion);
        var _acumular_errores = _error(_estado, _urls, _continuacion);
        
        for (var i = 0; i < _urls.length; i++) {
            XHR.get(_urls[i], _acumular_exitos, _acumular_errores);
        }
    };

    var _get_urls_query = function(_urls) {
        var _estado = {
            "exitos": [],
            "errores": []
        };

        var _exito = function(_estado, _urls, _continuacion) {
            return function(_xhr) {
                _estado.exitos.push(_xhr);
            };
        };

        var _error = function(_estado, _urls, _continuacion) {
            return function(_xhr) {
                _estado.errores.push(_xhr);
            };
        };

        var _acumular_exitos = _exito(_estado, _urls);
        var _acumular_errores = _error(_estado, _urls);

        for (var i = 0; i < _urls.length; i++) {
            XHR.get(_urls[i], _acumular_exitos, _acumular_errores);
        }

        var _consultar_estado = function(_estado, _urls) {
            return function() {
                if ((_estado.exitos.length + _estado.errores.length) === _urls.length) {
                    return {
                        "terminado": true,
                        "datos": _estado
                    };
                }
                return {
                    "terminado": false,
                    "datos": _estado
                };
            };
        };

        return _consultar_estado(_estado, _urls);
    };

    var _post_urls_query = function(_urls, _bodys) {
        var _estado = {
            "exitos": [],
            "errores": []
        };

        var _exito = function(_estado, _urls, _continuacion) {
            return function(_xhr) {
                _estado.exitos.push(_xhr);
            };
        };

        var _error = function(_estado, _urls, _continuacion) {
            return function(_xhr) {
                _estado.errores.push(_xhr);
            };
        };

        var _acumular_exitos = _exito(_estado, _urls);
        var _acumular_errores = _error(_estado, _urls);

        for (var i = 0; i < _urls.length; i++) {
            XHR.post(_urls[i], _bodys[i], _acumular_exitos, _acumular_errores);
        }

        var _consultar_estado = function(_estado, _urls) {
            return function() {
                if ((_estado.exitos.length + _estado.errores.length) === _urls.length) {
                    return {
                        "terminado": true,
                        "datos": _estado
                    };
                }
                return {
                    "terminado": false,
                    "datos": _estado
                };
            };
        };

        return _consultar_estado(_estado, _urls);
    };

    return {
        "get": _get,
        "get_urls": _get_urls,
        "get_urls_query": _get_urls_query,
        "post": _post,
        "post_urls_query": _post_urls_query,
        "del": _del
    };
})();