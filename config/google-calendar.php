<?php

return [

    /*
     * Path to the json file containing the credentials.
     */
    'service_account_credentials_json' => storage_path('app/google-calendar/service-account-credentials.json'),

    /*
     *  The id of the Google Calendar that will be used by default.
     */
    'calendar_id' => env('https://calendar.google.com/calendar/ical/damoaxaca%40gmail.com/private-683c9e79a0dedde0d61954eabc665434/basic.ics'),
];
