<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
	/**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        //---[ PRUEBAS UNITARIAS ROL: MENSAJERO ]-------------------------------------------
        Auth::loginUsingId(2);	//---[ MENSAJERO ]------------------------------------------
        $this->withoutMiddleware(VerifyCsrfToken::class);
        
        //---[ AGREGAR DOCUMENTO ]----------------------------------------------------------
        $response = $this->call('POST', '/documentos/ingreso', 
        	[
        		'numOficio' => '123',
        		'fechaOficio' => '2018-02-01',
        		'numHojas' => '1',
        		'tipo' => '1',
        		'numAnexos' => '0',
        		'asunto' => 'Prueba desde test unit',
        		'suscriptorInput' => 'nuevo suscriptor',
        		'cargoInput' => 'nuevo',
        		'dependenciaInput' => 'neva dependencia'
        	]
        );
        $this->assertEquals(302, $response->status());

        //---[ DETALLE DOCUMENTO ]----------------------------------------------------------
        $response = $this->call('GET', '/documentos/show/3');
        $this->assertEquals(200, $response->status());
        
        //---[ EDITAR DOCUMENTO ]-----------------------------------------------------------
        $response = $this->call('PUT', '/documentos/edit/3', 
        	[
        		'numOficio' => '456',
        		'fechaOficio' => '2018-02-03',
        		'numHojas' => '3',
        		'tipo' => '2',
        		'numAnexos' => '1',
        		'asunto' => 'Prueba desde test unit editado',
        		'suscriptorInput' => 'nuevo suscriptor editado',
        		'cargoInput' => 'nuevo',
        		'dependenciaInput' => 'neva dependencia editado'
        	]
        );
        $this->assertEquals(302, $response->status());

        //---[ TURNAR DOCUMENTO ]-----------------------------------------------------------
        $response = $this->call('POST', '/guiasturnados/create/3', 
        	[ 'observacion' => 'observacion mensajero' ]
        );
        $this->assertEquals(200, $response->status());
        
        /*
        //---[ ELIMINAR DOCUMENTO ]---------------------------------------------------------
        $response = $this->call('POST', '/documentos/destroy/3', [ '_method' => 'delete' ]);
        $this->assertEquals(200, $response->status());
        
       	Auth::logout();
       	//---[ PRUEBAS UNITARIAS ROL: DIRECTOR ]-------------------------------------------
        Auth::loginUsingId(4);	//---[ DIRECTOR ]------------------------------------------
        $this->withoutMiddleware(VerifyCsrfToken::class);

        //---[ REVISAR TURNADO ]-----------------------------------------------------------
        $response = $this->call('POST', '/reportesrevisiones/create/2/3', 
        	[ 'observacion' => 'observacion director' ]
        );
        $this->assertEquals(302, $response->status());

        //---[ CREAR DEPARTAMENTO ]--------------------------------------------------------
        $response = $this->call('POST', 'departamentos/create', 
        	[ 
        		'nombre' => 'nuevo depto',
        		'areaSelect' => 'ENFERMERIA'
        	]
        );
        $this->assertEquals(302, $response->status());

        //---[ DETALLE DEPARTAMENTO ]------------------------------------------------------
        $response = $this->call('GET', '/departamentos/show/15');
        $this->assertEquals(200, $response->status());

        //---[ EDITAR DEPARTAMENTO ]-------------------------------------------------------
        $response = $this->call('PUT', '/departamentos/edit/15', 
        	[
        		'nombre' => 'nuevo nombre depto',
        		'area' => 'ENFERMERIA'
        	]
        );
        $this->assertEquals(302, $response->status());

        //---[ ELIMINAR DEPARTAMENTO ]-----------------------------------------------------
        $response = $this->call('POST', '/departamentos/destroy/15', [ '_method' => 'delete' ]);
        $this->assertEquals(200, $response->status());

        //---[ CREAR USUARIO ]--------------------------------------------------------
        $response = $this->call('POST', 'departamentos/create', 
        	[ 
        		'nombre' => 'nuevo usuario',
        		'ap_paterno' => 'apellidopaterno',
        		'ap_materno' => 'apellidomaterno',
        		'curp' => 'XXXX123456ZXCV',
        		'email' => 'email@email.com',
        		'area' => 'ENFERMERIA',
        		'departamento' => '6',
        		'puesto' => 'JEFE',
        		'psw' => 'contraseña',
        		'pswconfirm' => 'contraseña'
        	]
        );
        $this->assertEquals(302, $response->status());

        //---[ DETALLE USUARIO ]------------------------------------------------------
        $response = $this->call('GET', '/empleados/show/10');
        $this->assertEquals(200, $response->status());

        //---[ EDITAR USUARIO ]-------------------------------------------------------
        $response = $this->call('PUT', '/empleados/edit/10', 
        	[ 
        		'nombre' => 'nuevo usuario editado',
        		'ap_paterno' => 'apellidopaterno',
        		'ap_materno' => 'apellidomaterno',
        		'curp' => 'XXXX123456ZXCV',
        		'email' => 'email@email.com',
        		'area' => 'ENFERMERIA',
        		'departamento' => '6',
        		'puesto' => 'JEFE',
        		'psw' => 'contraseña',
        		'pswconfirm' => 'contraseña'
        	]
        );
        $this->assertEquals(302, $response->status());

        //---[ ELIMINAR USUARIO ]-----------------------------------------------------
        $response = $this->call('POST', '/empleados/destroy/10', [ '_method' => 'delete' ]);
        $this->assertEquals(200, $response->status());
       
       	Auth::logout();
       	//---[ PRUEBAS UNITARIAS ROL: ASISTENTE ]-------------------------------------------
        Auth::loginUsingId(5);	//---[ ASISTENTE ]------------------------------------------
        $this->withoutMiddleware(VerifyCsrfToken::class);

        //---[ CREAR CITA ]-----------------------------------------------------------------
        $response = $this->call('POST', '/citas/create', 
        	[ 
        		'date' => '2018-02-01'
        		'start_time' => '16:44',
        		'title' => 'titulo evento',
        		'description' => 'descripcion evento'
        	]
        );
        $this->assertEquals(302, $response->status());

        //---[ DETALLE CITA ]---------------------------------------------------------------
        $response = $this->call('GET', '/citas/show/1');
        $this->assertEquals(200, $response->status());

        //---[ EDITAR CITA ]----------------------------------------------------------------
        $response = $this->call('PUT', '/citas/edit/1',
        	[ 
        		'date' => '2018-02-01'
        		'start_time' => '16:44',
        		'title' => 'titulo evento',
        		'description' => 'descripcion evento'
        	]
        );
        $this->assertEquals(302, $response->status());

        //---[ ELIMINAR CITA ]-----------------------------------------------------
        $response = $this->call('POST', '/citas/destroy/1', [ '_method' => 'delete' ]);
        $this->assertEquals(200, $response->status());

        
        //---[ CREAR RESPUESTA ]------------------------------------------------------------
        $response = $this->call('POST', '/respuestas/create/1/1', 
        	[
        		'fechaOficio' => '2018-02-10',
        		'numHojas' => '1',
        		'tipo' => '1',
        		'numAnexos' => '0',
        		'asunto' => 'Prueba desde test unit',
        		'suscriptorInput' => 'nuevo suscriptor',
        		'cargoInput' => 'nuevo',
        		'dependenciaInput' => 'neva dependencia'
        	]
        );
        $this->assertEquals(302, $response->status());

        //---[ DETALLE RESPUESTA ]----------------------------------------------------------
        $response = $this->call('GET', '/respuestas/show/1/1');
        $this->assertEquals(200, $response->status());
        
        //---[ EDITAR RESPUESTA ]-----------------------------------------------------------
        $response = $this->call('PUT', '/respuestas/edit/1/1', 
        	[
        		'fechaOficio' => '2018-02-13',
        		'numHojas' => '3',
        		'tipo' => '2',
        		'numAnexos' => '1',
        		'asunto' => 'Prueba desde test unit editado',
        		'suscriptorInput' => 'nuevo suscriptor editado',
        		'cargoInput' => 'nuevo',
        		'dependenciaInput' => 'neva dependencia editado'
        	]
        );
        $this->assertEquals(302, $response->status());

        //---[ ELIMINAR RESPUESTA ]---------------------------------------------------------        
        $response = $this->call('POST', '/respuesta/destroy/1/1', [ '_method' => 'delete' ]);
        $this->assertEquals(200, $response->status());
		*/
    }
}
