<!-- The Modal -->
<div class="modal fade" id="confirmDeleteModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="confirmModalTitle">Confirmar eliminación</h4>
      </div>

      <!-- Modal body -->
      <div class="modal-body alert alert-info" id="confirmModalBody">
        Esta operación puede causar la pérdida de información.
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="confirmAcceptButton">Aceptar</button>
        <button type="button" class="btn btn-secondary" id="confirmCancelButton" data-dismiss="modal">Cacelar</button>
      </div>

    </div>
  </div>
</div>