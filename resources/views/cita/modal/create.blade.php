<!-- Modal Create Event -->

<!-- The Modal -->
<div class="modal fade" id="createEventModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <button id="closeModalButton" type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="titleModal"><i class="fa fa-thumb-tack fw"></i> Nuevo Evento</h4>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="container-fluid">
          <form method="POST" id="formCita">
            {{ csrf_field() }} 
            <div class="form-row">
              <div class="form-group col-md-8">
                <label>Fecha inicio:</label>
                <input type="date" id="date" name="date" class="form-control" readonly>
              </div>
              <div class="form-group col-md-4">
                <label>Hora:</label>
                <input type="time" id="start_time" name="start_time" class="form-control">
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="">Título:</label>
                <input type="text" id="title" name="title" class="form-control">
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="">Descripción:</label>
                <textarea rows="3" id="description" name="description" class="form-control"></textarea>
              </div>
            </div>

            <div class="form-row" id="divLinkDocumento" hidden>
              <div class="form-group col-md-12">
                <label for=""><i class="glyphicon glyphicon-link"></i> Documento:</label>
                <input type="text" id="url" name="url" class="form-control" readonly>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="">Lugar:</label>
                <input type="text" id="location" name="location" class="form-control">
              </div>
            </div>

            <div class="form-row" hidden>
              <input type="text" id="id_solicitud" name="id_solicitud" class="form-control">
            </div>

            <div class="form-row">
              <div class="form-group text-center col-md-12"> 
                <button type="submit" id="submitButton" class="btn btn-primary"> 
                  Agregar evento 
                </button> 
              </div>
            </div>
          </form>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button id="cancelButton" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
  function removeChildren( _idnode ){
    var node = document.getElementById(_idnode);
    while (node.firstChild) {
        node.removeChild(node.firstChild);
    }
  }

  function getUserInput(){
    // Debe ser llamado cuando este verificado que los campos requeridos no esten vacios
    var date = document.querySelector("#date").value;
    var startTime = document.querySelector("#start_time").value;
    var endTime = (startTime.localeCompare("") === 0) ? "" : moment(startTime, 'HH:mm').add(1, 'hours').format('HH:mm');
    var eventTitle = document.querySelector("#title").value;
    var eventDescription = document.querySelector("#description").value;
    var eventLocation = document.querySelector("#location").value;
    var eventUrl = document.querySelector("#url").value;
    var id_solicitud = document.querySelector("#id_solicitud").value;

    return {
      'date': date, 
      'startTime': startTime, 
      'endTime': endTime, 
      'eventTitle': eventTitle, 
      'eventDescription': eventDescription,
      "eventLocation": eventLocation,
      "eventUrl": eventUrl,
      "id_solicitud": id_solicitud
    };
  }
</script>

<!-- BootstrapValidator Script -->
<script type="text/javascript">
  $(document).ready(function() {
    $("#formCita")
    .bootstrapValidator({
      message: "No valido",
      fields: {
        start_time: {
          validators: {
            notEmpty: {
              message: "Requerido"
            }
          }
        },
        title: {
          validators: {
            notEmpty: {
              message: "Requerido"
            }
          }
        },
        description: {
          validators: {
            notEmpty: {
              message: "Requerido"
            },
            stringLength: {
              min: 3,
              max: 250,
              message: "Longitud 3-250"
            }
          }
        }
      }
    })
    .on("success.form.bv", function(e) {
      // Prevent form submission
      e.preventDefault();

      // Inhabilitar
      function confirmationMessage(e){
        var confirmationMessage = "\o/";
        e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
        return confirmationMessage;              // Gecko, WebKit, Chrome <34
      }
      window.addEventListener("beforeunload", confirmationMessage);
      $("#cancelButton").prop("disabled", true);
      $("#closeModalButton").prop("disabled", true);

      // Obtener datos de formulario
      var _eventData = getUserInput();
      // Crear recurso
      var resource = {
        "summary": _eventData.eventTitle,
        "description": _eventData.eventDescription + " §" + _eventData.eventUrl,
        "location": _eventData.eventLocation,
        "colorId": 9,
        "start": {
          "dateTime": new Date(_eventData.date + " " + _eventData.startTime).toISOString()
        },
        "end": {
          "dateTime": new Date(_eventData.date + " " + _eventData.endTime).toISOString()
        }
      };
      
      // Crear la solicitud
      var request = gapi.client.calendar.events.insert({
        "calendarId": "primary",
        "sendNotifications": true,
        "resource": resource
      });

      // Ejecutar la solicitud y hacer algo con respuesta
      request.execute(function(respuesta) {
        if( respuesta.hasOwnProperty("status") ){
          if(respuesta.status.localeCompare("confirmed") === 0){
            _eventData.eventIdGoogle = respuesta.id;
            $.ajax({
              headers: { 
                "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
              }, 
              type: "post",
              data: _eventData,
              url: "{{url('/citas/storemodal')}}",
              success: function(_data){
                _tableSolicitudes.ajax.reload();  // Recargar DataTables Solicitudes
                $("#divSuccessMessage").load(window.location.href + " #divSuccessMessage" );  // Recargar seccion divSusccessMessage
                $("#fullCalendarDiv").fullCalendar("refetchEvents");  // Recargar FullCalendar
                $("#createEventModal").modal("hide");     // Ocultar ventana modal
              },
              error: function(_data){
                $("#createEventModal").modal("hide");     // Ocultar ventana modal
                var _bodyError = document.getElementById("errorPanelBodyDiv");
                removeChildren("errorPanelBodyDiv");
                var _mensaje = document.createTextNode(_data.responseJSON);
                _bodyError.appendChild(_mensaje);
                $("#errorPanelDiv").collapse("show");     // Mostrar panel de error
              }
            });
          }else{
            $("#createEventModal").modal("hide");     // Ocultar ventana modal
            var _bodyError = document.getElementById("errorPanelBodyDiv");
            removeChildren("errorPanelBodyDiv");
            var _mensaje = document.createTextNode("Error: el evento no fue agregado");
            _bodyError.appendChild(_mensaje);
            $("#errorPanelDiv").collapse("show");     // Mostrar panel de error
          }
        }else{
          $('#createEventModal').modal('hide');     // Ocultar ventana modal
            var _bodyError = document.getElementById("errorPanelBodyDiv");
            removeChildren("errorPanelBodyDiv");
            var _mensaje = document.createTextNode("Error: al agregar evento");
            _bodyError.appendChild(_mensaje);
            $('#errorPanelDiv').collapse("show");     // Mostrar panel de error
        }
        // Habilitar cierre con botones
        $("#cancelButton").prop("disabled", false);
        $("#closeModalButton").prop("disabled", false);
        window.removeEventListener("beforeunload", confirmationMessage);
      });
    });
  });
</script>