<!-- Modal Show Document -->

<!-- The Modal -->
<div class="modal fade" id="showDocumentoModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="titleModal"><i class="fa fa-file-text-o fw"></i> Detalle documento</h4>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="container-fluid">
          <div id="divEmbed" class="embed-responsive embed-responsive-16by9">
            <iframe id="iframeConArchivo" class="embed-responsive-item" allowfullscreen webkitallowfullscreen></iframe>
          </div>
        
          <div id="divSinArchivo" class="col-lg-12 col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    <i class="fa fa-file-pdf-o fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                    <div class="huge">NO</div>
                    <div>Existe copia digital</div>
                  </div>
                </div>
              </div>
              <div class="panel-footer">
                <span class="pull-left">Recurso no disponible.</span>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
          
          <form>
            <div class="form-row">
              <div class="form-group col-md-12">
                <p><b>NO. OFICIO: </b></p>
                <p id="numoficio"></p>
                <p><b>SUSCRIPTOR: </b></p>
                <p id="suscriptor"></p>
                <p><b>ASUNTO: </b> </p>
                <p id="asunto"></p>
              </div>
            </div>
          </form>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
  function removeChildren( _idnode ){
    var node = document.getElementById(_idnode);
    while (node.firstChild) {
        node.removeChild(node.firstChild);
    }
  }

  var showSolicitud = function(_url){
    $.ajax({
      headers: { 
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
      }, 
      type: 'GET',
      url: _url,
      success: function(_data){
        var _divEmbed = document.getElementById("divEmbed");
        var _iframeConArchivo = document.getElementById("iframeConArchivo");
        var _divSinArchivo = document.getElementById("divSinArchivo");
        removeChildren("numoficio");
        removeChildren("suscriptor");
        removeChildren("asunto");
        var _numoficio = document.getElementById("numoficio");
        var _suscriptor = document.getElementById("suscriptor");
        var _asunto = document.getElementById("asunto");
        if(_data.ruta_pdf.localeCompare("") !== 0){
          var _url = "{{ url('/') }}/" + _data.ruta_pdf;
          _iframeConArchivo.setAttribute("src", _url);
          _divEmbed.style.display = "block";
          _divSinArchivo.setAttribute("hidden", "");
        }else{
          _divEmbed.style.display = "none";
          _divSinArchivo.removeAttribute("hidden");
        }
        var _datosNumOficio = document.createTextNode(_data.num_oficio);
        var _datosSuscriptor = document.createTextNode(" " + _data.nombre + " - " + _data.cargo +" - " + _data.dependencia.toUpperCase());
        var _datosAsunto = document.createTextNode("" + _data.asunto);
        _numoficio.appendChild(_datosNumOficio);
        _suscriptor.appendChild(_datosSuscriptor);
        _asunto.appendChild(_datosAsunto);
        $('#showDocumentoModal').modal('show');
      },
      error: function(_data){
        console.warn(_data);
      }
    });
  }
</script>