@extends('layouts.master-director')

@section('content')

<div class="row">
  <nav aria-label="breadcrumb" style="margin-top: 10px;">
    <ol class="breadcrumb">
      <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
      <li class="breadcrumb-item" aria-current="page">Citas</li>
    </ol>
  </nav>
</div>

<div class="row" id="divSuccessMessage">
  <div class="col-lg-12">
    @if ( session('message') ) 
      <div class="alert alert-success alert-dismissable"> 
        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a> 
        <strong>EXITO: </strong> {{ session()->pull('message', 'default') }} 
      </div> 
    @endif 
  </div>
</div>
<!-- /.row -->

@if( count($errors) > 0) 
    <div class="alert alert-danger alert-dismissible" role="alert"> 
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> 
        <span aria-hidden="true">&times;</span> 
      </button> 
      <ul> 
        @foreach($errors->all() as $error) 
          <li>{!!$error!!}</li> 
        @endforeach 
      </ul> 
    </div> 
@endif 

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header" style="margin-top: 3px; margin-bottom: 15px;">CITAS</h3>
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row" id="errorRowDiv">
  <div class="col-lg-12">
    <div class="panel panel-danger panel-collapse collapse" id="errorPanelDiv">
      <div class="panel-heading">
        <strong>ERROR:</strong>
        <a data-toggle="collapse" href="#errorPanelDiv" class="close">&times</a>
      </div>
      <div class="panel-body" id="errorPanelBodyDiv">
      </div>
    </div>
  </div>
</div>
<!-- /.row -->

<div class="row">
  <div class="col-md-7">
    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="row">
          <div class="col-lg-8 col-md-8">
            <i class="fa fa-calendar fw"></i>
            Calendario    
          </div>
          <div class="col-lg-4 col-md-4 text-right">
            <div class="btn-group text-center">
              <a id="authorize-button" style="display: none;" class="btn btn-sm btn-default">
                <i class="glyphicon glyphicon-log-in"></i>
              </a>
              <a id="signout-button" style="display: none;" class="btn btn-sm btn-default">
                <i class="glyphicon glyphicon-log-out"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div id="fullCalendarDiv" class="panel-body">
      </div>
      <div id="panelFooterCalendar" class="panel-footer">
      </div>
    </div>
  </div>
  <div id="accordion" class="panel-group col-md-5">
    @include('cita.panel.show')
  </div>
</div>

<!-- FullCalendar CSS -->
<link href={{ url('/vendor/fullcalendar-3.8.2/fullcalendar.min.css') }} rel="stylesheet">

<!-- FullCalendar JS -->
<script src={{ url('/vendor/fullcalendar-3.8.2/lib/moment.min.js') }}></script>
<script src={{ url('/vendor/fullcalendar-3.8.2/fullcalendar.min.js') }}></script>
<script src={{ url('/vendor/fullcalendar-3.8.2/gcal.min.js') }}></script>
<script src={{ url('/vendor/fullcalendar-3.8.2/locale/es.js') }}></script>

<!-- The Modal -->
<div class="modal fade" id="mensajeModal">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="titleModal" style="color:#a81515"><i class="glyphicon glyphicon-warning-sign"></i> </h4>
      </div>

      <!-- Modal body -->
      <div class="modal-body alert alert-danger" id="bodyModal">
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>

    </div>
  </div>
</div>
@include('cita.modal.create')
@include('cita.modal.showdocumento')
@include('cita.modal.deleteconfirm')

<!-- GoogleCalendar JS -->

<script src={{ url("/js/GoogleCalendar.js") }}></script>
<script src="https://apis.google.com/js/api.js?onload=handleClientLoad"></script>


<script type="text/javascript">
    var _tableSolicitudes;
    function classPanelShowEvent(event){
      var now = moment();
      if(event.end.isBefore(now)){  //--- El evento ya ocurrio
        return "panel panel-default";
      }else if(event.start.isSameOrBefore(now) && event.end.isSameOrAfter(now)){  //--- El evento esta ocurriendo
        return "panel panel-info";
      }else{  //--- El evento aun no ocurre
        var diferencia = event.start.diff(now, "days");
        if(diferencia > 10){  //--- Diferencia >= 11 Dias
          return "panel panel-green";
        }else if(diferencia > 5){ //--- Diferencia 6-10 Dias
          return "panel panel-yellow";
        }else{  //--- Diferencia <= 5 Dias
          return "panel panel-red";
        }
      }
    }

    $( document ).ready(function() {
        //---[ Modal Message ]------------------------------------------
        var _mensaje = <?php echo json_encode( session('message') ); ?>;
        if(_mensaje !== null){
          var _mensaje = _mensaje.split(":");
          var _modalTitle = document.getElementById("titleModal");
          var _modalBody = document.getElementById("bodyModal");

          _modalTitle.appendChild( document.createTextNode(_mensaje[0]) );
          _modalBody.appendChild( document.createTextNode(_mensaje[1]) );
          
          $("#mensajeModal").modal("show");
        }

        //---[ Google FullCalendar ]-----------------------------------
        $('#fullCalendarDiv').fullCalendar({
          //weekends: false,     //--- Oculta sabados y domingos
          firstDay: 0,
          header:{
            left:   'prev,next,today',
            center: 'title',
            right:  'month,agendaWeek,agendaDay,list'
          },
          height: 550,
          handleWindowResize : true,
          contentHeight: 400,
          navLinks: true,
          editable: true,
          eventStartEditable: false,
          eventDurationEditable: false,
          selecHelper: true, //agregar eventos
          selectable: true,
          nowIndicator: true,
          googleCalendarApiKey : 'AIzaSyADnzxwMdxbgRQvQRu0f_a57o-G5y3SxlI',
          events:{
            googleCalendarId: "damoaxaca@gmail.com"
          },
          defaultView: 'month',
          visibleRange: function(currentDate) {
            return {
              start: currentDate.clone().subtract(15, 'days'),
              end: currentDate.clone().add(15, 'days') // exclusive end, so 3
            };
          },
          dayClick: function(fecha, jsEvent, event){
            //---[ Agregar ID Solicitud ]---------------------------------------
            var _idDocSolicitud = $('input[name=radioIdDocumento]:checked').val();
            
            var _linkdocumentoDiv = document.getElementById("divLinkDocumento");
            var _url = document.getElementById("url");
            
            if(_idDocSolicitud !== undefined){
              _objDocSolicitud = JSON.parse(_idDocSolicitud);
              $('#id_solicitud').val(_objDocSolicitud.id_solicitud);
              _url.value = "{{url('documentos/show')}}" + "/" + _objDocSolicitud.id_documento;
              _linkdocumentoDiv.removeAttribute("hidden");
            }else{
              $('#id_solicitud').val("");
              _linkdocumentoDiv.setAttribute("hidden", "");
              _url.value = "";
            }
            
            $('#date').val(fecha.format());
            $("#createEventModal").modal({backdrop: "static", keyboard: false}, "show");
            //$('#createEventModal').modal('show');
          },
          eventClick: function(event, jsEvent) {
            $('#panel_show_event').prop("class", classPanelShowEvent(event));
            var _descriptionArray = event.description.split("§");
            $('#date_show').val(event.start.format("YYYY-MM-DD"));
            $('#start_time_show').val(event.start.format("HH:mm"));
            $('#title_show').val(event.title);
            $('#description_show').val(_descriptionArray[0]);
            if(_descriptionArray[1].localeCompare("") !== 0){
              $('#divLinkDocumentoShow').prop( "hidden", null );
              $('#url_show').val(_descriptionArray[1]); 
            }else{
              $('#divLinkDocumentoShow').prop( "hidden", true );
              $('#url_show').val("");
            }
            $('#location_show').val( event.hasOwnProperty("location") ? event.location : "" );
            $('#id_event_google').val(event.id);
            
            $('#collapseSolicitudesDiv').collapse('hide');
            $('#collapseDetalleDiv').collapse('show');
            $('#panelDetalleTools').prop('hidden', null);

            deshabilitarUserInput(true);
            deshabilitarButtonsTools(false);

            if(event.url){
              if(jsEvent.ctrlKey){
                window.open(event.url); 
              }
              return false;
            }
          },
          eventAfterRender: function (event, element, view) {
            var now = moment();
            if(event.end.isBefore(now)){  //--- El evento ya ocurrio
              element.css("background-color", "#959595");
              element.css("border-color", "#959595");
            }else if(event.start.isSameOrBefore(now) && event.end.isSameOrAfter(now)){  //--- El evento esta ocurriendo
              element.css("background-color", "#00CBFF");
              element.css("border-color", "#00CBFF");
            }else{  //--- El evento aun no ocurre
              var diferencia = event.start.diff(now, "days");
              if(diferencia > 10){  //--- Diferencia >= 11 Dias
                element.css("background-color", "#00B200");
                element.css("border-color", "#00B200");
              }else if(diferencia > 5){ //--- Diferencia 6-10 Dias
                element.css("background-color", "#F0AD4E");
                element.css("border-color", "#F0AD4E");
              }else{  //--- Diferencia <= 5 Dias
                element.css("background-color", "#E1081F");
                element.css("border-color", "#E1081F");
              }
            }
          }
        });
    });
</script>

@endsection
