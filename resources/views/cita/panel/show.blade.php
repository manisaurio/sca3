<div id="panel_show_event" class="panel panel-default">
  <div class="panel-heading">
    <div class="row"> 
      <div class="col-lg-8 col-md-8">
        <h4 class="panel-title">
          <i class="fa fa-search fw"></i>
          <a id="aTitleShow" onclick="resetDetalleUserInput();" data-toggle="collapse" data-parent="#accordion" href="#collapseDetalleDiv">Detalle evento</a>
        </h4>
      </div> 
      <div id="panelDetalleTools" class="col-lg-4 col-md-4 text-right" hidden> 
        <div class="btn-group text-center">
          <a id="buttonEditShow" onclick="deshabilitarUserInput(false); deshabilitarButtonsTools(true);" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-pencil"></i></a> 
          <a id="buttonDeleteShow" onclick="confirmDeleteModalFunction();" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-trash"></i></a>
        </div> 
      </div> 
    </div>
  </div>
  <div id="collapseDetalleDiv" class="panel-collapse collapse">
    <div class="panel-body">
      <form method="POST" id="formCitaShow">
        <div class="form-row">
          <div class="form-group col-md-7">
            <label>Fecha inicio:</label>
            <input type="date" id="date_show" name="date_show" class="form-control" readonly>
          </div>
          <div class="form-group col-md-5">
            <label>Hora:</label>
            <input type="time" id="start_time_show" name="start_time_show" class="form-control" readonly>
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="">Título:</label>
            <input type="text" id="title_show" name="title_show" class="form-control" readonly>
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="">Descripción:</label>
            <textarea rows="3" id="description_show" name="description_show" class="form-control" readonly></textarea>
          </div>
        </div>

        <div class="form-row" id="divLinkDocumentoShow" hidden>
          <div class="form-group col-md-12">
            <label for=""><i class="glyphicon glyphicon-link"></i> Documento:</label>
            <input type="text" id="url_show" name="url_show" class="form-control" readonly>
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="">Lugar:</label>
            <input type="text" id="location_show" name="location_show" class="form-control" readonly>
          </div>
        </div>

        <div class="form-row" hidden>
          <div class="form-group col-md-12">
            <input type="text" id="id_event_google" name="id_event_google" class="form-control" readonly>
          </div>
        </div>

        <div id="divUpdateShow" class="form-row" hidden>
          <div class="form-group text-center col-md-12"> 
            <button id="submitButtonUpdate" type="submit" class="btn btn-primary"> 
              Actualizar evento 
            </button> 
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
    function resetDetalleUserInput(){
        $('#date_show').val("");
        $('#start_time_show').val("");
        $('#title_show').val("");
        $('#description_show').val("");
        $('#url_show').val("");
        $('#location_show').val("");
        $('#id_event_google').val("");

        $('#panel_show_event').prop("class", "panel panel-default");
        $('#panelDetalleTools').prop('hidden', true);
        $('#divLinkDocumentoShow').prop('hidden', true);
        deshabilitarUserInput(true);
    }

    function deshabilitarButtonsTools(_value){
      $("#buttonDeleteShow").attr("disabled", _value);
      $("#buttonEditShow").attr("disabled", _value);
    }

    function deshabilitarUserInput(_value){
      $("#date_show").attr("readonly", _value);
      $("#start_time_show").attr("readonly", _value);
      $("#title_show").attr("readonly", _value);
      $("#description_show").attr("readonly", _value);
      $("#location_show").attr("readonly", _value);
      $('#divUpdateShow').prop('hidden', _value);
    }

    function getDetalleUserInput(){
      // Debe ser llamado cuando este verificado que los campos requeridos no esten vacios
      var _userInputData = {};
      _userInputData.date = document.querySelector("#date_show").value;
      _userInputData.startTime = document.querySelector("#start_time_show").value;
      _userInputData.endTime = moment(_userInputData.startTime, 'HH:mm').add(1, 'hours').format('HH:mm');
      _userInputData.eventTitle = document.querySelector("#title_show").value;
      _userInputData.eventDescription = document.querySelector("#description_show").value;
      _userInputData.eventLocation = document.querySelector("#location_show").value;
      _userInputData.eventUrl = document.querySelector("#url_show").value;

      return _userInputData;
    }

    function confirmDeleteModalFunction(){
        $('#confirmDeleteModal').modal({backdrop: 'static', keyboard: false});
        $("#confirmDeleteModal").modal("show");
        $("#confirmAcceptButton").click(deleteEvent);
    }

    function deleteEvent(){
        $("#confirmDeleteModal").modal("hide");

        // Crear la solicitud
        var request = gapi.client.calendar.events.delete({
            "calendarId": "primary",
            "eventId": $("#id_event_google").val()
        });

        // Inhabilitar
        function confirmationMessage(e){
            var confirmationMessage = "\o/";

            e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
            return confirmationMessage;              // Gecko, WebKit, Chrome <34
        }
        window.addEventListener("beforeunload", confirmationMessage);
        document.getElementById("aTitleShow").removeAttribute("onclick");
        document.getElementById("aTitleShow").removeAttribute("href");
        $("#buttonDeleteShow").attr("disabled", true);
        $("#buttonEditShow").attr("disabled", true);

        // Ejecutar la solicitud y hacer algo con respuesta
        request.execute(function(respuesta) {
            if(respuesta){
                if(respuesta.hasOwnProperty("error")){
                    var _bodyError = document.getElementById("errorPanelBodyDiv");
                    removeChildren("errorPanelBodyDiv");
                    var _mensaje = document.createTextNode("Error: al eliminar evento.");
                    _bodyError.appendChild(_mensaje);
                    $('#errorPanelDiv').collapse("show");     // Mostrar panel de error
                }else{
                    $.ajax({
                      headers: { 
                        "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
                      }, 
                      type: "post",
                      data: {_method: "delete", eventIdGoogle: $("#id_event_google").val()},
                      url: "{{url('/citas/destroy')}}",
                      success: function(_data){
                        $("#collapseDetalleDiv").collapse("hide");
                        $("#panelDetalleTools").prop("hidden", true);
                        _tableSolicitudes.ajax.reload();  // Recargar DataTables Solicitudes
                        $("#divSuccessMessage").load(window.location.href + " #divSuccessMessage" );  // Recargar seccion divSusccessMessage
                        $("#fullCalendarDiv").fullCalendar("refetchEvents");  // Recargar FullCalendar
                      },
                      error: function(_data){
                        var _bodyError = document.getElementById("errorPanelBodyDiv");
                        removeChildren("errorPanelBodyDiv");
                        var _mensaje = document.createTextNode(_data.responseJSON);
                        _bodyError.appendChild(_mensaje);
                        $("#errorPanelDiv").collapse("show");     // Mostrar panel de error
                      }
                    });
                }
            }else{
                var _bodyError = document.getElementById("errorPanelBodyDiv");
                removeChildren("errorPanelBodyDiv");
                var _mensaje = document.createTextNode("Error: al eliminar evento.");
                _bodyError.appendChild(_mensaje);
                $('#errorPanelDiv').collapse("show");     // Mostrar panel de error
            }
            // Habilitar cierre con botones
            $("#buttonDeleteShow").attr("disabled", false);
            $("#buttonEditShow").attr("disabled", false);
            $('#panel_show_event').prop("class", "panel panel-default");
            document.getElementById("aTitleShow").onclick = resetDetalleUserInput;
            document.getElementById("aTitleShow").href = "#collapseDetalleDiv";
            window.removeEventListener("beforeunload", confirmationMessage);
        });
    }
</script>

<!-- BootstrapValidator Script -->
<script type="text/javascript">
  $(document).ready(function() {
    $("#formCitaShow")
    .bootstrapValidator({
      message: "No valido",
      fields: {
        date_show: {
          validators: {
            notEmpty: {
              message: "Requerido"
            }
          }
        },
        start_time_show: {
          validators: {
            notEmpty: {
              message: "Requerido"
            }
          }
        },
        title_show: {
          validators: {
            notEmpty: {
              message: "Requerido"
            }
          }
        },
        description_show: {
          validators: {
            notEmpty: {
              message: "Requerido"
            },
            stringLength: {
              min: 3,
              max: 250,
              message: "Longitud 3-250"
            }
          }
        }
      }
    })
    .on("success.form.bv", function(e) {
      // Prevent form submission
      e.preventDefault();

      // Obtener datos de formulario
      var _eventData = getDetalleUserInput();
      
      // Crear recurso
      var resource = {
        "summary": _eventData.eventTitle,
        "description": _eventData.eventDescription + " §" + _eventData.eventUrl,
        "location": _eventData.eventLocation,
        "colorId": 9,
        "start": {
          "dateTime": new Date(_eventData.date + " " + _eventData.startTime).toISOString()
        },
        "end": {
          "dateTime": new Date(_eventData.date + " " + _eventData.endTime).toISOString()
        }
      };

      // Crear la solicitud
      var request = gapi.client.calendar.events.patch({
        "calendarId": "primary",
        "eventId": $("#id_event_google").val(),
        "resource": resource,
        "sendNotifications": true
      });

      // Inhabilitar
      function confirmationMessage(e){
        var confirmationMessage = "\o/";
        e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
        return confirmationMessage;              // Gecko, WebKit, Chrome <34
      }
      window.addEventListener("beforeunload", confirmationMessage);
      document.getElementById("aTitleShow").removeAttribute("onclick");
      document.getElementById("aTitleShow").removeAttribute("href");
      deshabilitarUserInput(true);
      $("#submitButtonUpdate").attr("disabled", true);
      $('#divUpdateShow').prop('hidden', false);

      // Ejecutar la solicitud y hacer algo con respuesta
      request.execute(function(respuesta) {
        if( respuesta.hasOwnProperty("status") ){
          if(respuesta.status.localeCompare("confirmed") === 0){
            $.ajax({
              headers: { 
                "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
              }, 
              type: "post",
              data: {_method: "put"},
              url: "{{url('/citas/update')}}",
              success: function(_data){
                $("#divSuccessMessage").load(window.location.href + " #divSuccessMessage" );  // Recargar seccion divSusccessMessage
                $("#fullCalendarDiv").fullCalendar("refetchEvents");  // Recargar FullCalendar
              },
              error: function(_data){
                var _bodyError = document.getElementById("errorPanelBodyDiv");
                removeChildren("errorPanelBodyDiv");
                var _mensaje = document.createTextNode(_data.responseJSON);
                _bodyError.appendChild(_mensaje);
                $("#errorPanelDiv").collapse("show");     // Mostrar panel de error
              }
            });
          }else{
            var _bodyError = document.getElementById("errorPanelBodyDiv");
            removeChildren("errorPanelBodyDiv");
            var _mensaje = document.createTextNode("Error: el evento no fue actualizado.");
            _bodyError.appendChild(_mensaje);
            $("#errorPanelDiv").collapse("show");     // Mostrar panel de error
          }
        }else{
          var _bodyError = document.getElementById("errorPanelBodyDiv");
          removeChildren("errorPanelBodyDiv");
          var _mensaje = document.createTextNode("Error: al actualizar evento.");
          _bodyError.appendChild(_mensaje);
          $('#errorPanelDiv').collapse("show");     // Mostrar panel de error
        }
        // Habilitar cierre con botones
        document.getElementById("aTitleShow").onclick = resetDetalleUserInput;
        document.getElementById("aTitleShow").href = "#collapseDetalleDiv";
        deshabilitarButtonsTools(false);
        $('#divUpdateShow').prop('hidden', true);
        $('#panel_show_event').prop("class", "panel panel-default");
        
        window.removeEventListener("beforeunload",confirmationMessage);
      });

    });
  });
</script>