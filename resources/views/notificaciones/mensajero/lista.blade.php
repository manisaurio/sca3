@extends('layouts.master-mensajero') 
 
@section('content')

  <div class="row">
    <nav aria-label="breadcrumb" style="margin-top: 10px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item" aria-current="page">Notificaciones</li>
      </ol>
    </nav>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header" style="margin-top: 3px; margin-bottom: 15px;">Notificaciones</h3>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      
      <table width="100%" class="table table-striped table-bordered table-hover" id="documentosTable">
        <thead>
          <tr>
            <th>DESCRIPCION</th>
            <th>FECHA</th>
          </tr>
        </thead>
      </table>
      <!-- /.table-responsive -->
        
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->

  <script type="text/javascript">
    $(document).ready(function(){
      _tableDocumentos = $('#documentosTable').DataTable( { 
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": <?php echo json_encode( url('/api/nofiticaciones/dataTableList') ); ?>,
        "columns": [
          {data: "action", name: "data.action", orderable: false, searchable: false},
          {data: "fecha", name: "fecha"}
        ],
        "language": {
          "url":  <?php echo json_encode( url('/vendor/datatables-plugins/Spanish.json') ); ?>
        },
        "lengthChange": false,
        "pageLength": 10,
        "searching": false
      } );
    });
  </script>

@stop