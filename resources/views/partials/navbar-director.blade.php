<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    @include('partials.header')

    @if( Auth::check() )
        <ul class="nav navbar-top-links navbar-right">
            
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="notificacion">
                    <i class="fa fa-bell fa-fw"></i>
                    @if(count(auth()->user()->unreadNotifications) > 0)
                    <span class="badge" id="numero_notificaciones">{{count(auth()->user()->unreadNotifications)}}</span>
                    @endif
                    <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-alerts">
                    <li>
                        @if(count(auth()->user()->unreadNotifications) > 0)
                            @foreach(auth()->user()->unreadNotifications as $notification)
                                <a href="{{url('/documentos/show/'.$notification->data['id_documento'])}}">
                                    <div>
                                        <i class="fa fa-file-text-o fa-fw"></i> Folio {{$notification->data['id_documento']}} con respuesta
                                        <span class="pull-right text-muted small">{{$notification->created_at->format('d/m/Y h:i A')}}</span>
                                    </div>
                                </a>
                            @endforeach
                        @else
                            <a>
                                <div>
                                    <i class="fa fa-info-circle fa-fw"></i> No hay notificaciones nuevas
                                </div>
                            </a>
                        @endif
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="{{url('/nofiticaciones/lista')}}">
                            <strong>Ver todas</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-alerts -->
            </li>
            
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="{{ url('/empleados/show/' . Auth::user()->id_empleado) }}"><i class="fa fa-user fa-fw"></i> Perfil de Usuario</a>
                    </li>
                    <li><a href="{{ url('/empleados/edit/' . Auth::user()->id_empleado) }}"><i class="fa fa-gear fa-fw"></i> Editar Perfil</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out fa-fw"></i> Salir
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        @include('partials.sidebar-director')
        
    @endif

</nav>

<script type="text/javascript">
    document.getElementById("notificacion").onclick = function(){
        $.ajax({ 
            headers: { 
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
            }, 
            type: 'POST', 
            url: <?php echo json_encode( url('/nofiticaciones/marcarcomoleidas') ); ?>,
            success: function(){
                document.getElementById
                 var elemento = document.getElementById("numero_notificaciones");
                 elemento.parentNode.removeChild(elemento);
            }
        });
    };
</script>