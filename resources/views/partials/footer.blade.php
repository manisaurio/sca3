<!-- footer DOS -->
<div class="container-fluid" id="footer-dos" style="background-color:#DBDBDB;">
    <div class="container">
        <div class="row clearfix">
            <div class="col-md-4 column">
                <div class="textwidget">
                    <center>
                        <p class="direccion-footer" style="margin-top:20px;">
                            Servicios de Salud del Estado de Oaxaca<br />
                            J.P. García 103 Centro, Oax. CP. 68000<br />
                            Tels (951) 516 3464 y (951) 514 2876
                        </p>
                    </center>
                </div>
            </div>
            <div class="col-md-4 column site-name">
                <div class="textwidget">
                    <center>
                        <p style="font-weight:bold; margin-top:20px;">www.oaxaca.gob.mx</p>
                        GOBIERNO DEL ESTADO DE OAXACA 2016-2022
                    </center>
                </div>
            </div>
            <div class="col-md-4 column">
                <p class="footer-logo">
                     <img src="{{url('/images/footer-logo.png')}}" style="margin-top:20px;" alt="http://www.salud.oaxaca.gob.mx/wp-content/themes/DepGobOax/css/images/footer-logo.png">
                </p>
            </div>
        </div>
    </div>
</div>
<!-- Termina footer DOS -->