<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <img src="{{url('/images/logo2017.png')}}" class="navbar-brand" style="width:270px;height:75px;margin-top:-13px;margin-bottom:-7px;" alt="https://manuales.salud-oaxaca.gob.mx/img/logo2017.png">
    <a class="navbar-brand" href="{{url('/')}}">Sistema de Control de Actividades Administrativas</a>
</div>
<!-- /.navbar-header -->