<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <!--<form role="form">-->
                    <div class="row justify-content-md-center">
                        <div class="col col-lg-4 col-xs-4">
                            @if(App::environment('local'))
                                @if(Storage::disk('local')->exists(str_replace("storage", "public", Auth::user()->ruta_avatar)))
                                    <img alt="User Pic" src="{{ url( Auth::user()->ruta_avatar ) }}" class="img-circle img-responsive" style="height: 55px; width: 55px; border: 3px solid #CCCCCC;">
                                @else
                                    <img alt="User Pic" src={{ url('/images/default-avatar.png') }} class="img-circle img-responsive" style="height: 55px; width: 55px; border: 3px solid #CCCCCC;"> 
                                @endif
                            @else
                                @if(Storage::disk('s3')->exists(Auth::user()->ruta_avatar))
                                    <img alt="User Pic" src="{{Storage::url(Auth::user()->ruta_avatar)}}" class="img-circle img-responsive" style="height: 55px; width: 55px; border: 3px solid #CCCCCC;">
                                @else
                                    <img alt="User Pic" src={{ url('/images/default-avatar.png') }} class="img-circle img-responsive" style="height: 55px; width: 55px; border: 3px solid #CCCCCC;"> 
                                @endif
                            @endif
                        </div>
                        <div class="col col-lg-8 col-xs-8">
                            <p class="text-info">Email:</p>
                            <input type="text" value="{{ Auth::user()->email }}" class="form-control" style="border: none; padding-left: 3px; padding-right: 0px; border-radius: 0px; background-color: transparent;" readonly>
                        </div>
                    </div>    
                <!--</form>-->
            </li>
            
            <li>
                <a href="{{url('/')}}"><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
            </li>

            <li>
                <a href="#"><i class="fa fa-users fa-fw"></i> Usuarios<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ url('/empleados/create') }}"><i class="fa fa-plus-circle fa-fw"></i> Agregar</a>
                    </li>
                    <li>
                        <a href="{{ url('/empleados/list') }}"><i class="fa fa-sort-amount-asc fa-fw"></i> Listar</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>

            <li>
                <a href="{{ url('/citas/calendar') }}"><i class="fa fa-coffee fa-fw"></i> Citas</a>
            </li>

            <li>
                <a href="#"><i class="fa fa-files-o fa-fw"></i> Documentos<span class="fa arrow"></span></a>
                
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#"><i class="fa fa-refresh fa-fw"></i> Turnados <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="{{ url('/guiasturnados/listaturnadoentrada') }}"><i class="fa fa-arrow-circle-left fa-fw"></i> Bandeja de entrada</a>
                            </li>

                            <li>
                                <a href="{{ url('/reportesrevisiones/lista') }}"><i class="glyphicon glyphicon-transfer "></i> Revisados</a>
                            </li>
                        </ul>
                        <!-- /.nav-third-level -->
                    </li>

                    <li>
                        <a href="{{ url('/respuestas/lista') }}"><i class="fa fa-file-text fa-fw"></i> Con respuesta</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->