@extends('layouts.master-jefe') 
 
@section('content') 

  <div class="row">
    <nav aria-label="breadcrumb" style="margin-top: 10px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
        @if( $esturnador == true )
          <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/guiasturnados/listaturnadosalida')}}">Documentos Turnados - Salida</a></li>
        @else
          <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/guiasturnados/listaturnadoentrada')}}">Documentos Turnados - Entrada</a></li>
        @endif
        <li class="breadcrumb-item" aria-current="page">Detalle</li>
      </ol>
    </nav>
  </div>

  <div class="row">
    <div class="col-lg-12">
      @if ( session('message') ) 
        <div class="alert alert-success alert-dismissable"> 
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a> 
          <strong>EXITO: </strong> {{ session()->pull('message', 'default') }} 
        </div> 
      @endif 
    </div>
  </div>
  <!-- /.row -->

  <div class="row" id="errorRowDiv">
    <div class="col-lg-12">
      <div class="panel panel-danger panel-collapse collapse" id="errorPanelDiv">
        <div class="panel-heading">
          <strong>ERROR:</strong>
          <a data-toggle="collapse" href="#errorPanelDiv" class="close">&times</a>
        </div>
        <div class="panel-body" id="errorPanelBodyDiv">
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->
 
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default"> 

        <div class="panel-heading lead"> 
          Detalle Turnado
        </div> 
   
        <div class="panel-body">
          <div class="panel-group" id="accordion">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <i class="glyphicon glyphicon-file"></i>
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseDocumento">Documento</a>
                </h4>
              </div>
              <div id="collapseDocumento" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="row"> 
                    <div class="col-lg-12"> 
                      <div class="row"> 

                        @include('documento.partials.archivovisualizacion') 
           
                        <div class="col-lg-8 col-md-8"> 
                          <ul class="nav nav-tabs"> 
                            <li class="active"><a data-toggle="tab" href="#Summery" class="text-info"><i class="fa fa-indent"></i> Resumen</a></li>
                          </ul> 
           
                          <div class="tab-content"> 
                            <div id="Summery" class="tab-pane fade in active"> 
                              <div class="table-responsive panel"> 
                                 
                                <table class="table"> 
                                  <tbody> 
                                     
                                    <tr> 
                                      <td class="text-info"><i class="glyphicon glyphicon-list-alt"></i> No. Oficio</td> 
                                      <td>{{ strtoupper($documento->num_oficio) }}</td> 
                                    </tr> 
           
                                    <tr> 
                                      <td class="text-info"><i class="glyphicon glyphicon-file"></i> Tipo de documento</td> 
                                      <td>{{ strtoupper($documento->tipo) }}</td> 
                                    </tr> 
           
                                    <tr> 
                                      <td class="text-info"><i class="glyphicon glyphicon-inbox"></i> No. Hojas</td> 
                                      <td>{{ $documento->num_hojas }}</td> 
                                    </tr> 

                                    <tr> 
                                      <td class="text-info"><i class="glyphicon glyphicon-paperclip"></i> No. Anexos</td> 
                                      <td>{{ $documento->num_anexos }}</td> 
                                    </tr> 

                                    <tr> 
                                      <td class="text-info"><i class="glyphicon glyphicon-th"></i> Actividad</td> 
                                      <td id="tdDocumentoActividad">{{ $documento->actividad }}</td> 
                                    </tr>
           
                                    <tr> 
                                      <td class="text-info" colspan="2"><i class="glyphicon glyphicon-info-sign"></i> Asunto</td> 
                                    </tr> 
                                  </tbody> 
                                </table> 
           
                                <blockquote class="blockquote" style="font-size:15px;"> 
                                  <p>{{ $documento->asunto }}</p> 
                                </blockquote> 
                                 
                              </div> 
                            </div> 
                          </div> 
                        </div> 
                      </div> 
                    </div> 
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <i class="glyphicon glyphicon-info-sign"></i>
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseDetalleTurnado">Detalles Turnado</a>
              </h4>
            </div>
            <div id="collapseDetalleTurnado" class="panel-collapse collapse in">
              <div class="panel-body">

                <div class="row">
                  <div class="col-lg-4">
                      <div class="form-group">
                        <label>Fecha de turnado</label>
                        <p class="form-control-static">{{ $turnados->first()->registrado }}</p>
                      </div>
                  </div>

                  <div class="col-lg-4">
                      <div class="form-group">
                        <label>No. Orden</label>
                        <p class="form-control-static">{{ $turnados->first()->num_orden }}</p>
                      </div>
                  </div>

                  <div class="col-lg-4">
                      <div class="form-group">
                        <label>Documento - No. Folio</label>
                        <p class="form-control-static">{{ $documento->id }}</p>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-4">
                      <div class="form-group">
                        <label>Turnado por <span class="badge badge-secondary">{{ $turnador->puesto }}</span></label>
                        <p class="form-control-static">{{ $turnador->nombre }}</p>
                      </div>
                  </div>

                  <div class="col-lg-4">
                      <div class="form-group">
                        <label>Departamento</label>
                        <p class="form-control-static">{{ $turnador->departamento }}</p>
                      </div>
                  </div>

                  <div class="col-lg-4">
                      <div class="form-group">
                        <label>Área</label>
                        <p class="form-control-static">{{ $turnador->area }}</p>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                      <label>Observaciones</label>
                      <blockquote class="blockquote" style="font-size:15px;">
                        @if( isset($revision) )
                          <p>{{ $revision->observacion }}</p>
                        @else
                          <p class="text-muted" style="font-family: Courier, serif; font-size:12pt;">
                            --- Turnado sin observaciones ---
                          </p>
                        @endif 
                      </blockquote> 
                    </div>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-lg-12"> 
                    <div class="p-3 mb-2 text-white" style="background-color:#DDDDDD;">
                      <label>&emsp;Lista de usuarios turnados</label>
                    </div>
                    <table width="100%" class="table table-striped table-bordered table-hover" id="userempTurnadosTable">
                      <thead>
                        <tr>
                          <th>Instruccion</th>
                          <th>Estado</th>
                          <th>Nombre</th>
                          <th>Puesto</th>
                          <th>Departamento</th>
                          <th>Area</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach( $turnados->all() as $turnadoItem )
                          <tr>
                            <td>{{ $turnadoItem->instruccion }}</td>
                            <td>{{ $turnadoItem->estado }}</td>
                            <td>{{ $turnadoItem->nombre }}</td>
                            <td>{{ $turnadoItem->puesto }}</td>
                            <td>{{ $turnadoItem->departamento }}</td>
                            <td>{{ $turnadoItem->area }}</td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>  
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div> 
    </div>
    <!-- /.col-lg-12 -->
  </div>

  <script>
    $( document ).ready(function() {
      //---[ Datatables ]--------------------------------------------
      _tableTurnados = $('#userempTurnadosTable').DataTable({ 
        "responsive": true,
        "language": {
          "url":  <?php echo json_encode( url('/vendor/datatables-plugins/Spanish.json') ); ?>
        },
        "lengthChange": false,
        "pageLength": 3,
        "bInfo" : false,
        "searching": false,
        "dom": '<"top"i>rt<"bottom"flp><"clear">'
      });
    });
  </script>

@stop