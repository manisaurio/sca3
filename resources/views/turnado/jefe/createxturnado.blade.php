@extends('layouts.master-jefe') 
 
@section('content') 

  <div class="row">
    <nav aria-label="breadcrumb" style="margin-top: 10px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/reportesrevisiones/lista')}}">Documentos Turnados - Revisados</a></li>
        <li class="breadcrumb-item" aria-current="page">Turnar</li>
      </ol>
    </nav>
  </div>

  <div class="row">
    <div class="col-lg-12">
      @if ( session('message') ) 
        <div class="alert alert-success alert-dismissable"> 
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a> 
          <strong>EXITO: </strong> {{ session()->pull('message', 'default') }} 
        </div> 
      @endif 
    </div>
  </div>
  <!-- /.row -->

  <div class="row" id="errorRowDiv">
    <div class="col-lg-12">
      <div class="panel panel-danger panel-collapse collapse" id="errorPanelDiv">
        <div class="panel-heading">
          <strong>ERROR:</strong>
          <a data-toggle="collapse" href="#errorPanelDiv" class="close">&times</a>
        </div>
        <div class="panel-body" id="errorPanelBodyDiv">
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->
 
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default"> 
        <div class="panel-heading lead"> 
          <i class="glyphicon glyphicon-file"></i> Turnar documento
        </div> 
   
        <div class="panel-body"> 
          <div class="row"> 
            <div class="col-lg-12 col-md-12"> 
              <div class="row"> 
                
                @include('documento.partials.archivovisualizacion') 
           
                <div class="col-lg-8 col-md-8"> 
                  <ul class="nav nav-tabs"> 
                    <li class="active"><a data-toggle="tab" href="#Summery" class="text-info"><i class="fa fa-indent"></i> Resumen</a></li>
                  </ul> 
   
                  <div class="tab-content"> 
                    <div id="Summery" class="tab-pane fade in active"> 
                      <div class="table-responsive panel"> 
                         
                        <table class="table"> 
                          <tbody> 
                             
                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-list-alt"></i> No. Oficio</td> 
                              <td>{{ strtoupper($documento->num_oficio) }}</td> 
                            </tr> 
   
                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-file"></i> Tipo de documento</td> 
                              <td>{{ strtoupper($documento->tipo) }}</td> 
                            </tr> 
   
                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-inbox"></i> No. Hojas</td> 
                              <td>{{ $documento->num_hojas }}</td> 
                            </tr> 

                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-paperclip"></i> No. Anexos</td> 
                              <td>{{ $documento->num_anexos }}</td> 
                            </tr> 

                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-th"></i> Actividad</td> 
                              <td id="tdDocumentoActividad">{{ $documento->actividad }}</td> 
                            </tr>
   
                            <tr> 
                              <td class="text-info" colspan="2"><i class="glyphicon glyphicon-info-sign"></i> Asunto</td> 
                            </tr> 
                          </tbody> 
                        </table> 
   
                        <blockquote class="blockquote" style="font-size:15px;"> 
                          <p>{{ $documento->asunto }}</p> 
                        </blockquote> 
                         
                      </div> 
                    </div> 
                  </div> 
                </div> 
              </div> 
            </div> 
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                Turnado
              </h4>
            </div>
            <div class="panel-body">
              <form method="POST" id="turnadoForm">
              
                {{ csrf_field() }}

                <div class="form-row">
                  <div class="form-group col-md-12">
                    <label>Revision</label>
                    <textarea id="observacion" name="observacion" class="form-control" rows="2" placeholder="Ingrese sus observaciones..." readonly>{{ $revision->observacion }}</textarea>
                  </div>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-12">
                    <table id="example" class="table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Area</th>
                          <th>Departamento</th>
                          <th>Usuario</th>
                          <th>Puesto</th>
                          @if($respuestasinatender == true)
                            <th>Responer</th>
                          @endif
                          <th>Prioridad</th>
                        </tr>
                      </thead>
                      <tbody>
                        @forelse ($empleados as $empleado)
                          <tr>

                            <td>
                              @if( old('empAreaCheckbox[]') !== null ) 
                                <input type="checkbox" id="check{{ $empleado->id_empleado }}" name="empAreaCheckbox[]" id="empAreaCheckbox[]" onchange="_habilitar(this, 'respuesta{{$empleado->id_empleado}}', 'prioridad{{$empleado->id_empleado}}')" value="{{ $empleado->id_empleado }}" class="form-check-input" checked>
                                <label class="form-check-label" for="id{{ $empleado->area }}">{{ $empleado->area }}</label>
                              @else
                                <input type="checkbox" id="check{{ $empleado->id_empleado }}" name="empAreaCheckbox[]" id="empAreaCheckbox[]" onchange="_habilitar(this, 'respuesta{{$empleado->id_empleado}}', 'prioridad{{$empleado->id_empleado}}')" value="{{ $empleado->id_empleado }}" class="form-check-input">
                                <label class="form-check-label" for="id{{ $empleado->area }}">{{ $empleado->area }}</label>
                              @endif
                            </td>

                            <td>{{ $empleado->departamento }}</td>
                            
                            <td>{{ $empleado->nombre }}</td>

                            <td>{{ $empleado->puesto }}</td>

                            @if($respuestasinatender == true)
                              <td>
                                <div class="form-check">
                                  <input type="radio" name="empAreaRespuestaRadio[]" id="respuesta{{$empleado->id_empleado}}" value="{{ $empleado->id_empleado }}" class="form-check-input position-static" onclick="(function(_element){ if(event.ctrlKey){_element.checked = false;} })(this);" disabled>
                                </div>
                              </td>
                            @endif

                            <td>
                              <select name="prioridadSelect[]" id="prioridad{{$empleado->id_empleado}}" class="form-control" disabled>
                                <option value="ALTA" selected>Alta</option>
                                <option value="MEDIA">Media</option>
                                <option value="BAJA">Baja</option>
                              </select>
                            </td>

                          </tr>
                        @empty
                          <tr class="odd">
                            <td class="dataTables_emty" colspan="11" valing="top">
                              <p class="text-danger">
                                <i class="glyphicon glyphicon-exclamation-sign"></i> No hay usuarios disponibles.
                              </p>
                            </td>
                          </tr>
                        @endforelse
                      </tbody>
                    </table>
                    <small class="form-text text-danger" id="tableSmall" hidden></small>
                  </div>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-12 text-center">
                    <button type="button" onclick="_validar();" class="btn btn-primary">
                      <i class="glyphicon glyphicon-retweet"></i>
                      TURNAR
                    </button>
                  </div>
                </div>
                
              </form>
            </div>
          </div>
        </div>
      </div> 
    </div>
    <!-- /.col-lg-12 -->
  </div>

  <!-- The Modal -->
  <div class="modal fade" id="confirmModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title" id="confirmModalTitle">Confirmar turnado</h4>
        </div>

        <!-- Modal body -->
        <div class="modal-body alert alert-info" id="confirmModalBody">
          Esta seguro que desea turnar este documento, una vez turnado no podrá editarlo.
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="acceptButton">Aceptar</button>
          <button type="button" class="btn btn-secondary" id="cancelButton">Cacelar</button>
        </div>

      </div>
    </div>
  </div>

  @if($respuestasinatender == true)
    <script type="text/javascript">
      function _validar(){
        var _itemsCheck = [];
        $("input[name='empAreaCheckbox[]']:checked").each(
          function(){
            _itemsCheck.push($(this).val());
          });

        var _itemsRadio = [];
        $("input[name='empAreaRespuestaRadio[]']:checked").each(
          function(){
            _itemsRadio.push($(this).val());
          });

        var mensaje = "";
        if( _itemsCheck.length === 0){
          mensaje = "Seleccione por lo menos un destinatario.";
          if(_itemsRadio.length === 0){
            mensaje = "Seleccione por lo menos un destinatario responsable de respuesta";
          }
        }else{
          if(_itemsRadio.length === 0){
            mensaje = "Seleccione responsable de respuesta";
          }
        }

        var _tableSmall = document.getElementById("tableSmall");
        _tableSmall.textContent = mensaje;
        _tableSmall.removeAttribute("hidden");
        window.setTimeout(function() {
          _tableSmall.textContent = "";
          _tableSmall.setAttribute("hidden","");
        }, 2000);

        if(_itemsCheck.length !== 0 && _itemsRadio.length !== 0){
          _modalConfirmFunction();
        }
      }
    </script>
  @else
    <script type="text/javascript">
      function _validar(){
        var _itemsCheck = [];
        $("input[name='empAreaCheckbox[]']:checked").each(
          function(){
            _itemsCheck.push($(this).val());
          });

        if( _itemsCheck.length === 0){
          var _tableSmall = document.getElementById("tableSmall");
          _tableSmall.textContent = "Seleccione por lo menos un destinatario.";
          _tableSmall.removeAttribute("hidden");
          window.setTimeout(function() {
            _tableSmall.textContent = "";
            _tableSmall.setAttribute("hidden","");
          }, 2000);
        }

        if(_itemsCheck.length !== 0){
          _modalConfirmFunction();
        }
      }
    </script>
  @endif

  <script>
    $(document).ready( function() {
      $('#example').DataTable({
        "responsive": true,
        "language": {
          "url":  <?php echo json_encode( url('/vendor/datatables-plugins/Spanish.json') ); ?>
        },
        "lengthChange": false,
        "pageLength": 5,
        "bInfo" : false,
        "searching": false,
        "dom": '<"top"i>rt<"bottom"flp><"clear">'
      });
    });

    var _habilitar = function( _element, _idRespuesta, _idPrioridad ){
      if(_element.checked){
        var _radioRespuesta = document.getElementById(_idRespuesta);
        if(_radioRespuesta !== null){
          _radioRespuesta.removeAttribute("disabled");
        }
        document.getElementById(_idPrioridad).removeAttribute("disabled");
      }else{
        var _radioRespuesta = document.getElementById(_idRespuesta);
        if(_radioRespuesta !== null){
          _radioRespuesta.setAttribute("disabled", "");
          if(_radioRespuesta.checked){
            _radioRespuesta.checked = false;
          }
        }
        document.getElementById(_idPrioridad).setAttribute("disabled", "");
      }
    }

    var _modalConfirmFunction = function( _url ){
      //$('#confirmModal').modal({backdrop: 'static', keyboard: false});
      $("#confirmModal").modal("show");
      var _buttonAcept = document.getElementById("acceptButton");
      var _buttonCancel = document.getElementById("cancelButton");
      var _buttonRedirect = document.getElementById("redirectButton");
      _buttonAcept.onclick = function(){
        document.getElementById("turnadoForm").submit(); 
      };

      _buttonCancel.onclick = function(){
        $('#confirmModal').modal('hide');
      }
    }
  </script>

@stop