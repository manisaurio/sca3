@extends('layouts.master-mensajero') 
 
@section('content') 

  <div class="row">
    <nav aria-label="breadcrumb" style="margin-top: 10px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/documentos/listaingreso')}}">Documentos Ingresados</a></li>
        <li class="breadcrumb-item" aria-current="page">Turnar</li>
      </ol>
    </nav>
  </div>

  <div class="row">
    <div class="col-lg-12">
      @if ( session('message') ) 
        <div class="alert alert-success alert-dismissable"> 
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a> 
          <strong>EXITO: </strong> {{ session()->pull('message', 'default') }} 
        </div> 
      @endif 
    </div>
  </div>
  <!-- /.row -->

  <div class="row" id="errorRowDiv">
    <div class="col-lg-12">
      <div class="panel panel-danger panel-collapse collapse" id="errorPanelDiv">
        <div class="panel-heading">
          <strong>ERROR:</strong>
          <a data-toggle="collapse" href="#errorPanelDiv" class="close">&times</a>
        </div>
        <div class="panel-body" id="errorPanelBodyDiv">
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->
 
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default"> 
        <div class="panel-heading lead"> 
          <i class="glyphicon glyphicon-share-alt"></i> Turnar documento
        </div> 
   
        <div class="panel-body"> 
          <div class="row"> 
            <div class="col-lg-12 col-md-12"> 
              <div class="row">

                @include('documento.partials.archivovisualizacion')
           
                <div class="col-lg-8 col-md-8">
                  <ul class="nav nav-tabs"> 
                    <li class="active"><a data-toggle="tab" href="#Summery" class="text-info"><i class="fa fa-indent"></i> Resumen</a></li>
                  </ul> 
   
                  <div class="tab-content"> 
                    <div id="Summery" class="tab-pane fade in active"> 
                      <div class="table-responsive panel"> 
                         
                        <table class="table"> 
                          <tbody> 
                             
                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-list-alt"></i> No. Oficio</td> 
                              <td>{{ strtoupper($documento->num_oficio) }}</td> 
                            </tr> 
   
                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-file"></i> Tipo de documento</td> 
                              <td>{{ strtoupper($documento->tipo) }}</td> 
                            </tr> 
   
                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-inbox"></i> No. Hojas</td> 
                              <td>{{ $documento->num_hojas }}</td> 
                            </tr> 

                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-paperclip"></i> No. Anexos</td> 
                              <td>{{ $documento->num_anexos }}</td> 
                            </tr> 

                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-th"></i> Actividad</td> 
                              <td id="tdDocumentoActividad">{{ $documento->actividad }}</td> 
                            </tr>
   
                            <tr> 
                              <td class="text-info" colspan="2"><i class="glyphicon glyphicon-info-sign"></i> Asunto</td> 
                            </tr> 
                          </tbody> 
                        </table> 
   
                        <blockquote class="blockquote" style="font-size:15px;"> 
                          <p>{{ $documento->asunto }}</p> 
                        </blockquote> 
                         
                      </div> 
                    </div> 
                  </div> 
                </div> 
              </div> 
            </div> 
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                Observaciones
              </h4>
            </div>
            <div class="panel-body">
              <form method="POST" id="revisionForm">
              
                {{ csrf_field() }}

                <div class="form-group">
                  <textarea id="observacion" name="observacion" class="form-control" rows="2" placeholder="Ingrese sus observaciones..."></textarea>
                </div>

                <div class="form-group text-center">
                  <button type="button" onclick="_modalConfirmFunction('{{url('guiasturnados/create/'.$documento->id)}}');" class="btn btn-primary">
                    <i class="glyphicon glyphicon-retweet"></i>
                    TURNAR
                  </button>
                </div>
                
              </form>
            </div>
          </div>
        </div>
      </div> 
    </div>
    <!-- /.col-lg-12 -->
  </div>

  <!-- The Modal -->
  <div class="modal fade" id="confirmModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title" id="confirmModalTitle">Confirmar turnado</h4>
        </div>

        <!-- Modal body -->
        <div class="modal-body alert alert-info" id="confirmModalBody">
          Esta seguro que desea turnar este documento?, una vez turnado no podrá editarlo.
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" id="redirectButton" style="display:none;">Aceptar</button>
          <button type="button" class="btn btn-primary" id="acceptButton">Aceptar</button>
          <button type="button" class="btn btn-secondary" id="cancelButton">Cacelar</button>
        </div>

      </div>
    </div>
  </div>

  <script>
    var _modalConfirmFunction = function( _url ){
      //$('#confirmModal').modal({backdrop: 'static', keyboard: false});
      $("#confirmModal").modal("show");
      var _buttonAcept = document.getElementById("acceptButton");
      var _buttonCancel = document.getElementById("cancelButton");
      var _buttonRedirect = document.getElementById("redirectButton");
      _buttonAcept.onclick = function(){
        $.ajax({
          headers: { 
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
          }, 
          type: 'post',
          data: {"observacion": document.getElementById("observacion").value},
          url: _url,
          success: function(_data){
            window.location = _data.urlRedirect;
          },
          error: function(_data){
            $('#confirmModal').modal('hide');     // Ocultar ventana modal
            var _bodyError = document.getElementById("errorPanelBodyDiv");
            while(_bodyError.firstChild){
              _bodyError.removeChild(_bodyError.firstChild);
            }
            var _mensaje = document.createTextNode(_data.responseJSON.message);
            _bodyError.appendChild(_mensaje);
            $('#errorPanelDiv').collapse("show"); // Mostrar panel de error
          }
        });
      };

      _buttonCancel.onclick = function(){
        $('#confirmModal').modal('hide');
      }
    }
  </script>

@stop