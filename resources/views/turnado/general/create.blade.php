@extends('layouts.master-general') 
 
@section('content') 

  <div class="row">
    @include('partials.breadcrumb')
  </div>

  <div class="row">
    <div class="col-lg-12">
      @if ( session('message') ) 
        <div class="alert alert-success alert-dismissable"> 
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a> 
          <strong>EXITO: </strong> {{ session()->pull('message', 'default') }} 
        </div> 
      @endif 
    </div>
  </div>
  <!-- /.row -->

  <div class="row" id="errorRowDiv">
    <div class="col-lg-12">
      <div class="panel panel-danger panel-collapse collapse" id="errorPanelDiv">
        <div class="panel-heading">
          <strong>ERROR:</strong>
          <a data-toggle="collapse" href="#errorPanelDiv" class="close">&times</a>
        </div>
        <div class="panel-body" id="errorPanelBodyDiv">
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->
 
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default"> 
        <div class="panel-heading lead"> 
          <i class="glyphicon glyphicon-file"></i> Turnar documento
        </div> 
   
        <div class="panel-body"> 
          <div class="row"> 
            <div class="col-lg-12 col-md-12"> 
              <div class="row"> 
                
                @include('documento.partials.archivovisualizacion') 
           
                <div class="col-lg-8 col-md-8"> 
                  <ul class="nav nav-tabs"> 
                    <li class="active"><a data-toggle="tab" href="#Summery" class="text-info"><i class="fa fa-indent"></i> Resumen</a></li>
                  </ul> 
   
                  <div class="tab-content"> 
                    <div id="Summery" class="tab-pane fade in active"> 
                      <div class="table-responsive panel"> 
                         
                        <table class="table"> 
                          <tbody> 
                             
                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-list-alt"></i> No. Oficio</td> 
                              <td>{{ strtoupper($documento->num_oficio) }}</td> 
                            </tr> 
   
                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-file"></i> Tipo de documento</td> 
                              <td>{{ strtoupper($documento->tipo) }}</td> 
                            </tr> 
   
                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-inbox"></i> No. Hojas</td> 
                              <td>{{ $documento->num_hojas }}</td> 
                            </tr> 

                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-paperclip"></i> No. Anexos</td> 
                              <td>{{ $documento->num_anexos }}</td> 
                            </tr> 

                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-th"></i> Actividad</td> 
                              <td id="tdDocumentoActividad">{{ $documento->actividad }}</td> 
                            </tr>
   
                            <tr> 
                              <td class="text-info" colspan="2"><i class="glyphicon glyphicon-info-sign"></i> Asunto</td> 
                            </tr> 
                          </tbody> 
                        </table> 
   
                        <blockquote class="blockquote" style="font-size:15px;"> 
                          <p>{{ $documento->asunto }}</p> 
                        </blockquote> 
                         
                      </div> 
                    </div> 
                  </div> 
                </div> 
              </div> 
            </div> 
          </div>

          <div class="panel panel-info">
            <div class="panel-heading">
              <h4 class="panel-title">
                Turnado
              </h4>
            </div>
            <div class="panel-body">
              <form method="POST" id="revisionForm">
              
                {{ csrf_field() }}

                <div class="form-row">
                  <div class="form-group col-md-12">
                    <label>Revision</label>
                    <textarea id="observacion" name="observacion" class="form-control" rows="2" placeholder="Ingrese sus observaciones..." readonly>{{ $revision->observacion }}</textarea>
                  </div>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-12">
                    <table id="example" class="table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Area</th>
                          <th>Usuario</th>
                          <th>Puesto</th>
                          <th>Responer</th>
                          <th>Instruccion</th>
                          <th>Prioridad</th>
                        </tr>
                      </thead>
                      <tbody>
                        @forelse ($empleados as $empleado)
                          <tr>

                            <td>
                              @if( old('empAreaCheckbox[]') !== null ) 
                                <input type="checkbox" id="check{{ $empleado->id_empleado }}" name="empAreaCheckbox[]" onchange="_habilitar(this, 'respuesta{{$empleado->id_empleado}}', 'instruccion{{$empleado->id_empleado}}', 'prioridad{{$empleado->id_empleado}}')" value="{{ $empleado->id_empleado }}" class="form-check-input" checked>
                                <label class="form-check-label" for="id{{ $empleado->area }}">{{ $empleado->area }}</label>
                              @else
                                <input type="checkbox" id="check{{ $empleado->id_empleado }}" name="empAreaCheckbox[]" onchange="_habilitar(this, 'respuesta{{$empleado->id_empleado}}', 'instruccion{{$empleado->id_empleado}}', 'prioridad{{$empleado->id_empleado}}')" value="{{ $empleado->id_empleado }}" class="form-check-input">
                                <label class="form-check-label" for="id{{ $empleado->area }}">{{ $empleado->area }}</label>
                              @endif
                            </td>

                            <td>{{ $empleado->nombre }}</td>

                            <td>{{ $empleado->puesto }}</td>

                            <td>
                              <div class="form-check">
                                @if( strcmp( $empleado->puesto, 'MENSAJERO') == 0) 
                                  
                                @else
                                  <input type="radio" name="empAreaRespuestaRadio[]" id="respuesta{{$empleado->id_empleado}}" value="{{ $empleado->id_empleado }}" class="form-check-input position-static" onclick="(function(_element){ if(event.ctrlKey){_element.checked = false;} })(this);" disabled>
                                @endif
                              </div>
                            </td>

                            <td>
                              <select name="instruccionSelect[]" id="instruccion{{$empleado->id_empleado}}" class="form-control" disabled>
                                <option value="REVISION" selected>Revision</option>
                                <option value="DISTRIBUCION">Distribucion</option>
                                <option value="ARCHIVAR">Archivar</option>
                              </select>
                            </td>

                            <td>
                              <select name="prioridadSelect[]" id="prioridad{{$empleado->id_empleado}}" class="form-control" disabled>
                                <option value="ALTA" selected>Alta</option>
                                <option value="MEDIA">Media</option>
                                <option value="BAJA">Baja</option>
                              </select>
                            </td>

                          </tr>
                        @empty
                          <tr class="odd">
                            <td class="dataTables_emty" colspan="11" valing="top">
                              <p class="text-danger">
                                <i class="glyphicon glyphicon-exclamation-sign"></i> No hay usuarios disponibles.
                              </p>
                            </td>
                          </tr>
                        @endforelse
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">
                      <i class="glyphicon glyphicon-retweet"></i>
                      TURNAR
                    </button>
                  </div>
                </div>
                
              </form>
            </div>
          </div>
        </div>
      </div> 
    </div>
    <!-- /.col-lg-12 -->
  </div>

  <!-- The Modal -->
  <div class="modal fade" id="confirmModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title" id="confirmModalTitle">Confirmar eliminación</h4>
        </div>

        <!-- Modal body -->
        <div class="modal-body alert alert-info" id="confirmModalBody">
          Esta operación puede causar la pérdida de información.
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" id="redirectButton" style="display:none;">Aceptar</button>
          <button type="button" class="btn btn-primary" id="acceptButton">Aceptar</button>
          <button type="button" class="btn btn-secondary" id="cancelButton">Cacelar</button>
        </div>

      </div>
    </div>
  </div>

  <script>
    $(document).ready( function() {
      $('#example').DataTable({
        "responsive": true,
        "searching": false,
        "lengthChange": false,
        "pageLength": 5,
        "language": {
          "url":  <?php echo json_encode( url('/vendor/datatables-plugins/Spanish.json') ); ?>
        }
      });
    });

    var _habilitar = function( _element, _idRespuesta, _idInstruccion, _idPrioridad ){
      if(_element.checked){
        document.getElementById(_idRespuesta).removeAttribute("disabled");
        document.getElementById(_idInstruccion).removeAttribute("disabled");
        document.getElementById(_idPrioridad).removeAttribute("disabled");
      }else{
        document.getElementById(_idRespuesta).setAttribute("disabled", "");
        document.getElementById(_idInstruccion).setAttribute("disabled", "");
        document.getElementById(_idPrioridad).setAttribute("disabled", "");
      }
    }

    var _modalConfirmFunction = function( _url ){
      //$('#confirmModal').modal({backdrop: 'static', keyboard: false});
      $("#confirmModal").modal("show");
      var _buttonAcept = document.getElementById("acceptButton");
      var _buttonCancel = document.getElementById("cancelButton");
      var _buttonRedirect = document.getElementById("redirectButton");
      _buttonAcept.onclick = function(){
        $.ajax({
          headers: { 
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
          }, 
          type: 'post',
          data: {_method: "delete"},
          url: _url,
          success: function(_data){
            var _documentoActividad = document.getElementById("tdDocumentoActividad");
            $( "#acceptButton" ).remove();
            $( "#cancelButton" ).remove();
            $('#confirmModal').on('hidden.bs.modal', function () {
              if( _documentoActividad.textContent.localeCompare("ingreso") === 0 ){
                window.location = "{{ url('documentos/listaingreso') }}";
              }else{
                window.location = "{{ url('documentos/listaemision') }}";
              }
            });

            $( "#redirectButton" ).click(function(){
              if( _documentoActividad.textContent.localeCompare("ingreso") === 0 ){
                window.location = "{{ url('documentos/listaingreso') }}";
              }else{
                window.location = "{{ url('documentos/listaemision') }}";
              }
            });

            $( "#redirectButton" ).attr("style", "display:block;");
            $('#confirmModalTitle').empty();
            $('#confirmModalTitle').addClass("text-success");
            $('#confirmModalTitle').append("EXITO");
            $('#confirmModalBody').empty();
            $('#confirmModalBody').append("Eliminación realizada satisfactoriamente.");
            $('#confirmModalBody').attr("class", "modal-body alert alert-success");
          },
          error: function(_data){
            $('#confirmModal').modal('hide');     // Ocultar ventana modal
            var _bodyError = document.getElementById("errorPanelBodyDiv");
            while(_bodyError.firstChild){
              _bodyError.removeChild(_bodyError.firstChild);
            }
            var _mensaje = document.createTextNode(_data.responseJSON.message);
            _bodyError.appendChild(_mensaje);
            $('#errorPanelDiv').collapse("show"); // Mostrar panel de error
          }
        });
      };

      _buttonCancel.onclick = function(){
        $('#confirmModal').modal('hide');
      }
    }
  </script>

@stop