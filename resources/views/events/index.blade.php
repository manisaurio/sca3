@extends('layouts.admin') 
@section('content')

  <!--para mostrar el calendario-->   
  <div class="col-md-9">
    <h4>Mi calendario</h4>
    <div id="fullcal">    
    </div>
  </div>


  @include('events.form.events')
        

@endsection
<!--Aqui se cargara el script para eventos full calendar-->

@section('scripts')
{!!Html::script('assets/bootstrap-3.3.7-dist/js/crearEvento.js')!!}
{!!Html::script('assets/bootstrap-3.3.7-dist/js/accionesEvento.js')!!}
<script type='text/javascript'>
  $(document).ready(function() {

  // page is now ready, initialize the calendar...

  $('#fullcal').fullCalendar({
     header:{
        left:   'prev,next,today',
        center: 'title',
        right:  'month,agendaWeek,agendaDay,list'
    },
    height: 550,
    handleWindowResize : true,
    contentHeight: 500,
    navLinks: true,
    editable: true,
    eventStartEditable: false,
    eventDurationEditable: false,
    selecHelper: true, //agregar eventos
    selectable: true,
    //eventLimit: true,
    nowIndicator: true,
    googleCalendarApiKey : 'AIzaSyBTzL36mXmp4tgf5znNlMLKt1oST2OTD3Y',
    // put your options and callbacks here
    events: //'/cita', //api/cal     
                    //[{
                      //title: "primer evento",
                      //start: "2017-12-08",
                      //end: "2017-12-08",
                      //color: "red"
                    //}] 
    {
    googleCalendarId: "damoaxaca@gmail.com",
    //color: 'yellow',
    //eventColor: '#378006'
      
    }, 

    /*eventSources: [{
      googleCalendarId: "damoaxaca@gmail.com",
      color: purple,
    }],*/

    eventColor: '#378006',
    defaultView: 'month',

   

    visibleRange: function(currentDate) {
      return {
          start: currentDate.clone().subtract(15, 'days'),
          end: currentDate.clone().add(15, 'days') // exclusive end, so 3
            
        };
    },
      /////////////////

    //dateclick recibe varios parametros
    dayClick: function(fecha, jsEvent, event){ //fecha es un objeto de tipo moment, jsEvent es un objeto de tipo nativo, almacena las coord. view objet
      
      //alert("la fecha es: " + fecha.format());
      $('#createEventModal').modal('show');//-----------------------------------CREAR
      $('#start_date').val(fecha.format());
      $('#end_date').val(fecha.format());
      $('#horainicio').val(moment(fecha.start).format("HH:mm")); 
      $('#horafin').val(moment(fecha.end).add(1, 'hours').format("HH:mm")); // moment().add(1, 'hours').calendar(); 
      // $('#tituloEvento').val(event.title);

      //alert('Coordenadas: ' + jsEvent.pageX + ',' + jsEvent.pageY);
      //alert('Nombre de la vista: ' + view.name);
       //$(this).css('background-color', 'red');
    },
   
    eventMouseover: function(event, jsEvent, view){
      $('#tituloEvento').text("Título: " + event.title);
      $('#fechaEvento').text("Fecha : "+ moment(event.start).format("DD-MM-YYYY"));
      $('#descEvento').text(event.description); //event.description
      
      $(this).css('border-color', 'blue');
      //$(this).css('background-color', 'red');
      var fecEvento =  event.start.format("DD");
      var fecActual = moment().format("DD");
      if(fecActual < fecEvento){
        $('#proximo').css('background-color', 'yellow');
        $('#atiempo').css('background-color', 'white');
        $('#pasado').css('background-color', 'white');
        $(this.event).css('background-color', 'red');
      }else if(fecActual > fecEvento) {
        $('#proximo').css('background-color', 'white');
        $('#atiempo').css('background-color', 'white');
        $('#pasado').css('background-color', 'red');
       // $(this).css('background-color', 'red');
      } else {
         $('#atiempo').css('background-color', 'green');
         $('#proximo').css('background-color', 'white');
         $('#pasado').css('background-color', 'white');
      }
    },
  
    eventClick: function(event) {
          if (event.url) {
              //window.open(event.url);
              $('#accionesEventModal').modal('show');   ////////////////////////editar
              $('#id').val(event.id);
              //para actualizar y mostrar los datos
              $('#id2').val(event.id);
              $("#title2").val(event.title);
              $("#description2").val(event.description);
              $("#start_date2").val(moment(event.start).format("DD-MM-YYYY"));
              $("#end_date2").val(moment(event.end).format("DD-MM-YYYY"));
              $('#horainicio2').val(moment(event.start).format("HH:mm")); 
              $('#horafin2').val(moment(event.end).format("HH:mm")); 

              $("#lugar2").val(event.location);
              //var start = moment(event.start).format("DD-MM-YYYY HH:mm");



              $("#elimina").click(function(){  ////////eliminar
              var route2 = "http://dam04.dev/cita/" +event.id+ ""; ;//+ event.id; //+btn.value+ "";
              var token = $("#token2").val();
              console.log(event.id);
              $.ajax({
                url: route2,
                headers: {'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',
                success: function(){    //Actualizar la pagina con los datos que ahora tiene
                  Carga();
                  location.reload();
                  $("#msj-eliminado").fadeIn();
                }
              });
            });
              //MOSTRAR

            //ACTUALIZAR
            $("#actualiza2").click(function(){  //cachar el evento del boton actualizar, esta en el modal
            var datox= $("#id2").val();
            var dato1= $("#title2").val();
            var dato2= $("#description2").val();
            var dato3= $("#start_date2").val();
            var dato4= $("#end_date2").val();
            var dato5= $("#horainicio2").val();
            var dato6= $("#horafin2").val();
            var dato7= $("#lugar2").val();
            var dato0= '-06:00';
            var dato8= "T";
            var dato9= dato3 + dato8 + dato5 + dato0;
            var dato10= dato4 + dato8 + dato6 + dato0;
            var routeX = "http://dam04.dev/cita/"+event.id+ ""; //var route = "http://dam04.dev/cita";  // "+value+ "";
            var token3 = $("#token3").val();
            $.ajax({
              url: routeX,
              headers: {'X-CSRF-TOKEN': token3},
              type: 'PUT',
              dataType: 'json',
              data: {id: datox,title:dato1, description:dato2, start_date:dato9, end_date: dato10, lugar:dato7},
              success: function(){ //para actualizar la pagina
                Carga();//ocultar el modal
                $("#myModal").modal('toggle');
                location.reload();
                $("#msj-success").fadeIn();
              },    error:function(msj){ 
                $("#msj-error").fadeIn();
              }
            });
          });


              //PARA MOSTRAR GOOGLE CALENDAR EN OTRA PAGINA
              $('#calendario').click(function(){
                window.open(event.url);
                return false;
              });
              
              return false;

          }
      },



   /* eventDrop: function(event, delta, reverFunc){
      event_data = {
        event: {
          id: event.id,
          start: event.start.format(),
          end: event.end.format()
        }
      };
          $.ajax({
            url: event.update_url,
            data: event_data,
            type: "PATH",
            success: function(json){
            }
          });
        }, */
        /*

        eventDrop: function(event, delta){
          $.ajax({
            url: 'events/index.php',
            data: 'action=update&title='+event.title+'&start='+moment(event.start).format()+'&end='+moment(event.end).format()+'&id',
            type: "POST",
            success: function(json){
        
            }
          });
        },

        eventResize:function(event){
          $.ajax({
            url: 'events/index.php',
            data: 'action=update&title='+event.title+'&start='+moment(event.start).format()+'&end='+moment(event.end).format()+'&id',
            type: "POST",
            success: function(json){
        
            }

          });
        }*/
    });

          /*$('#submitButton').on('click', function(e){
        e.preventDefault();
        doSubmit();
          });
          $.('#deleteButton').on('click', function(e){
        e.preventDefault();
        doDelete();
          });

          function doDelete(){
            $('#calendarModal').modal('hide');
        var eventID = $('#eventID').val();
        $.ajax({
          url: 'events/index.php',
          data: 'action=delete$id='+eventID,
          type : "POST",
          success: function(json){
            if(json == 1)
              $("#calendar").fullCalendar('removeEvents', eventID);
              else
              return false;
          }
        });
          }

          function doSubmit(){
        $("#createEventModal").modal('hide');
        var title = $('$title').val();
        var startTime = $('#startTime').val();
        var endTime = $('#endTime').val();
        $.ajax({
          url: 'events/index.php',
          data: 'action=add&title='+title+'&start='startTime+'&end='+endTime,
          type: "POST",
          success: function(json){
            $("#calendar").fullCalendar('removeEvents',
            {
              id: json.id,
              title: title,
              start: starTime,
              end: endTime
            }, true);
          }
        });
          }*/

       
  });
</script> 

  
@endsection
 
       
  


