<!--para mostrar las notificaciones-->
	{!!Form::open()!!}
	<div id="msj-success" class="alert alert-success alert-dismissible" role="alert" style="display:none">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong> Nueva Cita Agregada Correctamente.</strong>
	
	</div>

	<div id="msj-error" class="alert alert-danger alert-dismissible" role="alert" style="display:none">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong id="msj">Algo anda mal</strong>
	</div>

		
	<!--para mostrar elmodal-->
				<div class="modal fade" id="createEventModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel">Agregar Cita o Evento</h4>
				      </div>
				      <div class="modal-body">
				      	<!--para token-->
					      	<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
					      	<input type="hidden" id="idx">
					      	<!--aqui va el formulario-->
					       @include('events.form.form')
					    
					   
				      </div>
				      <div class="modal-footer">
				      	<!--para token-->
				      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				      <!--<button type="button" class="btn btn-primary">Guardar</button> -->
				      <!--para boton registrar-->
				      {!!link_to('#', $title='Registrar', $attributes = ['id' => 'registro', 'class'=>'btn btn-primary'], $secure = null)!!}
				      </div>
				    </div>
				  </div>
				</div>

	<div class="modal fade" id="accionesEventModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
		    <div class="modal-content">
		        <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">¿Qué desea hacer?</h4>
			    </div>

				<div class="modal-body">
					      	
					<input type="hidden" name="_token2" value="{{ csrf_token() }}" id="token2">
					<input type="hidden" id="id">
							       <!--include('cita.form.cita')-->
									
					<div class="form-group" >
					    <label for="">Eliminar Cita o Evento </label>
					    <br>
					    <label for="">Actualizar Cita o Evento </label>
					    <br>
					</div>
				</div>

				<div class="modal-footer">
					      	<!--para token-->
				    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				    <!--para boton registrar-->
				    {!!link_to('#', $title='Google Calendar', $attributes = ['id' => 'calendario', 'class'=>'btn btn-primary'], $secure = null)!!}
				    {!!link_to('#', $title='Editar', $attributes = ['id' => 'actualiza', 'class'=>'btn btn-primary',
				     'data-toggle'=> 'modal', 'data-target'=>'#myModal'], $secure = null)!!}
				    {!!link_to('#', $title='Eliminar', $attributes = ['id' => 'elimina', 'class'=>'btn btn-danger'], $secure = null)!!}

				   <!--, 'data-toggle'=> 'modal', 'data-target'=>'#myModal'-->

				</div>
			</div>
		</div>	
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Actualizar Cita/Evento</h4>
	      </div>
	      <div class="modal-body">
	      
	      	<input type="hidden" name="_token3" value="{{ csrf_token() }}" id="token3">
	      	<input type="hidden" id="id3">
	        <!--aqui va el formulario-->
			@include('events.form.form2')
	      </div>
	      <div class="modal-footer">
	         <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	         
	      {!!link_to('#', $title='Actualizar', $attributes = ['id'=>'actualiza2', 'class'=>'btn btn-primary'], $secure = null)!!} 
	      </div> 
	    </div>
	  </div>
	</div>


	<div ng-app="myApp" ng-controller="myNgController">
	    <div class="row">
	        <!--<div class="col-md-9">
	            <div id="calendar" ui-calendar="uiConfig.calendar" ng-model="eventSources" calendar="myCalendar"></div>
	        </div>  -->
	        <div class="col-md-3">
	            <div ng-show="SelectedEvent" class="alert alert-success" style="margin-top:50px">
	                <h2 style="margin-top:0px"> Cita/Evento:</h2>
	                <div class="form-group">
	                	<h3 style="color:#A9A50E" id="tituloEvento"></h3>  <!--SelectedEvent.title-->
	                </div>
	                <div class="form-group">
	                	<h4 style="color:#A9A50E" id="fechaEvento"></h4>  <!--SelectedEvent.title-->
	                	<p id="descEvento"> Descripción :</p>  <!--SelectedEvent.description-->
	                </div>
	                <div class="form-group">
			            <div class="form-inline">
			            	<label>Actual </label>
			                <label>Próximos</label>
			                <label>Pasados</label>
			               	
			            </div>
			            <div class="form-inline">
			            	<input id="atiempo" type="" name="" style="background: white"  size="3">
			                <input type="" id="proximo" name="" style="background: white" size="3">
			                <input type="" id="pasado" name="" style="background: white" size="3">
			            </div>
			               	
			        </div> 

	                
	            </div>
	        </div>

	     			
	    </div>
	</div>

	
	
    {!!Form::close()!!}


   