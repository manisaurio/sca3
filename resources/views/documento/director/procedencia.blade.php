@extends('layouts.master-director')

@section('content')

  <div class="row">
    <nav aria-label="breadcrumb" style="margin-top: 10px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item" aria-current="page">Historial Documentos - Procedencia</li>
      </ol>
    </nav>
  </div>

  <!--
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header" style="margin-top: 3px; margin-bottom: 15px;">Home</h3>
    </div>
  </div>
  -->

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">PROCEDENCIA DE DOCUMENTOS</div>
        <div class="panel-body">
          <div id="legend"><h6>SIMBOLOGÍA</h6></div>
          <div id="legendMunicipio"></div>
          <div id="mapDiv" style="height: 400px;" class="col-xs-12 col-sm-6 col-lg-7"></div>
          <div class="col-xs-12 col-lg-5">
            <form role="form">

              <div class="form-row">
                <div class="form-group col-md-12"> 
                  <label for="dependenciaInputGroup">Dependencia</label> 
                  <input type="text" id="dependenciaInput" name="dependenciaInput" value="" class="form-control" placeholder="Nombre" required> 
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Fecha inicio:</label>
                  <input type="date" id="fechainicio" name="fechainicio" onchange="setMin(this, 'fechafin');" class="form-control">
                </div>
                <div class="form-group col-md-6">
                  <label>Fecha fin:</label>
                  <div class="input-group"> 
                    <input type="date" id="fechafin" name="fechafin" onchange="setMax(this, 'fechainicio');" class="form-control">
                    <span class="input-group-btn">
                      <button class="btn btn-primary" onclick="historial();" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-12">
                  <div id="chart_div" style="width: 100%;"></div>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <style>
    #legend {
      background: #fff;
      padding: 5px;
      margin: 5px;
      opacity: 0.7;
    }
    #legend h6 {
      margin-top: 0;
      margin-bottom: .50em;
    }
    #legend p {
      margin-bottom: 0;
    }

    #legendMunicipio {
      background: #fff;
      padding: 5px;
      margin: 5px;
      opacity: 0.9;
    }
    #legendMunicipio h6 {
      margin-top: 0;
      margin-bottom: 0;
    }
  </style>

  <!-- Jsdelivr CSS 
  <link href={{ url('/css/jsdelivr.css') }} rel="stylesheet">
  -->

  <script> 
    var map;
    var marcadoresArray = [];
    function initMap() { 
      map = new google.maps.Map(document.getElementById("mapDiv"), { 
        center: {lat: 17.0606433, lng: -96.5319102}, 
        zoom: 7,
        streetViewControl: false,
        fullscreenControl: false,
        mapTypeControl: false
      }); 

      //---[ Autocomplete - Places ]--------------------------------------------------------
      var input = /** @type {!HTMLInputElement} */( 
        document.getElementById("dependenciaInput"));

      var options = {
        location: new google.maps.LatLng(16.043726, -96.398101),
        radius: 297000,
        strictBounds: true
      };

      var autocomplete = new google.maps.places.Autocomplete(input, options); 
      autocomplete.bindTo("bounds", map); 

      var infowindow = new google.maps.InfoWindow(); 
      var marker = new google.maps.Marker({ 
        map: map, 
        anchorPoint: new google.maps.Point(0, -29) 
      }); 

      autocomplete.addListener("place_changed", function() { 
        infowindow.close(); 
        marker.setVisible(false); 
        var place = autocomplete.getPlace(); 
        if (!place.geometry) { 
          // User entered the name of a Place that was not suggested and 
          // // pressed the Enter key, or the Place Details request failed. 
          window.alert("No details available for input: " + place.name + ""); 
          return; 
        }
        console.log(place);

        // If the place has a geometry, then present it on a map. 
        if (place.geometry.viewport) { 
          map.fitBounds(place.geometry.viewport);
          map.setCenter({lat: 17.0606433, lng: -96.4319102});
          map.setZoom(7);
        } else { 
          map.setCenter(place.geometry.location); 
          map.setZoom(17);  // Why 17? Because it looks good. 
        } 
        marker.setIcon(/** @type {google.maps.Icon} */({ 
          url: place.icon, 
          size: new google.maps.Size(71, 71), 
          origin: new google.maps.Point(0, 0), 
          anchor: new google.maps.Point(17, 34), 
          scaledSize: new google.maps.Size(35, 35) 
        })); 
        marker.setPosition(place.geometry.location); 
        marker.setVisible(true); 

        var address = ""; 
        if (place.address_components) { 
          address = [ 
            (place.address_components[0] && place.address_components[0].short_name || ""), 
            (place.address_components[1] && place.address_components[1].short_name || ""), 
            (place.address_components[2] && place.address_components[2].short_name || "") 
          ].join(" "); 
        } 

        infowindow.setContent("<div><strong>" + place.name + "</strong><br>" + address); 
        infowindow.open(map, marker);
        input.value = place.name;
      });
      //---[ FIN - Autocomplete - Places ]--------------------------------------------------
      
      //---------------------------[ GeoJson ]----------------------------------------------
      
      //CANADA
      map.data.loadGeoJson('https://bitbucket.org/manisaurio/sca3/raw/e0ac1b9ca88fddfd3dd097cda96a7c21bd3e7a7f/public/geojson/canada.geojson');
      
      //MIXTECA
      map.data.loadGeoJson('https://bitbucket.org/manisaurio/sca3/raw/6f6ef3fa643cb5c9e219b4cc2bd371d0cdf755db/public/geojson/mixteca.geojson');
      
      //COSTA
      map.data.loadGeoJson('https://bitbucket.org/manisaurio/sca3/raw/683976de0144c2cecb3430e908593ce29cf1aee0/public/geojson/costa.geojson');
      
      //ISTMO
      map.data.loadGeoJson('https://bitbucket.org/manisaurio/sca3/raw/e0ac1b9ca88fddfd3dd097cda96a7c21bd3e7a7f/public/geojson/istmo.geojson');
      
      //PAPALOAPAN
      map.data.loadGeoJson('https://bitbucket.org/manisaurio/sca3/raw/e0ac1b9ca88fddfd3dd097cda96a7c21bd3e7a7f/public/geojson/papaloapan.geojson');
      
      //SIERRANORTE
      map.data.loadGeoJson('https://bitbucket.org/manisaurio/sca3/raw/a807dc49a86436b380dc8e4ec4fc8cbe17135f55/public/geojson/sierranorte.geojson');
      
      //SIERRASUR
      map.data.loadGeoJson('https://bitbucket.org/manisaurio/sca3/raw/cff85517ac97dcf3b6d0e13fd02bcb87cf7d4bf8/public/geojson/sierrasur.geojson');

      //VALLESCENTRALES
      map.data.loadGeoJson('https://bitbucket.org/manisaurio/sca3/raw/baa165b5d3e2262a8402dc95b39228887ace9c0b/public/geojson/vallescentrales.geojson');

      map.data.setStyle(function(feature) {
        var color = '';
        color = feature.getProperty('COLOR');
        return ({
          fillColor: color,
          strokeColor: color,
          strokeWeight: 1
        });
      });

      map.data.addListener('click', function(event) {
        console.log(event.feature.f);
      });

      map.data.addListener('mouseover', function(event) {
        if( event.feature.f.hasOwnProperty('Name')){
          document.getElementById("legendMunicipio").innerHTML = '<h6>' + event.feature.f.Name + ' </h6';
        }else{
          document.getElementById("legendMunicipio").innerHTML = '<h6>' + event.feature.f.MUNICIPIO1 + ' </h6';
        }

        map.data.revertStyle();
        map.data.overrideStyle(event.feature, {strokeWeight: 5});
      });

      map.data.addListener('mouseout', function(event) {
        document.getElementById("legendMunicipio").innerHTML = '';
        map.data.revertStyle();
      });

      var simbRegiones = [
        '<p><i class="fa fa-square" style="color:#ED1C24"></i> Cañada</p>',
        '<p><i class="fa fa-square" style="color:#FFA500"></i> Mixteca</p>',
        '<p><i class="fa fa-square" style="color:#FF1493"></i> Costa</p>',
        '<p><i class="fa fa-square" style="color:#008000"></i> Istmo</p>',
        '<p><i class="fa fa-square" style="color:#0000CD"></i> Sierra sur</p>',
        '<p><i class="fa fa-square" style="color:#616161"></i> Valles centrales</p>',
        '<p><i class="fa fa-square" style="color:#A52A2A"></i> Sierra norte</p>',
        '<p><i class="fa fa-square" style="color:#A349A4"></i> Papaloapan</p>'
      ];

      var legend = document.getElementById('legend');
      simbRegiones.forEach(function(item) {
        var div = document.createElement('div');
        div.innerHTML = item;
        legend.appendChild(div);
      });
      map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(legend);

      var legendMunicipio = document.getElementById('legendMunicipio');
      map.controls[google.maps.ControlPosition.TOP_RIGHT].push(legendMunicipio);
    }

    function eliminarMarcadores(){
      if (marcadoresArray) {
        for (i in marcadoresArray) {
          marcadoresArray[i].setMap(null);
        }
        marcadoresArray.length = 0;
      }
    }

    function crearMarcadores(_datos){
      eliminarMarcadores();
      _datos.forEach(function(_item) {
        var _geometryDependenciaJSON = JSON.parse(_item.geometry_dependencia);
        var contentString = 
          '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h5 id="firstHeading" class="firstHeading" style="margin-bottom: 0;">'+
              _item.dependencia +
            '</h5>'+
            '<div id="bodyContent">'+
              '<p style="margin-bottom: .30em;">'+
                _item.address_dependencia +
              '</p>'+
              '<p>'+
                '<b>Recibidos:</b> ' + _item.incidencias +
              '</p>'+
            '</div>'+
          '</div>'
        ;

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        var marker = new google.maps.Marker({
          position: _geometryDependenciaJSON.location,
          map: map,
          title: _item.dependencia
        });

        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });

        marcadoresArray.push(marker);
      });
    }

    function historial(){
      var _from = document.getElementById("fechainicio").value;
      var _to = document.getElementById("fechafin").value;

      $.ajax({ 
        headers: { 
          "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
        }, 
        type: "POST", 
        url: "{{url('api/documento/rangoMapa')}}",
        data: {fechainicio: _from, fechafin: _to}, 
        success: function( _datos ){
          crearMarcadores(_datos);
        },
        error: function( _datos ){
          console.error( _data );
        }
      });

      drawChart(_from, _to);
    }

    function setMin(_element, _id){
      if( _element.value ){
        document.getElementById(_id).setAttribute("min", _element.value);
      }else{
        document.getElementById(_id).removeAttribute("min");
      }
    }

    function setMax(_element, _id){
      if( _element.value ){
        document.getElementById(_id).setAttribute("max", _element.value);
      }else{
        document.getElementById(_id).removeAttribute("max");
      }
    }
  </script>

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADnzxwMdxbgRQvQRu0f_a57o-G5y3SxlI&libraries=places&callback=initMap" 
          async defer></script>

  <!-- Google Charts JS -->
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    // Load the Visualization API and the piechart package.
    google.charts.load('current', {'packages':['corechart']});

    function drawChart(_from, _to) {
      var jsonData = $.ajax({
          url: "{{url('api/maps/googleChartsByRegion')}}",
          data: {fechainicio: _from, fechafin: _to},
          dataType: "json",
          async: false
          }).responseText;
      
      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.PieChart(document.getElementById('chart_div'));

      var colores = [];
      JSON.parse(jsonData).rows.forEach(function(element) {
        switch( element.c[0].v ){
          case "CAÑADA":
            colores.push("#ED1C24");
            break;
          case "COSTA":
            colores.push("#FF1493");
            break;
          case "ISTMO":
            colores.push("#008000");
            break;
          case "MIXTECA":
            colores.push("#FFA500");
            break;
          case "PAPALOAPAN":
            colores.push("#A349A4");
            break;
          case "SIERRA NORTE":
            colores.push("#A52A2A");
            break;
          case "SIERRA SUR":
            colores.push("#0000CD");
            break;
          case "VALLES CENTRALES":
            colores.push("#616161");
            break;
        } 
      });

      chart.draw(data, {
        width: 400, 
        height: 240,
        is3D: true,
        colors: colores
      });
      
    }

  </script>


@endsection
