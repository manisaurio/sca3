@extends('layouts.master-mensajero') 
 
@section('content') 

  <div class="row">
    <nav aria-label="breadcrumb" style="margin-top: 10px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/documentos/listaingreso')}}">Documentos Ingresados</a></li>
        <li class="breadcrumb-item" aria-current="page">Detalle</li>
      </ol>
    </nav>
  </div>

  <div class="row">
    <div class="col-lg-12">
      @if ( session('message') ) 
        <div class="alert alert-success alert-dismissable"> 
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a> 
          <strong>EXITO: </strong> {{ session()->pull('message', 'default') }} 
        </div> 
      @endif 
    </div>
  </div>
  <!-- /.row -->

  <div class="row" id="errorRowDiv">
    <div class="col-lg-12">
      <div class="panel panel-danger panel-collapse collapse" id="errorPanelDiv">
        <div class="panel-heading">
          <strong>ERROR:</strong>
          <a data-toggle="collapse" href="#errorPanelDiv" class="close">&times</a>
        </div>
        <div class="panel-body" id="errorPanelBodyDiv">
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->
 
  <div class="row">

    <div class="col-lg-12">

    <div class="panel panel-default"> 

      <div class="panel-heading lead"> 
        <div class="row"> 
          <div class="col-lg-8 col-md-8"><i class="glyphicon glyphicon-file"></i> Documento Detalle</div> 
          <div class="col-lg-4 col-md-4 text-right"> 
            <div class="btn-group text-center">
              @if( strcmp( $documento->estado, 'INGRESADO') == 0 )
                <a href="{{ url('/documentos/edit/' . $documento->id) }}" class="btn btn-default btn-sm btn-default"><i class="glyphicon glyphicon-pencil"></i></a> 
                <a onclick="_modalConfirmFunction('{{ url('/documentos/destroy/'.$documento->id) }}')" class="btn btn-sm btn-default"><i class="glyphicon glyphicon-trash"></i></a>
              @endif
            </div> 
          </div> 
        </div> 
      </div> 
 
      <div class="panel-body"> 
        <div class="row"> 
          <div class="col-lg-12 col-md-12"> 
            <div class="row">

              @include('documento.partials.archivovisualizacion')
           
              <div class="col-lg-8 col-md-8"> 
                <ul class="nav nav-tabs"> 
                  <li class="active"><a data-toggle="tab" href="#Summery" class="text-info"><i class="fa fa-indent"></i> Resumen</a></li>
                </ul> 
 
                <div class="tab-content"> 
                  <div id="Summery" class="tab-pane fade in active"> 
                    <div class="table-responsive panel"> 
                       
                      <table class="table"> 
                        <tbody> 
                           
                          <tr> 
                            <td class="text-info"><i class="glyphicon glyphicon-list-alt"></i> No. Oficio</td> 
                            <td>{{ strtoupper($documento->num_oficio) }}</td> 
                          </tr> 
 
                          <tr> 
                            <td class="text-info"><i class="glyphicon glyphicon-file"></i> Tipo de documento</td> 
                            <td>{{ strtoupper($documento->tipo) }}</td> 
                          </tr> 
 
                          <tr> 
                            <td class="text-info"><i class="glyphicon glyphicon-inbox"></i> No. Hojas</td> 
                            <td>{{ $documento->num_hojas }}</td> 
                          </tr> 

                          <tr> 
                            <td class="text-info"><i class="glyphicon glyphicon-paperclip"></i> No. Anexos</td> 
                            <td>{{ $documento->num_anexos }}</td> 
                          </tr> 

                          <tr> 
                            <td class="text-info"><i class="glyphicon glyphicon-th"></i> Actividad</td> 
                            <td id="tdDocumentoActividad">{{ $documento->actividad }}</td> 
                          </tr>
 
                          <tr> 
                            <td class="text-info" colspan="2"><i class="glyphicon glyphicon-info-sign"></i> Asunto</td> 
                          </tr> 
                        </tbody> 
                      </table> 
 
                      <blockquote class="blockquote" style="font-size:15px;"> 
                        <p>{{ $documento->asunto }}</p> 
                      </blockquote> 
                       
                    </div> 
                  </div> 
                </div> 
              </div> 
            </div> 
          </div> 
        </div> 
      </div>

    </div> 

    </div>

  </div>

  <!-- The Modal -->
  <div class="modal fade" id="confirmModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title" id="confirmModalTitle">Confirmar eliminación</h4>
        </div>

        <!-- Modal body -->
        <div class="modal-body alert alert-info" id="confirmModalBody">
          Esta operación puede causar la pérdida de información.
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" id="redirectButton" style="display:none;">Aceptar</button>
          <button type="button" class="btn btn-primary" id="acceptButton">Aceptar</button>
          <button type="button" class="btn btn-secondary" id="cancelButton">Cacelar</button>
        </div>

      </div>
    </div>
  </div>

  <script>
    var _modalConfirmFunction = function( _url ){
      //$('#confirmModal').modal({backdrop: 'static', keyboard: false});
      $("#confirmModal").modal("show");
      var _buttonAcept = document.getElementById("acceptButton");
      var _buttonCancel = document.getElementById("cancelButton");
      var _buttonRedirect = document.getElementById("redirectButton");
      _buttonAcept.onclick = function(){
        $.ajax({
          headers: { 
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
          }, 
          type: 'post',
          data: {_method: "delete"},
          url: _url,
          success: function(_data){
            var _documentoActividad = document.getElementById("tdDocumentoActividad");
            $( "#acceptButton" ).remove();
            $( "#cancelButton" ).remove();
            $('#confirmModal').on('hidden.bs.modal', function () {
              if( _documentoActividad.textContent.localeCompare("INGRESO") === 0 ){
                window.location = "{{ url('documentos/listaingreso') }}";
              }else{
                window.location = "{{ url('documentos/listaemision') }}";
              }
            });

            $( "#redirectButton" ).click(function(){
              if( _documentoActividad.textContent.localeCompare("INGRESO") === 0 ){
                window.location = "{{ url('documentos/listaingreso') }}";
              }else{
                window.location = "{{ url('documentos/listaemision') }}";
              }
            });

            $( "#redirectButton" ).attr("style", "display:block;");
            $('#confirmModalTitle').empty();
            $('#confirmModalTitle').addClass("text-success");
            $('#confirmModalTitle').append("EXITO");
            $('#confirmModalBody').empty();
            $('#confirmModalBody').append("Eliminación realizada satisfactoriamente.");
            $('#confirmModalBody').attr("class", "modal-body alert alert-success");
          },
          error: function(_data){
            $('#confirmModal').modal('hide');     // Ocultar ventana modal
            var _bodyError = document.getElementById("errorPanelBodyDiv");
            while(_bodyError.firstChild){
              _bodyError.removeChild(_bodyError.firstChild);
            }
            var _mensaje = document.createTextNode(_data.responseJSON.message);
            _bodyError.appendChild(_mensaje);
            $('#errorPanelDiv').collapse("show"); // Mostrar panel de error
          }
        });
      };

      _buttonCancel.onclick = function(){
        $('#confirmModal').modal('hide');
      }
    }
  </script>

@stop