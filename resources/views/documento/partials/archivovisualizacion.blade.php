<div class="col-lg-4 col-md-4"> 
  <center> 
    <span class="text-left"> 
      @if( $documento->ruta_pdf )
        <iframe src="{{$documento->ruta_pdf}}" class="media" width="324" height="270" allowfullscreen webkitallowfullscreen></iframe> 
      @else
        <div class="col-lg-12 col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="row">
                <div class="col-xs-3">
                  <i class="fa fa-file-pdf-o fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                  <div class="huge">NO</div>
                  <div>Existe copia digital</div>
                </div>
              </div>
            </div>
            <div class="panel-footer">
              <span class="pull-left">Recurso no disponible.</span>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      @endif
    </span> 
  </center> 

  @if( $documento->ruta_pdf )
    <div class="table-responsive panel"> 
      <table class="table"> 
        <tbody> 
          <tr> 
            <td class="text-center">
              <a href="{{ url( $documento->ruta_pdf ) }}" class="btn btn-outline btn-primary btn-block" download><i class="glyphicon glyphicon-download-alt"></i> Descargar</a>
            </td> 
          </tr> 
        </tbody> 
      </table> 
    </div>
  @else
    <br> 
  @endif
</div>