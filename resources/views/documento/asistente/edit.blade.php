@extends("layouts.master-asistente") 
 
@section("content")
  
  <div class="row">
    <nav aria-label="breadcrumb" style="margin-top: 10px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
      </ol>
    </nav>
  </div>
  
  @if( count($errors) > 0) 
    <div class="alert alert-danger alert-dismissible" role="alert"> 
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> 
        <span aria-hidden="true">&times;</span> 
      </button> 
      <ul> 
        @foreach($errors->all() as $error) 
          <li>{!!$error!!}</li> 
        @endforeach 
      </ul> 
    </div> 
  @endif 
 
  <div class="row">

    <div class="col-md-offset-2 col-lg-8"> 
 
      <div class="panel panel-default"> 
        
        <div class="panel-heading"> 
          <h3 class="panel-title text-center"> 
            <span class="glyphicon glyphicon-file" aria-hidden="true"></span> 
            Documento - Editar 
          </h3> 
        </div> 
 
        <div class="panel-body" style="padding:30px"> 
         
          <form enctype="multipart/form-data" method="POST" id="editform"> 
           
            {{ method_field('PUT') }}

            {{ csrf_field() }} 

            <div class="form-row"> 
              <div class="form-group col-md-3"> 
                <label for="numOficio">No. Oficio</label>
                <input type="text" name="numOficio" id="numOficio" value="{{ old('num_oficio', $documento->num_oficio) }}" class="form-control" placeholder="NO. OFICIO" style="text-transform:uppercase" onkeypress="return Restriccion.Documento.numoficio(event);" required>                  
              </div> 

              <div class="form-group col-md-6"> 
                <label for="fechaOficio">Fecha Del Oficio</label>
                <input type="date" name="fechaOficio" id="fechaOficio" value="{{ old('fechaOficio', $documento->fecha_oficio) }}" class="form-control" required> 
              </div> 
 
              <div class="form-group col-md-3"> 
                <label for="numHojas">No. De Hojas</label>
                <input type="text" name="numHojas" id="numHojas" value="{{ old('numHojas', $documento->num_hojas) }}" class="form-control" placeholder="No. De Hojas" onkeypress="return Restriccion.Documento.numhojas(event);" required> 
                <small class="form-text text-info" id="nombreSmall" hidden><span class="glyphicon glyphicon-info-sign"></span> Sólo números</small> 
              </div> 
            </div>

            <div class="form-row"> 
              <div class="form-group col-md-4"> 
                <label for="tipo">Tipo de documento</label> 
                <select name="tipo" id="tipo" class="form-control"> 
                  @foreach($tiposdocumentos as $tipodocumento)
                    @if( old('tipo') !== null )
                      @if( strcmp(old('tipo'), $tipodocumento->tipo) == 0 ) 
                        <option value="{{ $tipodocumento->id }}" selected> {{ $tipodocumento->tipo }} </option> 
                      @else
                        <option value="{{ $tipodocumento->id }}"> {{ $tipodocumento->tipo }} </option> 
                      @endif
                    @else
                      @if( $tipodocumento->id == $documento->id_tipodocumento ) 
                        <option value="{{ $tipodocumento->id }}" selected> {{ $tipodocumento->tipo }} </option> 
                      @else 
                        <option value="{{ $tipodocumento->id }}" > {{ $tipodocumento->tipo }} </option> 
                      @endif 
                    @endif
                  @endforeach 
                </select> 
              </div> 
 
              <div class="form-group col-md-5"> 
                <label for="otroTipoInputGroup">Otro tipo:</label> 
                <div class="input-group" id="otroTipoInputGroup"> 
                  <span class="input-group-addon">
                    @if( old('otroTipoCheckbox') !== null )
                      <input type="checkbox" id="otroTipoCheckbox" name="otroTipoCheckbox" checked> 
                    @else
                      <input type="checkbox" id="otroTipoCheckbox" name="otroTipoCheckbox"> 
                    @endif
                  </span> 
                  <input type="text" id="otroTipoInput" name="otroTipoInput" value="{{ old('otroTipoInput') }}" class="form-control" style="text-transform:uppercase" onkeypress="return Restriccion.Documento.otrotipoinput(event);" disabled> 
                </div> 
                <small class="form-text text-info" id="nombreSmall" hidden><span class="glyphicon glyphicon-info-sign"></span> Solo palabra</small> 
              </div> 
 
              <div class="form-group col-md-3"> 
                <label for="numAnexos">No. De Anexos</label> 
                <input type="text" name="numAnexos" id="numAnexos" value="{{ old('numAnexos', $documento->num_anexos) }}" class="form-control" placeholder="No. De Anexos" onkeypress="return Restriccion.Documento.numanexos(event);" required> 
                <small class="form-text text-info" id="nombreSmall" hidden><span class="glyphicon glyphicon-info-sign"></span> Sólo numeros</small> 
              </div> 
            </div> 
 
            <div class="form-row"> 
              <div class="form-group col-md-12"> 
                <label for="asunto">Asunto</label> 
                <textarea name="asunto" id="asunto" class="form-control" rows="3" placeholder="Asunto del documento" required>{{ old('asunto', $documento->asunto) }}</textarea>
                <small class="form-text text-info" id="nombreSmall" hidden><span class="glyphicon glyphicon-info-sign"></span> Limite alcanzado</small> 
              </div> 
            </div> 
 
            <div class="form-row"> 
              <div class="form-group col-lg-8"> 
                <label for="respuestaInputGroup">Requiere respuesta</label> 
                <div class="input-group" id="respuestaInputGroup"> 
                  <span class="input-group-addon">
                    @if( old('respuestaCheckbox') !== null )
                      <input type="checkbox" id="respuestaCheckbox" name="respuestaCheckbox" aria-label="Checkbox for following text input" checked>
                    @else
                      @if( isset($solicitudrespuesta) ) 
                        <input type="checkbox" id="respuestaCheckbox" name="respuestaCheckbox" aria-label="Checkbox for following text input" checked> 
                      @else 
                        <input type="checkbox" id="respuestaCheckbox" name="respuestaCheckbox" aria-label="Checkbox for following text input"> 
                      @endif 
                    @endif
                  </span>
                  @if( isset($solicitudrespuesta) )
                    <input type="date" id="fechaRespuestaInput" name="fechaRespuestaInput" value="{{ old('fechaRespuestaInput', $solicitudrespuesta->fecha_limite) }}" class="form-control" disabled>
                  @else 
                    <input type="date" id="fechaRespuestaInput" name="fechaRespuestaInput" value="{{ old('fechaRespuestaInput') }}" class="form-control" disabled>
                  @endif
                </div> 
              </div> 
 
              <div class="form-group col-lg-4"> 
                <label for="respuesta">Solicita</label> 
                <div class="input-group" id="respuesta"> 
                  <span class="input-group-addon">
                    @if( old('citaCheckbox') !== null ) 
                      <input type="checkbox" name="citaCheckbox" aria-label="..." checked> 
                    @else
                      @if( isset($solicitudcita) ) 
                        <input type="checkbox" name="citaCheckbox" aria-label="..." checked> 
                      @else 
                        <input type="checkbox" name="citaCheckbox" aria-label="..."> 
                      @endif
                    @endif
                  </span> 
                  <span class="form-control" id="basic-addon2">CITA</span>
                </div> 
              </div> 
            </div> 
 
            <div class="form-row"> 
              <div class="form-group col-md-12"> 
                <label for="suscriptorInput">Suscriptor</label> 
                <input type="text" name="suscriptorInput" id="suscriptorInput" value="{{ old('suscriptorInput', $documento->nombre) }}" class="form-control" placeholder="SUSCRIPTOR" style="text-transform:uppercase" onkeypress="return Restriccion.Documento.suscriptorinput(event);" required> 
                <small class="form-text text-info" id="nombreSmall" hidden><span class="glyphicon glyphicon-info-sign"></span> Sólo palabra</small> 
              </div> 
            </div> 
 
            <div class="form-row"> 
              <div class="form-group col-md-4"> 
                <label for="cargodependenciaFormRow">Cargo</label> 
                <input type="text" id="cargoInput" name="cargoInput" value="{{ old('cargoInput', $documento->cargo) }}" class="form-control" placeholder="CARGO" style="text-transform:uppercase" onkeypress="return Restriccion.Documento.cargo(event);" required> 
                <small class="form-text text-info" id="nombreSmall" hidden><span class="glyphicon glyphicon-info-sign"></span> Sólo palabra</small> 
              </div> 
 
              <div class="form-group col-md-8"> 
                <label for="dependenciaInputGroup">Dependencia</label> 
                <div class="input-group" id="dependenciaInputGroup"> 
                  <div class="input-group-addon">Del</div> 
                  <input type="text" id="dependenciaInput" name="dependenciaInput" value="{{ old('dependenciaInput', $documento->dependencia) }}" class="form-control" placeholder="DEPENDENCIA" required> 
                </div> 
                 
              </div> 
            </div> 
 
            <div class="form-row"> 
              <div class="form-group col-md-12"> 
                <div id="mapDiv" style="height: 230px;" class="form-control"></div> 
                <input type="hidden" id="placeGeometryInput" name="placeGeometryInput" value="{{ old('placeGeometryInput', $documento->geometry_dependencia) }}"> 
                <input type="hidden" id="placeIconInput" name="placeIconInput" value="{{ old('placeIconInput', $documento->icon_dependencia) }}"> 
                <input type="hidden" id="placeAddressInput" name="placeAddressInput" value="{{ old('placeAddressInput', $documento->address_dependencia) }}"> 
                <input type="hidden" id="postalCodeInput" name="postalCodeInput" value="{{ old('postalCodeInput', $documento->postal_code) }}"> 
              </div> 
            </div>

            @if( isset($documento->ruta_pdf) )
            <div class="form-row" id="documentoPDFDiv">
            @else
            <div class="form-row" id="documentoPDFDiv" hidden>
            @endif
              <div class="form-group col-lg-12">
                <label>Documento</label>
                <div class="input-group"> 
                  <span class="input-group-addon"> 
                    <input type="checkbox" id="cambiarPDFCheckbox" name="cambiarPDFCheckbox" onchange="_eliminarPDF();" aria-label="Checkbox for following text input"> 
                  </span> 

                  <input type="text" value="CAMBIAR" class="form-control" readonly> 
 
                  <div class="input-group-btn"> 
                    <button type="button" id="pdfDropdownButton" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                      <i class="glyphicon glyphicon-option-vertical"></i> PDF <span class="caret"></span> 
                    </button> 
                    <ul class="dropdown-menu">
                      @if( isset($documento->ruta_pdf) )
                      <li><a href="javascript:_verPDF( '{{$documento->ruta_pdf}}' );"><i class="glyphicon glyphicon-eye-open"></i> VER</a></li> 
                      <li><a href="{{$documento->ruta_pdf}}" download><i class="glyphicon glyphicon-download-alt"></i> DESCARGAR</a></li> 
                      @endif
                    </ul> 
                  </div> 
                  
                </div>
              </div> 
            </div> 

            @if( isset($documento->ruta_pdf) )
            <div class="form-row" id="crearPDFDiv" hidden>
            @else
            <div class="form-row" id="crearPDFDiv">
            @endif
              <div class="form-group col-md-12">
                <label>Crear PDF</label>
                <div class="drop" id="dropTarget">
                  <!-- DRAG AND DROP AREA -->
                  <span class="btn btn-default" id="browseButton">Elegir Imagen</span>
                  <span class="btn btn-default" onclick="scanToJpg();">Escanear</span>
                  <strong>O</strong>
                  Arrastre y suelte sus imagenes aquí
                </div>
              </div>

              <div class="form-group col-lg-12" id="album">
                <!-- ALBUM AREA -->
              </div>

              <div class="form-group col-lg-12">
                <div class="input-group-btn"> 
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                    <i class="glyphicon glyphicon-option-vertical"></i> PDF <span class="caret"></span> 
                  </button> 
                  <ul class="dropdown-menu">
                    <li><a href="javascript:_verPDF();">VER</a></li> 
                    <li><a href="javascript:_descargarPDF();">DESCARGAR</a></li> 
                  </ul> 
                </div>
              </div>
            </div>

            @if( isset($documento->ruta_pdf) )
            <div class="form-row" id="elegirPDFDiv" hidden>
            @else
            <div class="form-row" id="elegirPDFDiv">
            @endif
              <div class="form-group col-md-12"> 
                <label for="pdfInputFile">Elegir PDF</label> 
                <input type="file" class="form-control-file" id="pdfInputFile" name="pdfInputFile" accept="application/pdf" aria-describedby="fileHelp">
                <!-- 
                <small id="fileHelp" class="form-text text-muted">Seleccione el archivo *.pdf correspondiente.</small> 
                -->                  
              </div> 
            </div>

            <div class="form-row">
              <div class="form-group text-center col-lg-12"> 
                <button type="submit" class="btn btn-primary"> 
                  Actualizar documento 
                </button> 
              </div>
            </div> 
             
          </form> 
 
        </div> 
      </div> 
    </div> 

  </div>

  <!-- The Modal -->
  <div class="modal fade" id="viewPDFModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">PDF
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </h4>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="iframe-container">
            <iframe id="viewPDFIframe"></iframe>
          </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <!--
          <button type="button" class="btn btn-primary" id="acceptButton">Aceptar</button>
          <button type="button" class="btn btn-secondary" id="cancelButton">Cacelar</button>
          -->
        </div>

      </div>
    </div>
  </div>

  <!-- Scanner JS -->
  <script src="{{ asset("vendor/scanner.js/dist/scanner.js") }}"></script>
  
  <!-- Flow JS -->
  <script src="{{ url("vendor/flowjs-2.13.0/dist/flow.min.js") }}"></script>

  <!-- PDFMake JS -->
  <script src={{ url("/vendor/pdfmake-0.1.35/build/pdfmake.min.js") }}></script>
  <script src={{ url("/vendor/pdfmake-0.1.35/build/vfs_fonts.js") }}></script>

  <!-- Flow CSS -->
  <link href="{{ url("vendor/flowjs-2.13.0/dist/flow.css") }}" rel="stylesheet">
  
  <!-- MODAL PDF CSS-->
  <style type="text/css">
    .iframe-container {    
      padding-bottom: 60%;
      padding-top: 30px; height: 0; overflow: hidden;
    }

    .iframe-container iframe,
    .iframe-container object,
    .iframe-container embed {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
    }
  </style>

  <script> 
    function initMap() { 
      var map = new google.maps.Map(document.getElementById("mapDiv"), { 
        center: {lat: 17.0606433, lng: -96.7319102}, 
        zoom: 13 
      }); 
 
      var input = /** @type {!HTMLInputElement} */( 
        document.getElementById("dependenciaInput")); 

      var autocomplete = new google.maps.places.Autocomplete(input); 
      autocomplete.bindTo("bounds", map); 

      var infowindow = new google.maps.InfoWindow(); 
      var marker = new google.maps.Marker({ 
        map: map, 
        anchorPoint: new google.maps.Point(0, -29) 
      }); 

      autocomplete.addListener("place_changed", function() { 
        infowindow.close(); 
        marker.setVisible(false); 
        var place = autocomplete.getPlace(); 
        if (!place.geometry) { 
          // User entered the name of a Place that was not suggested and 
          // // pressed the Enter key, or the Place Details request failed. 
          window.alert("No details available for input: " + place.name + ""); 
          return; 
        } 

        // If the place has a geometry, then present it on a map. 
        if (place.geometry.viewport) { 
          map.fitBounds(place.geometry.viewport); 
        } else { 
          map.setCenter(place.geometry.location); 
          map.setZoom(17);  // Why 17? Because it looks good. 
        } 
        marker.setIcon(/** @type {google.maps.Icon} */({ 
          url: place.icon, 
          size: new google.maps.Size(71, 71), 
          origin: new google.maps.Point(0, 0), 
          anchor: new google.maps.Point(17, 34), 
          scaledSize: new google.maps.Size(35, 35) 
        })); 
        marker.setPosition(place.geometry.location); 
        marker.setVisible(true); 

        var address = "";
        var postal_code = [];
        if (place.address_components) {
          postal_code = place.address_components.filter( function(_item){
            return _item.types[0].localeCompare("postal_code") === 0;
          });

          address = [ 
            (place.address_components[0] && place.address_components[0].short_name || ""), 
            (place.address_components[1] && place.address_components[1].short_name || ""), 
            (place.address_components[2] && place.address_components[2].short_name || "") 
          ].join(" "); 
        } 

        infowindow.setContent("<div><strong>" + place.name + "</strong><br>" + address); 
        infowindow.open(map, marker); 

        document.getElementById("placeGeometryInput").value = JSON.stringify(place.geometry); 
        document.getElementById("placeIconInput").value = place.icon; 
        document.getElementById("placeAddressInput").value = address; 
        document.getElementById("postalCodeInput").value = ((postal_code.length === 0) ? "" : postal_code[0].short_name);
        
        input.value = place.name; 
      });

      //-----[ ESTADO ACTUAL SUSCRIPTOR ]----------------------------------------------------- 
      var _nombreDependencia = "{{ old('dependenciaInput', $documento->dependencia) }}"; 
      var _geometryDependencia = <?php echo json_encode( old('placeGeometryInput', $documento->geometry_dependencia) ); ?>; 
      var _iconDependencia = "{{ old('placeIconInput', $documento->icon_dependencia) }}"; 
      var _addressDependencia = "{{ old('placeAddressInput', $documento->address_dependencia) }}";

      if( _geometryDependencia !== null ){
        var _geometryDependenciaJSON = JSON.parse(_geometryDependencia);

        // If the place has a geometry, then present it on a map. 
        if (_geometryDependenciaJSON.viewport) { 
          map.fitBounds(_geometryDependenciaJSON.viewport); 
        } else { 
          map.setCenter(_geometryDependenciaJSON.location); 
          map.setZoom(17);  // Why 17? Because it looks good. 
        } 
        marker.setIcon(/** @type {google.maps.Icon} */({ 
          url: _iconDependencia, 
          size: new google.maps.Size(71, 71), 
          origin: new google.maps.Point(0, 0), 
          anchor: new google.maps.Point(17, 34), 
          scaledSize: new google.maps.Size(35, 35) 
        })); 
        marker.setPosition(_geometryDependenciaJSON.location); 
        marker.setVisible(true); 

        infowindow.setContent('<div><strong>' + _nombreDependencia + '</strong><br>' + _addressDependencia); 
        infowindow.open(map, marker);
      }
      //-----[ FIN ESTADO ACTUAL SUSCRIPTOR ]--------------------------------------------------  
    } 
  </script> 
 
  <!-- 
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADnzxwMdxbgRQvQRu0f_a57o-G5y3SxlI&libraries=places&callback=initMap"></script> 
  --> 
  
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADnzxwMdxbgRQvQRu0f_a57o-G5y3SxlI&libraries=places&callback=initMap" 
        async defer></script>   
 
  <script src={{ url("/js/Restriccion.js") }}></script> 
 
  <script type="text/javascript"> 
    var _inputFilePdf = document.getElementById("pdfInputFile"); 
    _inputFilePdf.onchange = function(){
      if( _inputFilePdf.files[0].type !== "application/pdf" ){
        _inputFilePdf.value = "";
      }
    } 
 
    var _checkboxRespuesta = document.getElementById("respuestaCheckbox"); 
    var _inputDateFechaRespuesta = document.getElementById("fechaRespuestaInput");

    var _mindate = new Date(); 
    var _maxdate = new Date(); 
    var _i = 0;
    while( _i < 15 ){
      if( _maxdate.getDay() != 0 && _maxdate.getDay() != 6 ){
        _i = _i + 1;
      }
      _maxdate.setDate(_maxdate.getDate() + 1);
    }
     
    var _mindatestring = _mindate.toISOString().substring(0, 10); 
    var _maxdatestring = _maxdate.toISOString().substring(0, 10);
    _inputDateFechaRespuesta.setAttribute("min", _mindatestring); 
    _inputDateFechaRespuesta.setAttribute("max", _maxdatestring);

    _inputDateFechaRespuesta.oninput = function (_event){
      var day = new Date( _event.target.value ).getUTCDay();
      // Days in JS range from 0-6 where 0 is Sunday and 6 is Saturday
      if( day == 0 || day == 6 ){
        console.warn("SOLO DIAS HÁBILES");
      }
    }
    
    _checkboxRespuesta.onchange = function(){
      if( _checkboxRespuesta.checked ){
        _inputDateFechaRespuesta.setAttribute("required", ""); 
        _inputDateFechaRespuesta.removeAttribute("disabled");
      }else{
        _inputDateFechaRespuesta.value = "";
        _inputDateFechaRespuesta.setAttribute("disabled", "");
        _inputDateFechaRespuesta.removeAttribute("required"); 
      }
    }; 
 
    var _autocompleteNombreSuscriptor = new autoComplete({ 
      selector: "#suscriptorInput", 
      minChars: 3, 
      source: function(term, response){ 
        var _consultaResponse = function( _data ){ 
          var _sugerencias = []; 
          _data.forEach(  
            function(item, index) { 
              _sugerencias.push(item.nombre.toUpperCase()); 
            } 
          ); 

          response( _sugerencias ); 
        }; 

        $.ajax({ 
          headers: { 
            "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content") 
          }, 
          type: "POST", 
          url: <?php echo json_encode( url("api/suscriptor/likeNombre") ); ?>,
          data: JSON.stringify ( {"palabra": term} ), 
          success: _consultaResponse, 
          contentType: "application/json", 
          dataType: "json" 
        }); 
      } 
    }); 
 
    var _autocompleteCargoSuscriptor = new autoComplete({ 
      selector: "#cargoInput", 
      minChars: 3, 
      source: function(term, response){ 

        var _consultaResponse = function( _data ){ 
          var _sugerencias = []; 
          _data.forEach(  
            function(item, index) { 
              _sugerencias.push(item.cargo.toUpperCase()); 
            } 
          ); 

          response( _sugerencias ); 
        };

        $.ajax({ 
          headers: { 
            "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content") 
          }, 
          type: "POST", 
          url: <?php echo json_encode( url("api/suscriptor/likeCargo") ); ?>, 
          data: JSON.stringify ( {"palabra": term} ), 
          success: _consultaResponse, 
          contentType: "application/json", 
          dataType: "json" 
        }); 
      } 
    }); 

    var _divBtnGroup = document.createElement("div"); 
    _divBtnGroup.setAttribute("id", "btnGroupDiv"); 
    _divBtnGroup.setAttribute("class", "btn-group btn-group-xs"); 
    _divBtnGroup.setAttribute("role", "group"); 
 
    var _selectTipo = document.getElementById("tipo"); 
    var _otroTipoCheckbox = document.getElementById("otroTipoCheckbox"); 
    var _otroTipoInput = document.getElementById("otroTipoInput"); 
    _otroTipoCheckbox.onchange = function(){ 
      if( _otroTipoCheckbox.checked ){ 
        _otroTipoInput.removeAttribute("disabled"); 
        _selectTipo.setAttribute("disabled", ""); 
        _otroTipoInput.setAttribute("required", "");
      }else{ 
        _otroTipoInput.setAttribute("disabled", ""); 
        _otroTipoInput.value = ""; 
        _selectTipo.removeAttribute("disabled");
        _otroTipoInput.removeAttribute("required");
      } 
    }; 

    var _checkboxCambiarPDF = document.getElementById("cambiarPDFCheckbox");
    var _eliminarPDF = function(){
      var _divCrearPDF = document.getElementById("crearPDFDiv");
      var _divElegirPDF = document.getElementById("elegirPDFDiv");
      var _buttonPdfDropdown = document.getElementById("pdfDropdownButton");
      var _divDocumentoPDF = document.getElementById("documentoPDFDiv");

      if( !_divDocumentoPDF.hasAttribute("hidden") ){
        if( _checkboxCambiarPDF.checked ){
          _divCrearPDF.removeAttribute("hidden");
          _divElegirPDF.removeAttribute("hidden");
          _buttonPdfDropdown.setAttribute("disabled", "");
        }else{
          _divCrearPDF.setAttribute("hidden", "");
          _divElegirPDF.setAttribute("hidden", "");
          _buttonPdfDropdown.removeAttribute("disabled");
        }
      }     
    }

    // Crear un nuevo evento 'change'
    var _event = new Event('change'); 
 
    // Despacharlo 
    _otroTipoCheckbox.dispatchEvent(_event); 
    _checkboxRespuesta.dispatchEvent(_event);
    _checkboxCambiarPDF.dispatchEvent(_event);
  </script>

  <script>
    //------------------[ FlowJS ]----------------------------------------------
    var _flow = new Flow({
      //allowDuplicateUploads: true
    });

    //---------------------------------------------------------------------------
    var base64Images = [];
    var key = 0;

    var removeImg = function(key){
      $("#thumImg"+key).remove();
      base64Images = base64Images.filter(function(item) { 
        if(item.id !== key){
          return true;
        }else{
          _flow.removeFile(_flow.getFromUniqueIdentifier(item.ukid));
          return false;
        }
      });
    }

    var addImgToAlbum = function( _imgB64, _uniqueIdentifier){
      key = key + 1;
      if(_uniqueIdentifier){
        base64Images.push({id: key, img: _imgB64, ukid: _uniqueIdentifier});
      }else{
        base64Images.push({id: key, img: _imgB64});
      }
      var _imagen = $("<div></div>")
              .attr({id: "thumImg"+key})
              .addClass("gallery-box")
              .append($("<div></div>")
                  .addClass("thumbnail")
                  .append($("<img/>")
                    .attr({ src : _imgB64})
                  )
              )
              .append($("<div></div>")
                .addClass("btn-group")
                .append($("<a></a>")
                  .append($("<i></i>")
                    .addClass("glyphicon glyphicon-trash")
                  )
                  .addClass("btn btn-xs btn-danger")
                  .attr({onclick: "removeImg("+key+")"})
                )
              )
      ;
      $("#album").append(_imagen);
    }

    //------------------[ FlowJS ]----------------------------------------------
    // Flow.js isn"t supported, fall back on a different method
    if(!_flow.support) location.href = "/some-different-method";

    _flow.assignBrowse(document.getElementById("browseButton"), false, true, {accept: "image/*"});
    _flow.assignDrop(document.getElementById("dropTarget"));

    $("#dropTarget").on( "dragenter", function () {
      if( ! $("#dropTarget").hasClass("drag-over") ){
        $("#dropTarget").addClass("drag-over"); 
      }
    });

    $("#dropTarget").on( "dragover", function () {
      if( ! $("#dropTarget").hasClass("drag-over") ){
        $("#dropTarget").addClass("drag-over"); 
      }
    });

    $("#dropTarget").on( "drop", function () {
      $("#dropTarget").removeClass("drag-over");
    });

    $("#dropTarget").on( "dragleave", function () {
      $("#dropTarget").removeClass("drag-over");
    });

    _flow.on("fileAdded", function(file, event){
      //----[ VALIDACION ]-----------------------------------
      //if (file.type.startsWith("image/")){ continue }
      if( file.file.type.startsWith("image/") ){
        var reader = new FileReader();
        reader.readAsDataURL(file.file);
        reader.onload = function () {
          addImgToAlbum(reader.result, file.uniqueIdentifier);
        };

        reader.onerror = function (error) {
          console.error("Error: ", error);
        };
      }else{  //---[ REMOVE ]--------------------------------
        _flow.removeFile(_flow.getFromUniqueIdentifier(file.uniqueIdentifier));
      }
    });

    _flow.on("fileError", function(file, message){
      console.error(file, message);
    });
    
    //------------------[ ScannerJS ]-------------------------------------------
    /** Scan: output JPG original and JPG thumbnails */
    function scanToJpg(){
      scanner.scan(
        displayImagesOnPage,
        {
          "twain_cap_setting" : {
            "ICAP_PIXELTYPE" : "TWPT_GRAY", // Color:TWPT_RGB B/N:TWPT_BW
            "ICAP_XRESOLUTION" : "200", // DPI: 100
            "ICAP_YRESOLUTION" : "200",
            "ICAP_SUPPORTEDSIZES" : "TWSS_USLETTER" // Paper size: TWSS_USLETTER, TWSS_A4, ...
          },
          "output_settings": [
            {
              "type": "return-base64",
              "format": "jpg"
            }
          ],
          "i18n": {
            "lang": "es" /** en (default) | de | es | fr | it | ja | pt | zh | user (user"s OS locale) */
          },
        },
        false,
        false
      );
    }

    /** Processes the scan result */
    function displayImagesOnPage(successful, mesg, response) {
      if(!successful) { // On error
        console.error("Failed: " + mesg);
        return;
      }

      if(successful && mesg != null && mesg.toLowerCase().indexOf("user cancel") >= 0) { // User cancelled.
        console.info("User cancelled");
        return;
      }

      var scannedImages = scanner.getScannedImages(response, true, false); // returns an array of ScannedImage
      for(var i = 0; (scannedImages instanceof Array) && i < scannedImages.length; i++) {
        var scannedImage = scannedImages[i];
        processScannedImage(scannedImage);
      }      
    }

    /** Processes an original */
    function processScannedImage(scannedImage) {
      addImgToAlbum(scannedImage.src);
    }

    //------------------[ PDFMake ]-------------------------------------------
    var docDefinition = {
      info: {
        title: "sca_documento",
        author: "SCA-DAM",
        subject: "copia digital",
        keywords: "SCA-DAM",
      },
      pageSize: "LETTER",
      pageMargins: 10,
      pageBreakBefore: function(currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage) {
        return currentNode.headlineLevel === 1 && followingNodesOnPage.length === 0;
      }
    };

    var _verPDF = function( _urlPDF ){
      if( _urlPDF !== undefined ){
        const _iframeView = document.getElementById("viewPDFIframe");
        _iframeView.src = _urlPDF;
        $("#viewPDFModal").modal("show");
      }else{
        if( base64Images.length > 0 ){
          var _arrayContent = [];
          base64Images.forEach(function(element) {
            _arrayContent.push( { image: element.img, width: 593} );
          });

          docDefinition.content = _arrayContent;
          const pdfDocGenerator = pdfMake.createPdf(docDefinition);
          
          //--------------[ DataUrl ]--------------------------------------
          pdfDocGenerator.getDataUrl((dataUrl) => {
            const _iframeView = document.getElementById("viewPDFIframe");
            _iframeView.src = dataUrl;
            $("#viewPDFModal").modal("show");
          });
        }
      }
    }

    var _descargarPDF = function(){
      if( base64Images.length > 0 ){
        var _arrayContent = [];
        base64Images.forEach(function(element) {
          _arrayContent.push( { image: element.img, width: 593} );
        });

        docDefinition.content = _arrayContent;
        const pdfDocGenerator = pdfMake.createPdf(docDefinition);

        //--------------[ Download PDF ]--------------------------------------
        pdfDocGenerator.download("sca_documento.pdf");
      }
    }    
  </script>
 
  <!-- BootstrapValidator Script -->
  <script type="text/javascript">
    $(document).ready(function() {
      $("#editform").bootstrapValidator({
        message: "No valido",
        fields: {
          numOficio: {
            validators: {
              notEmpty: {
                message: "Requerido"
              },
              stringLength: {
                min: 1,
                max: 30,
                message: "Longitud 2-30"
              }
            }
          },
          fechaOficio: {
            validators: {
              notEmpty: {
                message: "Requerido"
              },
              date: {
                format: 'DD/MM/YYYY',
                message: 'No valido'
              }
            }
          },
          numHojas: {
            validators: {
              notEmpty: {
                message: "Requerido"
              },
              digit: {
                message: 'No valido'
              }
            }
          },
          otroTipoInput: {
            validators: {
              callback: {
                message: 'No valido',
                callback: function (value, validator, $field) {
                  // Determine the numbers which are generated in captchaOperation
                  if($("#otroTipoCheckbox")[0].checked){
                    var _regexp = /^[a-z,.'-]+$/i;
                    return _regexp.test(value);
                  }else{
                    return true;
                  }
                }
              }
            }
          },
          numAnexos: {
            validators: {
              notEmpty: {
                message: "Requerido"
              },
              digit: {
                message: 'No valido'
              }
            }
          },
          asunto: {
            validators: {
              notEmpty: {
                message: "Requerido"
              },
              stringLength: {
                min: 6,
                max: 250,
                message: "Longitud 6-250"
              }
            }
          },
          fechaRespuestaInput: {
            validators: {
              notEmpty: {
                message: "Requerido"
              },
              date: {
                format: 'DD/MM/YYYY',
                message: 'No valido'
              }
            }
          },
          suscriptorInput: {
            validators: {
              notEmpty: {
                message: "Requerido"
              }
            }
          },
          cargoInput: {
            validators: {
              notEmpty: {
                message: "Requerido"
              },
              stringLength: {
                min: 3,
                max: 25,
                message: "Longitud 3-25"
              }
            }
          },
          dependenciaInput: {
            validators: {
              notEmpty: {
                message: "Requerido"
              }
            }
          }
        }
      });
    });
  </script>
   
@stop