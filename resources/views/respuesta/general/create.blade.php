@extends('layouts.master-general') 
 
@section('content') 

  <div class="row">
    <nav aria-label="breadcrumb" style="margin-top: 10px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/reportesrevisiones/lista')}}">Documentos Turnados - Revisados</a></li>
        <li class="breadcrumb-item" aria-current="page">Responder</li>
      </ol>
    </nav>
  </div>

  <div class="row">
    <div class="col-lg-12">
      @if ( session('message') ) 
        <div class="alert alert-success alert-dismissable"> 
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a> 
          <strong>EXITO: </strong> {{ session()->pull('message', 'default') }} 
        </div> 
      @endif 
    </div>
  </div>
  <!-- /.row -->

  <div class="row" id="errorRowDiv">
    <div class="col-lg-12">
      <div class="panel panel-danger panel-collapse collapse" id="errorPanelDiv">
        <div class="panel-heading">
          <strong>ERROR:</strong>
          <a data-toggle="collapse" href="#errorPanelDiv" class="close">&times</a>
        </div>
        <div class="panel-body" id="errorPanelBodyDiv">
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->
 
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default"> 

        <div class="panel-heading lead"> 
          <i class="glyphicon glyphicon-copy"></i> Respuesta documento
        </div> 
   
        <div class="panel-body"> 
          <div class="panel-group" id="accordion">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <i class="glyphicon glyphicon-file"></i>
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseDocumento">Documento</a>
                </h4>
              </div>
              <div id="collapseDocumento" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="row"> 
                    <div class="col-lg-12 col-md-12"> 
                      <div class="row"> 
                        
                        @include('documento.partials.archivovisualizacion')
           
                        <div class="col-lg-8 col-md-8"> 
                          <ul class="nav nav-tabs"> 
                            <li class="active"><a data-toggle="tab" href="#Summery" class="text-info"><i class="fa fa-indent"></i> Resumen</a></li> 
                            <li><a data-toggle="tab" href="#TabRespuesta" class="text-info"><i class="fa fa-calendar"></i> Respuesta</a></li>
                          </ul> 
           
                          <div class="tab-content"> 
                            <div id="Summery" class="tab-pane fade in active"> 
                              <div class="table-responsive panel"> 
                                 
                                <table class="table"> 
                                  <tbody> 
                                     
                                    <tr> 
                                      <td class="text-info"><i class="glyphicon glyphicon-list-alt"></i> No. Oficio</td> 
                                      <td>{{ strtoupper($documento->num_oficio) }}</td> 
                                    </tr> 
           
                                    <tr> 
                                      <td class="text-info"><i class="glyphicon glyphicon-file"></i> Tipo de documento</td> 
                                      <td>{{ strtoupper($documento->tipo) }}</td> 
                                    </tr> 
           
                                    <tr> 
                                      <td class="text-info"><i class="glyphicon glyphicon-inbox"></i> No. Hojas</td> 
                                      <td>{{ $documento->num_hojas }}</td> 
                                    </tr> 

                                    <tr> 
                                      <td class="text-info"><i class="glyphicon glyphicon-paperclip"></i> No. Anexos</td> 
                                      <td>{{ $documento->num_anexos }}</td> 
                                    </tr> 

                                    <tr> 
                                      <td class="text-info"><i class="glyphicon glyphicon-th"></i> Actividad</td> 
                                      <td>{{ $documento->actividad }}</td> 
                                    </tr>
           
                                    <tr> 
                                      <td class="text-info" colspan="2"><i class="glyphicon glyphicon-info-sign"></i> Asunto</td> 
                                    </tr> 
                                  </tbody> 
                                </table> 
           
                                <blockquote class="blockquote" style="font-size:15px;"> 
                                  <p>{{ $documento->asunto }}</p> 
                                </blockquote> 
                                 
                              </div> 
                            </div> 

                            <div id="TabRespuesta" class="tab-pane fade"> 
                              <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="form-row"> 
                                      <div class="col-lg-4 col-md-4"> 
                                        <center> 
                                          <div class="col-lg-12 col-md-12">
                                            <div class="panel panel-default">
                                              <div class="panel-heading">
                                                <div class="row">
                                                  <div class="col-xs-12">
                                                    <div id="countdown" class="percircle animate gt50"></div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </center> 
                                      </div>

                                      <div class="form-group col-md-8"> 
                                        <label for="respuestaInputGroup">Solicitud respuesta</label> 
                                        <input type="text" id="fechaRespuestaInput" name="fechaRespuestaInput" value="{{ old('fechaRespuestaInput', isset($solicitudrespuesta) ? $solicitudrespuesta->fecha_limite : '') }}" placeholder="NO REQUIERE RESPUESTA" class="form-control" disabled>
                                      </div>
                                    </div>
                                </div>
                              </div> 
                            </div>
                          </div> 
                        </div> 
                      </div> 
                    </div> 
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                Respuesta
              </h4>
            </div>
            <div class="panel-body">

              <form enctype="multipart/form-data" method="POST" id="respuestaform"> 
           
                {{ csrf_field() }} 

                @if( isset($documentorespuesta) )
                  <div class="form-row"> 
                    <div class="form-group col-md-3"> 
                      <label for="numOficio">No. Oficio</label> 
                      <input type="text" name="numOficio" id="numOficio" value="{{ old('numOficio', $documentorespuesta->num_oficio) }}" class="form-control" placeholder="NO. OFICIO" style="text-transform:uppercase" readonly>
                    </div> 

                    <div class="form-group col-md-3"> 
                      <label for="fechaOficio">Fecha Del Oficio</label>
                      <input type="text" id="fechaOficio" name="fechaOficio" value="{{ $documentorespuesta->fecha_oficio }}" placeholder="Seleccione fecha" class="form-control" readonly>
                    </div>
       
                    <div class="form-group col-md-3"> 
                      <label for="numHojas">No. De Hojas</label> 
                      <input type="text" name="numHojas" id="numHojas" value="{{ old('numHojas') }}" class="form-control" placeholder="No. De Hojas" required>
                    </div> 

                    <div class="form-group col-md-3"> 
                      <label for="numAnexos">No. De Anexos</label> 
                      <input type="text" name="numAnexos" id="numAnexos" value="{{ old('numAnexos') }}" class="form-control" placeholder="No. De Anexos" required>
                    </div> 
                  </div>

                  <div class="form-row"> 
                    <div class="form-group col-md-12"> 
                      <label for="asunto">Asunto</label> 
                      <textarea name="asunto" id="asunto" class="form-control" rows="3" placeholder="Asunto del documento" readonly required>{{ old('asunto', $documentorespuesta->asunto) }}</textarea>
                    </div> 
                  </div>  
       
                  <div class="form-row"> 
                    <div class="form-group col-md-12"> 
                      <label for="suscriptorInput">Dirigido a:</label> 
                      <input type="text" name="suscriptorInput" id="suscriptorInput" value="{{ old('suscriptorInput', $documentorespuesta->suscriptor) }}" class="form-control" placeholder="SUSCRIPTOR" style="text-transform:uppercase" readonly>
                    </div> 
                  </div> 
       
                  <div class="form-row"> 
                    <div class="form-group col-md-4"> 
                      <label for="cargodependenciaFormRow">Cargo</label> 
                      <input type="text" id="cargoInput" name="cargoInput" value="{{ old('cargoInput', $documentorespuesta->cargo) }}" class="form-control" placeholder="CARGO" style="text-transform:uppercase" readonly>
                    </div> 
       
                    <div class="form-group col-md-8"> 
                      <label for="dependenciaInputGroup">Dependencia</label> 
                      <div class="input-group" id="dependenciaInputGroup"> 
                        <div class="input-group-addon">Del</div> 
                        <input type="text" id="dependenciaInput" name="dependenciaInput" value="{{ old('dependenciaInput', $documentorespuesta->dependencia) }}" class="form-control" placeholder="DEPENDENCIA" readonly> 
                      </div>
                    </div> 
                  </div> 

                  <div class="form-row">
                    <div class="form-group col-lg-6">
                      <div class="input-group-btn"> 
                        <button type="button" id="fechaRespuestaButton" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                          <i class="fa fa-file-word-o fw"></i> WORD <span class="caret"></span> 
                        </button> 
                        <ul class="dropdown-menu">
                          <li><a href="javascript:_descargarWord();">DESCARGAR</a></li> 
                        </ul> 
                      </div>
                    </div>
                  </div>

                  <div class="form-row"> 
                    <div class="form-group col-md-12"> 
                      <label for="wordInputFile">Elegir word</label> 
                      <input type="file" class="form-control-file" id="wordInputFile" name="wordInputFile" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document" required>
                      <small class="form-text text-danger" id="fileSmall" hidden><span class="glyphicon glyphicon-ok"></span> </small> 
                    </div> 
                  </div>

                  <div class="form-row">
                    <div class="form-group text-center col-lg-12"> 
                      <button type="submit" class="btn btn-outline btn-primary"> 
                        <i class="glyphicon glyphicon-floppy-open"></i> Guardar
                      </button> 
                    </div>
                  </div>
                @else
                  <div class="form-row"> 
                    <div class="form-group col-md-3"> 
                      <label for="numOficio">No. Oficio</label> 
                      <input type="text" name="numOficio" id="numOficio" value="{{ old('numOficio') }}" class="form-control" placeholder="NO. OFICIO" style="text-transform:uppercase" readonly>
                    </div> 

                    <div class="form-group col-md-3"> 
                      <label for="fechaOficio">Fecha Del Oficio</label>
                      <input type="text" id="fechaOficio" name="fechaOficio" value="{{ old('fechaOficio', $rangofecha['fecha_inicio']) }}" placeholder="Seleccione fecha" class="form-control" readonly required>
                    </div>
       
                    <div class="form-group col-md-3"> 
                      <label for="numHojas">No. De Hojas</label> 
                      <input type="text" name="numHojas" id="numHojas" value="{{ old('numHojas') }}" class="form-control" placeholder="No. De Hojas" readonly>
                      <small class="form-text text-info" id="nombreSmall" hidden><span class="glyphicon glyphicon-info-sign"></span> Sólo números</small> 
                    </div> 

                    <div class="form-group col-md-3"> 
                      <label for="numAnexos">No. De Anexos</label> 
                      <input type="text" name="numAnexos" id="numAnexos" value="{{ old('numAnexos') }}" class="form-control" placeholder="No. De Anexos" readonly>
                      <small class="form-text text-info" id="nombreSmall" hidden><span class="glyphicon glyphicon-info-sign"></span> Sólo numeros</small> 
                    </div> 
                  </div>
                  
                  <div class="form-row"> 
                    <div class="form-group col-md-12"> 
                      <label for="asunto">Asunto</label> 
                      <textarea name="asunto" id="asunto" class="form-control" rows="3" placeholder="Asunto del documento" required>{{ old('asunto') }}</textarea>
                      <small class="form-text text-info" id="nombreSmall" hidden><span class="glyphicon glyphicon-info-sign"></span> Limite alcanzado</small> 
                    </div> 
                  </div>  
       
                  <div class="form-row"> 
                    <div class="form-group col-md-12"> 
                      <label for="suscriptorInput">Dirigido a:</label> 
                      <input type="text" name="suscriptorInput" id="suscriptorInput" value="{{ old('suscriptorInput', $documento->suscriptor) }}" class="form-control" placeholder="SUSCRIPTOR" style="text-transform:uppercase" readonly> 
                      <small class="form-text text-info" id="nombreSmall" hidden><span class="glyphicon glyphicon-info-sign"></span> Sólo palabra</small> 
                    </div> 
                  </div> 
       
                  <div class="form-row"> 
                    <div class="form-group col-md-4"> 
                      <label for="cargodependenciaFormRow">Cargo</label> 
                      <input type="text" id="cargoInput" name="cargoInput" value="{{ old('cargoInput', $documento->cargo) }}" class="form-control" placeholder="CARGO" style="text-transform:uppercase" readonly> 
                      <small class="form-text text-info" id="nombreSmall" hidden><span class="glyphicon glyphicon-info-sign"></span> Sólo palabra</small> 
                    </div> 
       
                    <div class="form-group col-md-8"> 
                      <label for="dependenciaInputGroup">Dependencia</label> 
                      <div class="input-group" id="dependenciaInputGroup"> 
                        <div class="input-group-addon">Del</div> 
                        <input type="text" id="dependenciaInput" name="dependenciaInput" value="{{ old('dependenciaInput', $documento->dependencia) }}" class="form-control" placeholder="DEPENDENCIA" readonly> 
                      </div>
                    </div> 
                  </div>

                  <div class="form-row">
                    <div class="form-group text-center col-lg-12"> 
                      <button type="submit" class="btn btn-outline btn-primary"> 
                        <i class="fa fa-gears fw"></i> Generar
                      </button> 
                    </div>
                  </div>
                @endif
             
              </form>

            </div>
          </div>
        </div>
      </div> 
    </div>
    <!-- /.col-lg-12 -->
  </div>

  <!-- The Modal -->
  <div class="modal fade" id="confirmModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title" id="confirmModalTitle">Confirmar eliminación</h4>
        </div>

        <!-- Modal body -->
        <div class="modal-body alert alert-info" id="confirmModalBody">
          Esta operación puede causar la pérdida de información.
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" id="redirectButton" style="display:none;">Aceptar</button>
          <button type="button" class="btn btn-primary" id="acceptButton">Aceptar</button>
          <button type="button" class="btn btn-secondary" id="cancelButton">Cacelar</button>
        </div>

      </div>
    </div>
  </div>

  <!-- PercicleJS JS  -->
  <script src={{ url('/vendor/percicle-1.0.26/js/percircle.js') }}></script>
  <!-- Flatpickr JS   -->
  <script src={{ url('/vendor/flatpickr-4.2.4/js/flatpickr.min.js') }}></script>
  <script src={{ url('/vendor/flatpickr-4.2.4/l10n/es.js') }}></script>
  <!-- JSZip JS  -->
  <script src={{ url('/vendor/jszip-2.x/jszip.min.js') }}></script>
  <!-- Docxtemplater JS  -->
  <script src={{ url('/vendor/docxtemplater-3.2.4/docxtemplater.min.js') }}></script>
  <script src={{ url('/vendor/docxtemplater-3.2.4/vendor/jszip-utils.js') }}></script>
  <script src={{ url('/vendor/docxtemplater-3.2.4/vendor/file-saver.min.js') }}></script>
  <!--
    Mandatory in IE 6, 7, 8 and 9.
  -->
  <!--[if IE]>
    <script type="text/javascript" src={{ url('/vendor/docxtemplater-3.2.4/vendor/jszip-utils-ie.js') }}></script>
  <![endif]-->

  <!-- PercicleJS CSS -->
  <link href={{ url('/vendor/percicle-1.0.26/css/percircle.css') }} rel="stylesheet">
  <!-- Flatpickr CSS  -->
  <link href={{ url('/vendor/flatpickr-4.2.4/css/flatpickr.min.css') }} rel="stylesheet">
  <link href={{ url('/vendor/flatpickr-4.2.4/themes/airbnb.css') }} rel="stylesheet">

  <script>
    function getLocaleFechaOficioInput(){
      //var options = { weekday: "long", year: "numeric", month: "long", day: "numeric" };
      var options = { year: "numeric", month: "long", day: "numeric" };
      var _fechaSplitInput = document.getElementById("fechaOficio").value.split("-");
      var _fecha = new Date(_fechaSplitInput[2] + "-" + _fechaSplitInput[1] + "-" + _fechaSplitInput[0] );
      _fecha.setDate(_fecha.getDate() + 1);
      _fecha = _fecha.toLocaleDateString("es-mx", options);

      return _fecha.charAt(0).toUpperCase() + _fecha.slice(1);
    }

    var _wordInputFile = document.getElementById("wordInputFile");
    if(_wordInputFile !== null){
      _wordInputFile.onchange = function(event){
        var url = URL.createObjectURL(event.target.files[0]);
        loadFile(url, function(error,content){
          if (error) { throw error };
          const zip = new JSZip(content);
          const doc = new Docxtemplater().loadZip(zip);

          try {
            // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
            doc.render()
          }catch (error) {
            var e = {
              message: error.message,
              name: error.name,
              stack: error.stack,
              properties: error.properties,
            };
            console.log(JSON.stringify({error: e}));
            // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
            throw error;
          }
          var outputText = doc.getFullText();
          
          var _idDocxWord = "Oficio: " + document.getElementById("numOficio").value.trim() + "Asunto:";
          var _fileSmall = document.getElementById("fileSmall");
          if(outputText.indexOf(_idDocxWord) < 0){
            _fileSmall.textContent = "El archivo no coincide.";
            _fileSmall.removeAttribute("hidden");
            _wordInputFile.value = "";
            window.setTimeout(function() {
              _fileSmall.setAttribute("hidden","");
            }, 1000);
          }else{
            var _indiceInicial = outputText.indexOf("Asunto:") + 7;
            var _indiceFinal = outputText.indexOf(getLocaleFechaOficioInput());
            if( _indiceInicial === 6 || _indiceFinal < 0){
              _fileSmall.textContent = "El archivo no coincide.";
              _fileSmall.removeAttribute("hidden");
              _wordInputFile.value = "";
              window.setTimeout(function() {
                _fileSmall.setAttribute("hidden","");
              }, 1000);
            }else{
              document.getElementById("asunto").textContent = outputText.substring(_indiceInicial, _indiceFinal).trim();
              //$('#respuestaform').data('bootstrapValidator').updateStatus('asunto', status, null);
            }
          }
        });
      };
    }
    
    function loadFile(url,callback){
        JSZipUtils.getBinaryContent(url,callback);
    }

    var _descargarWord = function(){
      loadFile("{{url('/templates/plantilla_respuesta.docx')}}",function(error,content){
        if (error) { throw error };
        var zip = new JSZip(content);
        var doc = new Docxtemplater().loadZip(zip);
        
        doc.setData({
            fecha: getLocaleFechaOficioInput(),
            numoficio: document.getElementById("numOficio").value,
            asunto: document.getElementById("asunto").value,
            suscriptornombre: document.getElementById("suscriptorInput").value,
            suscriptorcargo: document.getElementById("cargoInput").value,
            dependencia: document.getElementById("dependenciaInput").value
        });

        try {
          // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
          doc.render()
        }catch (error) {
          var e = {
            message: error.message,
            name: error.name,
            stack: error.stack,
            properties: error.properties,
          };
          console.log(JSON.stringify({error: e}));
          // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
          throw error;
        }

        var out = doc.getZip().generate({
          type:"blob",
          mimeType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        }); //Output the document using Data-URI
        saveAs(out,"RESPUESTA_" + document.getElementById("numOficio").value + ".docx")
      });
    }
  </script>

  <script>
    var _playCountdown = function(){
      if(document.getElementById("fechaRespuestaInput").value.localeCompare("") === 0){
        $("#countdown").percircle({
          perdown: true,
          secs: 1,
          timeUpText: "---",
          progressBarColor: "#CCCCCC"
        });
      }else{
        var _fechaFinSplit = document.getElementById("fechaRespuestaInput").value.split("-");
        var _fechaFin = new Date(_fechaFinSplit[2] + "-" + _fechaFinSplit[1] + "-" + _fechaFinSplit[0]);
        
        var _fechaInicioSplit = "{{$rangofecha['fecha_inicio']}}".split("-");
        var _fechaInicio = new Date(_fechaInicioSplit[2] + "-" + _fechaInicioSplit[1] + "-" + _fechaInicioSplit[0]);

        var _numdias = 0;
        if( _fechaInicio > _fechaFin ){
          while( _fechaInicio.getTime() !== _fechaFin.getTime() ){
            if( _fechaInicio.getDay() !== 5 && _fechaInicio.getDay() !== 6 ){
              _numdias = _numdias - 1;
            }
            _fechaInicio.setDate(_fechaInicio.getDate() - 1);
          }
        }else{
          while( _fechaInicio.getTime() !== _fechaFin.getTime() ){
            if( _fechaInicio.getDay() !== 5 && _fechaInicio.getDay() !== 6 ){
              _numdias = _numdias + 1;
            }
            _fechaInicio.setDate(_fechaInicio.getDate() + 1);
          }
        }

        var _porcentaje = ((_numdias === 0) ? 0 : (_numdias * 100) / 15);
        if( _numdias > 10){
          $("#countdown").percircle({
            text: _numdias + " Dias",
            percent: _porcentaje,
            progressBarColor: "#00FF00"
          });  
        }else if( _numdias > 5){
          $("#countdown").percircle({
            text: _numdias + " Dias",
            percent: _porcentaje,
            progressBarColor: "#FFFF00"
          });
        }else if( _numdias > 0){
          $("#countdown").percircle({
            text: _numdias + ((_numdias === 1) ? " Dia" : " Dias"),
            percent: _porcentaje,
            progressBarColor: "#FF0000"
          });
        }else if( _numdias === 0){
          $("#countdown").percircle({
            perdown: true,
            secs: 1,
            timeUpText: "0 Dias",
            progressBarColor: "#CCCCCC"
          });
        }else if( _numdias < 0){
          $("#countdown").percircle({
            perdown: true,
            secs: 1,
            timeUpText: _numdias,
            progressBarColor: "#CCCCCC"
          });
        }
      }
    }

    $(document).ready(function(){
      $(".nav-tabs a").on("shown.bs.tab", function(event){
        if( event.target.getAttribute("href").localeCompare("#TabRespuesta") === 0){
          _playCountdown();
        }
      });

      var _existeDocumentoRespuesta = "{{isset($documentorespuesta)}}";
      if(_existeDocumentoRespuesta.localeCompare("1") !== 0){
        $("#fechaOficio").flatpickr({
          locale: "es",
          dateFormat: "d-m-Y",
          disable: [
            function(date) {
              return (date.getDay() === 0 || date.getDay() === 6);
            }
          ],
          minDate: "{{$rangofecha['fecha_inicio']}}"
        });

        var _inputRespuesta = document.getElementById("fechaRespuestaInput");
        _inputRespuesta.onchange = function(){
          _playCountdown();
        } 
      }      
    });
  </script>

  @if( isset($documentorespuesta) )
    <!-- BootstrapValidator Script -->
    <script type="text/javascript">
      $(document).ready(function() {
        $("#respuestaform").bootstrapValidator({
          message: "No valido",
          fields: {
            numHojas: {
              validators: {
                notEmpty: {
                  message: "Requerido"
                },
                integer: {
                  message: 'Sólo números'
                }
              }
            },
            numAnexos: {
              validators: {
                notEmpty: {
                  message: "Requerido"
                },
                integer: {
                  message: 'Sólo números'
                }
              }
            },
            asunto: {
              validators: {
                notEmpty: {
                  message: "Requerido"
                },
                stringLength: {
                  min: 6,
                  max: 250,
                  message: "Longitud 6-250"
                }
              }
            },
            wordInputFile: {
              validators: {
                notEmpty: {
                  message: "Requerido"
                },
                file: {
                  extension: 'docx',
                  type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                  maxSize: 1000000,
                  message: 'El archivo seleccionado no es valido.'
                }
              }
            }
          }
        });
      });
    </script>
  @else
    <!-- BootstrapValidator Script -->
    <script type="text/javascript">
      $(document).ready(function() {
        $("#respuestaform").bootstrapValidator({
          message: "No valido",
          fields: {
            asunto: {
              validators: {
                notEmpty: {
                  message: "Requerido"
                },
                stringLength: {
                  min: 6,
                  max: 30,
                  message: "Longitud 6-30"
                }
              }
            }
          }
        });
      });
    </script>
  @endif

@stop