@extends('layouts.master-admin') 
 
@section('content')

  <div class="row">
    <nav aria-label="breadcrumb" style="margin-top: 10px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item" aria-current="page">Respaldo</li>
      </ol>
    </nav>
  </div>

  <div class="row">
    <div class="col-lg-12">
      @if ( session('message') ) 
        <div class="alert alert-success alert-dismissable"> 
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a> 
          <strong>EXITO: </strong> {{ session()->pull('message', 'default') }} 
        </div> 
      @endif 
    </div>
  </div>
  <!-- /.row -->

  <div class="row" id="errorRowDiv">
    <div class="col-lg-12">
      <div class="panel panel-danger panel-collapse collapse" id="errorPanelDiv">
        <div class="panel-heading">
          <strong>ERROR:</strong>
          <a data-toggle="collapse" href="#errorPanelDiv" class="close">&times</a>
        </div>
        <div class="panel-body" id="errorPanelBodyDiv">
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header" style="margin-top: 3px; margin-bottom: 15px;">Respaldos</h3>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      
      <table width="100%" class="table table-striped table-bordered table-hover" id="documentosTable">
        <thead>
          <tr>
            <th>DESCRIPCION</th>
            <th>FECHA</th>
            <th>ACCION</th>
          </tr>
        </thead>
      </table>
      <!-- /.table-responsive -->
        
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->

  <script type="text/javascript">
    var _restoreBackup = function( _path ){
      //console.log(_path);
      $.ajax({
        headers: { 
          "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
        }, 
        type: "post",
        data: {path: _path},
        url: "{{url('/respaldos/restore')}}",
        success: function(_data){
          console.info('ok');
          /*
          $("#collapseDetalleDiv").collapse("hide");
          $("#panelDetalleTools").prop("hidden", true);
          _tableSolicitudes.ajax.reload();  // Recargar DataTables Solicitudes
          $("#divSuccessMessage").load(window.location.href + " #divSuccessMessage" );  // Recargar seccion divSusccessMessage
          $("#fullCalendarDiv").fullCalendar("refetchEvents");  // Recargar FullCalendar
          */
        },
        error: function(_data){
          console.error(_data);
          /*
          var _bodyError = document.getElementById("errorPanelBodyDiv");
          removeChildren("errorPanelBodyDiv");
          var _mensaje = document.createTextNode(_data.responseJSON);
          _bodyError.appendChild(_mensaje);
          $("#errorPanelDiv").collapse("show");     // Mostrar panel de error
          */
        }
      });
    }

    $(document).ready(function(){
      _tableDocumentos = $('#documentosTable').DataTable( { 
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": <?php echo json_encode( url('/api/respaldos/dataTableList') ); ?>,
        "columns": [
          {data: "0", name: "0", orderable: false},
          {data: "fecha", name: "fecha"},
          {data: "action", name: "action", orderable: false, searchable: false}
        ],
        "language": {
          "url":  <?php echo json_encode( url('/vendor/datatables-plugins/Spanish.json') ); ?>
        },
        "lengthChange": false,
        "pageLength": 10,
        "searching": false
      } );
    });
  </script>

@stop