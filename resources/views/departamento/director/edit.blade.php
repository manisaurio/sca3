@extends('layouts.master-director')

@section('content')

	<div class="row">
		<nav aria-label="breadcrumb" style="margin-top: 10px;">
		  <ol class="breadcrumb" id="navegacion">
		    <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
		    <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/departamentos/list')}}">Departamentos</a></li>
		    <li class="breadcrumb-item" aria-current="page">Editar</li>
		  </ol>
		</nav>   
	</div>

    <div class="row">

		<div class="col-md-offset-3 col-md-6">
			@if( count($errors) > 0)
				<div class="alert alert-danger alert-dismissible" role="alert"> 
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"> 
						<span aria-hidden="true">&times;</span> 
					</button> 
					<ul> 
						@foreach($errors->all() as $error) 
							<li>{!!strtoupper($error)!!}</li> 
						@endforeach 
					</ul> 
				</div> 
			@endif

			<div class="panel panel-default">

				<div class="panel-heading">
					<h3 class="panel-title text-center">
						<span class="fa fa-puzzle-piece fa-fw" aria-hidden="true"></span>
						Editar departamento
					</h3>
				</div>

				<div class="panel-body" style="padding:30px">
				
					<form method="POST" id="departamentoForm">
					
						{{ method_field('PUT') }}

            			{{ csrf_field() }}

						<div class="form-group">
							<label for="nombre">Nombre:</label>
							<input type="text" name="nombre" id="nombre" value="{{ old('nombre', $departamento->nombre) }}" class="form-control" style="text-transform:uppercase" required>
						</div>

						<div class="form-group">
							<label for="area">Area:</label>
							<input type="text" id="area" name="area" value="{{ $departamento->area }}" class="form-control" style="text-transform:uppercase" readonly>
						</div>

						<div class="form-group text-center">
							<button type="submit" class="btn btn-primary">
								Actualizar departamento
							</button>
						</div>
						
					</form>

				</div>
			</div>
		</div>
	</div>
	
	<script src={{ url('/js/Restriccion.js') }}></script> 

	<script type="text/javascript">

		document.onreadystatechange = function () { 
	      if (document.readyState === "complete") { 
	        Restriccion.Departamento.edit(); 
	      } 
	    }
	</script>

@stop