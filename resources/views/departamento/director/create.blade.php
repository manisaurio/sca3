@extends('layouts.master-director')

@section('content')

	<div class="row">
		<nav aria-label="breadcrumb" style="margin-top: 10px;">
		  <ol class="breadcrumb" id="navegacion">
		    <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
		    <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/departamentos/list')}}">Departamentos</a></li>
		    <li class="breadcrumb-item" aria-current="page">Crear</li>
		  </ol>
		</nav>   
	</div>

    <div class="row">

		<div class="col-md-offset-3 col-md-6">
			@if( count($errors) > 0)
				<div class="alert alert-danger alert-dismissible" role="alert"> 
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"> 
						<span aria-hidden="true">&times;</span> 
					</button> 
					<ul> 
						@foreach($errors->all() as $error) 
							<li>{!!strtoupper($error)!!}</li> 
						@endforeach 
					</ul> 
				</div> 
			@endif

			<div class="panel panel-default">

				<div class="panel-heading">
					<h3 class="panel-title text-center">
						<span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
						Nuevo departamento
					</h3>
				</div>

				<div class="panel-body" style="padding:30px">
				
					<form method="POST" id="departamentoForm">
					
						{{ csrf_field() }}

						<div class="form-group">
							<label for="nombre">Nombre:</label>
							<input type="text" name="nombre" id="nombre" value="{{ old('nombre') }}" class="form-control" style="text-transform:uppercase" required>
						</div>

						<div class="form-group">
							<label for="area">Area:</label>
							<select name="areaSelect" id="areaSelect" class="form-control">
								@foreach($departamentos as $departamento)
									@if( old('areaSelect') !== null ) 
										@if( strcmp(old('areaSelect'), $departamento->area) == 0 )
											<option value="{{ $departamento->area }}" selected> {{ $departamento->area }} </option>
										@else
											<option value="{{ $departamento->area }}"> {{ $departamento->area }} </option>
										@endif
									@else
										<option value="{{ $departamento->area }}"> {{ $departamento->area }} </option>
									@endif
								@endforeach
							</select>

							<label for="input_group">Otra área:</label>
							<div class="input-group" id="input_group">
								<span class="input-group-addon">
									@if( old('otraAreaCheckbox') !== null )
										<input type="checkbox" id="otraAreaCheckbox" name="otraAreaCheckbox" checked>
									@else
										<input type="checkbox" id="otraAreaCheckbox" name="otraAreaCheckbox">
									@endif
								</span>
								<input type="text" id="otra_area" name="otra_area" value="{{ old('otra_area') }}" class="form-control" style="text-transform:uppercase" disabled>
							</div>
						</div>

						<div class="form-group text-center">
							<button type="submit" class="btn btn-primary">
								Agregar departamento
							</button>
						</div>
						
					</form>

				</div>
			</div>
		</div>
	</div>
	
	<script src={{ url('/js/Restriccion.js') }}></script> 

	<script type="text/javascript">
		document.onreadystatechange = function () { 
	      if (document.readyState === "complete") { 
	        Restriccion.Departamento.init(); 
	      } 
	    } 

		var _selectArea = document.getElementById("areaSelect");
		var _checkboxOtraArea = document.getElementById("otraAreaCheckbox");
		var _inputOtraArea = document.getElementById("otra_area");
		var _nombreInput = document.getElementById("nombre");

		_checkboxOtraArea.onchange = function(){
			if( _checkboxOtraArea.checked ){
				_inputOtraArea.removeAttribute("disabled");
				_inputOtraArea.setAttribute("required", "");
				_selectArea.setAttribute("disabled", "");
				_nombreInput.setAttribute("disabled", "");
				_nombreInput.value = "JEFATURA";
				_nombreInput.removeAttribute("required");
			}else{
				_inputOtraArea.setAttribute("disabled", "");
				_inputOtraArea.removeAttribute("required");
				_inputOtraArea.value = "";
				_selectArea.removeAttribute("disabled");
				_nombreInput.removeAttribute("disabled", "");
				_nombreInput.value = "";
				_nombreInput.setAttribute("required", "");
			}
		}

		var _oldNombre = <?php echo json_encode( old('nombre') ); ?>;
		//var _oldArea = <?php echo json_encode( old('otra_area') ); ?>;
	    if( _oldNombre === null){
	    	var _event = new Event('change'); 
	    	_checkboxOtraArea.dispatchEvent(_event); 
	    }
	</script>

@stop