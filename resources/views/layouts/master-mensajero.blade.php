<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    
    <title>Sistema de Control de Actividades</title>

    <!-- Bootstrap -->
    <link href={{ url('/vendor/bootstrap/css/bootstrap.min.css') }} rel="stylesheet">

    <!-- AutoComplete CSS -->
    <link href={{ url('/css/auto-complete.css') }} rel="stylesheet">

    <link href={{ url('/css/gsdk-bootstrap-wizard.css') }} rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href={{ url('/vendor/metisMenu/metisMenu.min.css') }} rel="stylesheet">

    <!-- SBAdmin2 CSS -->
    <link href={{ url('/assets/sb-admin-2-dist/css/sb-admin-2.min.css') }} rel="stylesheet">

    <!-- Custom Fonts -->
    <link href={{ url('/vendor/font-awesome/css/font-awesome.min.css') }} rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <!--
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    -->
    <link href={{ url('/vendor/datatables-plugins/dataTables.bootstrap.css') }} rel="stylesheet" type="text/css">

    <!-- DataTables Responsive CSS -->
    <link href={{ url('/vendor/datatables-responsive/dataTables.responsive.css') }} rel="stylesheet" type="text/css">

    <!-- BootstrapValidator CSS -->
    <link href="{{ url("vendor/bootstrapvalidator-0.5.3/dist/css/bootstrapValidator.min.css") }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  </head>
  <body>
    <div id="wrapper">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!--
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        -->
        <script src={{ url('/js/lib/jquery-1.12.4.min.js') }}></script>
        
        @include('partials.navbar-mensajero')

        <!-- Autocomplete lib -->
        <script src={{ url('/js/lib/auto-complete.min.js') }}></script>

        <div  id="page-wrapper" style="min-height: 354px;">
            @yield('content')
        </div>
    
    </div>
    <!-- Bootstrap Compiled JavaScript -->
    <!--
    <script src={{ url('/assets/bootstrap-3.3.7-dist/js/bootstrap.min.js') }}></script>
    -->
    <script src={{ url('/vendor/bootstrap/js/bootstrap.min.js') }}></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src={{ url('/vendor/metisMenu/metisMenu.min.js') }}></script>
    
    <!-- SBAdmin2 JS -->
    <script src={{ url('/assets/sb-admin-2-dist/js/sb-admin-2.min.js') }}></script>

    <!-- DatatTables JS -->
    <!--
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    -->
    <script src={{ url('/vendor/datatables/js/jquery.dataTables.min.js') }}></script>
    <script src={{ url('/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}></script>
    <script src={{ url('/vendor/datatables-responsive/dataTables.responsive.js') }}></script>

    <!-- BootstrapValidator JS -->
    <script src={{ url("/vendor/bootstrapvalidator-0.5.3/dist/js/bootstrapValidator.min.js") }}></script>
    <script src={{ url("/vendor/bootstrapvalidator-0.5.3/dist/js/language/es_ES.js") }}></script>
    
    @include('partials.footer')
  </body>
</html>