<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Sistema de Control de Actividades</title>

    <!-- Bootstrap -->
    <link href={{ url('/vendor/bootstrap/css/bootstrap.min.css') }} rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  </head>
  <body>
    <div id="wrapper">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!--
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        -->
        <script src={{ url('/js/lib/jquery-1.12.4.min.js') }}></script>
        
        @include('partials.navbar-login')

        <!-- Autocomplete lib -->
        <script src={{ url('/js/lib/auto-complete.min.js') }}></script>

        <div  id="page-wrapper" style="min-height: 354px;">
            @yield('content')
        </div>
    
    </div>
    <!-- Bootstrap Compiled JavaScript -->
    <!--
    <script src={{ url('/assets/bootstrap-3.3.7-dist/js/bootstrap.min.js') }}></script>
    -->
    <script src={{ url('/vendor/bootstrap/js/bootstrap.min.js') }}></script>
    
  </body>
</html>