@extends('layouts.master-asistente')

@section('content')

  <div class="row">
    <div class="col-lg-12">
      <h3 style="margin-top: 13px;"></h3>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->

  <div class="row">
    <div class="container" style="width: auto;">
      <div class="component">
        <h2>Dirección de Atención Médica</h2>
        <!-- Start Nav Structure -->
        <button class="cn-button" id="cn-button">Menú</button>
        <div class="cn-wrapper" id="cn-wrapper">
          <ul>
            <li><a href="#"><span></span></a></li>
            <li><a href="#"><span></span></a></li>
            <li><a href="empleados/list"><span>Usuarios</span></a></li>
            <li><a href="citas/calendar"><span>Citas</span></a></li>
            <li><a href="guiasturnados/listaturnadoentrada"><span>Documentos</span></a></li>
            <li><a href="#"><span></span></a></li>
            <li><a href="#"><span></span></a></li>
          </ul>
        </div>
        <!-- End of Nav Structure -->
      </div>

      <section>
        <b><p>MISIÓN</p></b>
        <p>Brindar servicios de salud con calidad a la población oaxaqueña no asegurada, mediante la prevención y promoción del autocuidado de la salud del individuo y la comunidad, atención médica oportuna, vigilancia sanitaria y administración racional, justa y transparente de los recursos.</p>
        <p>VISIÓN</p>
        <p>Ser la Institución líder del Sector Salud, que garantice el acceso a los oaxaqueños y oaxaqueñas a un Sistema de Salud Universal, equitativo, participativo y de alta calidad, coadyuvando al desarrollo sustentable del Estado de Oaxaca. </p>
      </section>

      <div class="col-lg-3 col-md-6 col-md-offset-3">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xs-3">
            <i class="fa fa-tasks fa-5x"></i>
          </div>
      
          <div class="col-xs-9 text-right">
            <div class="huge">01</div>
            <div><b>Revisa aquí</b></div>
          </div>
        </div>
      </div>

      <a href="http://www.salud.oaxaca.gob.mx/direccion-de-atencion-medica/" target="_blank">
        <div class="panel-footer">
          <span class="pull-left">Dirección de Atención Médica</span>
          <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
          <div class="clearfix"></div>
        </div>
      </a>
      <a href="http://www.salud.oaxaca.gob.mx/" target="_blank">
        <div class="panel-footer">
          <span class="pull-left">Servicios de Salud de Oaxaca</span>
          <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
          <div class="clearfix"></div>
        </div>
      </a>
    </div>
  </div>

  <div class="col-lg-3 col-md-6 col-md-offset-0"> 
    <div class="panel panel-green">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xs-3">
            <i class="fa fa-tasks fa-5x"></i>
          </div>

          <div class="col-xs-9 text-right">
            <div class="huge">02</div>
            <div><b>Síguenos en:</b></div>
          </div>
        </div>
      </div>

      <div class="panel-footer">
        <p><b>Redes Sociales</b></p>
        <center>
          <a class="opacity" href="https://www.facebook.com/ssogoboax" target="_blank">
            <img src="http://www.salud.oaxaca.gob.mx/wp-content/themes/DepGobOax/css/images/facebook-iconactividades.png" alt="facebook">
          </a>
          <a class="opacity" href="https://www.twitter.com/SSO_GobOax" target="_blank">
            <img src="http://www.salud.oaxaca.gob.mx/wp-content/themes/DepGobOax/css/images/twitter-iconactividades.png" alt="twitter">
          </a>
          <a class="opacity" href="https://www.youtube.com/channel/UCHfB7q6fd6xjtzfFRN_uScw" target="_blank">
            <img src="https://www.youtube.com/yts/img/favicon-vfl8qSV2F.ico" alt="youtube" width="28">
          </a>
        </center>
      </div>
    </div>
  </div>
    </div>
  </div>

  

  <!-- The Modal -->
  <div class="modal fade" id="mensajeModal">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="titleModal" style="color:#a81515"><i class="glyphicon glyphicon-warning-sign"></i> </h4>
        </div>

        <!-- Modal body -->
        <div class="modal-body alert alert-danger" id="bodyModal">
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        </div>

      </div>
    </div>
  </div>


  <!-- CircularNavigation CSS -->
  <link href={{ url('/vendor/circular-navigation/css/normalize.css') }} rel="stylesheet">
  <link href={{ url('/vendor/circular-navigation/css/demo.css') }} rel="stylesheet">
  <link href={{ url('/vendor/circular-navigation/css/component2.css') }} rel="stylesheet">

  <!-- CircularNavigation JS -->
  <script src={{ url('/vendor/circular-navigation/js/modernizr-2.6.2.min.js') }}></script>
  <script src={{ url('/vendor/circular-navigation/js/polyfills.js') }}></script>
  <script src={{ url('/vendor/circular-navigation/js/demo2.js') }}></script>

  <script type="text/javascript">
    $( document ).ready(function() {
      var _mensaje = <?php echo json_encode( session('message') ); ?>;
      if(_mensaje !== null){
        var _mensaje = _mensaje.split(":");
        var _modalTitle = document.getElementById("titleModal");
        var _modalBody = document.getElementById("bodyModal");

        _modalTitle.appendChild( document.createTextNode(_mensaje[0]) );
        _modalBody.appendChild( document.createTextNode(_mensaje[1]) );
        
        $("#mensajeModal").modal("show");
      }
    });
  </script>

@endsection
