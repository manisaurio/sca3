@extends('layouts.master-admin')

@section('content')

  <div class="row">
    <div class="col-lg-12">
      <h3 style="margin-top: 15px;"></h3>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->

  <div class="row">
    <div class="container" style="width: auto;">
        <div class="component">
          
           <img src="{{url('/images/portadaquienessomo.png')}}" class="navbar-brand" style="width:auto;height:75%;position: absolute;left: -20px;overflow: hidden;" alt="http://www.salud.oaxaca.gob.mx/wp-content/uploads/2015/12/portadaquienessomo.png">
          <!-- Start Nav Structure -->
          <button class="cn-button" id="cn-button">Menu</button>
          <div class="cn-wrapper" id="cn-wrapper">
            <ul>
              <li><a href="#"></a></li>
              <li><a href="{{ url('/empleados/list') }}"><span>Usuarios</span></a></li>
              <li><a href="{{ url('/departamentos/list') }}"><span>Deptos.</span></a></li>
              <li><a href="{{ url('/documentos/listaingreso') }}"><span>Ingresados</span></a></li>
              <li><a href="{{ url('/documentos/listaemision') }}"><span>Respuestas</span></a></li>
              <li><a href="{{ url('/empleados/show/' . Auth::user()->id_empleado) }}"><span>Perfil</span></a></li>
              <li><a href="#"></a></li>
             </ul>
          </div>
          <!-- End of Nav Structure -->
        </div>
        <section>
          <center>
            <h3>Dirección de Atención Médica</h3>
          </center>
          <p>
            <b>Objetivo</b>
            </br>
            </br>
            Dirigir las estrategias alineadas a la política estatal en la prestación de servicios de atención médica, en las unidades médicas, a la población sin servicios formales de salud, a través de la asesoría técnica en materia normativa, uso de la tecnología adecuada y al aseguramiento de los recursos necesarios para proporcionar servicios médicos con calidad, equidad y eficiencia, propiciando una relación confiable entre el prestador del servicio y el usuario.
          </p>

          <p>
            <b>Contacto</b>
            </br>
            </br>
            Dirección: Miguel Cabrera No. 514, C.P. 68000, Col. Centro, Oaxaca de Juárez, oax.
            </br>
            Teléfono: (951) 51 4 8278
          </p>
        </section>
      </div>
      
  </div>

  <!-- The Modal -->
  <div class="modal fade" id="mensajeModal">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="titleModal" style="color:#a81515"><i class="glyphicon glyphicon-warning-sign"></i> </h4>
        </div>

        <!-- Modal body -->
        <div class="modal-body alert alert-danger" id="bodyModal">
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        </div>

      </div>
    </div>
  </div>


  <!-- CircularNavigation CSS -->
  <link href={{ url('/vendor/circular-navigation/css/normalize.css') }} rel="stylesheet">
  <link href={{ url('/vendor/circular-navigation/css/demo.css') }} rel="stylesheet">
  <link href={{ url('/vendor/circular-navigation/css/component2.css') }} rel="stylesheet">

  <!-- CircularNavigation JS -->
  <script src={{ url('/vendor/circular-navigation/js/modernizr-2.6.2.min.js') }}></script>
  <script src={{ url('/vendor/circular-navigation/js/polyfills.js') }}></script>
  <script src={{ url('/vendor/circular-navigation/js/demo2.js') }}></script>

  <script type="text/javascript">
    $( document ).ready(function() {
      var _mensaje = <?php echo json_encode( session('message') ); ?>;
      if(_mensaje !== null){
        var _mensaje = _mensaje.split(":");
        var _modalTitle = document.getElementById("titleModal");
        var _modalBody = document.getElementById("bodyModal");

        _modalTitle.appendChild( document.createTextNode(_mensaje[0]) );
        _modalBody.appendChild( document.createTextNode(_mensaje[1]) );
        
        $("#mensajeModal").modal("show");
      }
    });
  </script>

@endsection