@extends('layouts.master-jefe') 
 
@section('content') 

  <div class="row">
    <nav aria-label="breadcrumb" style="margin-top: 10px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/reportesrevisiones/lista')}}">Documentos Turnados - Revisados</a></li>
        <li class="breadcrumb-item" aria-current="page">Editar</li>
      </ol>
    </nav>
  </div>

  <div class="row">
    <div class="col-lg-12">
      @if ( session('message') ) 
        <div class="alert alert-success alert-dismissable"> 
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a> 
          <strong>EXITO: </strong> {{ session()->pull('message', 'default') }} 
        </div> 
      @endif 
    </div>
  </div>
  <!-- /.row -->

  <div class="row" id="errorRowDiv">
    <div class="col-lg-12">
      <div class="panel panel-danger panel-collapse collapse" id="errorPanelDiv">
        <div class="panel-heading">
          <strong>ERROR:</strong>
          <a data-toggle="collapse" href="#errorPanelDiv" class="close">&times</a>
        </div>
        <div class="panel-body" id="errorPanelBodyDiv">
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->
 
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default"> 
        <div class="panel-heading lead"> 
          <i class="glyphicon glyphicon-edit"></i> Editar revision
        </div> 
   
        <div class="panel-body"> 
          <div class="row"> 
            <div class="col-lg-12 col-md-12"> 
              <div class="row">

                @include('documento.partials.archivovisualizacion')

                <div class="col-lg-8 col-md-8">
                  <ul class="nav nav-tabs"> 
                    <li class="active"><a data-toggle="tab" href="#Summery" class="text-info"><i class="fa fa-indent"></i> Resumen</a></li> 
                    @if( isset($solicitudrespuesta) )
                      <li><a data-toggle="tab" href="#TabRespuesta" class="text-info"><i class="fa fa-calendar"></i> Respuesta</a></li>  
                    @endif
                  </ul> 
   
                  <div class="tab-content"> 
                    <div id="Summery" class="tab-pane fade in active"> 
                      <div class="table-responsive panel"> 
                        <table class="table"> 
                          <tbody> 
                             
                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-list-alt"></i> No. Oficio</td> 
                              <td>{{ strtoupper($documento->num_oficio) }}</td> 
                            </tr> 
   
                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-file"></i> Tipo de documento</td> 
                              <td>{{ strtoupper($documento->tipo) }}</td> 
                            </tr> 
   
                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-inbox"></i> No. Hojas</td> 
                              <td>{{ $documento->num_hojas }}</td> 
                            </tr> 

                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-paperclip"></i> No. Anexos</td> 
                              <td>{{ $documento->num_anexos }}</td> 
                            </tr> 

                            <tr> 
                              <td class="text-info"><i class="glyphicon glyphicon-th"></i> Actividad</td> 
                              <td id="tdDocumentoActividad">{{ $documento->actividad }}</td> 
                            </tr>
   
                            <tr> 
                              <td class="text-info" colspan="2"><i class="glyphicon glyphicon-info-sign"></i> Asunto</td> 
                            </tr> 
                          </tbody> 
                        </table> 
                        <blockquote class="blockquote" style="font-size:15px;"> 
                          <p>{{ $documento->asunto }}</p> 
                        </blockquote> 
                      </div> 
                    </div>

                    @if( isset($solicitudrespuesta) )
                      <div id="TabRespuesta" class="tab-pane fade"> 
                        <div class="panel panel-default">
                          <div class="panel-body">
                            <div class="form-row"> 
                              <div class="col-lg-4 col-md-4"> 
                                <center> 
                                  <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                      <div class="panel-heading">
                                        <div class="row">
                                          <div class="col-xs-12">
                                            <div id="countdown" class="percircle animate gt50"></div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </center> 
                              </div>
                              <div class="form-group col-md-8"> 
                                <label for="respuestaInputGroup">Solicitud respuesta</label> 
                                <input type="text" id="fechaRespuestaInput" name="fechaRespuestaInput" value="{{$solicitudrespuesta->fecha_limite}}" class="form-control" data-input disabled>
                              </div>
                            </div>
                          </div>
                        </div> 
                      </div>
                    @endif
                  </div> 
                </div> 
              </div> 
            </div> 
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                Observaciones
              </h4>
            </div>
            <div class="panel-body">
              <form method="POST" id="revisionForm">
              
                {{ method_field('PUT') }}

                {{ csrf_field() }}

                <div class="form-group">
                  <textarea id="observacion" name="observacion" class="form-control" rows="2" placeholder="Ingrese sus observaciones..." required>{{ old('observacion', $revision->observacion) }}</textarea>
                </div>

                <div class="form-group">
                  <div class="checkbox">
                    <label>
                      @if( old('checkpapel') !== null )
                        <input type="checkbox" name="checkpapel" checked>
                      @elseif( strcmp($revision->recibi_papel, '1') == 0 )
                        <input type="checkbox" name="checkpapel" checked>
                      @else
                        <input type="checkbox" name="checkpapel">
                      @endif

                      Recibi copia impresa.
                    </label>
                  </div>
                </div>

                <div class="form-group text-center">
                  <button type="submit" class="btn btn-outline btn-success">
                    <i class="glyphicon glyphicon-upload"></i>
                    ACTUALIZAR
                  </button>
                </div>
                
              </form>
            </div>
          </div>
        </div>
      </div> 
    </div>
    <!-- /.col-lg-12 -->
  </div>

  <!-- PercicleJS JS  -->
  <script src={{ url('/vendor/percicle-1.0.26/js/percircle.js') }}></script>

  <!-- PercicleJS CSS -->
  <link href={{ url('/vendor/percicle-1.0.26/css/percircle.css') }} rel="stylesheet">

  <script>
    var _playCountdown = function(){
      if(document.getElementById("fechaRespuestaInput").value.localeCompare("") === 0){
        $("#countdown").percircle({
          perdown: true,
          secs: 1,
          timeUpText: "---",
          progressBarColor: "#CCCCCC"
        });
      }else{
        var _fechaFinSplit = document.getElementById("fechaRespuestaInput").value.split("-");
        var _fechaFin = new Date(_fechaFinSplit[2] + "-" + _fechaFinSplit[1] + "-" + _fechaFinSplit[0]);
        
        var _fechaInicioSplit = "{{$rangofecha['fecha_inicio']}}".split("-");
        var _fechaInicio = new Date(_fechaInicioSplit[2] + "-" + _fechaInicioSplit[1] + "-" + _fechaInicioSplit[0]);

        var _numdias = 0;
        if( _fechaInicio > _fechaFin ){
          while( _fechaInicio.toLocaleDateString().localeCompare(_fechaFin.toLocaleDateString()) !== 0 ){
            if( _fechaInicio.getDay() !== 5 && _fechaInicio.getDay() !== 6 ){
              _numdias = _numdias - 1;
            }
            _fechaInicio.setDate(_fechaInicio.getDate() - 1);
          }
        }else{
          while( _fechaInicio.toLocaleDateString().localeCompare(_fechaFin.toLocaleDateString()) !== 0 ){
            if( _fechaInicio.getDay() !== 5 && _fechaInicio.getDay() !== 6 ){
              _numdias = _numdias + 1;
            }
            _fechaInicio.setDate(_fechaInicio.getDate() + 1);
          }
        }

        var _porcentaje = ((_numdias === 0) ? 0 : (_numdias * 100) / 15);
        if( _numdias > 10){
          $("#countdown").percircle({
            text: _numdias + " Dias",
            percent: _porcentaje,
            progressBarColor: "#00FF00"
          });  
        }else if( _numdias > 5){
          $("#countdown").percircle({
            text: _numdias + " Dias",
            percent: _porcentaje,
            progressBarColor: "#FFFF00"
          });
        }else if( _numdias > 0){
          $("#countdown").percircle({
            text: _numdias + ((_numdias === 1) ? " Dia" : " Dias"),
            percent: _porcentaje,
            progressBarColor: "#FF0000"
          });
        }else if( _numdias === 0){
          $("#countdown").percircle({
            perdown: true,
            secs: 1,
            timeUpText: "0 Dias",
            progressBarColor: "#CCCCCC"
          });
        }else if( _numdias < 0){
          $("#countdown").percircle({
            perdown: true,
            secs: 1,
            timeUpText: _numdias,
            progressBarColor: "#CCCCCC"
          });
        }
      }
    }

    $(document).ready(function(){
      $(".nav-tabs a").on("shown.bs.tab", function(event){
        if( event.target.getAttribute("href").localeCompare("#TabRespuesta") === 0){
          _playCountdown();
        }
      });
    });
  
    var _modalConfirmFunction = function( _url ){
      //$('#confirmModal').modal({backdrop: 'static', keyboard: false});
      $("#confirmModal").modal("show");
      var _buttonAcept = document.getElementById("acceptButton");
      var _buttonCancel = document.getElementById("cancelButton");
      var _buttonRedirect = document.getElementById("redirectButton");
      _buttonAcept.onclick = function(){
        $.ajax({
          headers: { 
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
          }, 
          type: 'post',
          data: {_method: "delete"},
          url: _url,
          success: function(_data){
            var _documentoActividad = document.getElementById("tdDocumentoActividad");
            $( "#acceptButton" ).remove();
            $( "#cancelButton" ).remove();
            $('#confirmModal').on('hidden.bs.modal', function () {
              if( _documentoActividad.textContent.localeCompare("ingreso") === 0 ){
                window.location = "{{ url('documentos/listaingreso') }}";
              }else{
                window.location = "{{ url('documentos/listaemision') }}";
              }
            });

            $( "#redirectButton" ).click(function(){
              if( _documentoActividad.textContent.localeCompare("ingreso") === 0 ){
                window.location = "{{ url('documentos/listaingreso') }}";
              }else{
                window.location = "{{ url('documentos/listaemision') }}";
              }
            });

            $( "#redirectButton" ).attr("style", "display:block;");
            $('#confirmModalTitle').empty();
            $('#confirmModalTitle').addClass("text-success");
            $('#confirmModalTitle').append("EXITO");
            $('#confirmModalBody').empty();
            $('#confirmModalBody').append("Eliminación realizada satisfactoriamente.");
            $('#confirmModalBody').attr("class", "modal-body alert alert-success");
          },
          error: function(_data){
            $('#confirmModal').modal('hide');     // Ocultar ventana modal
            var _bodyError = document.getElementById("errorPanelBodyDiv");
            while(_bodyError.firstChild){
              _bodyError.removeChild(_bodyError.firstChild);
            }
            var _mensaje = document.createTextNode(_data.responseJSON.message);
            _bodyError.appendChild(_mensaje);
            $('#errorPanelDiv').collapse("show"); // Mostrar panel de error
          }
        });
      };

      _buttonCancel.onclick = function(){
        $('#confirmModal').modal('hide');
      }
    }
  </script>
  
@stop