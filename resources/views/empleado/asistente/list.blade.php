@extends('layouts.master-asistente') 
 
@section('content') 

<div class="row">
    <nav aria-label="breadcrumb" style="margin-top: 10px;">
      <ol class="breadcrumb" id="navegacion">
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item" aria-current="page">Usuarios</li>
      </ol>
    </nav>   
  </div>

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header" style="margin-top: 3px; margin-bottom: 15px;">Usuarios</h3>
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row" id="successRowDiv">
  <div class="col-lg-12">
    <div class="panel panel-success panel-collapse collapse" id="successPanelDiv">
      <div class="panel-heading">
        <strong>ÉXITO:</strong>
        <a data-toggle="collapse" href="#successPanelDiv" class="close">&times</a>
      </div>
      <div class="panel-body" id="successPanelBodyDiv">
      </div>
    </div>
  </div>
</div>
<!-- /.row -->

<div class="row" id="errorRowDiv">
  <div class="col-lg-12">
    <div class="panel panel-danger panel-collapse collapse" id="errorPanelDiv">
      <div class="panel-heading">
        <strong>ERROR:</strong>
        <a data-toggle="collapse" href="#errorPanelDiv" class="close">&times</a>
      </div>
      <div class="panel-body" id="errorPanelBodyDiv">
      </div>
    </div>
  </div>
</div>
<!-- /.row -->

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Tabla Usuarios
      </div>
      <!-- /.panel-heading -->
      <div class="panel-body">
        <table width="100%" class="table table-striped table-bordered table-hover" id="usrempTable">
          <thead>
            <tr>
              <th>CURP</th>
              <th>Nombre</th>
              <th>Puesto</th>
              <th>Departamento</th>
              <th>Área</th>
              <th>Acción</th>
            </tr>
          </thead>
        </table>
        <!-- /.table-responsive -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<!-- The Modal -->
<div class="modal fade" id="confirmModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Confirmar eliminación</h4>
      </div>

      <!-- Modal body -->
      <div class="modal-body alert alert-info" id="">
        Esta operación puede causar la pérdida de información.
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="acceptButton">Aceptar</button>
        <button type="button" class="btn btn-secondary" id="cancelButton">Cacelar</button>
      </div>

    </div>
  </div>
</div>

<script>
  var _tableUsrEmp;

  $(document).ready(function(){
    _tableUsrEmp = $('#usrempTable').DataTable( { 
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "ajax": <?php echo json_encode( url('/api/usremp/datatable') ); ?>,
      "columns": [
        {data: "curp", name: "curp"},
        {data: "nombre", name: "nombre"},
        {data: "puesto", name: "puesto"},
        {data: "nombre_departamento", name: "nombre_departamento"},
        {data: "area", name: "area"},
        {data: "action", name: "action", orderable: false, searchable: false, width: "100%"}
      ],
      "language": {
        "url":  <?php echo json_encode( url('/vendor/datatables-plugins/Spanish.json') ); ?>
      },
      //"pageLength": 5,
      "lengthChange": false
    } );
  });

  var _switchUserFunction = function( _url ){
    $.ajax({
      headers: { 
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
      }, 
      type: 'PUT', 
      url: _url,
      success: function(_data) {
        _tableUsrEmp.ajax.reload();
      }
    });
  }

  var _modalConfirmFunction = function( _url ){
    $("#confirmModal").modal("show");
    var _buttonAcept = document.getElementById("acceptButton");
    var _buttonCancel = document.getElementById("cancelButton");
    _buttonAcept.onclick = function(){
      $.ajax({
        headers: { 
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
        }, 
        type: 'DELETE', 
        url: _url,
        success: function(_data) {
          $('#confirmModal').modal('hide');     // Ocultar ventana modal
          _tableUsrEmp.ajax.reload();           // Actualizar tabla
          var _bodySuccess = document.getElementById("successPanelBodyDiv");
          while(_bodySuccess.firstChild){
            _bodySuccess.removeChild(_bodySuccess.firstChild);
          }
          console.log(_data);
          var _mensaje = document.createTextNode("Operación realizada satisfactoriamente.");
          _bodySuccess.appendChild(_mensaje);
          $('#successPanelDiv').collapse("show"); // Mostrar panel de exito
        },
        error: function(_data){
          $('#confirmModal').modal('hide');     // Ocultar ventana modal
          var _bodyError = document.getElementById("errorPanelBodyDiv");
          while(_bodyError.firstChild){
            _bodyError.removeChild(_bodyError.firstChild);
          }
          var _mensaje = document.createTextNode(_data.responseJSON.message);
          _bodyError.appendChild(_mensaje);
          $('#errorPanelDiv').collapse("show"); // Mostrar panel de error
        }
      });
    };

    _buttonCancel.onclick = function(){
      $('#confirmModal').modal('hide');
    }
  }

</script>

@stop