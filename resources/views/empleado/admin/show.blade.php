@extends('layouts.master-admin') 
 
@section('content')

  <div class="row">
    @include('partials.breadcrumb')
  </div> 

  <div class="row">
    <div class="col-lg-12">
      @if ( session('message') ) 
        <div class="alert alert-success alert-dismissable"> 
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a> 
          <strong>EXITO: </strong> {{ session()->pull('message', 'default') }} 
        </div> 
      @endif 
    </div>
  </div>
  <!-- /.row -->

  <div class="row" id="errorRowDiv">
    <div class="col-lg-12">
      <div class="panel panel-danger panel-collapse collapse" id="errorPanelDiv">
        <div class="panel-heading">
          <strong>ERROR:</strong>
          <a data-toggle="collapse" href="#errorPanelDiv" class="close">&times</a>
        </div>
        <div class="panel-body" id="errorPanelBodyDiv">
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->

  <div class="row">

    <div class="col-lg-12"> 

      <div class="panel panel-default"> 
     
          <div class="panel-heading lead"> 
            <div class="row"> 
              <div class="col-lg-8 col-md-8"><i class="glyphicon glyphicon-user"></i> Usuario Detalle</div> 
              <div class="col-lg-4 col-md-4 text-right"> 
                <div class="btn-group text-center"> 
                  <a href={{ url('/empleados/edit/' . $empleado->id) }} class="btn btn-default btn-sm btn-default"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a onclick="_modalConfirmFunction('{{ url('/empleados/destroy/'.$empleado->id) }}')" class="btn btn-default btn-sm btn-default"><i class="glyphicon glyphicon-trash"></i></a>
                  @if($empleado->activo)
                    <a onclick="_switchUserFunction('{{ url('/empleados/switch/'.$empleado->id) }}')" class="btn btn-sm btn-warning" title='Desactivar'><i class="glyphicon glyphicon-off"></i></a>
                  @else
                    <a onclick="_switchUserFunction('{{ url('/empleados/switch/'.$empleado->id) }}')" class="btn btn-sm btn-success" title='Activar'><i class="glyphicon glyphicon-off"></i></a>
                  @endif
                </div> 
              </div> 
            </div> 
          </div> 
     
          <div class="panel-body"> 
            <div class="row"> 
              <div class="col-lg-3 col-md-3" align="center"> 
                @if( isset($empleado->ruta_avatar) ) 
                  <img alt="User Pic" src="{{$empleado->ruta_avatar}}" class="img-circle img-responsive" style="height: 170px; width: 170px; border: 4px solid #CCCCCC;"> 
                @else 
                  <img alt="User Pic" src={{ url('/images/default-avatar.png') }} class="img-circle img-responsive" style="height: 170px; width: 170px; border: 4px solid #CCCCCC;"> 
                @endif 
              </div> 
     
              <div class="col-lg-9 col-md-9"> 
                <div class="table-responsive panel"> 
                  <table class="table"> 
                    <tbody> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-bookmark"></i> NOMBRE</td> 
                        <td>{{ strtoupper($empleado->nombre) }}</td> 
                      </tr> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-object-align-horizontal"></i> APELLIDO PATERNO</td> 
                        <td>{{ strtoupper($empleado->ap_paterno) }}</td> 
                      </tr> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-object-align-left"></i> APELLIDO MATERNO</td> 
                        <td>{{ strtoupper($empleado->ap_materno) }}</td> 
                      </tr> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-credit-card"></i> CURP</td> 
                        <td>{{ $empleado->curp }}</td> 
                      </tr> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-envelope"></i> EMAIL</td> 
                        <td>{{ $empleado->email }}</td> 
                      </tr> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-record"></i> PUESTO</td> 
                        <td>{{ strtoupper($empleado->puesto) }}</td> 
                      </tr> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-tasks"></i> DEPARTAMENTO</td> 
                        <td>{{ strtoupper($empleado->nombre_departamento) }}</td> 
                      </tr> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-briefcase"></i> AREA</td> 
                        <td>{{ strtoupper($empleado->area) }}</td> 
                      </tr> 
     
                    </tbody> 
                  </table> 
                </div> 
              </div> 
            </div> 
          </div> 
      </div>

    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->

  <!-- The Modal -->
  <div class="modal fade" id="confirmModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title" id="confirmModalTitle">Confirmar eliminación</h4>
        </div>

        <!-- Modal body -->
        <div class="modal-body alert alert-info" id="confirmModalBody">
          Esta operación puede causar la pérdida de información.
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" id="redirectButton" style="display:none;">Aceptar</button>
          <button type="button" class="btn btn-primary" id="acceptButton">Aceptar</button>
          <button type="button" class="btn btn-secondary" id="cancelButton">Cacelar</button>
        </div>

      </div>
    </div>
  </div>

  <script>
    var _modalConfirmFunction = function( _url ){
      //$('#confirmModal').modal({backdrop: 'static', keyboard: false});
      $("#confirmModal").modal("show");
      var _buttonAcept = document.getElementById("acceptButton");
      var _buttonCancel = document.getElementById("cancelButton");
      var _buttonRedirect = document.getElementById("redirectButton");
      _buttonAcept.onclick = function(){
        $.ajax({
          headers: { 
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
          }, 
          type: 'post',
          data: {_method: "delete"},
          url: _url,
          success: function(_data){
            $( "#acceptButton" ).remove();
            $( "#cancelButton" ).remove();
            $('#confirmModal').on('hidden.bs.modal', function () {
              window.location = "{{ url('empleados/list') }}";
            });

            $( "#redirectButton" ).click(function(){
              window.location = "{{ url('empleados/list') }}";
            });

            $( "#redirectButton" ).attr("style", "display:block;");
            $('#confirmModalTitle').empty();
            $('#confirmModalTitle').addClass("text-success");
            $('#confirmModalTitle').append("EXITO");
            $('#confirmModalBody').empty();
            $('#confirmModalBody').append("Eliminación realizada satisfactoriamente.");
            $('#confirmModalBody').attr("class", "modal-body alert alert-success");
          },
          error: function(_data){
            $('#confirmModal').modal('hide');     // Ocultar ventana modal
            var _bodyError = document.getElementById("errorPanelBodyDiv");
            while(_bodyError.firstChild){
              _bodyError.removeChild(_bodyError.firstChild);
            }
            var _mensaje = document.createTextNode(_data.responseJSON.message);
            _bodyError.appendChild(_mensaje);
            $('#errorPanelDiv').collapse("show"); // Mostrar panel de error
          }
        });
      };

      _buttonCancel.onclick = function(){
        $('#confirmModal').modal('hide');
      }
    }

    var _switchUserFunction = function( _url ){
    $.ajax({
      headers: { 
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
      }, 
      type: 'PUT', 
      url: _url,
      success: function(_data) {
        location.reload();
      }
    });
  }
  </script>
 
@stop