@extends('layouts.master-director') 
 
@section('content') 
  <div class="row">
    <nav aria-label="breadcrumb" style="margin-top: 10px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/empleados/show/'.Auth::user()->id_empleado)}}">Perfil de Usuario</a></li>
        <li class="breadcrumb-item" aria-current="page">Editar</li>
      </ol>
    </nav>
  </div>
 
  <div class="row">
  
    <div class="col-md-offset-2 col-md-8"> 
 
      <div class="panel panel-default"> 
        @if( count($errors) > 0) 
          <div class="alert alert-danger alert-dismissible" role="alert"> 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> 
              <span aria-hidden="true">&times;</span> 
            </button> 
            <ul> 
              @foreach($errors->all() as $error) 
                <li>{!!strtoupper($error)!!}</li> 
              @endforeach 
            </ul> 
          </div> 
        @endif 
 
        <div class="panel-heading"> 
          <h3 class="panel-title text-center"> 
            <span class="glyphicon glyphicon-user" aria-hidden="true"></span> 
            Editar Perfil de Usuario
          </h3> 
        </div> 
 
        <div class="panel-body" style="padding:30px"> 
         
          <form method="POST" id="createEmpleadoForm"  enctype="multipart/form-data" novalidate> 
            {{ method_field('PUT') }} 
           
            {{ csrf_field() }} 
 
            <div class="form-row"> 
              <div class="col-md-4"> 
                <div class="picture-container"> 
                  <div class="picture"> 
                    @if( old('cambioAvatar') ) 
                      <img src={{ url('/images/default-avatar.png') }} defaultSrc={{ url('/images/default-avatar.png') }} class="picture-src" id="avatarImg" title=""/> 
                    @else 
                      @if( isset($empleado->ruta_avatar) ) 
                        <img src="{{$empleado->ruta_avatar}}" defaultSrc={{ url('/images/default-avatar.png') }} class="picture-src" id="avatarImg" title=""/> 
                      @else 
                        <img src={{ url('/images/default-avatar.png') }} defaultSrc={{ url('/images/default-avatar.png') }} class="picture-src" id="avatarImg" title=""/> 
                      @endif 
                    @endif 
                    <input type="file" id="avatarInputFile" name="avatarInputFile" accept="image/*"> 
                  </div> 
                  <input type="checkbox" id="cambioAvatar" name="cambioAvatar" hidden> 
                  <h5 style="font-weight:bold;">SELECCIONE IMAGEN</h5> 
                  <small class="form-text text-info" id="avatarSmall" hidden><span class="glyphicon glyphicon-info-sign"></span> </small> 
                </div> 
              </div> 
 
              <div class="form-group col-md-8"> 
                <label for="nombre">Nombre: <!--<small style="font-weight: normal;">(requerido)</small>--> </label> 
                <input type="text" name="nombre" id="nombre" value="{{ old('nombre', $empleado->nombre) }}" class="form-control" style="text-transform:uppercase" required> 
                <small class="form-text text-danger" id="nombreSmall" hidden><span class="glyphicon glyphicon-ok"></span> </small> 
              </div> 
 
              <div class="form-group col-md-8"> 
                <label for="ap_paterno">Apellido paterno:</label> 
                <input type="text" name="ap_paterno" id="ap_paterno" value="{{ old('ap_paterno', $empleado->ap_paterno) }}" class="form-control" style="text-transform:uppercase" required> 
                <small class="form-text text-danger" id="ap_paternoSmall" hidden><span class="glyphicon glyphicon-ok"></span> </small> 
              </div> 
 
              <div class="form-group col-md-8"> 
                <label for="ap_materno">Apellido materno:</label> 
                <input type="text" name="ap_materno" id="ap_materno" value="{{ old('ap_materno', $empleado->ap_materno) }}" class="form-control" style="text-transform:uppercase" required> 
                <small class="form-text text-danger" id="ap_maternoSmall" hidden><span class="glyphicon glyphicon-ok"></span> </small> 
              </div> 
            </div> 
 
            <div class="form-row"> 
              <div class="form-group col-md-5"> 
                <label for="curp">CURP:</label> 
                <input type="text" name="curp" id="curp" value="{{ old('curp', $empleado->curp) }}" class="form-control" style="text-transform:uppercase" required> 
                <small class="form-text text-danger" id="curpSmall" hidden><span class="glyphicon glyphicon-ok"></span> </small> 
              </div> 
              <div class="form-group col-md-7"> 
                <label for="email">E-Mail</label> 
                <input type="email" name="email" id="email" value="{{ old('email', $empleado->email) }}" class="form-control" required> 
                <small class="form-text text-danger" id="emailSmall" hidden><span class="glyphicon glyphicon-ok"></span> </small> 
              </div> 
            </div> 

            <div class="form-row"> 
              <div class="form-group col-md-12"> 
                <div class="checkbox"> 
                  <label> 
                    <input type="checkbox" id="passwordCheckbox" name="passwordCheckbox" {{ old('passwordCheckbox') ? "checked" : ""}}> CAMBIAR CONTRASEÑA 
                  </label> 
                </div> 
              </div> 
            </div> 
 
            <div class="form-row" id="passwordDiv" hidden> 
              <div class="form-group col-md-6"> 
                <label for="psw">Contraseña:</label> 
                <br> 
                <input type="password" id="psw" name="psw" class="form-control" required> 
                <small class="form-text text-danger" id="pswSmall" hidden><span class="glyphicon glyphicon-ok"></span> </small> 
              </div> 
 
              <div class="form-group col-md-6"> 
                <label for="pswconfirm">Confirmar:</label> 
                <br> 
                <input type="password" id="pswconfirm" name="pswconfirm" class="form-control" required> 
                <small class="form-text text-danger" id="pswconfirmSmall" hidden><span class="glyphicon glyphicon-ok"></span> </small> 
              </div> 
            </div> 
             
            <div class="form-row"> 
              <div class="form-group col-md-12 text-center"> 
                <button type="submit" class="btn btn-primary"> 
                  Actualizar usuario 
                </button> 
              </div> 
            </div> 
             
          </form> 
 
        </div> 
      </div> 
    </div>

  </div>
  
  <script src={{ url('/js/Restriccion.js') }}></script> 
 
  <script type="text/javascript"> 
 
    document.onreadystatechange = function () { 
      if (document.readyState === "complete") { 
        Restriccion.Empleado.init(); 
      } 
    }

    var _divPassword = document.getElementById("passwordDiv"); 
    var _checkboxPassword = document.getElementById("passwordCheckbox"); 
    _checkboxPassword.onchange = function(){ 
      if( _checkboxPassword.checked ){ 
        _divPassword.removeAttribute("hidden"); 
      }else{ 
        _divPassword.setAttribute("hidden", ""); 
      } 
    } 
     
    // Create a new 'change' event 
    var _event = new Event('change'); 
 
    // Dispatch it. 
    _checkboxPassword.dispatchEvent(_event); 
 
  </script> 
 
@stop