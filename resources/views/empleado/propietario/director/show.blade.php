@extends('layouts.master-director') 
 
@section('content')

  <div class="row">
    <nav aria-label="breadcrumb" style="margin-top: 10px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item" aria-current="page">Perfil de Usuario</li>
      </ol>
    </nav>
  </div>

  <div class="row">
    <div class="col-lg-12">
      @if ( session('message') ) 
        <div class="alert alert-success alert-dismissable"> 
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a> 
          <strong>EXITO: </strong> {{ session()->pull('message', 'default') }} 
        </div> 
      @endif 
    </div>
  </div>
  <!-- /.row -->

  <div class="row" id="errorRowDiv">
    <div class="col-lg-12">
      <div class="panel panel-danger panel-collapse collapse" id="errorPanelDiv">
        <div class="panel-heading">
          <strong>ERROR:</strong>
          <a data-toggle="collapse" href="#errorPanelDiv" class="close">&times</a>
        </div>
        <div class="panel-body" id="errorPanelBodyDiv">
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->

  <div class="row">

    <div class="col-lg-12"> 

      <div class="panel panel-default"> 
     
          <div class="panel-heading lead"> 
            <div class="row"> 
              <div class="col-lg-8 col-md-8"><i class="glyphicon glyphicon-user"></i> Usuario Detalle</div> 
              <div class="col-lg-4 col-md-4 text-right"> 
                <div class="btn-group text-center"> 
                  <a href={{ url('/empleados/edit/' . $empleado->id) }} class="btn btn-default btn-sm btn-default"><i class="glyphicon glyphicon-pencil"></i></a>
                </div> 
              </div> 
            </div> 
          </div> 
     
          <div class="panel-body"> 
            <div class="row"> 
              <div class="col-lg-3 col-md-3" align="center"> 
                @if( isset($empleado->ruta_avatar) ) 
                  <img alt="User Pic" src="{{$empleado->ruta_avatar}}" class="img-circle img-responsive" style="height: 170px; width: 170px; border: 4px solid #CCCCCC;"> 
                @else 
                  <img alt="User Pic" src={{ url('/images/default-avatar.png') }} class="img-circle img-responsive" style="height: 170px; width: 170px; border: 4px solid #CCCCCC;"> 
                @endif 
              </div> 
     
              <div class="col-lg-9 col-md-9"> 
                <div class="table-responsive panel"> 
                  <table class="table"> 
                    <tbody> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-bookmark"></i> NOMBRE</td> 
                        <td>{{ strtoupper($empleado->nombre) }}</td> 
                      </tr> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-object-align-horizontal"></i> APELLIDO PATERNO</td> 
                        <td>{{ strtoupper($empleado->ap_paterno) }}</td> 
                      </tr> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-object-align-left"></i> APELLIDO MATERNO</td> 
                        <td>{{ strtoupper($empleado->ap_materno) }}</td> 
                      </tr> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-credit-card"></i> CURP</td> 
                        <td>{{ $empleado->curp }}</td> 
                      </tr> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-envelope"></i> EMAIL</td> 
                        <td>{{ $empleado->email }}</td> 
                      </tr> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-record"></i> PUESTO</td> 
                        <td>{{ strtoupper($empleado->puesto) }}</td> 
                      </tr> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-tasks"></i> DEPARTAMENTO</td> 
                        <td>{{ strtoupper($empleado->nombre_departamento) }}</td> 
                      </tr> 
     
                      <tr> 
                        <td class="text-info"><i class="glyphicon glyphicon-briefcase"></i> AREA</td> 
                        <td>{{ strtoupper($empleado->area) }}</td> 
                      </tr> 
     
                    </tbody> 
                  </table> 
                </div> 
              </div> 
            </div> 
          </div> 
      </div>

    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->

@stop