@Echo Off
SETLOCAL EnableDelayedExpansion
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do     rem"') do (
  set "DEL=%%a"
)

echo(
call :colorEcho 0e "                    Artisan Migration"
echo(
call :colorEcho 0f " _________________________________________________________ "
echo(
call :colorEcho 03 "   Generando migraciones..."
call :colorEcho 03 "   "

set /p DUMMY=Hit ENTER to continue...

:: ------[ Start] ------
php artisan make:migration create_tiposdocumentos_table --create=tiposdocumentos
php artisan make:migration create_departamentos_table --create=departamentos
php artisan make:migration create_suscriptores_table --create=suscriptores
::php artisan make:migration create_puestos_table --create=puestos
php artisan make:migration create_documentos_table --create=documentos
php artisan make:migration create_empleados_table --create=empleados
php artisan make:migration add_id_empleado_to_users_table --table=users
php artisan make:migration create_empleadosdocumentos_table --create=empleadosdocumentos
php artisan make:migration create_solicitudescitas_table --create=solicitudescitas
php artisan make:migration create_citas_table --create=citas
php artisan make:migration create_agendas_table --create=agendas
php artisan make:migration create_solicitudesrespuestas_table --create=solicitudesrespuestas
php artisan make:migration create_guiasturnados_table --create=guiasturnados
php artisan make:migration create_detallesguiasturnados_table --create=detallesguiasturnados
php artisan make:migration create_reportesrevisiones_table --create=reportesrevisiones
php artisan make:migration create_respuestas_table --create=respuestas
php artisan make:migration create_eventos_table --create=eventos


php artisan make:migration create_documentosdbview_table --create=documentosdbview
php artisan make:migration create_cpsoaxaca --create=cpsoaxaca

pause
exit
:colorEcho
echo off
<nul set /p ".=%DEL%" > "%~2"
findstr /v /a:%1 /R "^$" "%~2" nul
del "%~2" > nul 2>&1i