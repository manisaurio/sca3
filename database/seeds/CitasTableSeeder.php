<?php

use Illuminate\Database\Seeder;
use App\Models\Cita;

class CitasTableSeeder extends Seeder
{
	private $arrayCitas = array(
        array(
            'nombre' => 'Reunio delegado sindical',
            'fecha' => '2017-09-11',
            'hora' => '13:30:00',
            'observacion' => 'Favor de ser puntual'
        ),
        array(
            'nombre' => 'Reunio delegado sindical',
            'fecha' => '2017-10-11',
            'hora' => '14:30:00',
            'observacion' => 'Asistencia voluntaria'
        )
    );
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Borramos los datos de la tabla
        DB::table('citas')->delete();

        // Añadimos entradas a esta tabla
        foreach( $this->arrayCitas as $cita ) {
            $evento = new Cita;
            $evento->nombre = strtoupper( $cita['nombre'] );
            $evento->fecha = $cita['fecha'];
            $evento->hora = $cita['hora'];
            $evento->observacion = $cita['observacion'];
            $evento->save();
        }
    }
}
