<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // Desabilitar verificacion foreign key para esta conexion antes de ejecutar seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(DepartamentosTableSeeder::class);
        $this->command->info('Tabla departamentos inicializada con datos!');

        $this->call(SuscriptoresTableSeeder::class);
        $this->command->info('Tabla suscriptores inicializada con datos!');

        $this->call(TiposdocumentosTableSeeder::class);
        $this->command->info('Tabla Tiposdocumentos inicializada con datos!');

        $this->call(DocumentosTableSeeder::class);
        $this->command->info('Tabla documentos inicializada con datos!');
       
        $this->call(EmpleadosTableSeeder::class);
        $this->command->info('Tabla empleados inicializada con datos!');

        $this->call(UsersTableSeeder::class);
        $this->command->info('Tabla users inicializada con datos!');

        $this->call(EmpleadosdocumentosTableSeeder::class);
        $this->command->info('Tabla empleadosdocumentos inicializada con datos!');

        $this->call(SolicitudescitasTableSeeder::class);
        $this->command->info('Tabla solicitudescitas inicializada con datos!');
       
        $this->call(SolicitudesrespuestasTableSeeder::class);
        $this->command->info('Tabla solicitudesrespuestas inicializada con datos!');
       
        $this->call(CpsoaxacaTableSeeder::class);
        $this->command->info('Tabla cpsoaxaca inicializada con datos!');

        factory('App\Models\Empleadodocumento', 1000)->create();
        $this->command->info('Tabla empleadodocumento inicializada con datos by factory!');
        
        /*
        factory(App\Models\Documento::class, 50)->create()->each(function ($u) {
            $u->posts()->save(factory(App\Post::class)->make());
        });
        */

        //$this->call(ReportesrevisionesTableSeeder::class);
        //$this->command->info('Tabla reportesrevisiones inicializada con datos!');
        /*
        $this->call(CitasTableSeeder::class);
        $this->command->info('Tabla citas inicializada con datos!');

        $this->call(AgendasTableSeeder::class);
        $this->command->info('Tabla agendas inicializada con datos!');

        $this->call(GuiasturnadosTableSeeder::class);
        $this->command->info('Tabla guiasturnados inicializada con datos!');

        $this->call(DetallesguiasturnadosTableSeeder::class);
        $this->command->info('Tabla detallesguiasturnados inicializada con datos!');
        */
        
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Model::reguard();
    }
}
