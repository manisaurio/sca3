<?php

use Illuminate\Database\Seeder;
use App\Models\Tipodocumento;

class TiposdocumentosTableSeeder extends Seeder
{
	private $arrayTiposdocumentos = array(
		array(
			'tipo' => 'OFICIO'
		),
		array(
			'tipo' => 'MEMORANDUM'
		)
	);
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Borramos los datos de la tabla
        DB::table('tiposdocumentos')->delete();

        // Añadimos entradas a esta tabla
        foreach( $this->arrayTiposdocumentos as $tipodocumento ) {
            $tipodoc = new Tipodocumento;
            $tipodoc->tipo = $tipodocumento['tipo'];
            $tipodoc->save();
        }
    }
}
