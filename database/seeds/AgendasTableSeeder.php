<?php

use Illuminate\Database\Seeder;
use App\Models\Agenda;

class AgendasTableSeeder extends Seeder
{
	private $arrayAgendas = array(
        array(
            'id_empleado' => 1,
            'id_documento' => 1,
            'id_cita' => 1
        )
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Borramos los datos de la tabla
        DB::table('agendas')->delete();

        // Añadimos entradas a esta tabla
        foreach( $this->arrayAgendas as $agenda ) {
            $agda = new Agenda;
            $agda->id_empleado = $agenda['id_empleado'];
            $agda->id_documento = $agenda['id_documento'];
            $agda->id_cita = $agenda['id_cita'];
            $agda->save();
        }
    }
}
