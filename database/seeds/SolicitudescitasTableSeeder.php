<?php

use Illuminate\Database\Seeder;
use App\Models\Solicitudcita;

class SolicitudescitasTableSeeder extends Seeder
{
	private $arraySolicitudescitas = array(
        array(
            'id_documento' => 1
        )
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Borramos los datos de la tabla
        DB::table('solicitudescitas')->delete();

        // Añadimos entradas a esta tabla
        foreach( $this->arraySolicitudescitas as $solicitudcita ) {
            $solicitud = new Solicitudcita;
            $solicitud->id_documento = $solicitudcita['id_documento'];
            $solicitud->save();
        }
    }
}
