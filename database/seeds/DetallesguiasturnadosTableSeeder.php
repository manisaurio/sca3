<?php

use Illuminate\Database\Seeder;
use App\Models\Detalleguiaturnado;

class DetallesguiasturnadosTableSeeder extends Seeder
{
    private $arrayDetallesGuiasturnados = array(
        array(
            'numorden_guiaturnado' => 1,
            'idemp_turnado' => 4,
            'instruccion' => 'ANALISIS'
        )
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Borramos los datos de la tabla
        DB::table('detallesguiasturnados')->delete();

        // Añadimos entradas a esta tabla
        foreach( $this->arrayDetallesGuiasturnados as $detalleGuiaTurnadoItem ) {
            $detalleguia = new Detalleguiaturnado;
            $detalleguia->numorden_guiaturnado = $detalleGuiaTurnadoItem['numorden_guiaturnado'];
            $detalleguia->idemp_turnado = $detalleGuiaTurnadoItem['idemp_turnado'];
            $detalleguia->instruccion = $detalleGuiaTurnadoItem['instruccion'];
            $detalleguia->save();
        }
    }
}
