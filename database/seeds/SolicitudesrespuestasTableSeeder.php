<?php

use Illuminate\Database\Seeder;
use App\Models\Solicitudrespuesta;

class SolicitudesrespuestasTableSeeder extends Seeder
{
	private $arraySolicitudesrespuestas = array(
        array(
            'id_documento' => 1,
            'fecha_limite' => '2018-02-28'
        )
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Borramos los datos de la tabla
        DB::table('solicitudesrespuestas')->delete();

        // Añadimos entradas a esta tabla
        foreach( $this->arraySolicitudesrespuestas as $solicitudrespuesta ) {
            $solicitud = new Solicitudrespuesta;
            $solicitud->id_documento = $solicitudrespuesta['id_documento'];
            $solicitud->fecha_limite = $solicitudrespuesta['fecha_limite'];
            $solicitud->save();
        }
    }
}
