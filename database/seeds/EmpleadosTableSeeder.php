<?php

use Illuminate\Database\Seeder;
use App\Models\Empleado;

class EmpleadosTableSeeder extends Seeder
{
	private $arrayEmpleados = array(
        array(
            'curp' => 'XXXX000000XXXXXX00',
            'nombre' => 'Administrador',
            'ap_paterno' => 'Sca', 
            'ap_materno' => 'Atencion Medica', 
            'puesto' => 'ADMIN', 
            'id_departamento' => 1
        ),
		array(
            'curp' => 'HEDM931223HOCRZN01',
			'nombre' => 'Mensajero1',
			'ap_paterno' => 'ApellidoP1', 
			'ap_materno' => 'ApellidoM1', 
			'puesto' => 'MENSAJERO', 
			'id_departamento' => 2
		),
        array(
            'curp' => 'HEDM931223HOCRZN02',
            'nombre' => 'Mensajero2',
            'ap_paterno' => 'ApellidoP2', 
            'ap_materno' => 'ApellidoM2', 
            'puesto' => 'MENSAJERO', 
            'id_departamento' => 2
        ),
        array(
            'curp' => 'HEDM931223HOCRZN03',
            'nombre' => 'Director1',
            'ap_paterno' => 'ApellidoP3', 
            'ap_materno' => 'ApellidoM3', 
            'puesto' => 'DIRECTOR', 
            'id_departamento' => 2
        ),
        array(
            'curp' => 'HEDM931223HOCRZN04',
            'nombre' => 'Asistente1',
            'ap_paterno' => 'ApellidoP4', 
            'ap_materno' => 'ApellidoM4', 
            'puesto' => 'ASISTENTE', 
            'id_departamento' => 2
        ),
        array(
            'curp' => 'HEDM931223HOCRZN05',
            'nombre' => 'JefeFortalecimiento',
            'ap_paterno' => 'ApellidoP5', 
            'ap_materno' => 'ApellidoM5', 
            'puesto' => 'JEFE', 
            'id_departamento' => 3
        ),
        array(
            'curp' => 'HEDM931223HOCRZN06',
            'nombre' => 'JefeEnfermeria',
            'ap_paterno' => 'ApellidoP6', 
            'ap_materno' => 'ApellidoM6', 
            'puesto' => 'JEFE', 
            'id_departamento' => 6
        ),
        array(
            'curp' => 'HEDM931223HOCRZN07',
            'nombre' => 'JefeEquidad',
            'ap_paterno' => 'ApellidoP7', 
            'ap_materno' => 'ApellidoM7', 
            'puesto' => 'JEFE', 
            'id_departamento' => 9
        ),
        array(
            'curp' => 'HEDM931223HOCRZN08',
            'nombre' => 'general',
            'ap_paterno' => 'ApellidoP', 
            'ap_materno' => 'ApellidoM', 
            'puesto' => 'GENERAL', 
            'id_departamento' => 7
        )
	);
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Borramos los datos de la tabla
        DB::table('empleados')->delete();

        // Añadimos entradas a esta tabla
        foreach( $this->arrayEmpleados as $empleado ) {
            $emp = new Empleado;
            $emp->curp = strtoupper( $empleado['curp'] );
            $emp->nombre = strtoupper( $empleado['nombre'] );
            $emp->ap_paterno = strtoupper( $empleado['ap_paterno'] );
            $emp->ap_materno = strtoupper($empleado['ap_materno'] );
            $emp->puesto = strtoupper( $empleado['puesto'] );
            $emp->id_departamento = $empleado['id_departamento'];
            $emp->save();
        }
    }
}
