<?php

use Illuminate\Database\Seeder;
use App\Models\Reporterevision;

class ReportesrevisionesTableSeeder extends Seeder
{
    private $arrayReportesrevisiones = array(
        array(
            'id_empleado' => 2,
            'id_documento' => 2,
            'observacion' => 'Alguna observación'
        )
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Borramos los datos de la tabla
        DB::table('reportesrevisiones')->delete();

        // Añadimos entradas a esta tabla
        foreach( $this->arrayReportesrevisiones as $reporteRevisionItem ) {
            $reporte = new Reporterevision;
            $reporte->id_empleado = $reporteRevisionItem['id_empleado'];
            $reporte->id_documento = $reporteRevisionItem['id_documento'];
            $reporte->observacion = $reporteRevisionItem['observacion'];
            $reporte->save();
        }
    }
}
