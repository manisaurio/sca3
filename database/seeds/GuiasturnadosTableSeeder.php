<?php

use Illuminate\Database\Seeder;
use App\Models\Guiaturnado;

class GuiasturnadosTableSeeder extends Seeder
{
    private $arrayGuiasturnados = array(
        array(
            'idemp_turnador' => 2,
            'id_documento' => 1,
            'reviso_turnador' => false
        )
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Borramos los datos de la tabla
        DB::table('guiasturnados')->delete();

        // Añadimos entradas a esta tabla
        foreach( $this->arrayGuiasturnados as $guiaTurnadoItem ) {
            $guia = new Guiaturnado;
            $guia->idemp_turnador = $guiaTurnadoItem['idemp_turnador'];
            $guia->id_documento = $guiaTurnadoItem['id_documento'];
            $guia->reviso_turnador = $guiaTurnadoItem['reviso_turnador'];
            $guia->save();
        }
    }
}
