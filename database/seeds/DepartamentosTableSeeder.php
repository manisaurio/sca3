<?php

use Illuminate\Database\Seeder;
use App\Models\Departamento;

class DepartamentosTableSeeder extends Seeder
{
	private $arrayDepartamentos = array(
        array(
            'nombre' => 'SISTEMA',
            'area' => 'direccion de atencion medica',
            'puestos' => '["ADMIN"]'
        ),
        array(
            'nombre' => 'direccion',
            'area' => 'direccion de atencion medica',
            'puestos' => '["DIRECTOR", "ASISTENTE", "MENSAJERO"]'
        ),
        array(
            'nombre' => 'jefatura',
            'area' => 'fortalecimiento a unidades medicas',
            'puestos' => '["JEFE"]'
        ),
        array(
            'nombre' => 'unidad de fortalecimiento en salud',
            'area' => 'fortalecimiento a unidades medicas',
            'puestos' => '["GENERAL"]'
        ),
        array(
            'nombre' => 'normatividad en equipamiento medico',
            'area' => 'fortalecimiento a unidades medicas',
            'puestos' => '["GENERAL"]'
        ),
        array(
            'nombre' => 'jefatura',
            'area' => 'enfermeria',
            'puestos' => '["JEFE"]'
        ),
        array(
            'nombre' => 'enfermeria asistencial',
            'area' => 'enfermeria',
            'puestos' => '["GENERAL"]'
        ),
        array(
            'nombre' => 'servicios primarios de enfermeria',
            'area' => 'enfermeria',
            'puestos' => '["GENERAL"]'
        ),
        array(
            'nombre' => 'jefatura',
            'area' => 'equidad en la atencion medica',
            'puestos' => '["JEFE"]'
        ),
        array(
            'nombre' => 'ampliacion de cobertura',
            'area' => 'equidad en la atencion medica',
            'puestos' => '["GENERAL"]'
        ),
        array(
            'nombre' => 'coordinacion del programa fortalecimiento a la atencion medica',
            'area' => 'equidad en la atencion medica',
            'puestos' => '["GENERAL"]'
        ),
        array(
            'nombre' => 'servicios medicos especializados',
            'area' => 'equidad en la atencion medica',
            'puestos' => '["GENERAL"]'
        ),
        array(
            'nombre' => 'coordinacion GENERAL de telemedicina',
            'area' => 'equidad en la atencion medica',
            'puestos' => '["GENERAL"]'
        ),
        array(
            'nombre' => 'servicios esenciales',
            'area' => 'equidad en la atencion medica',
            'puestos' => '["GENERAL"]'
        )
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Borramos los datos de la tabla
        DB::table('departamentos')->delete();

        // Añadimos entradas a esta tabla
        foreach( $this->arrayDepartamentos as $departamento ) {
            $depto = new Departamento;
            $depto->nombre = strtoupper( $departamento['nombre'] );
            $depto->area = strtoupper( $departamento['area'] );
            $depto->puestos = $departamento['puestos'];
            $depto->save();
        }
    }
}
