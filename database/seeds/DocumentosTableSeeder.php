<?php

use Illuminate\Database\Seeder;
use App\Models\Documento;

class DocumentosTableSeeder extends Seeder
{
	private $arrayDocumentos = array(
		array(
			'asunto' => 'copia para conocimiento de oficio Dirigido a la Dra. Judith Luna Angel y el Dr. Juan Carlos Cabrera León, respecto a la notificacion y en cumplimiento a lo ordenado en el Expediente 163/RA-8/2017',
			'id_tipodocumento' => 1, 
			'num_hojas' => 1, 
            'num_anexos' => 2,
			'num_oficio' => 'O-001', 
            'fecha_oficio' => '2017-08-08',
            'id_suscriptor' => 18
		)
	);

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Borramos los datos de la tabla
        DB::table('documentos')->delete();

        // Añadimos entradas a esta tabla
        foreach( $this->arrayDocumentos as $documento ) {
        	$doc = new Documento;
    		$doc->asunto = $documento['asunto'];
    		$doc->id_tipodocumento = $documento['id_tipodocumento'];
    		$doc->num_hojas = $documento['num_hojas'];
            $doc->num_anexos = $documento['num_anexos'];
    		$doc->num_oficio = strtoupper( $documento['num_oficio'] );
            $doc->fecha_oficio = $documento['fecha_oficio'];
            $doc->id_suscriptor = $documento['id_suscriptor'];
    		$doc->save();
		}
    }
}
