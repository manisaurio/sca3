<?php

use Illuminate\Database\Seeder;
use App\Models\Empleadodocumento;

class EmpleadosdocumentosTableSeeder extends Seeder
{
	private $arrayEmpleadosdocumentos = array(
        array(
            'actividad' => 'INGRESO',
            'id_empleado' => 2,
            'id_documento' => 1
        )
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Borramos los datos de la tabla
        DB::table('empleadosdocumentos')->delete();

        // Añadimos entradas a esta tabla
        foreach( $this->arrayEmpleadosdocumentos as $empleadodocumento ) {
            $empdoc = new Empleadodocumento;
            $empdoc->actividad = strtoupper( $empleadodocumento['actividad'] );
            $empdoc->id_empleado = $empleadodocumento['id_empleado'];
            $empdoc->id_documento = $empleadodocumento['id_documento'];
            $empdoc->save();
        }
    }
}
