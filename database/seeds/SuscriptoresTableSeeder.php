<?php

use Illuminate\Database\Seeder;
use App\Models\Suscriptor;

class SuscriptoresTableSeeder extends Seeder
{
	private $arraySuscriptores = array(
        array( // row #0
            'nombre' => 'DR. ROBERTO SALVADOR LUNA CRUZ',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL BÁSICO COMUNITARIO DE SAN JUAN BAUTISTA VALLE NACIONAL',
            'geometry_dependencia' => '{"location": {"lat": 17.7734065, "lng": -96.31414259999995}, "viewport": {"east": -96.31279361970849, "west": -96.3154915802915, "north": 17.7747554802915, "south": 17.7720575197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'Benito Juárez Isleta Delfin San Juan Bautista Valle Nacional',
            'postal_code' => '68480',
        ),
        array( // row #1
            'nombre' => 'DR. DANIEL HERNÁNDEZ LIMA',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL GENERAL DOCTOR ALBERTO VARGAS MERINO',
            'geometry_dependencia' => '{"location": {"lat": 17.8000359, "lng": -96.9612869}, "viewport": {"east": -96.95993791970848, "west": -96.96263588029149, "north": 17.8013848802915, "south": 17.79868691970849}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'SN Calle Zaragoza Barrio la Garita',
            'postal_code' => '68600',
        ),
        array( // row #2
            'nombre' => 'DR. OSCAR VICENTE MARTÍNEZ',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL GENERAL TUXTEPEC',
            'geometry_dependencia' => '{"location": {"lat": 18.0871919, "lng": -96.12969450000004}, "viewport": {"east": -96.12845481970852, "west": -96.13115278029152, "north": 18.08857053029151, "south": 18.0858725697085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => '310 Sebastián Ortiz María Luisa',
            'postal_code' => '68310',
        ),
        array( // row #3
            'nombre' => 'DR. NAHUM RICHARD OSORIO SÁNCHEZ',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL BÁSICO COMUNITARIO DE CHALCATONGO DE HIDALGO',
            'geometry_dependencia' => '{"location": {"lat": 17.0308889, "lng": -97.5708659}, "viewport": {"east": -97.56951691970852, "west": -97.57221488029154, "north": 17.0322378802915, "south": 17.0295399197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'SN Carretera Yosundua Centro',
            'postal_code' => '71100',
        ),
        array( // row #4
            'nombre' => 'DR. ALBERTO GUTIÉRREZ ARENAS',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL GENERAL',
            'geometry_dependencia' => '{"location": {"lat": 17.0266617, "lng": -97.9271819}, "viewport": {"east": -97.92583291970844, "west": -97.92853088029152, "north": 17.0280106802915, "south": 17.0253127197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'SN Carretera Alfonso Pérez Gazga Malpica',
            'postal_code' => '71000',
        ),
        array( // row #5
            'nombre' => 'DRA. MARÍA PAULINA LÓPEZ JUAN',
            'cargo' => 'DIRECTORA',
            'dependencia' => 'HOSPITAL GENERAL PUERTO ESCONDIDO',
            'geometry_dependencia' => '{"location": {"lat": 15.8801409, "lng": -97.0890888}, "viewport": {"east": -97.08773981970852, "west": -97.0904377802915, "north": 15.8814898802915, "south": 15.8787919197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'Jardines Jardines Puerto Escondido',
            'postal_code' => '71985',
        ),
        array( // row #6
            'nombre' => 'DR. JESÚS ALEJANDRO RAMÍREZ FIGUEROA',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL GENERAL DE TEHUANTEPEC',
            'geometry_dependencia' => '{"location": {"lat": 16.2913194, "lng": -95.23321679999998}, "viewport": {"east": -95.23187716970848, "west": -95.23457513029147, "north": 16.2926985802915, "south": 16.2900006197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'Universitario 4ta de Sta Cruz Tehuantepec',
            'postal_code' => '70760',
        ),
        array( // row #7
            'nombre' => 'DR. GILBERTO JIMÉNEZ VILA',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'SALINA CRUZ',
            'geometry_dependencia' => '{"location": {"lat": 16.2191018, "lng": -95.211727}, "viewport": {"east": -95.21037801970851, "west": -95.21307598029148, "north": 16.2204507802915, "south": 16.2177528197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'Salina Cruz Deportiva Nte. Oax.',
            'postal_code' => '70600',
        ),
        array( // row #8
            'nombre' => 'DR. REYNALDO URIEL PÉREZ REYES',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL COMUNITARIO',
            'geometry_dependencia' => '{"location": {"lat": 17.053704, "lng": -96.07301710000002}, "viewport": {"east": -96.07166811970848, "west": -96.0743660802915, "north": 17.0550529802915, "south": 17.0523550197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'Tamazulápam del Espíritu Santo Oax. MX',
            'postal_code' => '70280',
        ),
        array( // row #9
            'nombre' => 'DR. RAFAEL ARNAUD RÍOS',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL GENERAL SAN PEDRO POCHUTLA',
            'geometry_dependencia' => '{"location": {"lat": 15.7345118, "lng": -96.4724468}, "viewport": {"east": -96.47109781970852, "west": -96.47379578029154, "north": 15.7358607802915, "south": 15.73316281970849}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'Chapingo 3ra San Pedro Pochutla',
            'postal_code' => '70900',
        ),
        array( // row #10
            'nombre' => 'DR. REYNALDO CANSECO GARCÍA',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL BASICO COMUNITARIO DE SAN PEDRO HUAMELULA',
            'geometry_dependencia' => '{"location": {"lat": 16.0185346, "lng": -95.6653324}, "viewport": {"east": -95.66398341970849, "west": -95.6666813802915, "north": 16.0198835802915, "south": 16.0171856197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'San Pedro Huamelula Oax. MX',
            'postal_code' => '70770',
        ),
        array( // row #11
            'nombre' => 'DR. FRANCISCO GARCÍA SERAPIO',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL REGIONAL "DOCTOR PEDRO ESPINOSA RUEDA"',
            'geometry_dependencia' => '{"location": {"lat": 16.3471949, "lng": -98.04839800000002}, "viewport": {"east": -98.0468926197085, "west": -98.04959058029152, "north": 16.34847823029151, "south": 16.3457802697085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'SN Calle Segunda Nte. La Posta',
            'postal_code' => '71600',
        ),
        array( // row #12
            'nombre' => 'DRA. ARACELY DIAZ JIMÉNEZ',
            'cargo' => 'DIRECTORA',
            'dependencia' => 'HOSPITAL GENERAL MARÍA LOMBARDO DE CASO',
            'geometry_dependencia' => '{"location": {"lat": 17.449386, "lng": -95.432055}, "viewport": {"east": -95.43070601970852, "west": -95.43340398029147, "north": 17.4507349802915, "south": 17.4480370197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'S/N Cinco Señores Centro',
            'postal_code' => '70215',
        ),
        array( // row #13
            'nombre' => 'DR. JOSÉ ROBERTO VALADEZ  SÁNCHEZ',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL REGIONAL DE SAN PABLO HUIXTEPEC',
            'geometry_dependencia' => '{"location": {"lat": 16.8169126, "lng": -96.7861479}, "viewport": {"east": -96.78479891970846, "west": -96.78749688029149, "north": 16.8182615802915, "south": 16.8155636197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'Barrio la Guadalupe Barrio de la Guadalupe San Pablo Huixtepec',
            'postal_code' => '71270',
        ),
        array( // row #14
            'nombre' => 'DR. JUAN MANUEL CRUZ RUIZ',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL COMUNITARIO, SOLA DE VEGA',
            'geometry_dependencia' => '{"location": {"lat": 16.5292695, "lng": -96.9790076}, "viewport": {"east": -96.97765861970844, "west": -96.98035658029151, "north": 16.5306184802915, "south": 16.5279205197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'San Ildefonso Sola Oax. MX',
            'postal_code' => '71480',
        ),
        array( // row #15
            'nombre' => 'DR. RAMSÉS ENRIQUE GUEVARA ZARATE',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL BÁSICO COMUNITARIO DE LA PAZ TEOJOMULCO-TEXMELUCAN',
            'geometry_dependencia' => '{"location": {"lat": 16.578937, "lng": -97.2203427}, "viewport": {"east": -97.21899371970846, "west": -97.22169168029149, "north": 16.5802859802915, "south": 16.5775880197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'MX Oax.',
            'postal_code' => '71397',
        ),
        array( // row #16
            'nombre' => 'DR. FERNANDO RAMÍREZ GALARDE',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL COMUNITARIO SNT. CAT. JUQUILA',
            'geometry_dependencia' => '{"location": {"lat": 16.230699, "lng": -97.289039}, "viewport": {"east": -97.28769001970852, "west": -97.29038798029148, "north": 16.2320479802915, "south": 16.2293500197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'Santa Catarina Juquila Oax. MX',
            'postal_code' => '71900',
        ),
        array( // row #17
            'nombre' => 'DRA. MARITZA JENNY HERNÁNDEZ CUEVAS',
            'cargo' => 'DIRECTORA',
            'dependencia' => 'HOSPITAL GENERAL “DR AURELIO VALDIVIESO”',
            'geometry_dependencia' => '{"location": {"lat": 17.0820586, "lng": -96.71850970000004}, "viewport": {"east": -96.71723451970848, "west": -96.7199324802915, "north": 17.08342693029151, "south": 17.0807289697085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => '400 Calzada Porfirio Díaz Reforma',
            'postal_code' => '68050',
        ),
        array( // row #18
            'nombre' => 'DR. JOSÉ DANIEL CASTAÑEDA ALONSO',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL BÁSICO COMUNITARIO DE TEOTITLÁN DE FLORES MAGÓN, OAX.',
            'geometry_dependencia' => '{"location": {"lat": 18.1308955, "lng": -97.07501709999995}, "viewport": {"east": -97.07364086970848, "west": -97.07633883029148, "north": 18.1323444802915, "south": 18.1296465197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => '74 Av 5 de Mayo Guadalupe',
            'postal_code' => '68540',
        ),
        array( // row #19
            'nombre' => 'DR. VÍCTOR ANTONIO RICÁRDEZ PEÑA',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL BÁSICO COMUNITARIO IXTLÁN',
            'geometry_dependencia' => '{"location": {"lat": 17.3304839, "lng": -96.48718079999998}, "viewport": {"east": -96.48582416970852, "west": -96.48852213029151, "north": 17.3318443802915, "south": 17.3291464197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png',
            'address_dependencia' => '1 Av. Fidencio Hernández Campos Ixtlán de Juárez',
            'postal_code' => '68725',
        ),
        array( // row #20
            'nombre' => 'DR. JORGE PÉREZ SANTIAGO',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL DE LA COMUNIDAD - SANTA MARÍA HUATULCO',
            'geometry_dependencia' => '{"location": {"lat": 15.8316611, "lng": -96.31806669999996}, "viewport": {"east": -96.3168007697085, "west": -96.31949873029151, "north": 15.8328832302915, "south": 15.8301852697085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'Calle Juan de la Barrera Las Palmas Morelos',
            'postal_code' => '70980',
        ),
        array( // row #21
            'nombre' => 'DR. BRÍGIDO REYES FERNÁNDEZ',
            'cargo' => 'DIRECTOR',
            'dependencia' => 'HOSPITAL GENERAL DE CIUDAD IXTEPEC',
            'geometry_dependencia' => '{"location": {"lat": 16.5585394, "lng": -95.11808859999996}, "viewport": {"east": -95.11673961970848, "west": -95.1194375802915, "north": 16.5598883802915, "south": 16.5571904197085}}',
            'icon_dependencia' => 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png',
            'address_dependencia' => 'Kilómetro 2.5 Carretera Ixtepec A Chihuitan Raymundo Melendez',
            'postal_code' => '70110',
        )
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Borramos los datos de la tabla
        DB::table('suscriptores')->delete();

        // Añadimos entradas a esta tabla
        foreach( $this->arraySuscriptores as $suscriptor ) {
            $suscrip = new Suscriptor;
            $suscrip->nombre = $suscriptor['nombre'];
            $suscrip->cargo = $suscriptor['cargo'];
            $suscrip->dependencia = $suscriptor['dependencia'];
            $suscrip->geometry_dependencia = $suscriptor['geometry_dependencia'];
            $suscrip->icon_dependencia = $suscriptor['icon_dependencia'];
            $suscrip->address_dependencia = $suscriptor['address_dependencia'];
            $suscrip->postal_code = $suscriptor['postal_code'];
            $suscrip->save();
        }
    }
}
