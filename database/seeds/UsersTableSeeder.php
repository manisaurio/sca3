<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    private $arrayUsuarios = array(
        array(
            'email' => 'admin@gmail.com', 
            'password' => 'manuel',
            'id_empleado' => 1
        ),
        array(
            'email' => 'mensajero1@gmail.com', 
            'password' => 'manuel',
            'id_empleado' => 2
        ),
        array(
            'email' => 'mensajero2@gmail.com', 
            'password' => 'manuel',
            'id_empleado' => 3
        ),
        array(
            'email' => 'director1@gmail.com', 
            'password' => 'manuel',
            'id_empleado' => 4
        ),
        array(
            'email' => 'asistente1@gmail.com', 
            'password' => 'manuel',
            'id_empleado' => 5
        ),
        array(
            'email' => 'jefemedica@gmail.com', 
            'password' => 'manuel',
            'id_empleado' => 6
        ),
        array(
            'email' => 'jefeenfermeria@gmail.com', 
            'password' => 'manuel',
            'id_empleado' => 7
        ),
        array(
            'email' => 'jefefortalecimiento@gmail.com', 
            'password' => 'manuel',
            'id_empleado' => 8
        ),
        array(
            'email' => 'general@gmail.com', 
            'password' => 'manuel',
            'id_empleado' => 9
        )
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Borramos los datos de la tabla
        DB::table('users')->delete();

        // Añadimos entradas a esta tabla
        foreach( $this->arrayUsuarios as $usuario ) {
            $usr = new User;
            $usr->email = $usuario['email'];
            $usr->password = bcrypt( $usuario['password'] );
            $usr->id_empleado = $usuario['id_empleado'];
            $usr->save();
        }
        // Agregar roles de usuario para los grupos de usuario
    }
}
