<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Empleadodocumento::class, function (Faker $faker) {
    return [
        //dateTimeBetween($startDate = '-30 years', $endDate = 'now', $timezone = null)
        'registrado' => $faker->dateTimeBetween($startDate = '-3 months', $endDate = 'now', $timezone = null),
        'actividad' => 'INGRESO',
    	'id_empleado' => $faker->numberBetween($min = 2, $max = 3),
    	'id_documento' => function () {
            return factory(App\Models\Documento::class)->create()->id;
        },
    	'estado' => 'INGRESADO'
    ];

});
