<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Documento::class, function (Faker $faker) {
    return [
    	'asunto' => $faker->text($maxNbChars = 200),
    	'num_hojas' => $faker->numberBetween($min = 1, $max = 5),
    	'num_anexos' => $faker->numberBetween($min = 0, $max = 3),
    	'num_oficio' => $faker->numerify('O-###'),
    	'fecha_oficio' => $faker->date($format = 'Y-m-d', $max = 'now'),
    	'id_tipodocumento' => $faker->numberBetween($min = 1, $max = 2),
    	'id_suscriptor' => $faker->numberBetween($min = 1, $max = 22)
    ];
});
