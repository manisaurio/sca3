<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallesguiasturnadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detallesguiasturnados', function (Blueprint $table) {
            $table->bigInteger('numorden_guiaturnado')->unsigned();
            $table->foreign('numorden_guiaturnado')
                  ->references('num_orden')->on('guiasturnados')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->integer('idemp_turnado')->unsigned();
            $table->foreign('idemp_turnado')
                  ->references('id')->on('empleados')
                  ->onUpdate('cascade');
            $table->enum('instruccion', array('CONOCIMIENTO', 'REVISION_ATENCION', 'REDACTAR_RESPUESTA', 'ASIGNAR_RESPONSABLE', 'RESPONDER', 'ANALISIS', 'DISTRIBUCION', 'REVISION', 'ARCHIVAR'));
            $table->enum('estado', array('NO_VISTO', 'VISTO', 'REVISADO', 'REVISADO_TURNADO', 'TURNADO', 'CON_RESPUESTA', 'ARCHIVADO'))
                  ->default('NO_VISTO');
            $table->enum('prioridad', array('SIN_ASIGNAR', 'ALTA', 'MEDIA', 'BAJA'))
                  ->default('SIN_ASIGNAR');
            $table->timestamp('visto')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detallesguiasturnados');
    }
}
