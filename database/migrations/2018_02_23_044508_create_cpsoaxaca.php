<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCpsoaxaca extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpsoaxaca', function (Blueprint $table) {
            $table->string('cp', 100);
            $table->string('asentamiento', 700);
            $table->string('municipio', 700);
            $table->string('region', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpsoaxaca');
    }
}
