<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuiasturnadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guiasturnados', function (Blueprint $table) {
            $table->bigIncrements('num_orden');
            $table->integer('idemp_turnador')->unsigned();
            $table->foreign('idemp_turnador')
                  ->references('id')->on('empleados')
                  ->onUpdate('cascade');
            $table->bigInteger('id_documento')->unsigned();
            $table->foreign('id_documento')
                  ->references('id')->on('documentos')
                  ->onUpdate('cascade');
            $table->timestamp('registrado');
            $table->boolean('reviso_turnador')
                  ->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guiasturnados');
    }
}
