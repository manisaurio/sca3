<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosdocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleadosdocumentos', function (Blueprint $table) {
            $table->timestamp('registrado');
            $table->enum('actividad', array('EDICION', 'INGRESO', 'INGRESO-EDITADO', 'EMISION', 'RESPUESTA', 'SALIDA'));
            $table->integer('id_empleado')->unsigned();
            $table->foreign('id_empleado')
                  ->references('id')->on('empleados')
                  ->onUpdate('cascade');
            $table->bigInteger('id_documento')->unsigned();
            $table->foreign('id_documento')
                  ->references('id')->on('documentos')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->enum('estado', array('INGRESADO', 'REVISADO', 'EN_TRAMITE', 'ENVIADO', 'FINALIZADO'))   //---[ DONDE SE ENCUNTRA *ENVIADO(Cuando sale)]---
                  ->default('INGRESADO');
            $table->string('descripcion', 500)->default('SIN DESCRIPCION'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*
        Schema::table('empleadosdocumentos', function (Blueprint $table) {
            $table->dropForeign('empleadosdocumentos_id_empleado_foreign');
            $table->dropForeign('empleadosdocumentos_id_documento_foreign');
        });
        */

        Schema::dropIfExists('empleadosdocumentos');
    }
}
