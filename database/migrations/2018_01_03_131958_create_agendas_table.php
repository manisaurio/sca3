<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendas', function (Blueprint $table) {
            $table->timestamp('registrado');
            $table->integer('id_empleado')->unsigned();
            $table->foreign('id_empleado')
                  ->references('id')->on('empleados')
                  ->onUpdate('cascade');
            $table->bigInteger('id_documento')->unsigned();
            $table->foreign('id_documento')
                  ->references('id')->on('documentos')
                  ->onUpdate('cascade');    // Preguntar si eliminar agenda al eliminar documento ->onDelete('cascade');
            $table->bigInteger('id_cita')->unsigned();
            $table->foreign('id_cita')
                  ->references('id')->on('citas')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendas');
    }
}
