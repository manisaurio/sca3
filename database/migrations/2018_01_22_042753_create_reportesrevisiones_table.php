<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportesrevisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reportesrevisiones', function (Blueprint $table) {
            //$table->bigIncrements('id');
            $table->bigInteger('numorden_guiaturnado')->unsigned();
            $table->foreign('numorden_guiaturnado')
                  ->references('num_orden')->on('guiasturnados')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->integer('id_empleado')->unsigned();
            $table->foreign('id_empleado')
                  ->references('id')->on('empleados')
                  ->onUpdate('cascade');
            $table->bigInteger('id_documento')->unsigned();
            $table->foreign('id_documento')
                  ->references('id')->on('documentos')
                  ->onUpdate('cascade');
            $table->timestamp('registrado');
            $table->string('observacion', 700);
            $table->boolean('recibi_papel')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reportesrevisiones');
    }
}
