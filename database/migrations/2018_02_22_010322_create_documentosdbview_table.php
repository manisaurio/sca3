<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDocumentosdbviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW documentosdbview AS
            SELECT 
                documentos.id AS id_documento, 
                suscriptores.dependencia, 
                suscriptores.geometry_dependencia,
                suscriptores.icon_dependencia,
                suscriptores.address_dependencia,
                suscriptores.postal_code,
                empleadosdocumentos.registrado
            FROM documentos 
                INNER JOIN suscriptores ON documentos.id_suscriptor = suscriptores.id
                INNER JOIN empleadosdocumentos ON documentos.id = empleadosdocumentos.id_documento
            WHERE
                (empleadosdocumentos.actividad = 'ingreso')
            ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW documentosdbview");
    }
}
