<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudescitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudescitas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_documento')->unsigned();
            $table->foreign('id_documento')
                  ->references('id')->on('documentos')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->enum('estado', array('AGENDADO', 'NO_AGENDADO', 'DESCARTADO'))->default('NO_AGENDADO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudescitas');
    }
}
