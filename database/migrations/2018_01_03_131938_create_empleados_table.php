<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('curp', 18)->unique();
            $table->string('nombre', 250);
            $table->string('ap_paterno', 250);
            $table->string('ap_materno', 250);
            $table->enum('puesto', array('ADMIN', 'DIRECTOR', 'ASISTENTE', 'MENSAJERO', 'JEFE', 'GENERAL'));
            $table->integer('id_departamento')->unsigned();
            $table->foreign('id_departamento')
                  ->references('id')->on('departamentos')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*
        Schema::table('empleados', function (Blueprint $table) {
            $table->dropForeign('empleados_id_departamento_foreign');
        });
        */

        Schema::dropIfExists('empleados');
    }
}
