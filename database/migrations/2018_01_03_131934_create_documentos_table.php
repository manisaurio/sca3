<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('asunto', 250);
            //$table->enum('tipo', array('oficio', 'memorandum', 'otro'));
            $table->smallInteger('num_hojas');
            $table->smallInteger('num_anexos');
            $table->string('num_oficio', 30);
            $table->date('fecha_oficio');
            $table->string('ruta_pdf', 250);
            $table->integer('id_tipodocumento')->unsigned();
            $table->foreign('id_tipodocumento')
                  ->references('id')->on('tiposdocumentos')
                  ->onUpdate('cascade');
            $table->integer('id_suscriptor')->unsigned();
            $table->foreign('id_suscriptor')
                  ->references('id')->on('suscriptores')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*
        Schema::table('documentos', function (Blueprint $table) {
            $table->dropForeign('documentos_id_suscriptor_foreign');
        });
        */

        Schema::dropIfExists('documentos');
    }
}
