<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas', function (Blueprint $table) {
            $table->bigInteger('id_solicitud')->unsigned();
            $table->foreign('id_solicitud')
                  ->references('id')->on('solicitudesrespuestas')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->bigInteger('id_documento_respuesta')->unsigned();
            $table->foreign('id_documento_respuesta')
                  ->references('id')->on('documentos')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->timestamp('registrado');
            $table->integer('id_empleado')->unsigned();
            $table->foreign('id_empleado')
                  ->references('id')->on('empleados')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuestas');
    }
}
