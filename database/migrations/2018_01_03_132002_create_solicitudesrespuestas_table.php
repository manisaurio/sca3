<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudesrespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudesrespuestas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_documento')->unsigned();
            $table->foreign('id_documento')
                  ->references('id')->on('documentos')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->date('fecha_limite');
            $table->enum('estado', array('ATENDIDO', 'DESCARTADA', 'CON_RESPUESTA', 'EN_PROCESO', 'NO_ATENDIDO'))->default('NO_ATENDIDO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudesrespuestas');
    }
}
