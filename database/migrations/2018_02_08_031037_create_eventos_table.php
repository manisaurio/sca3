<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_eventgoogle', 30);
            $table->bigInteger('id_solicitud')->unsigned()->nullable();
            $table->foreign('id_solicitud')
                  ->references('id')->on('solicitudescitas')
                  ->onUpdate('cascade');
            $table->integer('id_empleado')->unsigned();
            $table->foreign('id_empleado')
                  ->references('id')->on('empleados')
                  ->onUpdate('cascade');
            $table->timestamp('registrado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
