@Echo Off
SETLOCAL EnableDelayedExpansion
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do     rem"') do (
  set "DEL=%%a"
)

for /f %%a in ('WMIC OS GET LocalDateTime ^| find "."') do set DTS=%%a
set fecha=%DTS:~0,8%-%DTS:~8,6%

echo(
call :colorEcho 0e "                    Artisan Migration"
echo(
call :colorEcho 0f " _________________________________________________________ "
echo(
call :colorEcho 03 "   Generando migraciones..."
call :colorEcho 03 "   "

set /p DUMMY=Hit ENTER to continue...

:: ------[ Start] ------
php artisan make:controller EmpleadosController
php artisan make:controller DepartamentosController
php artisan make:controller TiposdocumentosController
php artisan make:controller DocumentosController
php artisan make:controller SuscriptoresController
php artisan make:controller CitasController
php artisan make:controller UsersController
php artisan make:controller ReportesrevisionesController
php artisan make:controller GuiasturnadosController
php artisan make:controller RespuestasController

php artisan make:controller NotificacionesController
php artisan make:controller RespaldosController

php artisan make:controller missingMethod
php artisan make:controller HomeController


pause
exit
:colorEcho
echo off
<nul set /p ".=%DEL%" > "%~2"
findstr /v /a:%1 /R "^$" "%~2" nul
del "%~2" > nul 2>&1i