<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/login', function () {
    return view('auth.login');
});

Route::group(['middleware' => 'auth'], function() {

	Route::get('/documentos/procedencia', 'DocumentosController@procedencia');
    Route::get('/documentos/listaingreso', 'DocumentosController@listaIngreso');
    Route::get('/documentos/listaemision', 'DocumentosController@listaEmision');
	Route::get('/documentos/show/{id}', 'DocumentosController@show');
	Route::get('/documentos/ingreso', 'DocumentosController@createIngreso');
	Route::post('/documentos/ingreso', 'DocumentosController@storeIngreso');
	Route::get('/documentos/edit/{id}', 'DocumentosController@edit');
	Route::put('/documentos/edit/{id}', 'DocumentosController@update');
	Route::delete('/documentos/destroy/{id}', 'DocumentosController@destroy');

	Route::get('/users/{id}', 'UsersController@showProfile');

	Route::get('/home', 'HomeController@index')->name('home');

	Route::get('/citas/calendar', 'CitasController@calendar');
	Route::get('/citas/showmodal/{idsolicitud}', 'CitasController@showModal');
	//Route::get('/citas/create', 'CitasController@create');
	Route::post('/citas/storemodal', 'CitasController@storeModal');
	Route::delete('/citas/destroy', 'CitasController@destroyAjax');
	Route::put('/citas/update', 'CitasController@updateAjax');

	Route::get('/respuestas/lista', 'RespuestasController@lista');
	Route::get('/respuestas/show/{numorden}/{idsolicitud}', 'RespuestasController@show');
	Route::get('/respuestas/create/{numorden}/{idsolicitud}', 'RespuestasController@create');
	Route::post('/respuestas/create/{numorden}/{idsolicitud}', 'RespuestasController@store');
	Route::get('/respuestas/edit/{numorden}/{idsolicitud}', 'RespuestasController@edit');
	Route::put('/respuestas/edit/{numorden}/{idsolicitud}', 'RespuestasController@update');
	Route::delete('/respuestas/destroy/{numorden}/{idsolicitud}', 'RespuestasController@destroy');

	Route::get('/reportesrevisiones/lista', 'ReportesrevisionesController@listaxturnado');
	Route::get('/reportesrevisiones/show/{numorden}/{id}', 'ReportesrevisionesController@showxturnado');
	Route::get('/reportesrevisiones/create/{numorden}/{id}', 'ReportesrevisionesController@createxturnado');
	Route::post('/reportesrevisiones/create/{numorden}/{id}', 'ReportesrevisionesController@storexturnado');
	Route::get('/reportesrevisiones/edit/{numorden}/{id}', 'ReportesrevisionesController@editxturnado');
	Route::put('/reportesrevisiones/edit/{numorden}/{id}', 'ReportesrevisionesController@updatexturnado');
	Route::delete('/reportesrevisiones/destroy/{numorden}/{id}', 'ReportesrevisionesController@destroyxturnado');

	Route::get('/guiasturnados/listaturnadoentrada', 'GuiasturnadosController@listaTurnadoEntrada');
	Route::get('/guiasturnados/listaturnadosalida', 'GuiasturnadosController@listaTurnadoSalida');
	Route::get('/guiasturnados/show/{numorden}/{id}', 'GuiasturnadosController@show');
	Route::get('/guiasturnados/create/{id}', 'GuiasturnadosController@create');
	Route::post('/guiasturnados/create/{id}', 'GuiasturnadosController@store');

	Route::get('/guiasturnados/create/{oldnumorden}/{id}', 'GuiasturnadosController@createxturnado');
	Route::post('/guiasturnados/create/{oldnumorden}/{id}', 'GuiasturnadosController@storexturnado');

	Route::get('/empleados/list', 'EmpleadosController@index');
	Route::get('/empleados/create', 'EmpleadosController@create');
	Route::post('/empleados/create', 'EmpleadosController@store');
	Route::get('/empleados/show/{id}', 'EmpleadosController@show');
	Route::get('/empleados/edit/{id}', 'EmpleadosController@edit');
	Route::put('/empleados/edit/{id}', 'EmpleadosController@update');
	Route::put('/empleados/switch/{id}', 'EmpleadosController@switch');
	Route::delete('/empleados/destroy/{id}', 'EmpleadosController@destroy');

	Route::get('/departamentos/list', 'DepartamentosController@index');
	Route::get('/departamentos/show/{id}', 'DepartamentosController@show');
	Route::get('/departamentos/create', 'DepartamentosController@create');
	Route::post('/departamentos/create', 'DepartamentosController@store');
	Route::get('/departamentos/edit/{id}', 'DepartamentosController@edit');
	Route::put('/departamentos/edit/{id}', 'DepartamentosController@update');
	Route::delete('/departamentos/destroy/{id}', 'DepartamentosController@destroy');

	Route::post('/nofiticaciones/marcarcomoleidas', 'NotificacionesController@marcarComoLeidas');
	Route::get('/nofiticaciones/lista', 'NotificacionesController@lista');
	
	Route::get('api/nofiticaciones/dataTableList', 'NotificacionesController@dataTableList');

	Route::get('/respaldos/menu', 'RespaldosController@menu');
	Route::post('/respaldos/menu', 'RespaldosController@menu');
	Route::post('/respaldos/restore', 'RespaldosController@restore');

	Route::get('api/respaldos/dataTableList', 'RespaldosController@dataTableList');

});
//-----[ RESTful ]--------------------------------------------
//Route::resource('departamento', 'DepartamentoController');
Route::post('api/departamento/withArea', 'DepartamentoController@withArea');
Route::get('api/departamento/dataTableList', 'DepartamentoController@dataTableList');

Route::resource('api/suscriptor', 'SuscriptorController');
Route::post('api/suscriptor/likeNombre', 'SuscriptorController@likeNombre');
Route::post('api/suscriptor/likeCargo', 'SuscriptorController@likeCargo');
Route::post('api/suscriptor/likeDependencia', 'SuscriptorController@likeDependencia');
Route::post('api/suscriptor/dependencia', 'SuscriptorController@dependencia');
Route::resource('api/tipodocumento', 'TipodocumentoController');
Route::get('api/usremp/datatable', 'UsrempController@dataTableList');	// Usuario empleado
Route::get('api/documento/dataTableListaIngreso', 'DocumentoController@dataTableListaIngreso');
Route::get('api/documento/dataTableListaEmision', 'DocumentoController@dataTableListaEmision');
Route::get('api/guiaturnado/dataTableListaTurnadoEntrada', 'GuiaturnadoController@dataTableListaTurnadoEntrada');
Route::get('api/guiaturnado/dataTableListaTurnadoSalida', 'GuiaturnadoController@dataTableListaTurnadoSalida');

Route::get('api/revision/dataTableListaRevisado', 'ReporterevisionController@dataTableListaRevisado');

Route::get('api/respuesta/dataTableListaRespuesta', 'RespuestaController@dataTableListaRespuesta');

Route::post('api/documento/rangoMapa', 'DocumentosdbviewController@rangoMapa');

Route::get('api/cita/dataTablesSolicitud', 'CitaController@dataTableSolicitud');

Route::get('api/maps/googleChartsByRegion', 'DocumentoController@googleChartsByRegion');
/*37
php artisan route:list
*/
Auth::routes();