<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Solicitudrespuesta extends Model
{
    protected $table = 'solicitudesrespuestas';
    public $timestamps = false;
}
