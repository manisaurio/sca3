<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Detalleguiaturnado extends Model
{
    protected $table = 'detallesguiasturnados';
    public $timestamps = false;
}
