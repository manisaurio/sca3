<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empleadodocumento extends Model
{
    protected $table = 'empleadosdocumentos';
    public $timestamps = false;
}
