<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reporterevision extends Model
{
    protected $table = 'reportesrevisiones';
    public $timestamps = false;
}
