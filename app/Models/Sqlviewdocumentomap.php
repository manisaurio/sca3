<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sqlviewdocumentomap extends Model
{
    protected $table = 'sqlviewdocumentosmaps';
    public $timestamps = false;
}
