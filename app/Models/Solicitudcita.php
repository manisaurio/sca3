<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Solicitudcita extends Model
{
    protected $table = 'solicitudescitas';
    public $timestamps = false;
}
