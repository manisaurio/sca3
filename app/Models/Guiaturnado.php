<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guiaturnado extends Model
{
    protected $table = 'guiasturnados';
    public $timestamps = false;
}
