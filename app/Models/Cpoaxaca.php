<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cpoaxaca extends Model
{
    protected $table = 'cpsoaxaca';
    public $timestamps = false;
}
