<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class DocumentosdbviewController extends Controller
{
    public function rangoMapa(Request $request)
    {
        $from = $request->input('fechainicio') . ' 00:00:00';
        $to = $request->input('fechafin') . ' 23:57:00';
        
        $mapa = DB::select('
            select  
                dm.dependencia, 
                dm.geometry_dependencia,
                dm.address_dependencia,
                count(dm.dependencia) AS incidencias 
            from 
            (
                select * 
                from documentosdbview
                where registrado between :from and :to
            ) as dm
            where dm.geometry_dependencia is not null
            group by dm.dependencia, dm.geometry_dependencia, dm.address_dependencia', ['from' => $from, 'to' => $to]);

        return response()->json( $mapa );
    }
}