<?php
 
namespace App\Http\Controllers;

use App\Http\Requests\DepartamentoStoreRequest;
use App\Http\Requests\DepartamentoUpdateRequest;

use Illuminate\Http\Request;
use App\Models\Departamento;
use App\Models\Sqlviewdocumentomap;
 
use DB;
use Auth;
 
class DepartamentosController extends Controller
{
    private function getPuestoUser(){
        $usremp = DB::table('users')
                    ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
                    ->select('empleados.puesto')
                    ->where('users.id_empleado', Auth::user()->id_empleado)
                    ->first();

        return $usremp->puesto;
    }

    private function existeDepartamento($id){
        $departamento = DB::table('departamentos')
                        ->where('id', $id)
                        ->first();

        if( $departamento ){   //---[ SI EXISTE ]---
            return true;
        }
        return false;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                return view('departamento.admin.list');
                break;
            case "DIRECTOR":
                return view('departamento.director.list');
                break;
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $puestoUsr = $this->getPuestoUser();

        if( strcmp( $puestoUsr, 'ADMIN' ) == 0 ){
            $departamentos = DB::table('departamentos')
                    ->select(DB::raw('count(*) as area_count, area'))
                    ->where('area', '<>', '')
                    ->groupBy('area')
                    ->get();
 
            return view('departamento.admin.create')->with('departamentos', $departamentos);    
        }

        if( strcmp( $puestoUsr, 'DIRECTOR' ) == 0 ){
            $departamentos = DB::table('departamentos')
                    ->select(DB::raw('count(*) as area_count, area'))
                    ->where('area', '<>', '')
                    ->groupBy('area')
                    ->get();
 
            return view('departamento.director.create')->with('departamentos', $departamentos);    
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartamentoStoreRequest $request)
    {
        $id_departamento = '';

        DB::beginTransaction();
            if( $request->otraAreaCheckbox ){
                $id_departamento = DB::table('departamentos')->insertGetId(
                    [
                        'nombre' => 'JEFATURA',
                        'area' => strtoupper( $request->otra_area ),
                        'puestos' => '["JEFE"]'
                    ]
                );
            }else{
                $id_departamento = DB::table('departamentos')->insertGetId(
                    [
                        'nombre' => strtoupper( $request->nombre ),
                        'area' => $request->areaSelect,
                        'puestos' => '["GENERAL"]'
                    ]
                );
            }
        DB::commit();
        
        return redirect()->action('DepartamentosController@show', [$id_departamento])->with('message', 'Departamento ingresado satisfactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if( ! $this->existeDepartamento($id) ){
            return redirect()->action('HomeController@index')->with('message', 'ERROR: El recurso al que intenta acceder no esta disponible');
        }

        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                $departamento = departamento::findOrFail($id);
                return view('departamento.admin.show')->with('departamento', $departamento);
                break;
            case "DIRECTOR":
                $departamento = departamento::findOrFail($id);
                return view('departamento.director.show')->with('departamento', $departamento);
                break;
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if( ! $this->existeDepartamento($id) ){
            return redirect()->action('HomeController@index')->with('message', 'ERROR: El recurso al que intenta acceder no esta disponible');
        }

        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                $departamento = departamento::findOrFail($id);
                return view('departamento.admin.edit')->with('departamento', $departamento);
                break;
            case "DIRECTOR":
                $departamento = departamento::findOrFail($id);
                return view('departamento.director.edit')->with('departamento', $departamento);
                break;
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\DepartamentoUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DepartamentoUpdateRequest $request, $id)
    {
        $puestoUsr = $this->getPuestoUser();
        if( strcmp( $puestoUsr, 'ADMIN' ) == 0 || strcmp( $puestoUsr, 'DIRECTOR' ) == 0 ){
            DB::beginTransaction();
                $departamento = Departamento::findOrFail($id);
                $departamento->nombre = strtoupper( $request->nombre );
                $departamento->save();
            DB::commit();
            
            return redirect()->action('DepartamentosController@show', [$id])->with('message', 'Departamento actualizado satisfactoriamente');
        }
        echo "<script>console.warn('Usuario no autorizado')</script>";
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $puestoUsr = $this->getPuestoUser();
        if( strcmp( $puestoUsr, 'ADMIN' ) == 0 || strcmp( $puestoUsr, 'DIRECTOR' ) == 0 ){
            DB::beginTransaction();
                $departamento =  Departamento::where('id', $id)->firstOrFail();
                $departamento->delete();
            DB::commit();
        }
        echo "<script>console.warn('Usuario no autorizado')</script>";
    }
}