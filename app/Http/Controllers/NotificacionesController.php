<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;

class NotificacionesController extends Controller
{
	private function getPuestoUser()
    {
        $usremp = DB::table('users')
                    ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
                    ->select('empleados.puesto')
                    ->where('users.id_empleado', Auth::user()->id_empleado)
                    ->first();

        return $usremp->puesto;
    }

    public function marcarComoLeidas()
    {
    	auth()->user()->unreadNotifications->markAsRead();
    }

    public function lista()
    {
    	switch ( $this->getPuestoUser() ) {
            case "MENSAJERO":
                return view('notificaciones.mensajero.lista');
                break;
            case "DIRECTOR":
                return view('notificaciones.director.lista');
                break;
            default:
                return redirect()->action('HomeController@index')->with('message', 'ERROR: Vista no disponible para puesto desconocido.');
        }
    }

    public function dataTableList(){
    	$notificaciones = auth()->user()->notifications;

    	switch ( $this->getPuestoUser() ) {
            case "DIRECTOR":
                return datatables()->of( $notificaciones )
		        	->addColumn('action', function ( $notificacion ) {
		                $urlshow = url("/documentos/show") . "/" . $notificacion->data['id_documento'];
		                return 'Respuesta de documento con folio: <a href="'.$urlshow.'" class="btn btn-xs btn-info">' . $notificacion->data['id_documento'] . '</a> agregada.';
		            })
		            ->addColumn('fecha', function ( $notificacion ) {
		            	return ''.$notificacion->created_at->format('d/m/Y h:i A');
		            })
		            ->toJson();
                break;

            case "MENSAJERO":
                return datatables()->of( $notificaciones )
		        	->addColumn('action', function ( $notificacion ) {
		                $urlshow = url("/documentos/show") . "/" . $notificacion->data['id_documento'];
		                return 'Respuesta de documento con folio: <a href="'.$urlshow.'" class="btn btn-xs btn-info">' . $notificacion->data['id_documento'] . '</a> agregada. Acuda a la dirección.';
		            })
		            ->addColumn('fecha', function ( $notificacion ) {
		            	return ''.$notificacion->created_at->format('d/m/Y h:i A');
		            })
		            ->toJson();
                break;
        }
        
    }
}
