<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;

class DocumentoController extends Controller
{
    private function getPuestoUser(){
        $usremp = DB::table('users')
                    ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
                    ->select('empleados.puesto')
                    ->where('users.id_empleado', Auth::user()->id_empleado)
                    ->first();

        return $usremp->puesto;
    }

    private function queryAdminDirIngresoDataTable(){
        $documentos = DB::table('documentos')
            ->leftJoin('solicitudesrespuestas', 'documentos.id', '=', 'solicitudesrespuestas.id_documento')
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                     ->where('empleadosdocumentos.actividad', 'INGRESO');
            })
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->select(
                'documentos.id', 
                'documentos.asunto', 
                'documentos.num_hojas', 
                'documentos.num_anexos', 
                'documentos.num_oficio', 
                DB::raw('DATE_FORMAT(documentos.fecha_oficio, "%d-%m-%Y") as fecha_oficio'),
                'suscriptores.nombre as suscriptor', 
                DB::raw("CONCAT(suscriptores.cargo, ' - ',suscriptores.dependencia) AS cargo_dependencia"), 
                DB::raw('DATE_FORMAT(empleadosdocumentos.registrado, "%d-%m-%Y %r") as registrado'),
                'tiposdocumentos.tipo',
                'solicitudesrespuestas.estado'
            )
            ->whereNull('solicitudesrespuestas.estado')
            ->orWhere('solicitudesrespuestas.estado', '<>', 'CON_RESPUESTA')
            ->get();

        return datatables()->of( $documentos )
            ->addColumn('action', function ( $documento ) {
                $urlshow = url("/documentos/show") . "/" . $documento->id;
                $urledit = url("/documentos/edit") . "/" . $documento->id;
                $urldestroy = url("/documentos/destroy") . "/" . $documento->id;
                
                return '<a href="'.$urlshow.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Detalle</a>
                        <a href="'.$urledit.'" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-edit"></i> Editar</a>
                        <a onclick="_modalConfirmFunction(\''.$urldestroy.'\')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Eliminar</a>';                
            })
            ->toJson();
    }

    private function queryMensajeroIngresoDataTable(){
        $documentos = DB::table('documentos')
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                     ->where('empleadosdocumentos.actividad', 'INGRESO')
                     ->where('empleadosdocumentos.estado', 'INGRESADO')
                     ->where('empleadosdocumentos.id_empleado', Auth::user()->id_empleado);
            })
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
                ->select(
                    'documentos.id', 
                    'documentos.asunto', 
                    'documentos.num_hojas', 
                    'documentos.num_anexos', 
                    'documentos.num_oficio', 
                    DB::raw('DATE_FORMAT(documentos.fecha_oficio, "%d-%m-%Y") as fecha_oficio'),
                    'suscriptores.nombre as suscriptor', 
                    DB::raw("CONCAT(suscriptores.cargo, ' - ',suscriptores.dependencia) AS cargo_dependencia"), 
                    DB::raw('DATE_FORMAT(empleadosdocumentos.registrado, "%d-%m-%Y %r") as registrado'),
                    'tiposdocumentos.tipo')
            ->get();

        return datatables()->of( $documentos )
            ->addColumn('action', function ( $documento ) {
                $urlturnar = url("/guiasturnados/create") . "/" . $documento->id;
                $urlshow = url("/documentos/show") . "/" . $documento->id;
                $urledit = url("/documentos/edit") . "/" . $documento->id;
                $urldestroy = url("/documentos/destroy") . "/" . $documento->id;
                
                return '<a href="'.$urlturnar.'" class="btn btn-xs btn-secondary active"><i class="glyphicon glyphicon-share"></i> Turnar</a>
                        <a href="'.$urlshow.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Detalle</a>
                        <a href="'.$urledit.'" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-edit"></i> Editar</a>
                        <a onclick="_modalConfirmFunction(\''.$urldestroy.'\')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Eliminar</a>';                
            })
            ->toJson();
    }

    public function dataTableListaIngreso()
    {
        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                return $this->queryAdminDirIngresoDataTable();
                break;
            case "MENSAJERO":
                return $this->queryMensajeroIngresoDataTable();
                break;
            case "DIRECTOR":
                return $this->queryAdminDirIngresoDataTable();
                break;
        }
    }

    public function dataTableListaEmision()
    {
        $documentos = DB::table('documentos')
            ->join('solicitudesrespuestas', function ($join) {
                $join->on('documentos.id', '=', 'solicitudesrespuestas.id_documento')
                     ->where('solicitudesrespuestas.estado', '=', 'CON_RESPUESTA');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                     ->where('empleadosdocumentos.actividad', 'INGRESO');
            })
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
                ->select(
                    'documentos.id', 
                    'documentos.asunto', 
                    'documentos.num_hojas', 
                    'documentos.num_anexos', 
                    'documentos.num_oficio', 
                    'documentos.fecha_oficio', 
                    'suscriptores.nombre as suscriptor', 
                    DB::raw("CONCAT(suscriptores.cargo, ' - ',suscriptores.dependencia) AS cargo_dependencia"), 
                    'empleadosdocumentos.registrado', 
                    'tiposdocumentos.tipo')
            ->get();

        return datatables()->of( $documentos )
            ->addColumn('action', function ( $documento ) {
                $urlshow = url("/documentos/show") . "/" . $documento->id;
                $urledit = url("/documentos/edit") . "/" . $documento->id;
                $urldestroy = url("/documentos/destroy") . "/" . $documento->id;
                
                return '<a href="'.$urlshow.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Detalle</a>
                        <a href="'.$urledit.'" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-edit"></i> Editar</a>
                        <a onclick="_modalConfirmFunction(\''.$urldestroy.'\')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Eliminar</a>';
                

            })
            ->toJson();
    }

    public function googleChartsByRegion(Request $request)
    {
        $from = $request->input('fechainicio') . ' 00:00:00';
        $to = $request->input('fechafin') . ' 23:57:00';

        $ingresos = DB::table('documentos')
            ->select(
                DB::raw(
                    'CONCAT(
                        \'{"c":[{"v":"\', cpso.region,\'"},{"v":\', count(documentos.id_suscriptor),\'}]}\'
                    ) AS row'
                )
            )
            ->join('empleadosdocumentos', function ($join) use ($from, $to) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                    ->where('actividad', '=', 'INGRESO')
                    ->whereBetween('empleadosdocumentos.registrado', [$from, $to]);
            })
            ->join(
                DB::raw(
                    '(
                        SELECT suscriptores.id, suscriptores.postal_code, suscriptores.dependencia, cpsoaxaca.region
                        FROM
                            cpsoaxaca
                        INNER JOIN suscriptores ON cpsoaxaca.cp = suscriptores.postal_code
                        GROUP BY suscriptores.id
                    ) as cpso'
                ),
                'documentos.id_suscriptor',
                '=',
                'cpso.id'
            )
            ->groupBy('cpso.region')
            ->get();

        $ingresosjson = $ingresos->map(function ($item, $key) {
            return json_decode($item->row);
        });

        return response()->json( ['cols' => [
            json_decode('{"id": "", "label": "Topping", "pattern": "", "type": "string"}'),
            json_decode('{"id": "", "label": "Slices", "pattern": "", "type": "number"}')
        ], 'rows' => $ingresosjson] );
    }

    /*
    public function googleChartsByRegion(Request $request)
    {
        $from = $request->input('fechainicio') . ' 00:00:00';
        $to = $request->input('fechafin') . ' 23:57:00';

        $ingresos = DB::table('documentos')
            ->select(
                DB::raw(
                    'JSON_OBJECT(
                        \'c\',
                        JSON_ARRAY(
                            JSON_OBJECT(\'v\', cpso.region), 
                            JSON_OBJECT(\'v\', count(documentos.id_suscriptor))
                        )
                    ) AS row'
                )
            )
            ->join('empleadosdocumentos', function ($join) use ($from, $to) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                    ->where('actividad', '=', 'INGRESO')
                    ->whereBetween('empleadosdocumentos.registrado', [$from, $to]);
            })
            ->join(
                DB::raw(
                    '(
                        SELECT suscriptores.id, suscriptores.postal_code, suscriptores.dependencia, cpsoaxaca.region
                        FROM
                            cpsoaxaca
                        INNER JOIN suscriptores ON cpsoaxaca.cp = suscriptores.postal_code
                        GROUP BY suscriptores.id
                    ) as cpso'
                ),
                'documentos.id_suscriptor',
                '=',
                'cpso.id'
            )
            ->groupBy('cpso.region')
            ->get();

        $ingresosjson = $ingresos->map(function ($item, $key) {
            return json_decode($item->row);
        });

        return response()->json( ['cols' => [
            json_decode('{"id": "", "label": "Topping", "pattern": "", "type": "string"}'),
            json_decode('{"id": "", "label": "Slices", "pattern": "", "type": "number"}')
        ], 'rows' => $ingresosjson] );
    }
    */
}
