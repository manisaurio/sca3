<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;

class GuiaturnadoController extends Controller
{
    private function getPuestoUser(){
        $usremp = DB::table('users')
                    ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
                    ->select('empleados.puesto')
                    ->where('users.id_empleado', Auth::user()->id_empleado)
                    ->first();

        return $usremp->puesto;
    }

    private function queryAdminTurnadoDataTableSalida(){

    }

    private function queryGeneralTurnadoDataTableEntrada(){
        $documentos_turnados = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                     ->whereIn('empleadosdocumentos.actividad', ['INGRESO', 'EMISION']);
            })
            ->join('guiasturnados', function ($join) {
                $join->on('documentos.id', '=', 'guiasturnados.id_documento');
            })
            ->join('detallesguiasturnados', function ($join) {
                $join->on('guiasturnados.num_orden', '=', 'detallesguiasturnados.numorden_guiaturnado')
                     ->where('detallesguiasturnados.idemp_turnado', Auth::user()->id_empleado)
                     ->whereIn('detallesguiasturnados.estado', ['NO_VISTO', 'VISTO']);
            })
            ->select(
                'guiasturnados.num_orden', 
                'documentos.id as id_documento',
                'documentos.asunto', 
                'documentos.num_oficio', 
                DB::raw('DATE_FORMAT(documentos.fecha_oficio, "%d-%m-%Y") as fecha_oficio'),
                'suscriptores.nombre as suscriptor', 
                DB::raw("CONCAT(suscriptores.cargo, ' - ',suscriptores.dependencia) AS cargo_dependencia"),
                DB::raw('DATE_FORMAT(empleadosdocumentos.registrado, "%d-%m-%Y %r") as ingresado'),
                DB::raw('DATE_FORMAT(guiasturnados.registrado, "%d-%m-%Y %r") as registrado'),
                'tiposdocumentos.tipo')
            ->groupBy('guiasturnados.num_orden', 'id_documento', 'tiposdocumentos.tipo', 'documentos.num_oficio', 'documentos.fecha_oficio', 'ingresado', 'guiasturnados.registrado', 'suscriptor', 'cargo_dependencia', 'documentos.asunto')
            ->get();

        return datatables()->of( $documentos_turnados )
            ->addColumn('action', function ( $documento ) {
                $urlrevisar = url("/reportesrevisiones/create") . "/" . $documento->num_orden . "/" . $documento->id_documento;
                $urlturnar = url("/guiasturnados/create") . "/" . $documento->id_documento;
                $urlshow = url("/guiasturnados/show") . "/" . $documento->num_orden . "/" . $documento->id_documento;
                
                switch ( $this->getPuestoUser() ) {
                    case "MENSAJERO":
                        return '<a href="'.$urlshow.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Detalle</a>';
                        break;
                    case "DIRECTOR":
                    case "ASISTENTE":
                    case "JEFE":
                    case "GENERAL":
                        return '<a href="'.$urlrevisar.'" class="btn btn-xs btn-secondary active"><i class="glyphicon glyphicon-check"></i> Revisar</a>
                                <a href="'.$urlshow.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Detalle</a>';
                        break;
                }
                
            })
            ->toJson();
    }

    private function queryGeneralTurnadoDataTableSalida(){
        $documentos_turnados = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                     ->whereIn('empleadosdocumentos.actividad', ['INGRESO', 'EMISION']);
            })
            ->join('guiasturnados', function ($join) {
                $join->on('documentos.id', '=', 'guiasturnados.id_documento')
                     ->where('guiasturnados.idemp_turnador', Auth::user()->id_empleado);
            })
            ->select(
                'guiasturnados.num_orden', 
                'documentos.id as id_documento',
                'documentos.asunto', 
                'documentos.num_oficio', 
                DB::raw('DATE_FORMAT(documentos.fecha_oficio, "%d-%m-%Y") as fecha_oficio'),
                'suscriptores.nombre as suscriptor', 
                DB::raw("CONCAT(suscriptores.cargo, ' - ',suscriptores.dependencia) AS cargo_dependencia"), 
                DB::raw('DATE_FORMAT(empleadosdocumentos.registrado, "%d-%m-%Y %r") as ingresado'),
                DB::raw('DATE_FORMAT(guiasturnados.registrado, "%d-%m-%Y %r") as registrado'),
                'tiposdocumentos.tipo')
            ->groupBy('guiasturnados.num_orden', 'id_documento', 'tiposdocumentos.tipo', 'documentos.num_oficio', 'documentos.fecha_oficio', 'ingresado', 'guiasturnados.registrado', 'suscriptor', 'cargo_dependencia', 'documentos.asunto')
            ->get();

        return datatables()->of( $documentos_turnados )
            ->addColumn('action', function ( $documento ) {
                $urlturnar = url("/guiasturnados/create") . "/" . $documento->id_documento;
                $urlshow = url("/guiasturnados/show") . "/" . $documento->num_orden . "/" . $documento->id_documento;
                
                if( strcmp( $this->getPuestoUser(), 'MENSAJERO') == 0 ){
                    return '<a href="'.$urlshow.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Detalle</a>';    
                }
                /*
                return '<a href="'.$urlturnar.'" class="btn btn-xs btn-secondary active"><i class="glyphicon glyphicon-share"></i> Turnar</a>
                        <a href="'.$urlshow.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Detalle</a>';
                        */
                return '<a href="'.$urlshow.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Detalle</a>';
            })
            ->toJson();
    }

    public function dataTableListaTurnadoEntrada()
    {
        if( strcmp($this->getPuestoUser(), 'ADMIN') == 0 ) {
            //
        }
        return $this->queryGeneralTurnadoDataTableEntrada();
    }

    public function dataTableListaTurnadoSalida()
    {
        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                //return $this->queryAdminTurnadoDataTableSalida();
                break;
            case "MENSAJERO":
                return $this->queryGeneralTurnadoDataTableSalida();
                break;
            case "DIRECTOR":
                return $this->queryGeneralTurnadoDataTableSalida();
                break;
            case "JEFE":
                return $this->queryGeneralTurnadoDataTableSalida();
                break;
        }
    }
}
