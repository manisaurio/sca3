<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cornford\Backup\Facades\Backup;

use Illuminate\Support\Facades\File;

use Carbon\Carbon;

class RespaldosController extends Controller
{
    public function menu()
    {
    	//$this->create();
    	return view('respaldos.menu');
    }

    public function create()
    {
    	Backup::setCompress(false);
    	Backup::setPath('G:\\');
    	Backup::setFilename('backup-' . date('d-m-Y_H-i-s'));
    	Backup::export();
    	//dd(Backup::getProcessOutput());
    	//dd(Backup::getRestorationFiles());
    }

    public function restore(Request $request)
    {
    	//return $request->path;
    	Backup::restore($request->path);
    }

    public function dataTableList()
    {
    	if(File::exists('G:\\')) {
    		$restorationfiles = Backup::getRestorationFiles('G:\\');
	        return datatables()->of( $restorationfiles )
	        	->addColumn('fecha', function ( $restorationfile ) {
	            	return substr($restorationfile, 10, 10);
	            })
	        	->addColumn('action', function ( $restorationfile ) {
	        		$path = str_replace("\\","\\\\", $restorationfile);
	                return '<a onclick="_restoreBackup(\''.$path.'\')" class="btn btn-xs btn-info"><i class="fa fa-history fw"></i> Restablecer</a>';
	            })
	            ->toJson();
		}
		return ["draw"=>0,"recordsTotal"=>0,"recordsFiltered"=>0,"data"=>[],"input"=>[]];
    }
}
