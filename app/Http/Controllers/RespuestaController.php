<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;

class RespuestaController extends Controller
{
    private function getPuestoUser(){
        $usremp = DB::table('users')
                    ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
                    ->select('empleados.puesto')
                    ->where('users.id_empleado', Auth::user()->id_empleado)
                    ->first();

        return $usremp->puesto;
    }

    public function dataTableListaRespuesta()
    {
        $documentos_crespuesta = DB::table('respuestas')
            ->join('solicitudesrespuestas', function ($join) {
                $join->on('respuestas.id_solicitud', '=', 'solicitudesrespuestas.id')
                    ->where('respuestas.id_empleado', '=', Auth::user()->id_empleado);
            })
            ->join('guiasturnados', 'solicitudesrespuestas.id_documento', '=', 'guiasturnados.id_documento')
            ->join('detallesguiasturnados', function ($join) {
                $join->on('guiasturnados.num_orden', '=', 'detallesguiasturnados.numorden_guiaturnado')
                    ->where('detallesguiasturnados.idemp_turnado', '=', Auth::user()->id_empleado);
            })
            ->join('empleadosdocumentos', function ($join) {
                $join->on('solicitudesrespuestas.id_documento', '=', 'empleadosdocumentos.id_documento')
                     ->whereIn('empleadosdocumentos.actividad', ['INGRESO', 'EMISION']);    //*SOLO INGRESO
            })
            ->join('documentos', 'solicitudesrespuestas.id_documento', '=', 'documentos.id')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select(
                'solicitudesrespuestas.id as id_solicitudrespuesta',
                'guiasturnados.num_orden', 
                'documentos.id as id_documento',
                'documentos.asunto', 
                'documentos.num_oficio', 
                'documentos.fecha_oficio', 
                'suscriptores.nombre as suscriptor', 
                DB::raw("CONCAT(suscriptores.cargo, ' - ',suscriptores.dependencia) AS cargo_dependencia"), 
                'empleadosdocumentos.registrado AS ingresado', 
                'guiasturnados.registrado', 
                'tiposdocumentos.tipo')
            ->get();

        return datatables()->of( $documentos_crespuesta )
            ->addColumn('action', function ( $documento ) {
                $urlrevisar = url("/reportesrevisiones/create") . "/" . $documento->num_orden . "/" . $documento->id_documento;
                $urlturnarxturnado = url("/guiasturnados/create") . "/" . $documento->num_orden . "/" . $documento->id_documento;
                $urlshow = url("/respuestas/show") . "/" . $documento->num_orden . "/" . $documento->id_solicitudrespuesta;
                $urlshowrevision = url("/reportesrevisiones/show") . "/" . $documento->num_orden . "/" . $documento->id_documento;
                
                return '<a href="'.$urlshow.'" class="btn btn-xs btn-default active"><i class="fa fa-eye fw"></i> Detalle respuesta</a>';
                
            })
            ->toJson();
    }
}
