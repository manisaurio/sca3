<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Models\Cita;

use DB;
use Auth;
use DateTime;
 
class CitasController extends Controller
{
    private function getPuestoUser()
    {
        $usremp = DB::table('users')
                    ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
                    ->select('empleados.puesto')
                    ->where('users.id_empleado', Auth::user()->id_empleado)
                    ->first();

        return $usremp->puesto;
    }

    public function calendar()
    {
        switch ( $this->getPuestoUser() ) {
            case "DIRECTOR":
                return view('cita.director.calendar');
                break;
            case "ASISTENTE":
                return view('cita.asistente.calendar');
                break;
        }
    }

    /*
    public function create()
    {
        return view('cita.create');
    }
    */

    public function storeModal(Request $request)
    {
        DB::beginTransaction();
            if ($request->filled('id_solicitud')) {
                DB::table('eventos')->insert(
                    [
                        'id_solicitud' => $request->id_solicitud,
                        'id_eventgoogle' => $request->eventIdGoogle,
                        'id_empleado' => Auth::user()->id_empleado
                    ]
                );
            }else{
                DB::table('eventos')->insert(
                    [
                        'id_eventgoogle' => $request->eventIdGoogle,
                        'id_empleado' => Auth::user()->id_empleado
                    ]
                );
            }
            DB::table('solicitudescitas')
                ->where('id', $request->id_solicitud)
                ->update(['estado' => 'AGENDADO']);
        DB::commit();
        session(['message' => 'Cita agregada satisfactoriamente.']);
        //$request->session()->put('message', 'Cita agregada satisfactoriamente.');
    }

    public function showModal($id)
    {
        $solicitud = DB::table('solicitudescitas')
            ->join('documentos', function ($join) use($id) {
                $join->on('solicitudescitas.id_documento', '=', 'documentos.id')
                     ->where('solicitudescitas.id', '=', $id);
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select(
                'suscriptores.nombre', 
                'suscriptores.cargo',
                'suscriptores.dependencia',
                'documentos.num_oficio',
                'documentos.asunto',
                'documentos.ruta_pdf'
            )
            ->first();

        return response()->json( $solicitud );
    }

    public function updateAjax(Request $request)
    {
        DB::beginTransaction();
            
        DB::commit();
        session(['message' => 'Cita actualizada satisfactoriamente.']);
    }

    public function destroyAjax(Request $request)
    {
        DB::beginTransaction();
            $solicitud = DB::table('eventos')
                ->select('id_solicitud')
                ->where('id_eventgoogle', $request->eventIdGoogle)
                ->whereNotNull('id_solicitud')
                ->first();

            //---[ Eliminar evento ]
            DB::table('eventos')->where('id_eventgoogle', '=', $request->eventIdGoogle)->delete();

            if( $solicitud ){   //---[ SI EXISTE ]---
                //---[ Actualizar estado de solicitud ]---
                DB::table('solicitudescitas')
                    ->where('id', $solicitud->id_solicitud)
                    ->update(['estado' => 'NO_AGENDADO']);
            }
        DB::commit();
        session(['message' => 'Cita eliminada satisfactoriamente.']);
    }
}