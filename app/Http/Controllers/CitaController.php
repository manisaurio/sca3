<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;

class CitaController extends Controller
{
    public function dataTableSolicitud(){
        $documentos = DB::table('documentos')
            ->join('solicitudescitas', function($join) {
                $join->on('documentos.id', '=', 'solicitudescitas.id_documento')
                    ->where('solicitudescitas.estado', 'NO_AGENDADO');
            })
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                    ->where('empleadosdocumentos.actividad', '=', 'INGRESO');
            })
            ->join('guiasturnados', function ($join) {
                $join->on('documentos.id', '=', 'guiasturnados.id_documento');
            })
            ->join('detallesguiasturnados', function ($join) {
                $join->on('guiasturnados.num_orden', '=', 'detallesguiasturnados.numorden_guiaturnado')
                    ->where('detallesguiasturnados.idemp_turnado', '=', Auth::user()->id_empleado)
                    ->whereIn('detallesguiasturnados.estado', ['REVISADO', 'CON_RESPUESTA']);
            })
            ->select(
                'solicitudescitas.id as id_solicitud',
                'documentos.id',
                'documentos.num_oficio',
                DB::raw('DATE_FORMAT(empleadosdocumentos.registrado, "%d-%m-%Y %H:%i") as ingreso')
            )
            ->get();

        return datatables()->of( $documentos )
            ->addColumn('link', function ( $documento ) {
                $urlshow = url("/citas/showmodal") . "/" . $documento->id_solicitud;
                
                return '<a onclick="showSolicitud(\''.$urlshow.'\');" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-eye-open"></i></a>';
            })
            ->addColumn('action', function ( $documento ) {
                return '<input type="radio" name="radioIdDocumento" value=\'{"id_solicitud":"'.$documento->id_solicitud.'","id_documento":"'. $documento->id .'"}\' onclick="(function(_element){ if(event.ctrlKey){_element.checked = false;} })(this);"> ' . $documento->num_oficio;
            })
            ->rawColumns(['link', 'action'])
            ->toJson();
    }
}
