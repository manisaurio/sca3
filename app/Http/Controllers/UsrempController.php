<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class UsrempController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataTableList()
    {
        $usrempAll = DB::table('empleados')
            ->join('users', 'empleados.id', '=', 'users.id_empleado')
            ->join('departamentos', 'empleados.id_departamento', '=', 'departamentos.id')
                ->select('empleados.curp', DB::raw("CONCAT(empleados.nombre, ' ',empleados.ap_paterno) AS nombre"), 'empleados.puesto', 'departamentos.nombre as nombre_departamento', 'departamentos.area', 'users.id', 'users.activo')
            ->get();

        return datatables()->of( $usrempAll )
            ->addColumn('action', function ( $usremp ) {
                $urlshow = url("/empleados/show") . "/" . $usremp->id;
                $urledit = url("/empleados/edit") . "/" . $usremp->id;
                $urlswitch = url("/empleados/switch") . "/" . $usremp->id;
                $urldestroy = url("/empleados/destroy") . "/" . $usremp->id;
                
                if( $usremp->activo ){
                    return '<a href="'.$urlshow.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Detalle</a>
                            <a href="'.$urledit.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Editar</a>
                            <a onclick="_modalConfirmFunction(\''.$urldestroy.'\')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Eliminar</a>
                            <a onclick="_switchUserFunction(\''.$urlswitch.'\')" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-off"></i> Desactivar</a>';
                }else{
                    return '<a href="'.$urlshow.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Detalle</a>
                            <a href="'.$urledit.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Editar</a>
                            <a onclick="_modalConfirmFunction(\''.$urldestroy.'\')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Eliminar</a>
                            <a onclick="_switchUserFunction(\''.$urlswitch.'\')" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-off"></i> Activar</a>';
                }

            })
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
