<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Models\Departamento;
 
use DB;
 
class DepartamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departamentos = departamento::all();
        return response()->json( $departamentos );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataTableList()
    {
        $departamentos = DB::table('departamentos')->get();

        return datatables()->of( $departamentos )
            ->addColumn('action', function ( $departamento ) {
                $urlshow = url("/departamentos/show") . "/" . $departamento->id;
                $urledit = url("/departamentos/edit") . "/" . $departamento->id;
                $urldestroy = url("/departamentos/destroy") . "/" . $departamento->id;
                
                return '<a href="'.$urlshow.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Detalle</a>
                        <a href="'.$urledit.'" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-edit"></i> Editar</a>
                        <a onclick="_modalConfirmFunction(\''.$urldestroy.'\')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Eliminar</a>';
            })
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function withArea(Request $request)
    {
        //$palabra = Str::lower(Input::get('palabra'));
 
        $palabra = strtolower( $request->input('palabra') );
 
        $departamentos = DB::table('departamentos')
                    ->select('id', 'nombre', 'puestos')
                    ->where('area', $palabra)
                    ->get();
        
        //echo "<script>console.log('$nombre')</script>";
        
        //return Response::json( $returnArray );
        return response()->json( $departamentos );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}