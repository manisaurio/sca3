<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reporterevision;
use Illuminate\Support\Facades\Storage;
 
use App; 
use DB;
use Auth;
use DateTime;

class ReportesrevisionesController extends Controller
{
    private function getPuestoUser()
    {
        $usremp = DB::table('users')
                    ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
                    ->select('empleados.puesto')
                    ->where('users.id_empleado', Auth::user()->id_empleado)
                    ->first();

        return $usremp->puesto;
    }

    private function existeDocumento($id)
    {
        $documento = DB::table('documentos')
                        ->where('id', $id)
                        ->first();

        if( $documento ){   //---[ SI EXISTE ]---
            return true;
        }
        return false;
    }

    private function existeTurnado($numorden, $id)             //---[num_orden, id_documento]-----
    {
        $turnado = DB::table('guiasturnados')
            ->where('num_orden', $numorden)
            ->where('id_documento', $id)
            ->first();

        if( $turnado ){   //---[ SI EXISTE ]---
            return true;
        }
        return false;
    }

    //---[ EXISTENCIA Y PROPIEDAD POR TURNADO ]---------------------------------------------------
    private function existeRevisionXTurnado($numorden, $id)     //---[num_orden, id_documento]----
    {
        $revision = DB::table('reportesrevisiones') 
            ->where('numorden_guiaturnado', $numorden)
            ->where('id_empleado', Auth::user()->id_empleado)
            ->where('id_documento', $id)
            ->first();

        if( $revision ){   //---[ SI EXISTE ]---
            return true;
        }
        return false;
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    private function privilegioTurnado($numorden)               //---[ Turnado o turnador ]-------
    {
        $privilegio = DB::table('guiasturnados')
            ->join('detallesguiasturnados', function ($join) use ($numorden) {
                $join->on('guiasturnados.num_orden', '=', 'detallesguiasturnados.numorden_guiaturnado')
                     ->where('guiasturnados.num_orden', '=', $numorden);
            })
            ->select('guiasturnados.registrado')
            ->where('guiasturnados.idemp_turnador', Auth::user()->id_empleado)
            ->orWhere('detallesguiasturnados.idemp_turnado', Auth::user()->id_empleado)
            ->first();

        if( $privilegio ){   //---[ SI EXISTE ]---
            return true;
        }
        return false;
    }

    public function listaxturnado()
    {
        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                //return view('turnado.admin.listaturnadoentrada');
                break;
            case "DIRECTOR":
                return view('revision.xturnado.director.lista');
                break;
            case "JEFE":
                return view('revision.xturnado.jefe.lista');
                break;
            case "GENERAL":
                return view('revision.xturnado.general.lista');
                break;
            case "ASISTENTE":
                return view('revision.xturnado.asistente.lista');
                break;
            default:
                return redirect()->action('HomeController@index')->with('message', 'ERROR: Vista no disponible para puesto desconocido.');
        }
    }

    private function getDateRango()
    {
        $date_start = new DateTime();
        $date_end = new DateTime();

        for( $i = 0; $i < 15; $i++ ){
            $date_end->modify('+1 day');
            $newDay = date('w', $date_end->getTimestamp());
            if($newDay == 0 || $newDay == 6) {
                $i--;
            }
        }

        $array = array(
            'fecha_inicio' => $date_start->format('d-m-Y'),
            'fecha_fin' => $date_end->format('d-m-Y')
        );
        
        return $array;
    }

    private function existeSolicitudCita($id)
    {
        $solicitud = DB::table('solicitudescitas')
            ->where('id_documento', $id)
            ->first();

        if( $solicitud ){   //---[ SI EXISTE ]---
            return $solicitud;
        }
        return false;
    }

    private function getInstruccionTurnado($numorden)          //---[ Turnado(Destinatario) ]----
    {
        $instruccion = DB::table('guiasturnados')
            ->join('detallesguiasturnados', function ($join) use ($numorden) {
                $join->on('guiasturnados.num_orden', '=', 'detallesguiasturnados.numorden_guiaturnado')
                     ->where('guiasturnados.num_orden', '=', $numorden);
            })
            ->select('detallesguiasturnados.instruccion')
            ->Where('detallesguiasturnados.idemp_turnado', Auth::user()->id_empleado)
            ->first();

        return $instruccion;
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    private function queryCreateXTurnadoDir( $numorden, $id, $carpeta_auth )   //---[id: id_documento]---
    {
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $solicitudrespuesta = DB::table('solicitudesrespuestas')
            ->select(DB::raw('DATE_FORMAT(fecha_limite, "%d-%m-%Y") as fecha_limite'))
            ->where('id_documento', $id)
            ->first();

        if( $solicitudrespuesta ){      //---[ SI EXISTE ]---------------
            return view('revision.xturnado.' . $carpeta_auth . '.create')
            ->with('documento', $documento)
            ->with('solicitudrespuesta', $solicitudrespuesta)
            ->with('solicitudcita', $this->existeSolicitudCita($id))
            ->with('rangofecha', $this->getDateRango());
        }

        return view('revision.xturnado.' . $carpeta_auth . '.create')
            ->with('documento', $documento)
            ->with('solicitudcita', $this->existeSolicitudCita($id))
            ->with('rangofecha', $this->getDateRango());
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    private function queryCreateXTurnadoAsist( $numorden, $id){   //---[id: id_documento]---
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $consultainstruccion = $this->getInstruccionTurnado($numorden);
        if( strcmp($consultainstruccion->instruccion, 'REVISION_ATENCION') == 0){   //---[ INSTRUCCION: RESPONDER ]
            //---[ Debe existir solicitud respuesta ]-------------------------------------
            $solicitudrespuesta = DB::table('solicitudesrespuestas')
                ->select(DB::raw('DATE_FORMAT(fecha_limite, "%d-%m-%Y") as fecha_limite'))
                ->where('id_documento', $id)
                ->first();

            return view('revision.xturnado.asistente.create')
                ->with('documento', $documento)
                ->with('solicitudrespuesta', $solicitudrespuesta)
                ->with('solicitudcita', $this->existeSolicitudCita($id))
                ->with('rangofecha', $this->getDateRango());
        }
        //---[ ELSE ]------------------------------------------------
        return view('revision.xturnado.asistente.create')
            ->with('documento', $documento)
            ->with('solicitudcita', $this->existeSolicitudCita($id))
            ->with('rangofecha', $this->getDateRango());
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    private function queryCreateXTurnado( $numorden, $id, $carpeta_auth){   //---[id: id_documento]---
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $consultainstruccion = $this->getInstruccionTurnado($numorden);
        if( strcmp($consultainstruccion->instruccion, 'REVISION_ATENCION') == 0){   //---[ INSTRUCCION: RESPONDER ]
            //---[ Debe existir solicitud respuesta ]-------------------------------------
            $solicitudrespuesta = DB::table('solicitudesrespuestas')
                ->select(DB::raw('DATE_FORMAT(fecha_limite, "%d-%m-%Y") as fecha_limite'))
                ->where('id_documento', $id)
                ->first();

            return view('revision.xturnado.'.$carpeta_auth.'.create')
                ->with('documento', $documento)
                ->with('solicitudrespuesta', $solicitudrespuesta)
                ->with('rangofecha', $this->getDateRango());
        }
        //---[ ELSE ]------------------------------------------------
        return view('revision.xturnado.'.$carpeta_auth.'.create')
            ->with('documento', $documento)
            ->with('rangofecha', $this->getDateRango());
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    public function createxturnado($numorden, $id)            //---[num_orden, id_documento]------
    {
        if( ! $this->existeTurnado($numorden, $id) ){
            return redirect()->action('HomeController@index')->with('message', 'ERROR: El recurso al que intenta acceder no esta disponible');
        }

        if( $this->privilegioTurnado($numorden) ){
            switch( $this->getPuestoUser() ){
                case "DIRECTOR":
                    return $this->queryCreateXTurnadoDir($numorden, $id, 'director');
                    break;
                case "JEFE":
                    return $this->queryCreateXTurnado($numorden, $id, 'jefe');
                    break;
                case "ASISTENTE":
                    return $this->queryCreateXTurnadoAsist($numorden, $id);
                    break;
                case "GENERAL":
                    return $this->queryCreateXTurnado($numorden, $id, 'general');
                    break;
            }
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');   
    }

    //----[ REVISION POR EMISION ]----------------------------------------------------------------
    public function create($id)     //id documento
    {
        if( ! $this->existeDocumento($id) ){
            return redirect()->action('HomeController@index')->with('message', 'ERROR: El recurso al que intenta acceder no esta disponible');
        }
    }
 
    //----[ POR TURNADO ]-------------------------------------------------------------------------
    public function storexturnadoDir($request, $numorden, $id) //---[ id_documento ]
    {
        DB::beginTransaction();
            //Recuperar SOLICITUDCITA, si existe en la BD
            $solicitudCita = DB::table('solicitudescitas')
                ->select('id')
                ->where('id_documento', $id)
                ->first();

            //---[ SOLICITUD CITA ]---------------------------
            if( $request->solicitudcitaCheckbox ){          //---[ USUARIO SOLICITA CITA ]----------------
                if( ! $solicitudCita ){  //--[ SI NO EXISTE ]-------------------------------------------------
                    DB::table('solicitudescitas')->insert(
                        ['id_documento' => $id]
                    );
                }
            }else{      //---------------------------------------[ USUARIO ELIMINA O NO SOLICITA CITA ]---
                if( $solicitudCita ){  //--[ SI EXISTE ]------------------------------------------------------
                    DB::table('solicitudescitas')->where('id', '=', $solicitudCita->id)->delete();
                }
            }
            //---[ FIN SOLICITUD CITA ]-----------------------

            //Recuperar SOLICITUDRESPUESTA, si existe en la BD
            $solicitud = DB::table('solicitudesrespuestas')
                ->select('id', 'fecha_limite')
                ->where('id_documento', $id)
                ->first();

            if ($request->filled('fechaRespuestaInput')) {  //---[ USUARIO SOLICITA RESPUESTA    ]---
                //Dar formato a fecha YYYY-MM-DD
                $nuevaFechaLimite = explode("-", $request->fechaRespuestaInput);
                $nuevaFechaLimite = $nuevaFechaLimite[2] . '-' . $nuevaFechaLimite[1] . '-' . $nuevaFechaLimite[0];

                if( $solicitud ){   //---[ SI EXISTE ]---
                    if (strcmp($solicitud->fecha_limite, $nuevaFechaLimite) !== 0) {    //---[ SI HUBO CAMBIO ]---
                        //---[ ACTUALIZACION ]---
                        DB::table('solicitudesrespuestas')
                            ->where('id', $solicitud->id)
                            ->update(['fecha_limite' => $nuevaFechaLimite]);
                        //---[ REGISTRO DE ACTIVIDAD ]-------------------
                        DB::table('empleadosdocumentos')->insert(
                            [
                                'actividad' => 'EDICION', 
                                'id_empleado' => Auth::user()->id_empleado,
                                'id_documento' => $id,
                                'descripcion' => $solicitud->id .':EDICION:SOLICITUD_RESPUESTA'
                            ]
                        );
                    }
                }else{
                    //---[ AGREGACION ]---
                    $idNuevaSolicitudRespuesta = DB::table('solicitudesrespuestas')->insertGetId(
                        ['id_documento' => $id, 'fecha_limite' => $nuevaFechaLimite]
                    );
                    //---[ REGISTRO DE ACTIVIDAD ]-------------------
                    DB::table('empleadosdocumentos')->insert(
                        [
                            'actividad' => 'EDICION', 
                            'id_empleado' => Auth::user()->id_empleado,
                            'id_documento' => $id,
                            'descripcion' => $idNuevaSolicitudRespuesta .':AGREGACION:SOLICITUD_RESPUESTA'
                        ]
                    );
                }   
            }else{                                          //---[ USUARIO NO SOLICITA RESPUESTA ]---
                if( $solicitud ){   //---[ SI EXISTE SOLICITUDRESPUESTA EN BD ]----------------------
                    //---[ ELIMINACION DE SOLICITUD EN BD ]------------------------------------------
                    DB::table('solicitudesrespuestas')->where('id', $solicitud->id)->delete();
                    //---[ REGISTRO DE ACTIVIDAD ]---------------------------------------------------
                    DB::table('empleadosdocumentos')->insert(
                        [
                            'actividad' => 'EDICION', 
                            'id_empleado' => Auth::user()->id_empleado,
                            'id_documento' => $id,
                            'descripcion' => $solicitud->id .':ELIMINACION:SOLICITUD_RESPUESTA'
                        ]
                    );
                }
            }
            
            $revision = new Reporterevision;
            $revision->numorden_guiaturnado = $numorden;
            $revision->id_empleado = Auth::user()->id_empleado;
            $revision->id_documento = $id;
            $revision->observacion = $request->observacion;
            $revision->recibi_papel = ($request->checkpapel ? true : false);
            $revision->save();

            //Localizar turnado y actualizar a REVISADO
            DB::table('detallesguiasturnados')
                ->where('numorden_guiaturnado', $numorden)
                ->where('idemp_turnado', Auth::user()->id_empleado)
                ->update(['estado' => 'REVISADO']);

        DB::commit();

        return redirect()->action('ReportesrevisionesController@showxturnado', [ $numorden, $id ])->with('message', 'Revision guardada satisfactoriamente');
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    public function storexturnadoAsist($request, $numorden, $id) //---[ id_documento ]
    {
        DB::beginTransaction();
            //Recuperar SOLICITUDCITA, si existe en la BD
            $solicitud = DB::table('solicitudescitas')
                ->select('id')
                ->where('id_documento', $id)
                ->first();

            if( $request->solicitudcitaCheckbox ){          //---[ USUARIO SOLICITA CITA ]----------------
                if( ! $solicitud ){  //--[ SI NO EXISTE ]-------------------------------------------------
                    DB::table('solicitudescitas')->insert(
                        ['id_documento' => $id]
                    );
                }
            }else{      //---------------------------------------[ USUARIO ELIMINA O NO SOLICITA CITA ]---
                if( $solicitud ){  //--[ SI EXISTE ]------------------------------------------------------
                    DB::table('solicitudescitas')->where('id', '=', $solicitud->id)->delete();
                }
            }
            
            $revision = new Reporterevision;
            $revision->numorden_guiaturnado = $numorden;
            $revision->id_empleado = Auth::user()->id_empleado;
            $revision->id_documento = $id;
            $revision->observacion = $request->observacion;
            $revision->recibi_papel = ($request->checkpapel ? true : false);
            $revision->save();

            //Localizar turnado y actualizar a REVISADO
            DB::table('detallesguiasturnados')
                ->where('numorden_guiaturnado', $numorden)
                ->where('idemp_turnado', Auth::user()->id_empleado)
                ->update(['estado' => 'REVISADO']);

        DB::commit();

        return redirect()->action('ReportesrevisionesController@showxturnado', [ $numorden, $id ])->with('message', 'Revision guardada satisfactoriamente');
    }

    public function storexturnadoJefe($request, $numorden, $id) //---[ id_documento ]
    {
        DB::beginTransaction();
            $revision = new Reporterevision;
            $revision->numorden_guiaturnado = $numorden;
            $revision->id_empleado = Auth::user()->id_empleado;
            $revision->id_documento = $id;
            $revision->observacion = $request->observacion;
            $revision->recibi_papel = ($request->checkpapel ? true : false);
            $revision->save();

            //Localizar turnado y actualizar a REVISADO
            DB::table('detallesguiasturnados')
                ->where('numorden_guiaturnado', $numorden)
                ->where('idemp_turnado', Auth::user()->id_empleado)
                ->update(['estado' => 'REVISADO']);

        DB::commit();

        return redirect()->action('ReportesrevisionesController@showxturnado', [ $numorden, $id ])->with('message', 'Revision guardada satisfactoriamente');
    }

    public function storexturnado(Request $request, $numorden, $id) //---[ id_documento ]
    {
        switch( $this->getPuestoUser() ){
            case "DIRECTOR":
                return $this->storexturnadoDir($request, $numorden, $id);
                break;
            case "ASISTENTE":
                return $this->storexturnadoAsist($request, $numorden, $id);
                break; 
            case "JEFE":
                return $this->storexturnadoJefe($request, $numorden, $id);
                break;
            case "GENERAL":
                return $this->storexturnadoJefe($request, $numorden, $id);
                break;
        }
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    private function queryShowPrivilegioTurnadoDir( $numorden, $id)
    {
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $revision = DB::table('reportesrevisiones')
            ->select('observacion', 'recibi_papel')
            ->where('numorden_guiaturnado', $numorden)
            ->where('id_documento', $id)
            ->where('id_empleado', Auth::user()->id_empleado)
            ->first();

        $solicitudrespuesta = DB::table('solicitudesrespuestas')
            ->select(DB::raw('DATE_FORMAT(fecha_limite, "%d-%m-%Y") as fecha_limite'))
            ->where('id_documento', $id)
            ->first();

        if( $solicitudrespuesta ){   //---[ SI EXISTE SOLICITUD RESPUESTA ]
            return view('revision.xturnado.director.show')
                ->with('documento', $documento)
                ->with('solicitudrespuesta', $solicitudrespuesta)
                ->with('solicitudcita', $this->existeSolicitudCita($id))
                ->with('rangofecha', $this->getDateRango())
                ->with('revision', $revision)
                ->with('num_orden', $numorden)
                ->with('id_documento', $id);
        }
        //---[ ELSE ]------------------------------------------------
        return view('revision.xturnado.director.show')
            ->with('documento', $documento)
            ->with('solicitudcita', $this->existeSolicitudCita($id))
            ->with('rangofecha', $this->getDateRango())
            ->with('revision', $revision)
            ->with('num_orden', $numorden)
            ->with('id_documento', $id);
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    private function queryShowPrivilegioTurnadoAsist( $numorden, $id)
    {
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $revision = DB::table('reportesrevisiones')
            ->select('observacion', 'recibi_papel')
            ->where('numorden_guiaturnado', $numorden)
            ->where('id_documento', $id)
            ->where('id_empleado', Auth::user()->id_empleado)
            ->first();

        $consultainstruccion = $this->getInstruccionTurnado($numorden);
        if( strcmp($consultainstruccion->instruccion, 'REVISION_ATENCION') == 0){   //---[ INSTRUCCION: RESPONDER ]
            //---[ Debe existir solicitud respuesta ]-------------------------------------
            $solicitudrespuesta = DB::table('solicitudesrespuestas')
                ->select(DB::raw('DATE_FORMAT(fecha_limite, "%d-%m-%Y") as fecha_limite'))
                ->where('id_documento', $id)
                ->first();

            return view('revision.xturnado.asistente.show')
                ->with('documento', $documento)
                ->with('solicitudrespuesta', $solicitudrespuesta)
                ->with('solicitudcita', $this->existeSolicitudCita($id))
                ->with('rangofecha', $this->getDateRango())
                ->with('revision', $revision)
                ->with('num_orden', $numorden)
                ->with('id_documento', $id);
        }
        //---[ ELSE ]------------------------------------------------
        return view('revision.xturnado.asistente.show')
            ->with('documento', $documento)
            ->with('solicitudcita', $this->existeSolicitudCita($id))
            ->with('rangofecha', $this->getDateRango())
            ->with('revision', $revision)
            ->with('num_orden', $numorden)
            ->with('id_documento', $id);
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    private function queryShowPrivilegioTurnado( $numorden, $id, $carpeta_auth)
    {
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $revision = DB::table('reportesrevisiones')
            ->select('observacion', 'recibi_papel')
            ->where('numorden_guiaturnado', $numorden)
            ->where('id_documento', $id)
            ->where('id_empleado', Auth::user()->id_empleado)
            ->first();

        $consultainstruccion = $this->getInstruccionTurnado($numorden);
        if( strcmp($consultainstruccion->instruccion, 'REVISION_ATENCION') == 0){   //---[ INSTRUCCION: RESPONDER ]
            //---[ Debe existir solicitud respuesta ]-------------------------------------
            $solicitudrespuesta = DB::table('solicitudesrespuestas')
                ->select(DB::raw('DATE_FORMAT(fecha_limite, "%d-%m-%Y") as fecha_limite'))
                ->where('id_documento', $id)
                ->first();

            return view('revision.xturnado.'.$carpeta_auth.'.show')
                ->with('documento', $documento)
                ->with('solicitudrespuesta', $solicitudrespuesta)
                ->with('rangofecha', $this->getDateRango())
                ->with('revision', $revision)
                ->with('num_orden', $numorden)
                ->with('id_documento', $id);
        }
        //---[ ELSE ]------------------------------------------------
        return view('revision.xturnado.'.$carpeta_auth.'.show')
            ->with('documento', $documento)
            ->with('rangofecha', $this->getDateRango())
            ->with('revision', $revision)
            ->with('num_orden', $numorden)
            ->with('id_documento', $id);
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    public function showxturnado($numorden, $id)            //---[num_orden, id_documento]--------
    {
        if( $this->existeRevisionXTurnado($numorden, $id) ){
            switch( $this->getPuestoUser() ){
                case "DIRECTOR":
                    return $this->queryShowPrivilegioTurnadoDir($numorden, $id);
                    break;
                case "JEFE":
                    return $this->queryShowPrivilegioTurnado($numorden, $id, 'jefe');
                    break;
                case "GENERAL":
                    return $this->queryShowPrivilegioTurnado($numorden, $id, 'general');
                    break;
                case "ASISTENTE":
                    return $this->queryShowPrivilegioTurnadoAsist($numorden, $id);
                    break;
            }
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: El recurso al que intenta acceder no esta disponible');
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    private function queryEditPrivilegioTurnadoDir( $numorden, $id)
    {
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $revision = DB::table('reportesrevisiones')
            ->select('observacion', 'recibi_papel')
            ->where('numorden_guiaturnado', $numorden)
            ->where('id_documento', $id)
            ->where('id_empleado', Auth::user()->id_empleado)
            ->first();

        $solicitudrespuesta = DB::table('solicitudesrespuestas')
            ->select(DB::raw('DATE_FORMAT(fecha_limite, "%d-%m-%Y") as fecha_limite'))
            ->where('id_documento', $id)
            ->first();

        if( $solicitudrespuesta ){   //---[ SI EXISTE SOLICITUD RESPUESTA ]
            return view('revision.xturnado.director.edit')
                ->with('documento', $documento)
                ->with('solicitudrespuesta', $solicitudrespuesta)
                ->with('solicitudcita', $this->existeSolicitudCita($id))
                ->with('rangofecha', $this->getDateRango())
                ->with('revision', $revision);
        }
        //---[ ELSE ]------------------------------------------------
        return view('revision.xturnado.director.edit')
            ->with('documento', $documento)
            ->with('solicitudcita', $this->existeSolicitudCita($id))
            ->with('rangofecha', $this->getDateRango())
            ->with('revision', $revision);
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    private function queryEditPrivilegioTurnadoAsist( $numorden, $id)
    {
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $revision = DB::table('reportesrevisiones')
            ->select('observacion', 'recibi_papel')
            ->where('numorden_guiaturnado', $numorden)
            ->where('id_documento', $id)
            ->where('id_empleado', Auth::user()->id_empleado)
            ->first();

        $consultainstruccion = $this->getInstruccionTurnado($numorden);
        if( strcmp($consultainstruccion->instruccion, 'REVISION_ATENCION') == 0){   //---[ INSTRUCCION: RESPONDER ]
            //---[ Debe existir solicitud respuesta ]-------------------------------------
            $solicitudrespuesta = DB::table('solicitudesrespuestas')
                ->select(DB::raw('DATE_FORMAT(fecha_limite, "%d-%m-%Y") as fecha_limite'))
                ->where('id_documento', $id)
                ->first();

            return view('revision.xturnado.asistente.edit')
                ->with('documento', $documento)
                ->with('solicitudrespuesta', $solicitudrespuesta)
                ->with('solicitudcita', $this->existeSolicitudCita($id))
                ->with('rangofecha', $this->getDateRango())
                ->with('revision', $revision);
        }
        //---[ ELSE ]------------------------------------------------
        return view('revision.xturnado.asistente.edit')
            ->with('documento', $documento)
            ->with('solicitudcita', $this->existeSolicitudCita($id))
            ->with('rangofecha', $this->getDateRango())
            ->with('revision', $revision);
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    private function queryEditPrivilegioTurnado( $numorden, $id, $carpeta_auth )
    {
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $revision = DB::table('reportesrevisiones')
            ->select('observacion', 'recibi_papel')
            ->where('numorden_guiaturnado', $numorden)
            ->where('id_documento', $id)
            ->where('id_empleado', Auth::user()->id_empleado)
            ->first();

        $consultainstruccion = $this->getInstruccionTurnado($numorden);
        if( strcmp($consultainstruccion->instruccion, 'REVISION_ATENCION') == 0){   //---[ INSTRUCCION: RESPONDER ]
            //---[ Debe existir solicitud respuesta ]-------------------------------------
            $solicitudrespuesta = DB::table('solicitudesrespuestas')
                ->select(DB::raw('DATE_FORMAT(fecha_limite, "%d-%m-%Y") as fecha_limite'))
                ->where('id_documento', $id)
                ->first();

            return view('revision.xturnado.'.$carpeta_auth.'.edit')
                ->with('documento', $documento)
                ->with('solicitudrespuesta', $solicitudrespuesta)
                ->with('rangofecha', $this->getDateRango())
                ->with('revision', $revision);
        }
        //---[ ELSE ]------------------------------------------------
        return view('revision.xturnado.'.$carpeta_auth.'.edit')
            ->with('documento', $documento)
            ->with('rangofecha', $this->getDateRango())
            ->with('revision', $revision);
    }
 
    //----[ POR TURNADO ]-------------------------------------------------------------------------
    public function editxturnado($numorden, $id)            //---[num_orden, id_documento]--------
    {
        if( $this->existeRevisionXTurnado($numorden, $id) ){
            switch( $this->getPuestoUser() ){
                case "DIRECTOR":
                    return $this->queryEditPrivilegioTurnadoDir($numorden, $id);
                    break;
                case "JEFE":
                    return $this->queryEditPrivilegioTurnado($numorden, $id, 'jefe');
                    break;
                case "GENERAL":
                    return $this->queryEditPrivilegioTurnado($numorden, $id, 'general');
                    break;
                case "ASISTENTE":
                    return $this->queryEditPrivilegioTurnadoAsist($numorden, $id);
                    break;
            }
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: El recurso al que intenta acceder no esta disponible');
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    private function queryUpdateXTurnadoAsist($request, $numorden, $id)//---[num_orden, id_documento]
    {
        DB::beginTransaction();
            //Recuperar SOLICITUDCITA, si existe en la BD
            $solicitud = DB::table('solicitudescitas')
                ->select('id', 'estado')
                ->where('id_documento', $id)
                ->first();

            if( $request->solicitudcitaCheckbox ){          //---[ USUARIO SOLICITA CITA ]----------------
                if( ! $solicitud ){  //--[ SI NO EXISTE ]-------------------------------------------------
                    DB::table('solicitudescitas')->insert(
                        ['id_documento' => $id]
                    );
                }
            }else{      //---------------------------------------[ USUARIO ELIMINA O NO SOLICITA CITA ]---
                if( $solicitud ){  //--[ SI EXISTE ]------------------------------------------------------
                    if( strcmp($solicitud->estado, 'AGENDADO') !== 0 ){
                        DB::table('solicitudescitas')->where('id', '=', $solicitud->id)->delete();
                    }
                }
            }

            DB::table('reportesrevisiones')
                ->where('numorden_guiaturnado', $numorden)
                ->where('id_documento', $id)
                ->where('id_empleado', Auth::user()->id_empleado)
                ->update([
                    'observacion' => $request->observacion, 
                    'recibi_papel' => ($request->filled('checkpapel') ? true : false)
                ]);
        DB::commit();

        return redirect()->action('ReportesrevisionesController@showxturnado', [$numorden, $id])->with('message', 'Revision actualizada satisfactoriamente');
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    private function queryUpdateXTurnadoDir($request, $numorden, $id)//---[num_orden, id_documento]
    {
        DB::beginTransaction();
            //Recuperar SOLICITUDCITA, si existe en la BD
            $solicitudCita = DB::table('solicitudescitas')
                ->select('id', 'estado')
                ->where('id_documento', $id)
                ->first();

            //---[ SOLICITUD CITA ]---------------------------
            if( $request->solicitudcitaCheckbox ){          //---[ USUARIO SOLICITA CITA ]----------------
                if( ! $solicitudCita ){  //--[ SI NO EXISTE ]-------------------------------------------------
                    DB::table('solicitudescitas')->insert(
                        ['id_documento' => $id]
                    );
                }
            }else{      //---------------------------------------[ USUARIO ELIMINA O NO SOLICITA CITA ]---
                if( $solicitudCita ){  //--[ SI EXISTE ]------------------------------------------------------
                    if( strcmp($solicitudCita->estado, 'AGENDADO') !== 0 ){
                        DB::table('solicitudescitas')->where('id', '=', $solicitudCita->id)->delete();
                    }
                }
            }
            //---[ FIN SOLICITUD CITA ]-----------------------
            
            //Recuperar SOLICITUDRESPUESTA, si existe en la BD
            $solicitudRespuesta = DB::table('solicitudesrespuestas')
                ->select('id', 'fecha_limite')
                ->where('id_documento', $id)
                ->first();

            if ($request->filled('fechaRespuestaInput')) {  //---[ USUARIO SOLICITA RESPUESTA    ]---
                //Dar formato a fecha YYYY-MM-DD
                $nuevaFechaLimite = explode("-", $request->fechaRespuestaInput);
                $nuevaFechaLimite = $nuevaFechaLimite[2] . '-' . $nuevaFechaLimite[1] . '-' . $nuevaFechaLimite[0];

                if( $solicitudRespuesta ){   //---[ SI EXISTE ]---
                    if (strcmp($solicitudRespuesta->fecha_limite, $nuevaFechaLimite) !== 0) {    //---[ SI HUBO CAMBIO ]---
                        //---[ ACTUALIZACION ]---
                        DB::table('solicitudesrespuestas')
                            ->where('id', $solicitudRespuesta->id)
                            ->update(['fecha_limite' => $nuevaFechaLimite]);
                        //---[ REGISTRO DE ACTIVIDAD ]-------------------
                        DB::table('empleadosdocumentos')->insert(
                            [
                                'actividad' => 'EDICION', 
                                'id_empleado' => Auth::user()->id_empleado,
                                'id_documento' => $id,
                                'descripcion' => $solicitudRespuesta->id .':EDICION:SOLICITUD_RESPUESTA'
                            ]
                        );
                    }
                }else{
                    //---[ AGREGACION ]---
                    $idNuevaSolicitudRespuesta = DB::table('solicitudesrespuestas')->insertGetId(
                        ['id_documento' => $id, 'fecha_limite' => $nuevaFechaLimite]
                    );
                    //---[ REGISTRO DE ACTIVIDAD ]-------------------
                    DB::table('empleadosdocumentos')->insert(
                        [
                            'actividad' => 'EDICION', 
                            'id_empleado' => Auth::user()->id_empleado,
                            'id_documento' => $id,
                            'descripcion' => $idNuevaSolicitudRespuesta .':AGREGACION:SOLICITUD_RESPUESTA'
                        ]
                    );
                }   
            }else{                                          //---[ USUARIO NO SOLICITA RESPUESTA ]---
                if( $solicitudRespuesta ){   //---[ SI EXISTE SOLICITUDRESPUESTA EN BD ]----------------------
                    //---[ ELIMINACION DE SOLICITUD EN BD ]------------------------------------------
                    DB::table('solicitudesrespuestas')->where('id', $solicitudRespuesta->id)->delete();
                    //---[ REGISTRO DE ACTIVIDAD ]---------------------------------------------------
                    DB::table('empleadosdocumentos')->insert(
                        [
                            'actividad' => 'EDICION', 
                            'id_empleado' => Auth::user()->id_empleado,
                            'id_documento' => $id,
                            'descripcion' => $solicitudRespuesta->id .':ELIMINACION:SOLICITUD_RESPUESTA'
                        ]
                    );
                }
            }
            
            //Recuperar REPORTEREVISION y actualizar
            DB::table('reportesrevisiones')
                ->where('numorden_guiaturnado', $numorden)
                ->where('id_documento', $id)
                ->where('id_empleado', Auth::user()->id_empleado)
                ->update([
                    'observacion' => $request->observacion, 
                    'recibi_papel' => ($request->filled('checkpapel') ? true : false)
                ]);
        DB::commit();

        return redirect()->action('ReportesrevisionesController@showxturnado', [$numorden, $id])->with('message', 'Revision actualizada satisfactoriamente');
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    private function queryUpdateXTurnado($request, $numorden, $id)//---[num_orden, id_documento]
    {
        DB::beginTransaction();
            DB::table('reportesrevisiones')
                ->where('numorden_guiaturnado', $numorden)
                ->where('id_documento', $id)
                ->where('id_empleado', Auth::user()->id_empleado)
                ->update([
                    'observacion' => $request->observacion, 
                    'recibi_papel' => ($request->filled('checkpapel') ? true : false)
                ]);
        DB::commit();

        return redirect()->action('ReportesrevisionesController@showxturnado', [$numorden, $id])->with('message', 'Revision actualizada satisfactoriamente');
    }

    //----[ POR TURNADO ]-------------------------------------------------------------------------
    public function updatexturnado(Request $request, $numorden, $id)//---[num_orden, id_documento]
    {
        switch( $this->getPuestoUser() ){
            case "DIRECTOR":
                return $this->queryUpdateXTurnadoDir($request, $numorden, $id);
                break;
            case "ASISTENTE":
                return $this->queryUpdateXTurnadoAsist($request, $numorden, $id);
                break;
            case "JEFE":
                return $this->queryUpdateXTurnado($request, $numorden, $id);
                break;
            case "GENERAL":
                return $this->queryUpdateXTurnado($request, $numorden, $id);
                break;
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Vista no disponible para puesto desconocido.');
    }
    
    //----[ POR TURNADO ]-------------------------------------------------------------------------
    public function destroyxturnado($numorden, $id)                 //---[num_orden, id_documento]
    {
        //---[ VERIFICAR: Si se volvio a turnar no eliminar ]-------------------------------------
        $fue_turnado = DB::table('guiasturnados')
            ->where('idemp_turnador', Auth::user()->id_empleado)
            ->where('id_documento', $id)
            ->first();

        if( $fue_turnado ){
            echo "<script>console.error('Documento revisado ha sido turnado')</script>";
        }

        //---[ VERIFICAR: Solicitud cita ]--------------------------------------------------------
        $solicitud = DB::table('solicitudescitas')
            ->select('estado')
            ->where('id_documento', $id)
            ->first();

        if( $solicitud ){   //---[ Existe solicitud ]---------------------------------------------
            if( strcmp($solicitud->estado, 'AGENDADO') == 0 ){
                abort(403, 'Esta revision tiene asociada una cita agendada, debe eliminar primero la cita.');
            }
        }

        DB::beginTransaction();
            DB::table('reportesrevisiones')
                ->where('numorden_guiaturnado', $numorden)
                ->where('id_documento', $id)
                ->where('id_empleado', Auth::user()->id_empleado)
                ->delete();

            //Localizar turnado y actualizar a VISTO
            DB::table('detallesguiasturnados')
                ->where('numorden_guiaturnado', $numorden)
                ->where('idemp_turnado', Auth::user()->id_empleado)
                ->update(['estado' => 'VISTO']);
        DB::commit();
    }
}
