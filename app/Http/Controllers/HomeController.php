<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use DB;
 
class HomeController extends Controller
{
    private function getPuestoUser(){
        $usremp = DB::table('users')
                    ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
                    ->select('empleados.puesto')
                    ->where('users.id_empleado', Auth::user()->id_empleado)
                    ->first();

        return $usremp->puesto;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            switch ( $this->getPuestoUser() ) {
                case "ADMIN":
                    return view('home.admin');
                    break;
                case "MENSAJERO":
                    return view('home.mensajero');
                    break;
                case "DIRECTOR":
                    return view('home.director');
                    break;
                case "JEFE":
                    return view('home.jefe');
                    break;
                case "ASISTENTE":
                    return view('home.asistente');
                    break;
                case "GENERAL":
                    return view('home.general');
                    break;
            }
        }

        return redirect('/login');
    }
}