<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Models\Suscriptor;
 
use DB;
 
class SuscriptorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suscriptores = suscriptor::all();
        return response()->json( $suscriptores );
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function likeNombre(Request $request)
    {
        //$palabra = Str::lower(Input::get('palabra'));
 
        $palabra = strtolower( $request->input('palabra') );
 
        $suscriptores = DB::table('suscriptores')
                    ->select('nombre')
                    ->where('nombre', 'LIKE', $palabra.'%')
                    ->groupBy('nombre')
                    ->take(10)
                    ->get();
        
        //echo "<script>console.log('$nombre')</script>";
        
        //return Response::json( $returnArray );
        return response()->json( $suscriptores );
    }

     /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function likeCargo(Request $request)
    {
        $palabra = strtolower( $request->input('palabra') );
 
        $suscriptores = DB::table('suscriptores')
                    ->select('cargo')
                    ->where('cargo', 'LIKE', $palabra.'%')
                    ->groupBy('cargo')
                    ->take(10)
                    ->get();
        
        //echo "<script>console.log('$nombre')</script>";
        
        return response()->json( $suscriptores );
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function likeDependencia(Request $request)
    {
        $palabra = strtolower( $request->input('palabra') );
 
        $suscriptores = DB::table('suscriptores')
                    ->select('dependencia')
                    ->where('dependencia', 'LIKE', $palabra.'%')
                    ->groupBy('dependencia')
                    ->take(10)
                    ->get();
        
        //echo "<script>console.log('$nombre')</script>";
        
        return response()->json( $suscriptores );
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function dependencia(Request $request)
    {
        $palabra = strtolower( $request->input('palabra') );
 
        $suscriptores = DB::table('suscriptores')
                    ->select('direccion_dependencia')
                    ->where('dependencia', $palabra)
                    ->groupBy('direccion_dependencia')
                    ->take(10)
                    ->get();
        
        //echo "<script>console.log('$nombre')</script>";
        
        return response()->json( $suscriptores );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function existe($nombre, $cargo, $dependencia, $direccion)
    {
        $suscriptor = DB::table('suscriptores')
                        ->where('nombre', $nombre)
                        ->where('cargo', $cargo)
                        ->where('dependencia', $dependencia)
                        ->where('direccion_dependencia', $direccion)
                        ->get();
 
        echo "<script>console.log('$direccion')</script>";
        //return $suscriptor;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
 
    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*
    public function nombreLike(Request $request)
    {
 
        $nombre = $request->input('palabra');
        echo "<script>console.log('$nombre')</script>";
    }
    */
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}