<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Notification;

use App\Models\Documento;
use App\Models\Tipodocumento;
use App\Models\Empleadodocumento;
use App\Notifications\InvoicePaid;
use App\User;

use App;
use DB;
use Auth;
use DateTime;

class RespuestasController extends Controller
{
    private function getPuestoUser()
    {
        $usremp = DB::table('users')
                    ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
                    ->select('empleados.puesto')
                    ->where('users.id_empleado', Auth::user()->id_empleado)
                    ->first();

        return $usremp->puesto;
    }

    private function propietarioRespuesta($idSolicitudResp)
    {
        $respuesta = DB::table('respuestas')
            ->join('solicitudesrespuestas', function ($join) use ($idSolicitudResp) {
                $join->on('respuestas.id_solicitud', '=', 'solicitudesrespuestas.id')
                     ->where([
                        ['solicitudesrespuestas.estado', '=', 'CON_RESPUESTA'],
                        ['respuestas.id_solicitud', '=', $idSolicitudResp],
                        ['respuestas.id_empleado', '=', Auth::user()->id_empleado]
                    ]);
            })
            ->select('respuestas.registrado')
            ->first();

        if( $respuesta ){   //---[ SI EXISTE ]---
            return true;
        }
        return false;
    }

    private function getDateRango()
    {
        $date_start = new DateTime();
        $date_end = new DateTime();

        for( $i = 0; $i < 15; $i++ ){
            $date_end->modify('+1 day');
            $newDay = date('w', $date_end->getTimestamp());
            if($newDay == 0 || $newDay == 6) {
                $i--;
            }
        }

        $array = array(
            'fecha_inicio' => $date_start->format('d-m-Y'),
            'fecha_fin' => $date_end->format('d-m-Y')
        );
        
        return $array;
    }

    private function existeSolicitudCita($id)
    {
        $solicitud = DB::table('solicitudescitas')
            ->where('id_documento', $id)
            ->first();

        if( $solicitud ){   //---[ SI EXISTE ]---
            return $solicitud;
        }
        return false;
    }

    private function getSolicitudRespuesta($idSolicitudResp)
    {
        $solicitud = DB::table('solicitudesrespuestas')
            ->select('id_documento', DB::raw('DATE_FORMAT(fecha_limite, "%d-%m-%Y") as fecha_limite'), 'estado')
            ->where('id', $idSolicitudResp)
            ->first();

        if( $solicitud ){    //---[SI EXISTE]-------
            return $solicitud;
        }else{
            return false;
        }
    }

    private function getRespuestaGuardada($idSolicitudResp)
    {
        $respuesta = DB::table('solicitudesrespuestas')
            ->join('respuestas', function ($join) use($idSolicitudResp){
                $join->on('solicitudesrespuestas.id', '=', 'respuestas.id_solicitud')
                     ->where([
                        ['solicitudesrespuestas.id', '=', $idSolicitudResp],
                        ['solicitudesrespuestas.estado', '=', 'CON_RESPUESTA']
                    ]);
            })
            ->select('respuestas.id_solicitud', 'respuestas.id_documento_respuesta', 'respuestas.registrado' )
            ->first();

        if( $respuesta ){    //---[SI EXISTE]-------
            return $respuesta;
        }else{
            return false;
        }
    }

    private function getRespuestaEnProceso($idSolicitudResp)
    {
        $respuesta = DB::table('solicitudesrespuestas')
            ->join('respuestas', function ($join) use($idSolicitudResp){
                $join->on('solicitudesrespuestas.id', '=', 'respuestas.id_solicitud')
                     ->where([
                        ['solicitudesrespuestas.id', '=', $idSolicitudResp],
                        ['solicitudesrespuestas.estado', '=', 'EN_PROCESO']
                    ]);
            })
            ->select('respuestas.id_solicitud', 'respuestas.id_documento_respuesta', 'respuestas.registrado' )
            ->first();

        if( $respuesta ){    //---[SI EXISTE]-------
            return $respuesta;
        }else{
            return false;
        }
    }

    public function lista()
    {
        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                return view('respuesta.admin.lista');
                break;
            case "DIRECTOR":
                return view('respuesta.director.lista');
                break;
            case "ASISTENTE":
                return view('respuesta.asistente.lista');
                break;
            case "JEFE":
                return view('respuesta.jefe.lista');
                break;
            case "GENERAL":
                return view('respuesta.general.lista');
                break;
            default:
                return redirect()->action('HomeController@index')->with('message', 'ERROR: Vista no disponible para puesto desconocido.');
        }
    }
    
    public function create($numorden, $idSolicitudResp)
    {
    	$solicitudrespuesta = $this->getSolicitudRespuesta( $idSolicitudResp );
        if( $solicitudrespuesta ){
            $carpeta_auth = strtolower( $this->getPuestoUser() );
            if( strcmp($solicitudrespuesta->estado, 'NO_ATENDIDO') == 0 || strcmp($solicitudrespuesta->estado, 'EN_PROCESO') == 0){
                $documento = DB::table('documentos')
                    ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
                    ->join('empleadosdocumentos', function ($join) {
                        $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                        ->where('empleadosdocumentos.actividad', 'ingreso');
                    })
                    ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
                    ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre as suscriptor', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
                    ->where('documentos.id', $solicitudrespuesta->id_documento)
                    ->first();

                if(!is_null($documento->ruta_pdf)){
                    if (App::environment('local')) {
                        if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                            $documento->ruta_pdf = url($documento->ruta_pdf);
                        }else{
                            $documento->ruta_pdf = null;
                        }
                    }else{
                        if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                            $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                        }else{
                            $documento->ruta_pdf = null;
                        }
                    }
                }

                $respuesta = $this->getRespuestaEnProceso($idSolicitudResp);
                if($respuesta){     //---[ RESPUESTA GENERADA ]------------------------
                    echo "<script>console.info('Respuesta generada')</script>";
                    $documentorespuesta = DB::table('documentos')
                        ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
                        ->join('empleadosdocumentos', function ($join) {
                            $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                            ->where('empleadosdocumentos.actividad', 'RESPUESTA');
                        })
                        ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
                        ->select(DB::raw('DATE_FORMAT(documentos.fecha_oficio, "%d-%m-%Y") as fecha_oficio'), 'documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'suscriptores.nombre as suscriptor', 'suscriptores.cargo', 'suscriptores.dependencia')
                        ->where('documentos.id', $respuesta->id_documento_respuesta)
                        ->first();

                    if( strcmp($carpeta_auth, 'asistente') == 0 || strcmp($carpeta_auth, 'director') == 0 ){
                        return view('respuesta.' . $carpeta_auth . '.create')
                            ->with('documento', $documento)
                            ->with('documentorespuesta', $documentorespuesta)
                            ->with('solicitudrespuesta', $solicitudrespuesta)
                            ->with('rangofecha', $this->getDateRango())
                            ->with('solicitudcita', $this->existeSolicitudCita($solicitudrespuesta->id_documento));
                    }   //---[ ELSE ]---------------------------------------------------
                    return view('respuesta.' . $carpeta_auth . '.create')
                        ->with('documento', $documento)
                        ->with('documentorespuesta', $documentorespuesta)
                        ->with('solicitudrespuesta', $solicitudrespuesta)
                        ->with('rangofecha', $this->getDateRango());
                    
                }

                if( strcmp($carpeta_auth, 'asistente') == 0 || strcmp($carpeta_auth, 'director') == 0 ){
                    return view('respuesta.' . $carpeta_auth . '.create')
                        ->with('documento', $documento)
                        ->with('solicitudrespuesta', $solicitudrespuesta)
                        ->with('rangofecha', $this->getDateRango())
                        ->with('solicitudcita', $this->existeSolicitudCita($solicitudrespuesta->id_documento));
                }   //---[ ELSE ]---------------------------------------------------
                return view('respuesta.' . $carpeta_auth . '.create')
                    ->with('documento', $documento)
                    ->with('solicitudrespuesta', $solicitudrespuesta)
                    ->with('rangofecha', $this->getDateRango());
            }else{
                return redirect()->action('HomeController@index')->with('message', 'ERROR: La operacion que intenta realizar no esta disponible');
            }
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: El recurso al que intenta acceder no esta disponible');
    }

    private function getUsersToNotify($idSolicitudRespuesta){
        $mensajero = DB::table('solicitudesrespuestas')
            ->join('empleadosdocumentos', function ($join) use ($idSolicitudRespuesta){
                $join->on('solicitudesrespuestas.id_documento', '=', 'empleadosdocumentos.id_documento')
                    ->where('solicitudesrespuestas.id', $idSolicitudRespuesta)
                    ->where('empleadosdocumentos.actividad', '=', 'INGRESO');
            })
            ->select('empleadosdocumentos.id_empleado')
            ->first();

        $director_activo = DB::table('users')
            ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
            ->join('departamentos', 'empleados.id_departamento', '=', 'departamentos.id')
            ->select('empleados.id AS id_empleado')
            ->where('users.activo', true)
            ->where('departamentos.nombre', 'DIRECCION')
            ->where('departamentos.area', 'DIRECCION DE ATENCION MEDICA')
            ->where('empleados.puesto', 'DIRECTOR')
            ->first();

        if($director_activo){   //--------[SI EXITE DIRECTOR ACTIVO]            
            $usuarios = User::whereIn('id_empleado', array($mensajero->id_empleado, $director_activo->id_empleado))->get();

            return $usuarios;
        }else{            
            $usuarios = User::whereIn('id_empleado', array($mensajero->id_empleado))->get();

            return $usuarios;
        }

        return false;
    }

    public function store(Request $request, $numorden, $idSolicitudResp)
    {

        $respuesta = $this->getRespuestaEnProceso($idSolicitudResp);
        if( $respuesta ){ //---[ Existe respuesta ]------------------
            DB::beginTransaction();
                $path = '';
                //---------[ FILE ]-----------------------------------------
                if ( $request->hasFile('wordInputFile') ) {
                    if ( $request->file('wordInputFile')->isValid() ) {
                        if (App::environment('local')) {
                            $path = $request->wordInputFile->store('public/documentos/respuesta');
                            $path = str_replace("public", "storage", $path);
                        }else{
                            $path = $request->wordInputFile->store('documentos/respuesta', 's3');
                        }
                    }
                }

                $fecha = DateTime::createFromFormat('d-m-Y', $request->fechaOficio);
                //---[ ACTUALIZAR DOCUMENTO RESPUESTA ]--------------
                DB::table('documentos')
                    ->where('id', $respuesta->id_documento_respuesta)
                    ->update([
                        'num_hojas' => $request->numHojas,
                        'num_anexos' => $request->numAnexos,
                        'ruta_pdf' => $path,
                        'fecha_oficio' => date_format($fecha, 'Y-m-d'),
                        'asunto' => $request->asunto
                    ]);

                //---------[ ACTUALIZAR ESTADO SOLICITUD ]-------------------
                DB::table('solicitudesrespuestas')
                    ->where('id', $idSolicitudResp)
                    ->update(['estado' => 'CON_RESPUESTA']);
                //---------[ ACTUALIZAR ESTADO DETALLE TURNADO ]-------------
                DB::table('detallesguiasturnados')
                    ->where([
                        ['numorden_guiaturnado', '=', $numorden],
                        ['idemp_turnado', '=', Auth::user()->id_empleado],
                        ['estado', '=', 'REVISADO']
                    ])
                    ->update(['estado' => 'CON_RESPUESTA']);

                //---------[ NOTIFICAR ]-------------------------------------
                $usuarios = $this->getUsersToNotify($idSolicitudResp);
                if($usuarios){  //---[ SI EXISTEN USUARIOS A NOTIFICAR ]-----
                    $solicitud = DB::table('solicitudesrespuestas')
                        ->select('id_documento')
                        ->where('id', $idSolicitudResp)
                        ->first();

                    $datos = array(
                        'id_documento' => $solicitud->id_documento,
                        'mensaje' => 'Nuevo documento con respuesta'
                    );

                    Notification::send($usuarios, new InvoicePaid($datos));
                }

            DB::commit();

            return redirect()->action('RespuestasController@show', [$numorden, $idSolicitudResp])->with('message', 'Respuesta guardada.');
        }else{
            DB::beginTransaction();
                $id_tipodocumento = DB::table('tiposdocumentos')
                    ->select('id')
                    ->where('tipo', 'OFICIO')
                    ->first();

                $fecha = DateTime::createFromFormat('d-m-Y', $request->fechaOficio);
                //---------[ SUSCRIPTOR ]------------------------------------
                $id_suscriptor = DB::table('suscriptores')
                    ->select('id')
                    ->where('nombre', $request->suscriptorInput)
                    ->where('cargo', $request->cargoInput)
                    ->where('dependencia', $request->dependenciaInput)
                    ->first();
                
                //---------[ DOCUMENTO RESPUESTA ]---------------------------
                $id_documento_respuesta = DB::table('documentos')->insertGetId(
                    [
                        'asunto' => $request->asunto, 
                        'num_hojas' => 0,
                        'num_anexos' => 0,
                        'num_oficio' => 0,
                        'fecha_oficio' => date_format($fecha, 'Y-m-d'),
                        'ruta_pdf' => '',
                        'id_tipodocumento' => $id_tipodocumento->id,
                        'id_suscriptor' => $id_suscriptor->id
                    ]
                );

                //---------[ ACTUALIZAR DOCUMENTO RESPUESTA ]----------------
                $objDateTime = new DateTime('NOW');
                $newNumOficio = 'DAT-' . date_format($objDateTime, 'dmY') . '-' . $id_documento_respuesta;
                
                DB::table('documentos')
                    ->where('id', $id_documento_respuesta)
                    ->update(['num_oficio' => $newNumOficio ]);
                
                //---------[ RESPUESTA ]-------------------------------------
                DB::table('respuestas')->insert(
                    [
                        'id_solicitud' => $idSolicitudResp,
                        'id_documento_respuesta' => $id_documento_respuesta,
                        'id_empleado' => Auth::user()->id_empleado
                    ]
                );

                //---------[ EMPLEADO - DOCUMENTO ]--------------------------
                $empdoc = new Empleadodocumento;
                $empdoc->actividad = 'RESPUESTA';
                $empdoc->id_empleado = Auth::user()->id_empleado;
                $empdoc->id_documento = $id_documento_respuesta;
                $empdoc->save();

                //---------[ ACTUALIZAR ESTADO SOLICITUD ]-------------------
                DB::table('solicitudesrespuestas')
                    ->where('id', $idSolicitudResp)
                    ->update(['estado' => 'EN_PROCESO']);
                
            DB::commit();

            return redirect()->action('RespuestasController@create', [$numorden, $idSolicitudResp])->with('message', 'Respuesta generada.');
        }
    }

    private function getDocumentoRespuestaGuardada($idSolicitudResp)
    {
        $respuesta = DB::table('documentos')
            ->join('respuestas', 'documentos.id', '=', 'respuestas.id_documento_respuesta')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) use ($idSolicitudResp) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->whereIn('empleadosdocumentos.actividad', ['RESPUESTA']);
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('respuestas.id_solicitud', $idSolicitudResp)
            ->first();

        if( $respuesta ){    //---[SI EXISTE]-------
            if(!is_null($respuesta->ruta_pdf)){
                if (App::environment('local')) {
                    if(Storage::disk('local')->exists(str_replace("storage", "public", $respuesta->ruta_pdf))){
                        $respuesta->ruta_pdf = url($respuesta->ruta_pdf);
                    }else{
                        $respuesta->ruta_pdf = null;
                    }
                }else{
                    if(Storage::disk('s3')->exists($respuesta->ruta_pdf)){
                        $respuesta->ruta_pdf = Storage::temporaryUrl($respuesta->ruta_pdf, now()->addMinutes(5));
                    }else{
                        $respuesta->ruta_pdf = null;
                    }
                }
            }
            return $respuesta;
        }else{
            return false;
        }
    }

    private function queryShowAdminDir($numorden, $respuesta, $carpeta_auth)
    {
        $solicitudrespuesta = DB::table('solicitudesrespuestas')
            ->select(DB::raw('DATE_FORMAT(fecha_limite, "%d-%m-%Y") as fecha_limite'), 'id_documento', 'id')
            ->where('id', $respuesta->id_solicitud)
            ->first();

        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                    ->whereIn('empleadosdocumentos.actividad', ['INGRESO', 'EMISION']);
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $solicitudrespuesta->id_documento)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $documentorespuesta = $this->getDocumentoRespuestaGuardada( $respuesta->id_solicitud);

        return view('respuesta.' . $carpeta_auth . '.show')
            ->with('documento', $documento)
            ->with('documentorespuesta', $documentorespuesta)
            ->with('solicitudrespuesta', $solicitudrespuesta)
            ->with('rangofecha', $this->getDateRango())
            ->with('numorden', $numorden);
    }
 
    public function show($numorden, $idSolicitudResp)
    {
        $respuesta = $this->getRespuestaGuardada($idSolicitudResp);
        if( ! $respuesta ){
            return redirect()->action('HomeController@index')->with('message', 'ERROR: El recurso al que intenta acceder no esta disponible');
        }

        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                return $this->queryShowAdminDir($numorden, $respuesta, 'admin');
                break;
            case "DIRECTOR":
                return $this->queryShowAdminDir($numorden, $respuesta, 'director');
                break;
            case "JEFE":
                return $this->queryShowAdminDir($numorden, $respuesta, 'jefe');
                break;
            case "ASISTENTE":
                return $this->queryShowAdminDir($numorden, $respuesta, 'asistente');
                break;
            case "GENERAL":
                return $this->queryShowAdminDir($numorden, $respuesta, 'general');
                break;
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }

    private function queryEditPropietario($numorden, $respuesta, $carpeta_auth)
    {
        $solicitudrespuesta = $this->getSolicitudRespuesta( $respuesta->id_solicitud );
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre as suscriptor', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $solicitudrespuesta->id_documento)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $documentorespuesta = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->where('empleadosdocumentos.actividad', 'RESPUESTA');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select(DB::raw('DATE_FORMAT(documentos.fecha_oficio, "%d-%m-%Y") as fecha_oficio'), 'documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.ruta_pdf', 'suscriptores.nombre as suscriptor', 'suscriptores.cargo', 'suscriptores.dependencia')
            ->where('documentos.id', $respuesta->id_documento_respuesta)
            ->first();

        if( $documentorespuesta ){    //---[SI EXISTE]-------
            if(!is_null($documentorespuesta->ruta_pdf)){
                if (App::environment('local')) {
                    if(Storage::disk('local')->exists(str_replace("storage", "public", $documentorespuesta->ruta_pdf))){
                        $documentorespuesta->ruta_pdf = url($documentorespuesta->ruta_pdf);
                    }else{
                        $documentorespuesta->ruta_pdf = null;
                    }
                }else{
                    if(Storage::disk('s3')->exists($documentorespuesta->ruta_pdf)){
                        $documentorespuesta->ruta_pdf = Storage::temporaryUrl($documentorespuesta->ruta_pdf, now()->addMinutes(5));
                    }else{
                        $documentorespuesta->ruta_pdf = null;
                    }
                }
            }
        }

        return view('respuesta.' . $carpeta_auth . '.edit')
            ->with('documento', $documento)
            ->with('documentorespuesta', $documentorespuesta)
            ->with('solicitudrespuesta', $solicitudrespuesta)
            ->with('rangofecha', $this->getDateRango());
    }

    public function edit($numorden, $idSolicitudResp)
    {
        $respuesta = $this->getRespuestaGuardada($idSolicitudResp);
        if( ! $respuesta ){
            return redirect()->action('HomeController@index')->with('message', 'ERROR: La operacion que intenta realiar no esta disponible');
        }
        $puestoUserEmp = $this->getPuestoUser();
        switch ( $puestoUserEmp ) {
            case "ADMIN":
                return $this->queryEditPropietario($numorden, $respuesta, 'admin');
                break;
            case "DIRECTOR":
                return $this->queryEditPropietario($numorden, $respuesta, 'director');
                break;
        }

        if( $this->propietarioRespuesta($idSolicitudResp) ){
            return $this->queryEditPropietario($numorden, $respuesta, strtolower($puestoUserEmp));
        }
        
        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }

    public function update(Request $request, $numorden, $idSolicitudResp)
    {
        DB::beginTransaction();
            
            $respuesta = DB::table('respuestas')->where('id_solicitud', $idSolicitudResp)->first();
            $documento = Documento::findOrFail($respuesta->id_documento_respuesta);
            $path = $documento->ruta_pdf;
            
            //---------[ FILE ]------------------------------------------
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){     // Existe PDF
                    if ( $request->hasFile('wordInputFile') ) {
                        if ( $request->file('wordInputFile')->isValid() ) {
                            //---------[ ELIMINAR PDF ]------------------------------
                            Storage::delete( str_replace("storage", "public", $documento->ruta_pdf) );
                            //---------[ ALMACENAR NUEVO ]---------------------------
                            $path = $request->wordInputFile->store('public/documentos/respuesta');
                            $path = str_replace("public", "storage", $path);
                        }
                    }
                }else{
                    if ( $request->hasFile('wordInputFile') ) {
                        if ( $request->file('wordInputFile')->isValid() ) {
                            $path = $request->wordInputFile->store('public/documentos/respuesta');
                            $path = str_replace("public", "storage", $path);
                        }
                    }
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){  // Existe PDF
                    if ( $request->hasFile('wordInputFile') ) {
                        if ( $request->file('wordInputFile')->isValid() ) {
                            //---------[ ELIMINAR PDF ]------------------------------
                            Storage::disk('s3')->delete($documento->ruta_pdf);
                            //---------[ ALMACENAR NUEVO ]---------------------------
                            $path = $request->wordInputFile->store('documentos/respuesta', 's3');
                        }
                    }
                }else{
                    if ( $request->hasFile('wordInputFile') ) {
                        if ( $request->file('wordInputFile')->isValid() ) {
                            $path = $request->wordInputFile->store('documentos/respuesta', 's3');
                        }
                    }
                }
            }

            $fecha = DateTime::createFromFormat('d-m-Y', $request->fechaOficio);
            //---------[ DOCUMENTO ]------------------------------------
            $documento->fecha_oficio = date_format($fecha, 'Y-m-d');
            $documento->num_hojas = $request->numHojas;
            $documento->num_anexos = $request->numAnexos;
            $documento->asunto = $request->asunto;
            $documento->ruta_pdf = $path;
            $documento->save();

            //---------[ EMPLEADO - DOCUMENTO ]--------------------
            /*
            $empdoc = new Empleadodocumento;
            $empdoc->actividad = 'INGRESO-EDITADO';
            $empdoc->id_empleado = Auth::user()->id_empleado;
            $empdoc->id_documento = $id;
            $empdoc->save();
            */

        DB::commit();

        return redirect()->action('RespuestasController@show', [$numorden, $idSolicitudResp])->with('message', 'Respuesta actualizada satisfactoriamente');
    }

    public function destroy($numorden, $idSolicitudResp)
    {
        DB::beginTransaction();
            $respuesta = DB::table('respuestas')->where('id_solicitud', $idSolicitudResp)->first();
            $ruta = DB::table('documentos')->select('ruta_pdf')->where('id', '=', $respuesta->id_documento_respuesta)->first();
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $ruta->ruta_pdf))){ // Existe PDF
                    //---------[ ELIMINAR PDF ]------------------------------
                    Storage::delete( str_replace("storage", "public", $ruta->ruta_pdf) );
                }
            }else{
                if(Storage::disk('s3')->exists($ruta->ruta_pdf)){  // Existe PDF
                    //---------[ ELIMINAR PDF ]------------------------------
                    Storage::disk('s3')->delete($ruta->ruta_pdf);
                }
            }
            $documentorespuesta = DB::table('documentos')->where('id', '=', $respuesta->id_documento_respuesta)->delete();
            
            //---------[ ACTUALIZAR ESTADO SOLICITUD ]-------------------
            DB::table('solicitudesrespuestas')
                ->where('id', $idSolicitudResp)
                ->update(['estado' => 'NO_ATENDIDO']);
            //---------[ ACTUALIZAR ESTADO DETALLE TURNADO ]-------------
            DB::table('detallesguiasturnados')
                ->where([
                    ['numorden_guiaturnado', '=', $numorden],
                    ['idemp_turnado', '=', Auth::user()->id_empleado],
                    ['estado', '=', 'CON_RESPUESTA']
                ])
                ->update(['estado' => 'REVISADO']);
        DB::commit();        
    }
}
