<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Detalleguiaturnado;
use App\Models\Empleadodocumento;
use App\Models\Reporterevision;
use App\Models\Guiaturnado;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App;
use DB;
use Auth;

class GuiasturnadosController extends Controller
{
	private function getPuestoUser()
    {
        $usremp = DB::table('users')
                    ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
                    ->select('empleados.puesto')
                    ->where('users.id_empleado', Auth::user()->id_empleado)
                    ->first();

        return $usremp->puesto;
    }

    private function existeTurnado($numorden, $id)      //---[num_orden, id_documento]---------
    {
        $turnado = DB::table('guiasturnados')
            ->where('num_orden', $numorden)
            ->where('id_documento', $id)
            ->first();

        if( $turnado ){   //---[ SI EXISTE ]---
            return true;
        }
        return false;
    }

    private function privilegioTurnado($numorden)       //---[ Turnado o turnador ]------------
    {
        $privilegio = DB::table('guiasturnados')
            ->join('detallesguiasturnados', function ($join) use ($numorden) {
                $join->on('guiasturnados.num_orden', '=', 'detallesguiasturnados.numorden_guiaturnado')
                     ->where('guiasturnados.num_orden', '=', $numorden);
            })
            ->select('guiasturnados.registrado')
            ->where('guiasturnados.idemp_turnador', Auth::user()->id_empleado)
            ->orWhere('detallesguiasturnados.idemp_turnado', Auth::user()->id_empleado)
            ->first();

        if( $privilegio ){   //---[ SI EXISTE ]---
            return true;
        }
        return false;
    }

    private function esTurnador($numorden)
    {
        $turnador = DB::table('guiasturnados')
            ->where([
                ['num_orden', '=', $numorden],
                ['idemp_turnador', '=', Auth::user()->id_empleado],
            ])
            ->select('guiasturnados.registrado')
            ->first();

        if( $turnador ){   //---[ SI EXISTE ]---
            return true;
        }
        return false;
    }

    private function getUsrsActivos( $depto, $area, $exc_puesto )   //---[ Lista IDS Activos]--
    {
        $usuarios_empleados = DB::table('users')
            ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
            ->join('departamentos', 'empleados.id_departamento', '=', 'departamentos.id')
            ->select('empleados.id')
            ->where('users.activo', true)
            ->where('departamentos.nombre', $depto)
            ->where('departamentos.area', $area)
            ->where('empleados.puesto', '<>', strtoupper($exc_puesto))
            ->get();

        return $usuarios_empleados->toArray();
    }

    private function getDirActivo()
    {
        $director = DB::table('users')
            ->join('empleados', function ($join) {
                $join->on('users.id_empleado', '=', 'empleados.id')
                    ->where([
                        ['empleados.puesto', '=', 'DIRECTOR'],
                        ['users.activo', '=', true]
                    ]);
            })
            ->select('users.id_empleado')
            ->first();

        if( $director ){   //---[ SI EXISTE ]---
            return $director;
        }
        return false;
    }

    public function listaTurnadoEntrada()
    {
        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                //return view('turnado.admin.listaturnadoentrada');
                break;
            case "MENSAJERO":
                //
                break;
            case "DIRECTOR":
                return view('turnado.director.listaturnadoentrada');
                break;
            case "JEFE":
                return view('turnado.jefe.listaturnadoentrada');
                break;
            case "ASISTENTE":
                return view('turnado.asistente.listaturnadoentrada');
                break;
            case "GENERAL":
                return view('turnado.general.listaturnadoentrada');
                break;
            default:
                return redirect()->action('HomeController@index')->with('message', 'ERROR: Vista no disponible para puesto desconocido.');
        }
    }
    
    public function listaTurnadoSalida()
    {
        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                //return view('turnado.admin.listaturnadosalida');
                break;
            case "MENSAJERO":
                return view('turnado.mensajero.listaturnadosalida');
                break;
            case "JEFE":
                return view('turnado.jefe.listaturnadosalida');
                break;
            case "ASISTENTE":
                return view('turnado.asistente.listaturnadosalida');
                break;
            case "DIRECTOR":
                return view('turnado.director.listaturnadosalida');
                break;
            default:
                return redirect()->action('HomeController@index')->with('message', 'ERROR: Vista no disponible para puesto desconocido.');
        }
    }

    private function queryCreateMensajero($id)          //---[ Turnado automatico ]-------------
    {
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        return view('turnado.mensajero.create')->with('documento', $documento);
    }

    private function queryCreateGeneral($id, $carpeta_auth) //---[ id_documento ]----------------
    {
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                    ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $empleados = DB::table('empleados')
            ->join('users', function ($join) {
                $join->on('empleados.id', '=', 'users.id_empleado')
                    ->where('users.activo', true)
                    ->whereIn('empleados.puesto', ['JEFE', 'MENSAJERO']);
            })
            ->join('departamentos', 'empleados.id_departamento', 'departamentos.id')
            ->select('empleados.id AS id_empleado', 'empleados.nombre', 'empleados.puesto', 'departamentos.area')
            ->get();

        //---[ Recuperar revision *Debe existir por eso primero se hace REVISION ]--------------
        $revision = DB::table('reportesrevisiones')
            ->where('id_documento', $id)
            ->where('id_empleado', Auth::user()->id_empleado)
            ->first();

        if( $revision ){    //---[ Si existe ]--------------------------------------------------
            return view('turnado.' . $carpeta_auth . '.create')
                ->with('documento', $documento)
                ->with('empleados', $empleados)
                ->with('revision', $revision);
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }

    public function create($id)                         //---[ id_documento ]--------------------
    {
        switch ( $this->getPuestoUser() ) {
            case "MENSAJERO":
                return $this->queryCreateMensajero($id);
                break;
            case "DIRECTOR":
                //return $this->queryCreateGeneral($id, 'director');
                break;
            case "ASISTENTE":
                //return $this->queryCreateGeneral($id, 'asistente');
                break;
            case "JEFE":
                //return $this->queryCreateGeneral($id, 'jefe');
                break;
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }

    private function solicitudRespuestaSinAtender($id)
    {
        $solicitud = DB::table('solicitudesrespuestas')
            ->where([
                ['id_documento', '=', $id],
                ['estado', '=', 'NO_ATENDIDO']
            ])
            ->first();

        if( $solicitud ){   //---[ SI EXISTE ]---
            return true;
        }
        return false;
    }

    private function queryCreateXTurnadoGeneral($oldnumorden, $id, $carpeta_auth) //---[ id_documento ]-
    {
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                    ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $empleados = DB::table('empleados')
            ->join('users', function ($join) {
                $join->on('empleados.id', '=', 'users.id_empleado')
                    ->where('users.activo', true)
                    ->whereIn('empleados.puesto', ['JEFE', 'ASISTENTE']);
            })
            ->join('departamentos', 'empleados.id_departamento', 'departamentos.id')
            ->select('empleados.id AS id_empleado', 'empleados.nombre', 'empleados.puesto', 'departamentos.area')
            ->get();

        //---[ Recuperar revision *Debe existir por eso primero se hace REVISION ]--------------
        $revision = DB::table('reportesrevisiones')
            ->where('numorden_guiaturnado', $oldnumorden)
            ->where('id_documento', $id)
            ->where('id_empleado', Auth::user()->id_empleado)
            ->first();

        if( $revision ){    //---[ Si existe ]--------------------------------------------------
            return view('turnado.' . $carpeta_auth . '.createxturnado')
                ->with('documento', $documento)
                ->with('empleados', $empleados)
                ->with('revision', $revision)
                ->with('respuestasinatender', $this->solicitudRespuestaSinAtender($id));
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }

    private function getDeptoEmp()
    {
        $deptoEmp = DB::table('empleados')
            ->join('departamentos', function ($join) {
                $join->on('empleados.id_departamento', '=', 'departamentos.id')
                    ->where('empleados.id', '=', Auth::user()->id_empleado);
            })
            ->select('empleados.id', 'empleados.puesto', 'empleados.id_departamento', 'departamentos.nombre', 'departamentos.area')
            ->first();

        if( $deptoEmp ){   //---[ SI EXISTE ]---
            return $deptoEmp;
        }
        return false;
    }

    private function getInstruccionTurnado($numorden)          //---[ Turnado(Destinatario) ]----
    {
        $instruccion = DB::table('guiasturnados')
            ->join('detallesguiasturnados', function ($join) use ($numorden) {
                $join->on('guiasturnados.num_orden', '=', 'detallesguiasturnados.numorden_guiaturnado')
                     ->where('guiasturnados.num_orden', '=', $numorden);
            })
            ->select('detallesguiasturnados.instruccion')
            ->Where('detallesguiasturnados.idemp_turnado', Auth::user()->id_empleado)
            ->first();

        return $instruccion;
    }

    private function queryCreateXTurnadoJefe($oldnumorden, $id) //---[ id_documento ]-
    {
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                    ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $authDeptoEmp = $this->getDeptoEmp();
        $empleados = DB::table('empleados')
            ->join('departamentos', function ($join) use($authDeptoEmp) {
                $join->on('empleados.id_departamento', '=', 'departamentos.id')
                    ->where('departamentos.area', '=', $authDeptoEmp->area)
                    ->whereNotIn('empleados.puesto', [$authDeptoEmp->puesto]);
            })
            ->join('users', function ($join) {
                $join->on('empleados.id', '=', 'users.id_empleado')
                    ->where('users.activo', true);
            })
            ->select('empleados.id AS id_empleado', 'empleados.nombre', 'empleados.puesto', 'departamentos.area', 'departamentos.nombre as departamento')
            ->get();

        //---[ Recuperar revision *Debe existir por eso primero se hace REVISION ]--------------
        $revision = DB::table('reportesrevisiones')
            ->where('numorden_guiaturnado', $oldnumorden)
            ->where('id_documento', $id)
            ->where('id_empleado', Auth::user()->id_empleado)
            ->first();

        if( $revision ){    //---[ Si existe ]--------------------------------------------------
            $consultainstruccion = $this->getInstruccionTurnado($oldnumorden);
            if( strcmp($consultainstruccion->instruccion, 'REVISION_ATENCION') == 0){   //---[ INSTRUCCION: RESPONDER ]
                //---[ Debe existir solicitud respuesta sin atender ]-------------------------------------
                return view('turnado.jefe.createxturnado')
                    ->with('documento', $documento)
                    ->with('empleados', $empleados)
                    ->with('revision', $revision)
                    ->with('respuestasinatender', $this->solicitudRespuestaSinAtender($id));
            }   //---[ ELSE ]
            return view('turnado.jefe.createxturnado')
                    ->with('documento', $documento)
                    ->with('empleados', $empleados)
                    ->with('revision', $revision)
                    ->with('respuestasinatender', false);
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }

    public function createxturnado($oldnumorden, $id)      //---[num_orden, id_documento]-----------
    {
        switch ( $this->getPuestoUser() ) {
            case "DIRECTOR":
                return $this->queryCreateXTurnadoGeneral($oldnumorden, $id, 'director');
                break;
            case "JEFE":
                return $this->queryCreateXTurnadoJefe($oldnumorden, $id);
                break;
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }

    private function storeByMensajero(Request $request, $id)
    {
        DB::beginTransaction();
            
            //--------------[ TURNADO ]----------------------------------------------------
            $num_orden = DB::table('guiasturnados')->insertGetId(
                [
                    'idemp_turnador' => Auth::user()->id_empleado, 
                    'id_documento' => $id,
                    'reviso_turnador' => $request->filled('observacion')
                ]
            );

            if( $request->filled('observacion') ){  //------[ Si existe una observación ]--------------
                //----------[ REPORTE - Revisión ]-----------------------------------------------------
                $revision = new Reporterevision;
                $revision->numorden_guiaturnado = $num_orden;
                $revision->id_empleado = Auth::user()->id_empleado;
                $revision->id_documento = $id;
                $revision->observacion = $request->observacion;
                $revision->save();          
            }
            
            //--------------[ DETALLE TURNADO ]--------------------------------------------------------
            $director = $this->getDirActivo();
            if($director){
                $detalle_turnado = new Detalleguiaturnado;
                $detalle_turnado->numorden_guiaturnado = $num_orden;
                $detalle_turnado->idemp_turnado = $director->id_empleado;
                $detalle_turnado->instruccion = 'ANALISIS';
                $detalle_turnado->save();
            }else{
                abort(403, 'No existe director activo.');
            }
            
            //-------------[ ACTUALIZACION ]-----------------------------------------------------------
            Empleadodocumento::where('actividad', 'INGRESO')
                ->where('id_empleado', Auth::user()->id_empleado)
                ->where('id_documento', $id)
                ->update(['estado' => 'EN_TRAMITE']);
        DB::commit();
        session(['message' => 'Documento turnado satisfactoriamente.']);
        return response()->json([
            'urlRedirect' => url("guiasturnados/show/".$num_orden."/".$id)
        ]);
        //return redirect()->action('GuiasturnadosController@show', [ $num_orden, $id ])->with('message', 'Documento turnado satisfactoriamente');
    }

    private function storeByDirector(Request $request, $id)
    {
        DB::beginTransaction();
            
            //--------------[ TURNADO ]----------------------------------------------------
            $num_orden = DB::table('guiasturnados')->insertGetId(
                [
                    'idemp_turnador' => Auth::user()->id_empleado, 
                    'id_documento' => $id,
                    'reviso_turnador' => $request->filled('observacion')
                ]
            );

            
            if( $request->filled('observacion') ){  //------[ Si existe una observación ]--------------
                //----------[ REPORTE - Revisión ]-----------------------------------------------------
                $revision = new Reporterevision;
                $revision->numorden_guiaturnado = $num_orden;
                $revision->id_empleado = Auth::user()->id_empleado;
                $revision->id_documento = $id;
                $revision->observacion = $request->observacion;
                $revision->save();          
            }
            
            //--------------[ DETALLE TURNADO ]--------------------------------------------------------
            if ($request->has(['empAreaCheckbox', 'instruccionSelect', 'prioridadSelect'])) {
                $idEmpleados = Input::get('empAreaCheckbox');
                $instrucciones = Input::get('instruccionSelect');
                $prioridades = Input::get('prioridadSelect');

                if( $request->filled('empAreaRespuestaRadio') ){
                    $idEmpRespuesta = Input::get('empAreaRespuestaRadio');
                    $key = array_search($idEmpRespuesta[0], $idEmpleados);
                    $instrucciones[$key] = 'REVISION_ATENCION';
                }

                for ($i = 0; $i < count($idEmpleados); $i++) {
                    $detalle_turnado = new Detalleguiaturnado;
                    $detalle_turnado->numorden_guiaturnado = $num_orden;
                    $detalle_turnado->idemp_turnado = $idEmpleados[$i];
                    $detalle_turnado->instruccion = $instrucciones[$i];
                    $detalle_turnado->prioridad = $prioridades[$i];
                    $detalle_turnado->save();
                }
            }

            //Localizar turnado y actualizar a TURNADO
            DB::table('detallesguiasturnados')
                ->where('numorden_guiaturnado', $numorden)
                ->where('idemp_turnado', Auth::user()->id_empleado)
                ->update(['estado' => 'TURNADO']);

        DB::commit();

        return redirect()->action('GuiasturnadosController@show', [ $num_orden, $id ])->with('message', 'Documento turnado satisfactoriamente');
    }

    public function store(Request $request, $id)        //---[ id_documento ]--------------------
    {
        switch( $this->getPuestoUser() ){
            case "MENSAJERO":
                return $this->storeByMensajero($request, $id);
                break;
            case "DIRECTOR":
                return $this->storeByDirector($request, $id);
                break;
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');   
    }

    private function storeXTurnadoByDirector(Request $request, $oldnumorden, $id)
    {
        DB::beginTransaction();
            
            //--------------[ TURNADO ]----------------------------------------------------
            $newnumorden = DB::table('guiasturnados')->insertGetId(
                [
                    'idemp_turnador' => Auth::user()->id_empleado, 
                    'id_documento' => $id,
                    'reviso_turnador' => $request->filled('observacion')
                ]
            );
            
            if( $request->filled('observacion') ){  //------[ Si existe una observación ]--------------
                //----------[ REPORTE - Revisión ]-----------------------------------------------------
                $revision = new Reporterevision;
                $revision->numorden_guiaturnado = $newnumorden;
                $revision->id_empleado = Auth::user()->id_empleado;
                $revision->id_documento = $id;
                $revision->observacion = $request->observacion;
                $revision->save();          
            }
            
            //--------------[ DETALLE TURNADO ]--------------------------------------------------------
            if ($request->has(['empAreaCheckbox', 'prioridadSelect'])) {
                $idEmpleados = Input::get('empAreaCheckbox');
                $prioridades = Input::get('prioridadSelect');

                if( $request->filled('empAreaRespuestaRadio') ){
                    $idEmpRespuesta = Input::get('empAreaRespuestaRadio');
                    $key = array_search($idEmpRespuesta[0], $idEmpleados);
                    
                    for ($i = 0; $i < count($idEmpleados); $i++) {
                        $detalle_turnado = new Detalleguiaturnado;
                        $detalle_turnado->numorden_guiaturnado = $newnumorden;
                        $detalle_turnado->idemp_turnado = $idEmpleados[$i];
                        $detalle_turnado->instruccion = ($i === $key ? 'REVISION_ATENCION' : 'REVISION');
                        $detalle_turnado->prioridad = $prioridades[$i];
                        $detalle_turnado->save();
                    }
                }else{
                    for ($i = 0; $i < count($idEmpleados); $i++) {
                        $detalle_turnado = new Detalleguiaturnado;
                        $detalle_turnado->numorden_guiaturnado = $newnumorden;
                        $detalle_turnado->idemp_turnado = $idEmpleados[$i];
                        $detalle_turnado->instruccion = 'REVISION';
                        $detalle_turnado->prioridad = $prioridades[$i];
                        $detalle_turnado->save();
                    }
                }

                
            }

            //Localizar turnado anterior y actualizar a REVISADO_TURNAD | TURNADO
            DB::table('detallesguiasturnados')
                ->where('numorden_guiaturnado', $oldnumorden)
                ->where('idemp_turnado', Auth::user()->id_empleado)
                ->update([ 'estado' => DB::raw( 'if(estado = "REVISADO", "REVISADO_TURNADO", "TURNADO")' ) ]);

        DB::commit();

        return redirect()->action('GuiasturnadosController@show', [ $newnumorden, $id ])->with('message', 'Documento turnado satisfactoriamente');
    }

    private function storeXTurnadoByJefe(Request $request, $oldnumorden, $id)
    {
        DB::beginTransaction();
            
            //--------------[ TURNADO ]----------------------------------------------------
            $newnumorden = DB::table('guiasturnados')->insertGetId(
                [
                    'idemp_turnador' => Auth::user()->id_empleado, 
                    'id_documento' => $id,
                    'reviso_turnador' => $request->filled('observacion')
                ]
            );
            
            if( $request->filled('observacion') ){  //------[ Si existe una observación ]--------------
                //----------[ REPORTE - Revisión ]-----------------------------------------------------
                $revision = new Reporterevision;
                $revision->numorden_guiaturnado = $newnumorden;
                $revision->id_empleado = Auth::user()->id_empleado;
                $revision->id_documento = $id;
                $revision->observacion = $request->observacion;
                $revision->save();          
            }
            
            //--------------[ DETALLE TURNADO ]--------------------------------------------------------
            if ($request->has(['empAreaCheckbox', 'prioridadSelect'])) {
                $idEmpleados = Input::get('empAreaCheckbox');
                $prioridades = Input::get('prioridadSelect');

                if( $request->filled('empAreaRespuestaRadio') ){
                    $idEmpRespuesta = Input::get('empAreaRespuestaRadio');
                    $key = array_search($idEmpRespuesta[0], $idEmpleados);
                    
                    for ($i = 0; $i < count($idEmpleados); $i++) {
                        $detalle_turnado = new Detalleguiaturnado;
                        $detalle_turnado->numorden_guiaturnado = $newnumorden;
                        $detalle_turnado->idemp_turnado = $idEmpleados[$i];
                        $detalle_turnado->instruccion = ($i === $key ? 'REVISION_ATENCION' : 'REVISION');
                        $detalle_turnado->prioridad = $prioridades[$i];
                        $detalle_turnado->save();
                    }
                }else{
                    for ($i = 0; $i < count($idEmpleados); $i++) {
                        $detalle_turnado = new Detalleguiaturnado;
                        $detalle_turnado->numorden_guiaturnado = $newnumorden;
                        $detalle_turnado->idemp_turnado = $idEmpleados[$i];
                        $detalle_turnado->instruccion = 'REVISION';
                        $detalle_turnado->prioridad = $prioridades[$i];
                        $detalle_turnado->save();
                    }
                }           
            }

            //Localizar turnado anterior y actualizar a REVISADO_TURNAD | TURNADO
            DB::table('detallesguiasturnados')
                ->where('numorden_guiaturnado', $oldnumorden)
                ->where('idemp_turnado', Auth::user()->id_empleado)
                ->update([ 'estado' => DB::raw( 'if(estado = "REVISADO", "REVISADO_TURNADO", "TURNADO")' ) ]);

        DB::commit();

        return redirect()->action('GuiasturnadosController@show', [ $newnumorden, $id ])->with('message', 'Documento turnado satisfactoriamente');
    }

    public function storexturnado(Request $request, $oldnumorden, $id)
    {
        switch( $this->getPuestoUser() ){
            case "DIRECTOR":
                return $this->storeXTurnadoByDirector($request, $oldnumorden, $id);
                break;
            case "JEFE":
                return $this->storeXTurnadoByJefe($request, $oldnumorden, $id);
                break;
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');   
    }

    private function queryShowAdminDir( $numorden, $id, $carpeta_auth )
    {
        $turnados = DB::table('guiasturnados')
            ->join('detallesguiasturnados', 'guiasturnados.num_orden', '=', 'detallesguiasturnados.numorden_guiaturnado')
            ->join('empleados', 'detallesguiasturnados.idemp_turnado', '=', 'empleados.id')
            ->join('departamentos', 'empleados.id_departamento', '=', 'departamentos.id')
            ->select( 
                DB::raw("CONCAT(empleados.nombre, ' ', empleados.ap_paterno) AS nombre"), 
                'empleados.puesto',
                'departamentos.nombre as departamento',
                'departamentos.area',
                DB::raw('DATE_FORMAT(guiasturnados.registrado, "%d-%m-%Y %r") as registrado'),
                'guiasturnados.num_orden',
                'guiasturnados.reviso_turnador',
                'guiasturnados.idemp_turnador',
                'detallesguiasturnados.instruccion',
                'detallesguiasturnados.estado')
            ->where('num_orden', $numorden)
            ->get();

        $turnador = DB::table('empleados')
            ->join('users', 'empleados.id', '=', 'users.id_empleado')
            ->join('departamentos', 'empleados.id_departamento', '=', 'departamentos.id')
            ->select(
                DB::raw("CONCAT(empleados.nombre, ' ', empleados.ap_paterno) AS nombre"),
                'empleados.puesto',
                'departamentos.nombre AS departamento',
                'departamentos.area')
            ->where( 'empleados.id', $turnados->first()->idemp_turnador)
            ->first();

        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        if( $turnados->first()->reviso_turnador ){
            $revision = DB::table('reportesrevisiones')
                ->select('observacion')
                ->where('numorden_guiaturnado', $numorden)
                ->where('id_documento', $id)
                ->first();

            return view('turnado.' . $carpeta_auth . '.show')
                ->with('turnador', $turnador)
                ->with('turnados', $turnados)
                ->with('documento', $documento)
                ->with('revision', $revision)
                ->with('esturnador', $this->esTurnador($numorden));
        }

        return view('turnado.' . $carpeta_auth . '.show')
            ->with('turnador', $turnador)
            ->with('turnados', $turnados)
            ->with('documento', $documento)
            ->with('esturnador', $this->esTurnador($numorden));
    }

    private function queryShowPropietario( $numorden, $id, $carpeta_auth )
    {
        $turnados = DB::table('guiasturnados')
            ->join('detallesguiasturnados', 'guiasturnados.num_orden', '=', 'detallesguiasturnados.numorden_guiaturnado')
            ->join('empleados', 'detallesguiasturnados.idemp_turnado', '=', 'empleados.id')
            ->join('departamentos', 'empleados.id_departamento', '=', 'departamentos.id')
            ->select( 
                DB::raw("CONCAT(empleados.nombre, ' ', empleados.ap_paterno) AS nombre"), 
                'empleados.puesto',
                'departamentos.nombre as departamento',
                'departamentos.area',
                DB::raw('DATE_FORMAT(guiasturnados.registrado, "%d-%m-%Y %r") as registrado'),
                'guiasturnados.num_orden',
                'guiasturnados.reviso_turnador',
                'guiasturnados.idemp_turnador',
                'detallesguiasturnados.instruccion',
                'detallesguiasturnados.estado')
            ->where('num_orden', $numorden)
            ->get();

        $turnador = DB::table('empleados')
            ->join('users', 'empleados.id', '=', 'users.id_empleado')
            ->join('departamentos', 'empleados.id_departamento', '=', 'departamentos.id')
            ->select(
                DB::raw("CONCAT(empleados.nombre, ' ', empleados.ap_paterno) AS nombre"),
                'empleados.puesto',
                'departamentos.nombre AS departamento',
                'departamentos.area')
            ->where( 'empleados.id', $turnados->first()->idemp_turnador)
            ->first();

        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->where('empleadosdocumentos.actividad', 'ingreso');
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        if( $turnados->first()->reviso_turnador ){
            $revision = DB::table('reportesrevisiones')
                ->select('observacion')
                ->where('numorden_guiaturnado', $numorden)
                ->where('id_documento', $id)
                ->first();

            return view('turnado.' . $carpeta_auth . '.show')
                ->with('turnador', $turnador)
                ->with('turnados', $turnados)
                ->with('documento', $documento)
                ->with('revision', $revision);
        }

        return view('turnado.' . $carpeta_auth . '.show')
            ->with('turnador', $turnador)
            ->with('turnados', $turnados)
            ->with('documento', $documento);
    }

    public function show($numorden, $id)            //---[num_orden, id_documento]---------
    {
        if( ! $this->existeTurnado($numorden, $id) ){
            return redirect()->action('HomeController@index')->with('message', 'ERROR: El recurso al que intenta acceder no esta disponible');
        }

        $puestoUsr = $this->getPuestoUser();
        switch ( $puestoUsr ) {
            case "ADMIN":
                return $this->queryShowAdminDir($numorden, $id, 'admin');
                break;
            case "DIRECTOR":
                return $this->queryShowAdminDir($numorden, $id, 'director');
                break;
        }

        if( $this->privilegioTurnado($numorden) ){
            switch( $puestoUsr ){
                case "MENSAJERO":
                    return $this->queryShowPropietario($numorden, $id, 'mensajero');
                    break;
                case "GENERAL":
                    return $this->queryShowPropietario($numorden, $id, 'general');
                    break;
                case "ASISTENTE":
                    return $this->queryShowPropietario($numorden, $id, 'asistente');
                    break;
                case "JEFE":
                    return $this->queryShowAdminDir($numorden, $id, 'jefe');
                    break;
            }
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');   
    }
}
