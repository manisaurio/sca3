<?php
 
namespace App\Http\Controllers;
 
use App\Http\Requests\DocumentoIngresoRequest;
use App\Http\Requests\DocumentoUpdateRequest;
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
 
use App\Models\Documento;
use App\Models\Tipodocumento;
use App\Models\Solicitudrespuesta;
use App\Models\Solicitudcita;
use App\Models\Empleadodocumento;

use App;
use DB;
use Auth;
use DateTime;

class DocumentosController extends Controller
{
    private function getDateRango()
    {
        $date_start = new DateTime();
        $date_end = new DateTime();

        for( $i = 0; $i < 15; $i++ ){
            $date_end->modify('+1 day');
            $newDay = date('w', $date_end->getTimestamp());
            if($newDay == 0 || $newDay == 6) {
                $i--;
            }
        }

        $array = array(
            'fecha_inicio' => $date_start->format('d-m-Y'),
            'fecha_fin' => $date_end->format('d-m-Y')
        );
        
        return $array;
    }

    private function getPuestoUser()
    {
        $usremp = DB::table('users')
                    ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
                    ->select('empleados.puesto')
                    ->where('users.id_empleado', Auth::user()->id_empleado)
                    ->first();

        return $usremp->puesto;
    }

    private function existeDocumento($id)
    {
        $documento = DB::table('documentos')
                        ->where('id', $id)
                        ->first();

        if( $documento ){   //---[ SI EXISTE ]---
            return true;
        }
        return false;
    }

    private function documentoTurnado($id){
        $turnado = DB::table('empleadosdocumentos')
            ->select('estado')
            ->whereIn('actividad', ['INGRESO', 'EMISION'])
            ->where('id_documento', $id)
            ->where('id_empleado', Auth::user()->id_empleado)
            ->first();

        if( strcmp($turnado->estado, 'EN_TRAMITE') == 0 ){
            return true;
        }
        return false;
    }
    
    private function propietarioDocumento($id)
    {
        $propietario = DB::table('empleadosdocumentos')
            ->select('registrado')
            ->whereIn('actividad', ['INGRESO', 'EMISION'])
            ->where('id_documento', $id)
            ->where('id_empleado', Auth::user()->id_empleado)
            ->first();
        
        if( $propietario ){  //--[ SI EXISTE ]------------------------
            return true;
        }
        return false;
    }

    public function procedencia()
    {
        switch ( $this->getPuestoUser() ) {
            case "DIRECTOR":
                return view('documento.director.procedencia');
                break;
        }

        return redirect('/login');
    }

    public function listaIngreso()
    {
        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                return view('documento.admin.listaingreso');
                break;
            case "MENSAJERO":
                return view('documento.mensajero.listaingreso');
                break;
            case "DIRECTOR":
                return view('documento.director.listaingreso');
                break;
            case "ASISTENTE":
                return view('documento.asistente.listaingreso');
                break;
            default:
                return redirect()->action('HomeController@index')->with('message', 'ERROR: Vista no disponible para puesto desconocido.');
        }
    }

    public function listaEmision()
    {
        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                return view('documento.admin.listaemision');
                break;
            case "DIRECTOR":
                return view('documento.director.listaemision');
                break;
            case "ASISTENTE":
                return view('documento.asistente.listaemision');
                break;
            default:
                return redirect()->action('HomeController@index')->with('message', 'ERROR: Vista no disponible para puesto desconocido.');
        }
    }

    public function createIngreso()
    {
        if( strcmp( $this->getPuestoUser(), 'MENSAJERO' ) !== 0 ){
            return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
        }

        $tiposdocumentos = Tipodocumento::all();
        return view('documento.mensajero.entry')->with( 'tiposdocumentos', $tiposdocumentos );
    }
    
    public function storeIngreso(DocumentoIngresoRequest $request)
    {
        DB::beginTransaction();
            
            $id_tipodocumento = 0;
            $id_suscriptor = 0;
            $id_documento = 0;
            $path = '';

            //---------[ TIPO DOCUMETO ]------------------------------------
            if($request->otroTipoCheckbox){
                $tipodocumento = DB::table('tiposdocumentos')
                                ->select('id')
                                ->where('tipo', $request->otroTipoInput)
                                ->first();
 
                if( $tipodocumento ){  //--[ SI EXISTE ]------------------------
                    $id_tipodocumento = $tipodocumento->id;
                }else{
                    $id_tipodocumento = DB::table('tiposdocumentos')->insertGetId(
                        [
                            'tipo' => $request->otroTipoInput
                        ]
                    );
                }
            }else{
                $id_tipodocumento = $request->tipo;
            };

            //---------[ SUSCRIPTOR ]------------------------------------
            $suscriptor = DB::table('suscriptores')
                        ->select('id')
                        ->where('nombre', $request->suscriptorInput)
                        ->where('cargo', $request->cargoInput)
                        ->where('dependencia', $request->dependenciaInput)
                        ->first();
 
            if( $suscriptor ){  //--[ SI EXISTE ]------------------------
                $id_suscriptor = $suscriptor->id;
            }else{
                if( $request->has('placeGeometryInput') && $request->has('placeIconInput') && $request->has('placeAddressInput') && $request->has('postalCodeInput') ){
                    $id_suscriptor = DB::table('suscriptores')->insertGetId(
                        [
                            'nombre' => $request->suscriptorInput, 
                            'cargo' => $request->cargoInput,
                            'dependencia' => $request->dependenciaInput,
                            'geometry_dependencia' => $request->placeGeometryInput,
                            'icon_dependencia' => $request->placeIconInput,
                            'address_dependencia' => $request->placeAddressInput,
                            'postal_code' => $request->postalCodeInput
                        ]
                    );
                }else{
                	$id_suscriptor = DB::table('suscriptores')->insertGetId(
                        [
                            'nombre' => $request->suscriptorInput, 
                            'cargo' => $request->cargoInput,
                            'dependencia' => $request->dependenciaInput
                        ]
                    );
                }
            };
            
            //---------[ FILE ]-----------------------------------------
            if ( $request->hasFile('pdfInputFile') ) {
                if ( $request->file('pdfInputFile')->isValid() ) {
                    if (App::environment('local')) {
                        $path = $request->pdfInputFile->store('public/documentos/ingreso');
                        $path = str_replace("public", "storage", $path);
                    }else{
                        $path = $request->pdfInputFile->store('documentos/ingreso', 's3');
                    }
                }
            }

            //---------[ DOCUMENTO ]------------------------------------
            $id_documento = DB::table('documentos')->insertGetId(
                [
                    'asunto' => $request->asunto, 
                    'num_hojas' => $request->numHojas,
                    'num_anexos' => $request->numAnexos,
                    'num_oficio' => $request->numOficio,
                    'fecha_oficio' => $request->fechaOficio,
                    'ruta_pdf' => $path,
                    'id_tipodocumento' => $id_tipodocumento,
                    'id_suscriptor' => $id_suscriptor
                ]
            );

            //---------[ RESPUESTA ]------------------------------------
            if ($request->respuestaCheckbox){
                $fechalimite = '';
                if($request->fechaRespuestaInput == 'PLAZO 15 DIAS'){
                    date_default_timezone_set("America/Mexico_City");
                    $fechalimite = date( 'Y-m-d', strtotime('+15 days') );
                }else{
                    $fechalimite = $request->fechaRespuestaInput;
                }
 
                $solicitudrespuesta = new Solicitudrespuesta;
                $solicitudrespuesta->id_documento = $id_documento;
                $solicitudrespuesta->fecha_limite = $fechalimite;
                $solicitudrespuesta->save();
            };

            //---------[ CITA ]------------------------------------
            if ($request->citaCheckbox){
                $solicitudcita = new Solicitudcita;
                $solicitudcita->id_documento = $id_documento;
                $solicitudcita->save();
            };
 
            //---------[ EMPLEADO - DOCUMENTO ]--------------------
            $empdoc = new Empleadodocumento;
            $empdoc->actividad = 'INGRESO';
            $empdoc->id_empleado = Auth::user()->id_empleado;
            $empdoc->id_documento = $id_documento;
            $empdoc->save();
 
        DB::commit();
 
        return redirect()->action('DocumentosController@show', [$id_documento])->with('message', 'Documento ingresado satisfactoriamente');
    }

    private function getDocumentoRespuesta($idSolicitudResp)
    {   
        /*
        $estado = DB::table('solicitudesrespuestas')
            ->where([
                ['id', '=', $idSolicitudResp],
                ['estado', '=', 'ATENDIDO']
            ])
            ->first();
        */

        $respuesta = DB::table('documentos')
            ->join('respuestas', 'documentos.id', '=', 'respuestas.id_documento_respuesta')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                ->whereIn('empleadosdocumentos.actividad', ['RESPUESTA']);
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('respuestas.id_solicitud', $idSolicitudResp)
            ->first();

        if( /*$estado && */$respuesta ){    //---[SI EXISTE]-------
            return $respuesta;
        }else{
            return false;
        }
    }

    private function queryAdminDir($id, $carpeta_auth)
    {
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                    ->whereIn('empleadosdocumentos.actividad', ['INGRESO', 'EMISION']);
            })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        $solicitudrespuesta = DB::table('solicitudesrespuestas')
            ->select(DB::raw('DATE_FORMAT(fecha_limite, "%d-%m-%Y") as fecha_limite'), 'id')
            ->where('id_documento', $id)
            ->first();

        if( $solicitudrespuesta ){      //---[ SI EXISTE SOLICITUD RESPUESTA ]---------------
            $documentorespuesta = $this->getDocumentoRespuesta( $solicitudrespuesta->id);
            if( $documentorespuesta ){  //---[ SI EXISTE DOCUMENTO RESPUESTA ]---------------
                return view('documento.' . $carpeta_auth . '.show')
                    ->with('documento', $documento)
                    ->with('documentorespuesta', $documentorespuesta)
                    ->with('solicitudrespuesta', $solicitudrespuesta)
                    ->with('rangofecha', $this->getDateRango());
            }else{
                return view('documento.' . $carpeta_auth . '.show')
                    ->with('documento', $documento)
                    ->with('solicitudrespuesta', $solicitudrespuesta)
                    ->with('rangofecha', $this->getDateRango());
            }
        }

        return view('documento.' . $carpeta_auth . '.show')
            ->with('documento', $documento)
            ->with('rangofecha', $this->getDateRango());
    }
    
    private function queryShowPropietario($id)
    {
        $documento = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                    ->whereIn('empleadosdocumentos.actividad', ['INGRESO', 'EMISION']);
                })
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.id', 'documentos.asunto', 'documentos.num_hojas', 'documentos.num_anexos', 'documentos.num_oficio', 'documentos.fecha_oficio', 'documentos.ruta_pdf', 'tiposdocumentos.tipo', 'empleadosdocumentos.actividad', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia', 'empleadosdocumentos.estado')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }

        switch ( $this->getPuestoUser() ) {
            case "MENSAJERO":
                return view('documento.mensajero.show')->with('documento', $documento);
                break;
            case "ASISTENTE":
                return view('documento.asistente.show')->with('documento', $documento);
                break;
            default:
                return redirect()->action('HomeController@index')->with('message', 'ERROR: Vista no disponible para puesto desconocido.');                
        }
    }

    public function show($id)
    {
        if( ! $this->existeDocumento($id) ){
            return redirect()->action('HomeController@index')->with('message', 'ERROR: El recurso al que intenta acceder no esta disponible');
        }

        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                return $this->queryAdminDir($id, 'admin');
                break;
            case "DIRECTOR":
                return $this->queryAdminDir($id, 'director');
                break;
        }

        if( $this->propietarioDocumento($id) ){
            return $this->queryShowPropietario($id);
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }

    private function queryEditAdminDir($id, $carpeta_auth){
        $tiposdocumentos = Tipodocumento::all();
        $documento = DB::table('documentos')
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.*', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia', 'suscriptores.postal_code')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }
 
        //------------[ SOLICITUD CITA ]---------------------
        $solicitudcita = DB::table('solicitudescitas')
                        ->select('id')
                        ->where('id_documento', $id)
                        ->first();

        //------------[ SOLICITUD RESPUESTA ]---------------------
        $solicitudrespuesta = DB::table('solicitudesrespuestas')
                        ->select('id', 'fecha_limite')
                        ->where('id_documento', $id)
                        ->first();
 
        if( $solicitudcita ){  //--[ SI EXISTE ]------------------------  
            if( $solicitudrespuesta ){
                return view('documento.' . $carpeta_auth . '.edit')
                    ->with( 'tiposdocumentos', $tiposdocumentos )
                    ->with( 'solicitudcita', $solicitudcita )
                    ->with( 'solicitudrespuesta', $solicitudrespuesta )
                    ->with( 'documento', $documento);
            } //--[ ELSE ]----------------------------------------------
 
            return view('documento.' . $carpeta_auth . '.edit')
                ->with( 'tiposdocumentos', $tiposdocumentos )
                ->with( 'solicitudcita', $solicitudcita )
                ->with( 'documento', $documento);

        }elseif ( $solicitudrespuesta ) {
            return view('documento.' . $carpeta_auth . '.edit')
                ->with( 'tiposdocumentos', $tiposdocumentos )
                ->with( 'solicitudrespuesta', $solicitudrespuesta )
                ->with( 'documento', $documento);
        } //------[ ELSE ]-----------------------------------------------
        
        return view('documento.' . $carpeta_auth . '.edit')
            ->with( 'tiposdocumentos', $tiposdocumentos )
            ->with( 'documento', $documento);
    }

    private function queryEditPropietario($id, $carpeta_auth){
        $tiposdocumentos = Tipodocumento::all();
        $documento = DB::table('documentos')
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->select('documentos.*', 'suscriptores.nombre', 'suscriptores.cargo', 'suscriptores.dependencia', 'suscriptores.geometry_dependencia', 'suscriptores.icon_dependencia', 'suscriptores.address_dependencia', 'suscriptores.postal_code')
            ->where('documentos.id', $id)
            ->first();

        if(!is_null($documento->ruta_pdf)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){
                    $documento->ruta_pdf = url($documento->ruta_pdf);
                }else{
                    $documento->ruta_pdf = null;
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){
                    $documento->ruta_pdf = Storage::temporaryUrl($documento->ruta_pdf, now()->addMinutes(5));
                }else{
                    $documento->ruta_pdf = null;
                }
            }
        }
 
        //------------[ SOLICITUD CITA ]---------------------
        $solicitudcita = DB::table('solicitudescitas')
                        ->select('id')
                        ->where('id_documento', $id)
                        ->first();

        //------------[ SOLICITUD RESPUESTA ]---------------------
        $solicitudrespuesta = DB::table('solicitudesrespuestas')
                        ->select('id', 'fecha_limite')
                        ->where('id_documento', $id)
                        ->first();
 
        if( $solicitudcita ){  //--[ SI EXISTE ]------------------------  
            if( $solicitudrespuesta ){
                return view('documento.' . $carpeta_auth . '.edit')
                    ->with( 'tiposdocumentos', $tiposdocumentos )
                    ->with( 'solicitudcita', $solicitudcita )
                    ->with( 'solicitudrespuesta', $solicitudrespuesta )
                    ->with( 'documento', $documento);
            } //--[ ELSE ]----------------------------------------------
 
            return view('documento.' . $carpeta_auth . '.edit')
                ->with( 'tiposdocumentos', $tiposdocumentos )
                ->with( 'solicitudcita', $solicitudcita )
                ->with( 'documento', $documento);

        }elseif ( $solicitudrespuesta ) {
            return view('documento.' . $carpeta_auth . '.edit')
                ->with( 'tiposdocumentos', $tiposdocumentos )
                ->with( 'solicitudrespuesta', $solicitudrespuesta )
                ->with( 'documento', $documento);
        } //------[ ELSE ]-----------------------------------------------
        
        return view('documento.' . $carpeta_auth . '.edit')
            ->with( 'tiposdocumentos', $tiposdocumentos )
            ->with( 'documento', $documento);
    }

    public function edit($id)
    {
        if( ! $this->existeDocumento($id) ){
            return redirect()->action('HomeController@index')->with('message', 'ERROR: El recurso al que intenta acceder no esta disponible');
        }

        $puestoUsr = $this->getPuestoUser();
        switch ( $puestoUsr ) {
            case "ADMIN":
                return $this->queryEditAdminDir($id, 'admin');
                break;
            case "DIRECTOR":
                return $this->queryEditAdminDir($id, 'director');
                break;
        }

        if( $this->propietarioDocumento($id) ){
            if( ! $this->documentoTurnado($id) ){
                switch ( $puestoUsr ) {
                    case "MENSAJERO":
                        return $this->queryEditPropietario($id, 'mensajero');
                        break;
                    case "ASISTENTE":
                        return $this->queryEditPropietario($id, 'asistente');
                        break;
                }
            }else{
                return redirect()->action('HomeController@index')->with('message', 'ERROR: Documento turnado, ya no puede ser editado.');                
            }
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }

    public function update(DocumentoUpdateRequest $request, $id)
    {
        DB::beginTransaction();
            $id_tipodocumento = 0;
            $id_suscriptor = 0;
            $path = '';

            $documento = Documento::findOrFail($id);

            //---------[ TIPO DOCUMETO ]------------------------------------
            if($request->otroTipoCheckbox){
                $tipodocumento = DB::table('tiposdocumentos')
                                ->select('id')
                                ->where('tipo', $request->otroTipoInput)
                                ->first();
 
                if( $tipodocumento ){  //--[ SI EXISTE ]------------------------
                    $id_tipodocumento = $tipodocumento->id;
                }else{
                    $id_tipodocumento = DB::table('tiposdocumentos')->insertGetId(
                        [
                            'tipo' => $request->otroTipoInput
                        ]
                    );
                }
            }else{
                $id_tipodocumento = $request->tipo;
            };

            //---------[ SUSCRIPTOR ]------------------------------------
            $suscriptor = DB::table('suscriptores')
                        ->select('id')
                        ->where('nombre', $request->suscriptorInput)
                        ->where('cargo', $request->cargoInput)
                        ->where('dependencia', $request->dependenciaInput)
                        ->first();
 
            if( $suscriptor ){  //--[ SI EXISTE ]------------------------
                $id_suscriptor = $suscriptor->id;
            }else{
                if( $request->has('placeGeometryInput') && $request->has('placeIconInput') && $request->has('placeAddressInput') && $request->has('postalCodeInput') ){
                    $id_suscriptor = DB::table('suscriptores')->insertGetId(
                        [
                            'nombre' => $request->suscriptorInput, 
                            'cargo' => $request->cargoInput,
                            'dependencia' => $request->dependenciaInput,
                            'geometry_dependencia' => $request->placeGeometryInput,
                            'icon_dependencia' => $request->placeIconInput,
                            'address_dependencia' => $request->placeAddressInput,
                            'postal_code' => $request->postalCodeInput
                        ]
                    );
                }else{
                    $id_suscriptor = DB::table('suscriptores')->insertGetId(
                        [
                            'nombre' => $request->suscriptorInput, 
                            'cargo' => $request->cargoInput,
                            'dependencia' => $request->dependenciaInput
                        ]
                    );
                }
            };

            //---------[ FILE ]------------------------------------------
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){ // Existe PDF
                    if($request->cambiarPDFCheckbox){   // Pide cambiarlo
                        //---------[ ELIMINAR PDF ]------------------------------
                        Storage::delete( str_replace("storage", "public", $documento->ruta_pdf) );
                    }
                }

                if ( $request->hasFile('pdfInputFile') ) {
                    if ( $request->file('pdfInputFile')->isValid() ) {
                        $path = $request->pdfInputFile->store('public/documentos/ingreso');
                        $path = str_replace("public", "storage", $path);
                    }
                }
                
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){  // Existe PDF
                    if($request->cambiarPDFCheckbox){   // Pide cambiarlo
                        //---------[ ELIMINAR PDF ]------------------------------
                        Storage::disk('s3')->delete($documento->ruta_pdf);
                    }
                }

                if ( $request->hasFile('pdfInputFile') ) {
                    if ( $request->file('pdfInputFile')->isValid() ) {
                        $path = $request->pdfInputFile->store('documentos/ingreso', 's3');
                    }
                }
            }

            //---------[ DOCUMENTO ]------------------------------------
            $documento->num_oficio = $request->numOficio;
            $documento->fecha_oficio = $request->fechaOficio;
            $documento->num_hojas = $request->numHojas;
            $documento->id_tipodocumento = $id_tipodocumento;
            $documento->num_anexos = $request->numAnexos;
            $documento->asunto = $request->asunto;
            $documento->id_suscriptor = $id_suscriptor;
            $documento->ruta_pdf = $path;
            $documento->save();

            //---------[ RESPUESTA ]------------------------------------
            $solicitudrespuesta = DB::table('solicitudesrespuestas')->where('id_documento', $id)->first();

            if( $solicitudrespuesta ){  //--[ SI EXISTE ]---------------
                if ($request->respuestaCheckbox){
                    DB::table('solicitudesrespuestas')
                        ->where('id_documento', $id)
                        ->update(['fecha_limite' => $request->fechaRespuestaInput]
                    );
                }else{
                    DB::table('solicitudesrespuestas')->where('id_documento', $id)->delete();
                }
            }else{  //----------------------[ SI NO EXISTE ]------------
                if ($request->respuestaCheckbox){
                    DB::table('solicitudesrespuestas')->insert(
                        ['id_documento' => $id, 'fecha_limite' => $request->fechaRespuestaInput]
                    );
                }
            }
            
            //---------[ CITA ]------------------------------------
            $solicitudcita = DB::table('solicitudescitas')->where('id_documento', $id)->first();
            if( $solicitudcita ){  //--[ SI EXISTE ]---------------
                if ( !$request->citaCheckbox ){
                    DB::table('solicitudescitas')->where('id_documento', $id)->delete();
                }
            }else{  //-----------------[ SI NO EXISTE ]------------
                if ( $request->citaCheckbox ){
                    DB::table('solicitudescitas')->insert(
                        ['id_documento' => $id]
                    );
                }
            }

            //---------[ EMPLEADO - DOCUMENTO ]--------------------
            $empdoc = new Empleadodocumento;
            $empdoc->actividad = 'INGRESO-EDITADO';
            $empdoc->id_empleado = Auth::user()->id_empleado;
            $empdoc->id_documento = $id;
            $empdoc->save();

        DB::commit();

        return redirect()->action('DocumentosController@show', [$id])->with('message', 'Documento actualizado satisfactoriamente');
    }

    public function destroy($id)
    {
        DB::beginTransaction();
            $documento = Documento::findOrFail($id);
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $documento->ruta_pdf))){ // Existe PDF
                    //---------[ ELIMINAR PDF ]------------------------------
                    Storage::delete( str_replace("storage", "public", $documento->ruta_pdf) );
                }
            }else{
                if(Storage::disk('s3')->exists($documento->ruta_pdf)){  // Existe PDF
                    //---------[ ELIMINAR PDF ]------------------------------
                    Storage::disk('s3')->delete($documento->ruta_pdf);
                }
            }
            $documento->delete();
        DB::commit();
    }
}