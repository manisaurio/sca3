<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;

class ReporterevisionController extends Controller
{
    private function getPuestoUser(){
        $usremp = DB::table('users')
                    ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
                    ->select('empleados.puesto')
                    ->where('users.id_empleado', Auth::user()->id_empleado)
                    ->first();

        return $usremp->puesto;
    }

    public function dataTableListaRevisadoAsist(){
        $documentos_revisados = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                     ->whereIn('empleadosdocumentos.actividad', ['INGRESO', 'EMISION']);
            })
            ->join('guiasturnados', function ($join) {
                $join->on('documentos.id', '=', 'guiasturnados.id_documento');
            })
            ->join('detallesguiasturnados', function ($join) {
                $join->on('guiasturnados.num_orden', '=', 'detallesguiasturnados.numorden_guiaturnado')
                    ->where([
                        ['detallesguiasturnados.idemp_turnado', '=', Auth::user()->id_empleado],
                        ['detallesguiasturnados.estado', '=', 'REVISADO'],
                    ]);
            })
            ->leftJoin('solicitudesrespuestas', 'documentos.id', '=', 'solicitudesrespuestas.id_documento')
            ->select(
                'solicitudesrespuestas.id as id_solicitudrespuesta',
                'solicitudesrespuestas.estado as edo_solicitudrespuesta',
                'guiasturnados.num_orden',
                'detallesguiasturnados.instruccion as instruccion_turnado',
                'documentos.id as id_documento',
                'documentos.asunto', 
                'documentos.num_oficio',
                DB::raw('DATE_FORMAT(documentos.fecha_oficio, "%d-%m-%Y") as fecha_oficio'), 
                'suscriptores.nombre as suscriptor', 
                DB::raw("CONCAT(suscriptores.cargo, ' - ',suscriptores.dependencia) AS cargo_dependencia"), 
                DB::raw('DATE_FORMAT(empleadosdocumentos.registrado, "%d-%m-%Y %r") as ingresado'),
                DB::raw('DATE_FORMAT(guiasturnados.registrado, "%d-%m-%Y %r") as registrado'),
                'tiposdocumentos.tipo')
            ->groupBy('id_solicitudrespuesta', 'guiasturnados.num_orden', 'id_documento', 'tiposdocumentos.tipo', 'documentos.num_oficio', 'documentos.fecha_oficio', 'ingresado', 'guiasturnados.registrado', 'suscriptor', 'cargo_dependencia', 'documentos.asunto')
            ->get();



        return datatables()->of( $documentos_revisados )
            ->addColumn('action', function ( $documento ) {
                $urlshowrevision = url("/reportesrevisiones/show") . "/" . $documento->num_orden . "/" . $documento->id_documento;
                
                $actionstring = '<a href="'.$urlshowrevision.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Revision</a>';
                if( strcmp($documento->instruccion_turnado, 'REVISION_ATENCION') == 0){         //---[ Instruccion: Contestar ]---
                    //----------[ DEBE EXISTIR SOLICITUD RESPUESTA ]--------------------------------------------------------------
                    if( strcmp($documento->edo_solicitudrespuesta, 'CON_RESPUESTA') !== 0 ){    //---[ No tiene respuesta ]-------
                        $urlresponder = url("/respuestas/create") . "/" . $documento->num_orden . "/" . $documento->id_solicitudrespuesta;
                        $actionstring .= ' <a href="'.$urlresponder.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-copy"></i> Responder</a>';
                    }                    
                }

                return $actionstring;
            })
            ->toJson();
    }

    public function dataTableListaRevisadoJefe(){
        $documentos_revisados = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                     ->whereIn('empleadosdocumentos.actividad', ['INGRESO', 'EMISION']);
            })
            ->join('guiasturnados', function ($join) {
                $join->on('documentos.id', '=', 'guiasturnados.id_documento');
            })
            ->join('detallesguiasturnados', function ($join) {
                $join->on('guiasturnados.num_orden', '=', 'detallesguiasturnados.numorden_guiaturnado')
                    ->where([
                        ['detallesguiasturnados.idemp_turnado', '=', Auth::user()->id_empleado],
                        ['detallesguiasturnados.estado', '=', 'REVISADO'],
                    ]);
            })
            ->leftJoin('solicitudesrespuestas', 'documentos.id', '=', 'solicitudesrespuestas.id_documento')
            ->select(
                'solicitudesrespuestas.id as id_solicitudrespuesta',
                'solicitudesrespuestas.estado as edo_solicitudrespuesta',
                'guiasturnados.num_orden',
                'detallesguiasturnados.instruccion as instruccion_turnado',
                'documentos.id as id_documento',
                'documentos.asunto', 
                'documentos.num_oficio',
                DB::raw('DATE_FORMAT(documentos.fecha_oficio, "%d-%m-%Y") as fecha_oficio'), 
                'suscriptores.nombre as suscriptor', 
                DB::raw("CONCAT(suscriptores.cargo, ' - ',suscriptores.dependencia) AS cargo_dependencia"), 
                DB::raw('DATE_FORMAT(empleadosdocumentos.registrado, "%d-%m-%Y %r") as ingresado'),
                DB::raw('DATE_FORMAT(guiasturnados.registrado, "%d-%m-%Y %r") as registrado'),
                'tiposdocumentos.tipo')
            ->groupBy('id_solicitudrespuesta', 'guiasturnados.num_orden', 'id_documento', 'tiposdocumentos.tipo', 'documentos.num_oficio', 'documentos.fecha_oficio', 'ingresado', 'guiasturnados.registrado', 'suscriptor', 'cargo_dependencia', 'documentos.asunto')
            ->get();



        return datatables()->of( $documentos_revisados )
            ->addColumn('action', function ( $documento ) {
                $urlshowrevision = url("/reportesrevisiones/show") . "/" . $documento->num_orden . "/" . $documento->id_documento;
                $urlturnarxturnado = url("/guiasturnados/create") . "/" . $documento->num_orden . "/" . $documento->id_documento;
                
                $actionstring = '<a href="'.$urlshowrevision.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Revision</a>';
                if( strcmp($documento->instruccion_turnado, 'REVISION_ATENCION') == 0){         //---[ Instruccion: Contestar ]---
                    //----------[ DEBE EXISTIR SOLICITUD RESPUESTA ]--------------------------------------------------------------
                    if( strcmp($documento->edo_solicitudrespuesta, 'CON_RESPUESTA') !== 0 ){    //---[ No tiene respuesta ]-------
                        $urlresponder = url("/respuestas/create") . "/" . $documento->num_orden . "/" . $documento->id_solicitudrespuesta;
                        $actionstring .= ' <a href="'.$urlresponder.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-copy"></i> Responder</a>';
                        if( strcmp($documento->edo_solicitudrespuesta, 'EN_PROCESO') !== 0 ){    //---[ No tiene respuesta en proceso]-------
                            $actionstring .= ' <a href="'.$urlturnarxturnado.'" class="btn btn-xs btn-secondary active"><i class="glyphicon glyphicon-share"></i> Turnar</a>';
                        }
                    }                    
                }else{
                    $actionstring .= ' <a href="'.$urlturnarxturnado.'" class="btn btn-xs btn-secondary active"><i class="glyphicon glyphicon-share"></i> Turnar</a>';
                }

                return $actionstring;
            })
            ->toJson();
    }

    public function dataTableListaRevisadoDir(){
        $documentos_revisados = DB::table('documentos')
            ->join('tiposdocumentos', 'documentos.id_tipodocumento', '=', 'tiposdocumentos.id')
            ->join('suscriptores', 'documentos.id_suscriptor', '=', 'suscriptores.id')
            ->join('empleadosdocumentos', function ($join) {
                $join->on('documentos.id', '=', 'empleadosdocumentos.id_documento')
                     ->whereIn('empleadosdocumentos.actividad', ['INGRESO', 'EMISION']);
            })
            ->join('guiasturnados', function ($join) {
                $join->on('documentos.id', '=', 'guiasturnados.id_documento');
            })
            ->join('detallesguiasturnados', function ($join) {
                $join->on('guiasturnados.num_orden', '=', 'detallesguiasturnados.numorden_guiaturnado')
                     ->where('detallesguiasturnados.idemp_turnado', Auth::user()->id_empleado)
                     ->where('detallesguiasturnados.estado', 'REVISADO');
            })
            ->leftJoin('solicitudesrespuestas', 'documentos.id', '=', 'solicitudesrespuestas.id_documento')
            ->select(
                'solicitudesrespuestas.id as id_solicitudrespuesta',
                'solicitudesrespuestas.estado as edo_solicitudrespuesta',
                'guiasturnados.num_orden', 
                'documentos.id as id_documento',
                'documentos.asunto', 
                'documentos.num_oficio', 
                DB::raw('DATE_FORMAT(documentos.fecha_oficio, "%d-%m-%Y") as fecha_oficio'), 
                'suscriptores.nombre as suscriptor', 
                DB::raw("CONCAT(suscriptores.cargo, ' - ',suscriptores.dependencia) AS cargo_dependencia"), 
                DB::raw('DATE_FORMAT(empleadosdocumentos.registrado, "%d-%m-%Y %r") as ingresado'),
                DB::raw('DATE_FORMAT(guiasturnados.registrado, "%d-%m-%Y %r") as registrado'),
                'tiposdocumentos.tipo')
            ->groupBy('id_solicitudrespuesta', 'guiasturnados.num_orden', 'id_documento', 'tiposdocumentos.tipo', 'documentos.num_oficio', 'documentos.fecha_oficio', 'ingresado', 'guiasturnados.registrado', 'suscriptor', 'cargo_dependencia', 'documentos.asunto')
            ->get();



        return datatables()->of( $documentos_revisados )
            ->addColumn('action', function ( $documento ) {
                $urlrevisar = url("/reportesrevisiones/create") . "/" . $documento->num_orden . "/" . $documento->id_documento;
                $urlturnarxturnado = url("/guiasturnados/create") . "/" . $documento->num_orden . "/" . $documento->id_documento;
                $urlshow = url("/guiasturnados/show") . "/" . $documento->num_orden . "/" . $documento->id_documento;
                $urlshowrevision = url("/reportesrevisiones/show") . "/" . $documento->num_orden . "/" . $documento->id_documento;

                
                switch ( $this->getPuestoUser() ) {
                    case "MENSAJERO":
                        return '<a href="'.$urlshow.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Detalle</a>';
                        break;
                    case "DIRECTOR":
                        if( is_null($documento->id_solicitudrespuesta) ){
                            return '<a href="'.$urlturnarxturnado.'" class="btn btn-xs btn-secondary active"><i class="glyphicon glyphicon-share"></i> Turnar</a>
                                    <a href="'.$urlshowrevision.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Revision</a>';
                        }else{
                            $urlresponder = url("/respuestas/create") . "/" . $documento->num_orden . "/" . $documento->id_solicitudrespuesta;
                            if( strcmp($documento->edo_solicitudrespuesta, 'EN_PROCESO') !== 0 ){    //---[ No tiene respuesta en proceso]-------
                                return '<a href="'.$urlturnarxturnado.'" class="btn btn-xs btn-secondary active"><i class="glyphicon glyphicon-share"></i> Turnar</a>
                                        <a href="'.$urlshowrevision.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Revision</a>
                                        <a href="'.$urlresponder.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-copy"></i> Responder</a>';
                            }else{
                                return '<a href="'.$urlshowrevision.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Revision</a>
                                        <a href="'.$urlresponder.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-copy"></i> Responder</a>';
                            }
                        }
                        break;
                }
                
            })
            ->toJson();
    }

    public function dataTableListaRevisado(){
        switch( $this->getPuestoUser() ){
            case "DIRECTOR":
                return $this->dataTableListaRevisadoDir();
                break;
            case "ASISTENTE":
                return $this->dataTableListaRevisadoAsist();
                break;
            case "JEFE":
                return $this->dataTableListaRevisadoJefe();
                break;
            case "GENERAL":
                return $this->dataTableListaRevisadoAsist();
                break;
        }
    }
}
