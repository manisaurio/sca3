<?php
 
namespace App\Http\Controllers;
use App\Http\Requests\EmpleadoStoreRequest;
use App\Http\Requests\EmpleadoUpdateRequest;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\Empleado;
use App\User;
use App;
 
use DB;
use Auth;

class EmpleadosController extends Controller
{
    private function getPuestoUser(){
        $usremp = DB::table('users')
                    ->join('empleados', 'users.id_empleado', '=', 'empleados.id')
                    ->select('empleados.puesto')
                    ->where('users.id_empleado', Auth::user()->id_empleado)
                    ->first();

        return $usremp->puesto;
    }

    private function propietarioEmpleado($id){
        if( strcmp($id, Auth::user()->id_empleado) == 0){
            return true;
        }
        return false;
    }

    private function existeEmpleado($id){
        $empleado = DB::table('empleados')
                        ->where('id', $id)
                        ->first();

        if( $empleado ){   //---[ SI EXISTE ]---
            return true;
        }
        return false;
    }

    public function index()
    {
        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                return view('empleado.admin.list');
                break;
            case "DIRECTOR":
                return view('empleado.director.list');
                break;
            case "ASISTENTE":
                return view('empleado.asistente.list');
                break;
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }

    public function create()
    {
        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                $areasdepartamento = DB::table('departamentos')->select('area')->groupBy('area')->get();
                return view('empleado.admin.create')->with( 'areasdepartamento', $areasdepartamento );
                break;
            case "DIRECTOR":
                $areasdepartamento = DB::table('departamentos')->select('area')->groupBy('area')->get();
                return view('empleado.director.create')->with( 'areasdepartamento', $areasdepartamento );
                break;
            case "ASISTENTE":
                $areasdepartamento = DB::table('departamentos')->select('area')->groupBy('area')->get();
                return view('empleado.asistente.create')->with( 'areasdepartamento', $areasdepartamento );
                break;
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');        
    }

    public function store(EmpleadoStoreRequest $request)
    {
        DB::beginTransaction();
            $path = '';
 
            //---------[ FILE ]-----------------------------------------
            if ( $request->hasFile('avatarInputFile') ) {
                if ( $request->file('avatarInputFile')->isValid() ) {
                    if (App::environment('local')) {
                        $path = $request->avatarInputFile->store('public/usuarios');
                        $path = str_replace("public", "storage", $path);
                    }else{
                        $path = $request->avatarInputFile->store('usuarios', 's3');    
                    }
                }
            }
            //---------[ FIN FILE ]-------------------------------------
            
            $id_empleado = DB::table('empleados')->insertGetId(
                [
                    'curp' => strtoupper( $request->curp ),
                    'nombre' => strtoupper( $request->nombre ),
                    'ap_paterno' => strtoupper( $request->ap_paterno ),
                    'ap_materno' => strtoupper( $request->ap_materno ),
                    'puesto' => strtoupper( $request->puesto ),
                    'id_departamento' => $request->departamento
                ]
            );

            if( strlen($path) == 0 ){
                $usuario = new User;
                $usuario->email = $request->email;
                $usuario->password = bcrypt( $request->psw );
                $usuario->id_empleado = $id_empleado;
                $usuario->save();
            }else{
                $usuario = new User;
                $usuario->email = $request->email;
                $usuario->password = bcrypt( $request->psw );
                $usuario->id_empleado = $id_empleado;
                $usuario->ruta_avatar = $path;
                $usuario->save();
            }
        DB::commit();

        return redirect()->action('EmpleadosController@show', [$id_empleado])->with('message', 'Usuario ingresado satisfactoriamente');
    }

    private function queryShowPropietario($id){
        $empleado = DB::table('empleados')
            ->join('users', 'empleados.id', '=', 'users.id_empleado')
            ->join('departamentos', 'empleados.id_departamento', '=', 'departamentos.id')
            ->select('empleados.*', 'users.id as id_user', 'users.email', 'users.activo', 'users.ruta_avatar','departamentos.nombre as nombre_departamento', 'departamentos.area')
            ->where('empleados.id', $id)
            ->first();

        if(!is_null($empleado->ruta_avatar)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $empleado->ruta_avatar))){
                    $empleado->ruta_avatar = url($empleado->ruta_avatar);
                }else{
                    $empleado->ruta_avatar = null;
                }
            }else{
                if(Storage::disk('s3')->exists($empleado->ruta_avatar)){
                    $empleado->ruta_avatar = Storage::url($empleado->ruta_avatar);
                }else{
                    $empleado->ruta_avatar = null;
                }
            }
        }

        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                return view('empleado.propietario.admin.show')->with('empleado', $empleado);
                break;
            case "DIRECTOR":
                return view('empleado.propietario.director.show')->with('empleado', $empleado);
                break;
            case "JEFE":
                return view('empleado.propietario.jefe.show')->with('empleado', $empleado);
                break;
            case "MENSAJERO":
                return view('empleado.propietario.mensajero.show')->with('empleado', $empleado);
                break;
            case "ASISTENTE":
                return view('empleado.propietario.asistente.show')->with('empleado', $empleado);
                break;
            case "GENERAL":
                return view('empleado.propietario.general.show')->with('empleado', $empleado);
                break;
            default:
                return redirect()->action('HomeController@index')->with('message', 'ERROR: Vista no disponible para puesto desconocido.');
        }
    }

    private function queryAdminDir($id){
        $empleado = DB::table('empleados')
            ->join('users', 'empleados.id', '=', 'users.id_empleado')
            ->join('departamentos', 'empleados.id_departamento', '=', 'departamentos.id')
            ->select('empleados.*', 'users.id as id_user', 'users.email', 'users.activo', 'users.ruta_avatar', 'users.ruta_avatar','departamentos.nombre as nombre_departamento', 'departamentos.area')
            ->where('empleados.id', $id)
            ->first();

        if(!is_null($empleado->ruta_avatar)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $empleado->ruta_avatar))){
                    $empleado->ruta_avatar = url($empleado->ruta_avatar);
                }else{
                    $empleado->ruta_avatar = null;
                }
            }else{
                if(Storage::disk('s3')->exists($empleado->ruta_avatar)){
                    $empleado->ruta_avatar = Storage::url($empleado->ruta_avatar);
                }else{
                    $empleado->ruta_avatar = null;
                }
            }
        }
 
        return $empleado;
    }

    public function show($id)
    {
        if( ! $this->existeEmpleado($id) ){
            return redirect()->action('HomeController@index')->with('message', 'ERROR: El recurso al que intenta acceder no esta disponible');
        }

        if( $this->propietarioEmpleado($id) ){
            return $this->queryShowPropietario($id);
        }

        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                return view('empleado.admin.show')->with('empleado', $this->queryAdminDir($id));
                break;
            case "DIRECTOR":
                return view('empleado.director.show')->with('empleado', $this->queryAdminDir($id));
                break;
            case "ASISTENTE":
                return view('empleado.asistente.show')->with('empleado', $this->queryAdminDir($id));
                break;
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }

    private function queryEditPropietario($id)
    {
        $empleado = DB::table('empleados')
            ->join('users', 'empleados.id', '=', 'users.id_empleado')
            ->select('empleados.*', 'users.id as id_user', 'users.email', 'users.ruta_avatar')
            ->where('empleados.id', $id)
            ->first();

        if(!is_null($empleado->ruta_avatar)){
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $empleado->ruta_avatar))){
                    $empleado->ruta_avatar = url($empleado->ruta_avatar);
                }else{
                    $empleado->ruta_avatar = null;
                }
            }else{
                if(Storage::disk('s3')->exists($empleado->ruta_avatar)){
                    $empleado->ruta_avatar = Storage::url($empleado->ruta_avatar);
                }else{
                    $empleado->ruta_avatar = null;
                }
            }
        }

        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                return view('empleado.propietario.admin.edit')->with('empleado', $empleado);
                break;
            case "DIRECTOR":
                return view('empleado.propietario.director.edit')->with('empleado', $empleado);
                break;
            case "JEFE":
                return view('empleado.propietario.jefe.edit')->with('empleado', $empleado);
                break;
            case "MENSAJERO":
                return view('empleado.propietario.mensajero.edit')->with('empleado', $empleado);
                break;
            case "ASISTENTE":
                return view('empleado.propietario.asistente.edit')->with('empleado', $empleado);
                break;
            case "GENERAL":
                return view('empleado.propietario.general.edit')->with('empleado', $empleado);
                break;
            default:
                return redirect()->action('HomeController@index')->with('message', 'ERROR: Vista no disponible para puesto desconocido.');
        }
    }

    public function edit($id)
    {
        if( ! $this->existeEmpleado($id) ){
            return redirect()->action('HomeController@index')->with('message', 'ERROR: El recurso al que intenta acceder no esta disponible');
        }

        if( $this->propietarioEmpleado($id) ){
            return $this->queryEditPropietario($id);
        }

        switch ( $this->getPuestoUser() ) {
            case "ADMIN":
                $areasdepartamento = DB::table('departamentos')->select('area')->groupBy('area')->get();
                return view('empleado.admin.edit')->with('empleado', $this->queryAdminDir($id))->with( 'areasdepartamento', $areasdepartamento );
                break;
            case "DIRECTOR":
                $areasdepartamento = DB::table('departamentos')->select('area')->groupBy('area')->get();
                return view('empleado.director.edit')->with('empleado', $this->queryAdminDir($id))->with( 'areasdepartamento', $areasdepartamento );
                break;
            case "ASISTENTE":
                $areasdepartamento = DB::table('departamentos')->select('area')->groupBy('area')->get();
                return view('empleado.asistente.edit')->with('empleado', $this->queryAdminDir($id))->with( 'areasdepartamento', $areasdepartamento );
                break;
        }

        return redirect()->action('HomeController@index')->with('message', 'ERROR: Usuario no autorizado para realizar la operación solicitada.');
    }

    public function update(EmpleadoUpdateRequest $request, $id)
    {
        DB::beginTransaction();
 
            $empleado = Empleado::findOrFail($id);
            $empleado->nombre = strtoupper( $request->nombre );
            $empleado->ap_paterno = strtoupper( $request->ap_paterno );
            $empleado->ap_materno = strtoupper( $request->ap_materno );
            $empleado->curp = strtoupper( $request->curp );
            if ($request->has(['puesto', 'departamento'])) {
                $empleado->puesto = strtoupper( $request->puesto );
                $empleado->id_departamento = $request->departamento;    
            }
            $empleado->save();
 
            $usuario =  User::where('id_empleado', $id)->firstOrFail();

            if ( $request->cambioAvatar ){
                //---------[ ELIMINAR AVATAR ]------------------------------
                if (App::environment('local')) {
                    if(Storage::disk('local')->exists(str_replace("storage", "public", $usuario->ruta_avatar))){
                        Storage::delete( str_replace("storage", "public", $usuario->ruta_avatar) );
                    }
                }else{
                    if(Storage::disk('s3')->exists($usuario->ruta_avatar)){
                        Storage::disk('s3')->delete($usuario->ruta_avatar);
                    }
                }
 
                $path = '';
                //---------[ FILE ]-----------------------------------------
                if ( $request->hasFile('avatarInputFile') ) {
                    if ( $request->file('avatarInputFile')->isValid() ) {
                        if (App::environment('local')) {
                            $path = $request->avatarInputFile->store('public/usuarios');
                            $path = str_replace("public", "storage", $path);
                        }else{
                            $path = $request->avatarInputFile->store('usuarios', 's3');
                            Storage::setVisibility($path, 'public');
                        }
                    }
                }
                //---------[ FIN FILE ]-------------------------------------
                
                if( strlen($path) == 0 ){
                    $usuario->ruta_avatar = null;
                }else{
                    $usuario->ruta_avatar = $path;
                }
            }

            if ($request->passwordCheckbox){
                $usuario->password = bcrypt( $request->psw );
            }
 
            $usuario->email = $request->email;
 
            $usuario->save();
 
        DB::commit();
 
        return redirect()->action('EmpleadosController@show', [$id])->with('message', 'Usuario actualizado satisfactoriamente');
    }

    public function switch($id)
    {
        if( ! $this->existeEmpleado($id) ){
            echo "<script>console.warn('ERROR: El recurso al que intenta acceder no esta disponible')</script>";
        }

        if( strcmp( $this->getPuestoUser(), 'ADMIN' ) !== 0 && strcmp( $this->getPuestoUser(), 'DIRECTOR' ) !== 0 && strcmp( $this->getPuestoUser(), 'ASISTENTE' ) !== 0){
            echo "<script>console.warn('ERROR: Usuario no autorizado para realizar la operación solicitada.')</script>";
        }

        DB::beginTransaction();
            $usuario = User::findOrFail($id);
            $usuario->activo = ! $usuario->activo;
            $usuario->save();
        DB::commit();

        //return redirect()->action('EmpleadosController@index');
    }

    public function destroy($id)
    {
        if( ! $this->existeEmpleado($id) ){
            echo "<script>console.warn('ERROR: El recurso al que intenta acceder no esta disponible')</script>";
        }

        if( strcmp( $this->getPuestoUser(), 'ADMIN' ) !== 0 && strcmp( $this->getPuestoUser(), 'DIRECTOR' ) !== 0 && strcmp( $this->getPuestoUser(), 'ASISTENTE' ) !== 0){
            echo "<script>console.warn('ERROR: Usuario no autorizado para realizar la operación solicitada.')</script>";
        }
        
        DB::beginTransaction();
            $usuario =  User::where('id_empleado', $id)->firstOrFail();
            if (App::environment('local')) {
                if(Storage::disk('local')->exists(str_replace("storage", "public", $usuario->ruta_avatar))){
                    Storage::delete( str_replace("storage", "public", $usuario->ruta_avatar) );     // Eliminar avatar
                }
            }else{
                if(Storage::disk('s3')->exists($usuario->ruta_avatar)){
                    Storage::disk('s3')->delete($usuario->ruta_avatar);                            // Eliminar avatar
                }
            }
            $usuario->delete();
            $empleado = Empleado::findOrFail($id);
            $empleado->delete();
        DB::commit();
    }
}