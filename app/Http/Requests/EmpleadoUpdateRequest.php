<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpleadoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
 
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // El id debe ser el mismo para 'user' y 'empleado'.
        return [
            'curp' => 'unique:empleados,curp,' . $this->id,
            'email' => 'unique:users,email,' . $this->id,
            'nombre' => 'required'
        ];
    }
 
    public function messages(){
        return [
            'unique' => ':attribute ya ha sido asignado a otro usuario'
        ];
    }
}
