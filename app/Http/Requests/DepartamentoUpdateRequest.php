<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
//use Illuminate\Contracts\Validation\Rule;
use Illuminate\Validation\Rule;

class DepartamentoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => Rule::unique('departamentos')->where(function ($query) {
                            return $query->where('area', $this->area);
                        })->ignore($this->id)
            
        ];
    }

    public function messages(){
        return [
            'unique' => ':attribute ya existe.'
        ];
    }
}
