<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numOficio' => 'max:200'
            //'pdfInputFile' => 'size:500'
            //'textId' => 'size:2'
        ];
    }

    public function messages(){
        return [
            'max' => ':attribute longitud excedido.'
        ];
    }
}
