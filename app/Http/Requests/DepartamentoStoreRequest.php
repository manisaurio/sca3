<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepartamentoStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'sometimes|required|unique:departamentos,nombre,' . $this->areaSelect,
            'otra_area' => 'sometimes|required|unique:departamentos,area'
        ];
    }

    public function messages(){
        return [
            'unique' => ':attribute ya existe.',
            'exists' => ':attribute ya existe.'
        ];
    }
}
