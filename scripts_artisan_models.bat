@Echo Off
SETLOCAL EnableDelayedExpansion
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do     rem"') do (
  set "DEL=%%a"
)

for /f %%a in ('WMIC OS GET LocalDateTime ^| find "."') do set DTS=%%a
set fecha=%DTS:~0,8%-%DTS:~8,6%

echo(
call :colorEcho 0e "                    Artisan Models"
echo(
call :colorEcho 0f " _________________________________________________________ "
echo(
call :colorEcho 03 "   Generando modelos..."
call :colorEcho 03 "   "

set /p DUMMY=Hit ENTER to continue...

:: ------[ Start] ------

php artisan make:model Models\Departamento
::php artisan make:model Models\Puesto
php artisan make:model Models\Suscriptor
php artisan make:model Models\Tipodocumento
php artisan make:model Models\Documento
php artisan make:model Models\Empleado
php artisan make:model Models\Empleadodocumento
php artisan make:model Models\Solicitudcita
php artisan make:model Models\Cita
php artisan make:model Models\Agenda
php artisan make:model Models\Solicitudrespuesta
php artisan make:model Models\Guiaturnado
php artisan make:model Models\Detalleguiaturnado
php artisan make:model Models\Reporterevision

php artisan make:model Models\Sqlviewdocumentomap
php artisan make:model Models\Cpoaxaca

pause
exit
:colorEcho
echo off
<nul set /p ".=%DEL%" > "%~2"
findstr /v /a:%1 /R "^$" "%~2" nul
del "%~2" > nul 2>&1i
